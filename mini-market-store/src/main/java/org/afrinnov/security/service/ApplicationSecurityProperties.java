package org.afrinnov.security.service;

import io.jsonwebtoken.io.Decoders;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.time.Duration;

@ConfigurationProperties("afrinnov.security")
@Component
@Getter
public class ApplicationSecurityProperties {
    private final JwtProperties jwt = new JwtProperties();
    @Setter
    private Duration expiredOtp;
    @Setter
    private String type;
    @Setter
    private String privateKeyFile;
    @Setter
    private String publicKeyFile;
    @Setter
    private String method;

    public long expirationTime() {
        return System.currentTimeMillis() + jwt.getExpirationTime().toMillis();
    }

    public byte[] key() {
        return Decoders.BASE64.decode(jwt.getKey());
    }

    public long expirationRefreshTokenTime() {
        return System.currentTimeMillis() + jwt.getRefreshExpirationTime().toMillis();
    }

    @Getter
    @Setter
    public static class JwtProperties {
        private String key;
        private Duration expirationTime;
        private Duration refreshExpirationTime;
    }
}
