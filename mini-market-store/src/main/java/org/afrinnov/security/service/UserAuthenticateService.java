package org.afrinnov.security.service;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.afrinnov.security.dto.LoginRequest;
import org.afrinnov.security.dto.LoginResponse;
import org.afrinnov.security.entities.TokenEntity;
import org.afrinnov.security.entities.TokenType;
import org.afrinnov.security.entities.UserEntity;
import org.afrinnov.security.repository.TokenRepository;
import org.afrinnov.security.repository.UserRepository;
import org.afrinnov.security.utils.JwtUtil;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class UserAuthenticateService {
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final UserRepository userRepository;
    private final TokenRepository tokenRepository;
    private final JwtUtil jwtUtil;

    public void authenticate(LoginRequest loginRequest) {
        var authenticationToken = new UsernamePasswordAuthenticationToken(loginRequest.userName(), loginRequest.password());
        var authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    public String createToken(LoginRequest loginRequest) {
        return jwtUtil.generateToken(getUserEntity(loginRequest));
    }

    private UserEntity getUserEntity(LoginRequest loginRequest) {
        return userRepository.findByUsernameIgnoreCase(loginRequest.userName()).orElseThrow();
    }

    @Transactional
    public void manageToken(LoginRequest loginRequest, String jwt) {
        UserEntity user = getUserEntity(loginRequest);
        revokeAllUserTokens(user);
        saveUserToken(user, jwt);
    }

    private void saveUserToken(UserEntity user, String jwt) {
        TokenEntity entity = new TokenEntity();
        entity.setToken(jwt);
        entity.setUser(user);
        entity.setTokenType(TokenType.BEARER);
        entity.setRevoked(false);
        entity.setExpired(false);
        tokenRepository.save(entity);
    }

    private void revokeAllUserTokens(UserEntity user) {
        List<TokenEntity> tokenEntities = tokenRepository.findAllValidTokenByUser(user.getUsername());
        if (tokenEntities.isEmpty()) {
            return;
        }
        tokenEntities.forEach(token -> {
            token.setRevoked(true);
            token.setExpired(true);
        });
        tokenRepository.saveAll(tokenEntities);
    }

    public String createRefreshToken(LoginRequest loginRequest) {
        return jwtUtil.generateRefreshToken(getUserEntity(loginRequest));
    }

    @Transactional
    public LoginResponse refresh(HttpServletRequest request) {
        String refreshToken = jwtUtil.resolveToken(request);
        String username = jwtUtil.extractUsername(refreshToken);
        UserEntity user = userRepository.findByUsernameIgnoreCase(username).orElseThrow();
        if (jwtUtil.isTokenValidByUser(refreshToken, user)) {
            String token = jwtUtil.generateToken(user);
            revokeAllUserTokens(user);
            saveUserToken(user, token);
            return new LoginResponse(token, refreshToken);
        }
        throw new IllegalArgumentException("Unknown token");
    }
}
