package org.afrinnov.security.service;

import lombok.RequiredArgsConstructor;
import org.afrinnov.security.entities.RoleEntity;
import org.afrinnov.security.entities.UserEntity;
import org.afrinnov.security.dto.UserAuthDto;
import org.afrinnov.security.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class AfrinnovUserDetailService implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userRepository.findByUsernameIgnoreCase(username)
                .orElseThrow(() -> new UsernameNotFoundException("unknow user"));
        List<RoleEntity> roles = user.getGroup().getRoles();
        return new UserAuthDto(user, roles).createUserDetails();
    }
}
