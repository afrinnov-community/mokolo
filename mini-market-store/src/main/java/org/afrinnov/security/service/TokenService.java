package org.afrinnov.security.service;

import lombok.RequiredArgsConstructor;
import org.afrinnov.security.entities.TokenEntity;
import org.afrinnov.security.repository.TokenRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class TokenService {
    private final TokenRepository tokenRepository;

    public Optional<TokenEntity> getToken(String token) {
        return tokenRepository.findByToken(token);
    }
}
