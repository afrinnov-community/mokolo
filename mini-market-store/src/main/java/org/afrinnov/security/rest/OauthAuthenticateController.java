package org.afrinnov.security.rest;


import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.security.dto.LoginRequest;
import org.afrinnov.security.dto.LoginResponse;
import org.afrinnov.security.service.UserAuthenticateService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/authenticate")
@RequiredArgsConstructor
public class OauthAuthenticateController {
    private final UserAuthenticateService authenticateService;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<LoginResponse> loginResponse(@RequestBody @Valid LoginRequest loginRequest) {
        authenticateService.authenticate(loginRequest);
        String jwt = authenticateService.createToken(loginRequest);

        authenticateService.manageToken(loginRequest, jwt);
        String refreshToken = authenticateService.createRefreshToken(loginRequest);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(jwt);
        return new ResponseEntity<>(new LoginResponse(jwt, refreshToken), httpHeaders, HttpStatus.OK);
    }

}
