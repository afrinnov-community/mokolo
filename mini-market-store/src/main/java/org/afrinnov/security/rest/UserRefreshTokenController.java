package org.afrinnov.security.rest;


import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.security.dto.LoginResponse;
import org.afrinnov.security.dto.UserOtpRequest;
import org.afrinnov.security.service.UserAuthenticateService;
import org.afrinnov.security.users.service.UserRegistrationService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class UserRefreshTokenController {
    private final UserAuthenticateService userAuthenticateService;
    private final UserRegistrationService userRegistrationService;

    @PostMapping(path = "/refresh-token", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public LoginResponse loginResponse(HttpServletRequest request) {
        return userAuthenticateService.refresh(request);
    }

    @PostMapping(path = "/resend-otp", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> resendOtp(@RequestBody @Valid UserOtpRequest request) {
        boolean mailSend = userRegistrationService.resendOtp(request);
        return Map.of("status", "UPDATED", "mailSent", mailSend);
    }
}
