package org.afrinnov.security.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.security.users.service.dto.UserCreate;
import org.hibernate.annotations.UuidGenerator;

import java.time.LocalDateTime;
import java.util.*;

import static org.afrinnov.security.entities.UserStatus.PENDING;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Entity
@Table(schema = "security", name = "s_user")
@SecondaryTable(schema = "security", name = "s_profile")
@Getter
@Setter
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @UuidGenerator
    private String sid;
    private String username;
    private String password;
    private String otp;
    @Column(name = "otp_create_date")
    private LocalDateTime otpCreateDate;
    @Enumerated(EnumType.STRING)
    private UserStatus status;
    @Enumerated(EnumType.STRING)
    @Column(name = "mail_notification")
    private Notification mailNotification;
    @Column(name = "mail_notification_date")
    private LocalDateTime mailNotificationDate;
    @Enumerated(EnumType.STRING)
    @Column(name = "sms_notification")
    private Notification smsNotification;
    @Column(name = "sms_notification_date")
    private LocalDateTime smsNotificationDate;
    @ManyToOne(fetch = FetchType.LAZY)
    private GroupEntity group;
    @Column(table = "s_profile")
    private String firstname;
    @Column(table = "s_profile")
    private String lastname;
    @Column(table = "s_profile")
    private String phone;
    @Column(table = "s_profile")
    private String email;
    @Column(table = "s_profile", name = "url_photo")
    private String urlPhoto;
    @OneToMany(mappedBy = "user")
    private List<TokenEntity> tokens;
    @OneToMany(mappedBy = "user")
    private List<UserShopEntity> userShops = new ArrayList<>();


    public String getStatusAsString() {
        return Optional.ofNullable(status).map(Enum::name).orElse(null);
    }

    public Map<String, Object> makeClaims() {
        List<RoleEntity> roles = group.getRoles();
        Map<String, Object> claims = new HashMap<>();
        claims.put("roles", roles.stream().map(RoleEntity::getName).toList());
        claims.put("group", group.getName());
        claims.put("email", email);
        claims.put("firstName", firstname);
        List<String> tenants = Optional.ofNullable(userShops)
                .map(uss -> uss.stream().map(UserShopEntity::getTenant).toList()).orElse(List.of());
        claims.put("tenants", tenants);
        tenants.stream().findFirst().ifPresent(tenant -> {
            claims.put("tenant", tenant);
        });
        return claims;
    }

    public static UserEntity newEntity(UserCreate request, GroupEntity group) {
        var entity = new UserEntity();
        entity.setEmail(request.getEmail());
        entity.setUsername(request.getEmail());
        entity.setFirstname(request.getFirstname());
        entity.setLastname(request.getLastname());
        entity.setPhone(request.getPhoneNumber());
        String otp = randomNumeric(6);
        entity.setOtp(otp);
        entity.setOtpCreateDate(LocalDateTime.now());
        entity.setStatus(PENDING);
        entity.setGroup(group);
        return entity;
    }
}
