package org.afrinnov.security.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UuidGenerator;

import java.util.ArrayList;
import java.util.List;


@Entity
@Table(schema = "security", name = "s_group")
@Getter
@Setter
public class GroupEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @UuidGenerator
    private String sid;
    private String code;
    private String name;
    private String description;

    @ManyToMany
    @JoinTable(name = "s_role_group", schema = "security",
            joinColumns = @JoinColumn(name = "group_sid"),
            inverseJoinColumns = @JoinColumn(name = "role_name"))
    private List<RoleEntity> roles = new ArrayList<>();
}
