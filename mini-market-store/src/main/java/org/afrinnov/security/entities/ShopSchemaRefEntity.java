package org.afrinnov.security.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.shop.entities.ShopEntity;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.UUID;

@Entity(name = "ShopSchemaRefEntity")
@Table(schema = "public", name = "t_shop_schema")
@Getter
@Setter
public class ShopSchemaRefEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    protected UUID sid;
    private String nameSchema;
    private Boolean enable;
    private String status;
    @OneToOne
    private ShopRefEntity shop;
}
