package org.afrinnov.security.entities;

public enum Notification {
    PENDING,
    RECEIVED,
    SENT, FAILED, EXPIRED
}
