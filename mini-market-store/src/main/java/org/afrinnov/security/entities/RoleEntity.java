package org.afrinnov.security.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "security", name = "s_role")
@Getter
@Setter
public class RoleEntity {
    @Id
    private String name;
}
