package org.afrinnov.security.entities;

public enum UserStatus {
    PENDING,
    DELETE,
    ACTIVATED
}
