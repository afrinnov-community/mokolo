package org.afrinnov.security.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.UUID;

@Entity
@Table(schema = "security", name = "s_role_group")
@Getter
@Setter
public class RoleGroupEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID sid;
    @ManyToOne
    private RoleEntity role;
    @ManyToOne
    private GroupEntity group;

}
