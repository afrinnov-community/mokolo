package org.afrinnov.security.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UuidGenerator;

@Entity
@Table(schema = "security", name = "c_user_shop")
@Getter
@Setter
public class UserShopEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @UuidGenerator
    protected String sid;
    @ManyToOne(fetch = FetchType.LAZY)
    private ShopRefEntity shop;
    @ManyToOne(fetch = FetchType.LAZY)
    private UserEntity user;
    private String status;

    public String getTenant() {
        return shop.getShopSchema().getNameSchema();
    }
}
