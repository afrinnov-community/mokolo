package org.afrinnov.security.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity(name = "ShopRefEntity")
@Table(schema = "eshop", name = "c_shop")
@Getter
@Setter
public class ShopRefEntity {
    @Id
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID sid;
    private String code;
    private String name;
    @OneToOne(mappedBy = "shop")
    private ShopSchemaRefEntity shopSchema;
    @OneToMany(mappedBy = "shop")
    private List<UserShopEntity> userShops = new ArrayList<>();
}
