package org.afrinnov.security;

import java.util.Map;

public record Messager(String to, String subject, MailTemplate mailTemplate, Map<String, Object> data) {
}
