package org.afrinnov.security.dto;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@Builder
public class RegUser {
    private String phoneNumber;
    private String email;
    private String firstname;
    private String lastname;
    private String confirmationPending;
    private String mailSent;
    private LocalDateTime mailSentDate;
    private String smsSent;
    private LocalDateTime smsSentDate;
}
