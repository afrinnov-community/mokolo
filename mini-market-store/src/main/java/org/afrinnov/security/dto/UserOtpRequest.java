package org.afrinnov.security.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

public record UserOtpRequest(@NotBlank @Email String email) {
}
