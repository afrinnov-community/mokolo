package org.afrinnov.security.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class RegistrationRequest {
    @NotEmpty
    private String phoneNumber;
    @NotEmpty
    @Email
    private String email;
    @NotEmpty
    private String firstname;
    private String lastname;
    @JsonIgnore
    private String shopSchema;
    @NotEmpty(message = "GROUP_IS_REQUIRED")
    private String groupCode;
}
