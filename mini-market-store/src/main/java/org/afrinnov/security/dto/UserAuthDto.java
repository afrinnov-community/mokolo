package org.afrinnov.security.dto;

import org.afrinnov.security.entities.RoleEntity;
import org.afrinnov.security.entities.UserEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public record UserAuthDto(UserEntity user, List<RoleEntity> roles ) {
    public UserDetails createUserDetails() {
        return new User(user.getUsername(),
                user.getPassword(),
                roles.stream().map(role->new SimpleGrantedAuthority(role.getName())).toList());
    }
}
