package org.afrinnov.security;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.core.userdetails.UserDetails;

public interface TokenApiService {
    boolean isTokenValid(String tokenValue);

    String resolveToken(HttpServletRequest request);

    String extractUsername(String accessToken);

    boolean hasTokenValid(String accessToken, UserDetails userDetails);
}
