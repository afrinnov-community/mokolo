package org.afrinnov.security;

public class ExpiredUserException extends RuntimeException {
    public ExpiredUserException(String message) {
        super(message);
    }
}
