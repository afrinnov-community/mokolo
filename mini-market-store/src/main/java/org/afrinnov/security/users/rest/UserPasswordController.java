package org.afrinnov.security.users.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.security.users.dto.PasswordRequest;
import org.afrinnov.security.users.service.UserPasswordService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/users/first-change-password")
@RequiredArgsConstructor
public class UserPasswordController {
    private final UserPasswordService service;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, Object> save(@RequestBody @Valid PasswordRequest request) {
        service.changePwdFirstTime(request);
        return Map.of("status", "CHANGED");
    }
}
