package org.afrinnov.security.users.service;

import lombok.RequiredArgsConstructor;
import org.afrinnov.security.Messager;
import org.afrinnov.security.MessagerSent;
import org.afrinnov.security.dto.RegistrationRequest;
import org.afrinnov.security.dto.UserOtpRequest;
import org.afrinnov.security.users.service.dto.UserCreate;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.modulith.events.ApplicationModuleListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.Map;

import static org.afrinnov.security.MailTemplate.ACTIVATE_NEW_USER;

@Service
@RequiredArgsConstructor
public class UserRegistrationService {
    private final UserService userService;
    private final ApplicationEventPublisher events;

    @Transactional
    public void create(RegistrationRequest request) {
        UserCreate userCreate= UserCreate.builder()
                .email(request.getEmail())
                .firstname(request.getFirstname())
                .groupCode(request.getGroupCode())
                .lastname(request.getLastname())
                .phoneNumber(request.getPhoneNumber())
                .build();
        var userCreated = userService.create(userCreate);
        events.publishEvent(new Messager(request.getEmail(),
                "Activate account", ACTIVATE_NEW_USER, Map.of("otp", userCreated.generatedCode())));
    }

    @Transactional
    public boolean resendOtp(UserOtpRequest request) {
        var userCreated = userService.resendOtp(request);

        events.publishEvent(new Messager(request.email(),
                "Activate account", ACTIVATE_NEW_USER, Map.of("otp", userCreated.generatedCode())));
        return true;
    }

    @Async
    @TransactionalEventListener
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    void on(MessagerSent messager) {
        userService.saveMailSentStatus(messager.email(), messager.sent());
    }
}
