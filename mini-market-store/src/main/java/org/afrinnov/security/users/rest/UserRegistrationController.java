package org.afrinnov.security.users.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.security.dto.RegistrationRequest;
import org.afrinnov.security.users.service.UserRegistrationService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/users/registration")
@RequiredArgsConstructor
public class UserRegistrationController {
    private final UserRegistrationService service;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, Object> save(@RequestBody @Valid RegistrationRequest request) {
        service.create(request);
        return Map.of("status", "CREATED");
    }
}
