package org.afrinnov.security.users.service;

import lombok.RequiredArgsConstructor;
import org.afrinnov.security.Messager;
import org.afrinnov.security.dto.RegistrationRequest;
import org.afrinnov.security.spi.AccountShop;
import org.afrinnov.security.users.dto.UserRegistered;
import org.afrinnov.security.users.service.dto.UserCreate;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.modulith.events.ApplicationModuleListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.Map;

import static org.afrinnov.security.MailTemplate.ACTIVATE_NEW_SHOP_ADMIN_USER;

@Service
@RequiredArgsConstructor
public class ShopUserRegistrationService {
    private final UserService userService;
    private final ApplicationEventPublisher events;


    @TransactionalEventListener
    void on(AccountShop accountShop) {
        var request = UserCreate.builder()
                .email(accountShop.getEmail())
                .firstname("")
                .lastname(accountShop.getName())
                .phoneNumber(accountShop.getPhoneNumber())
                .groupCode("SHOP_ADMIN")
                .schemaName(accountShop.getShopSchema().name())
                .shopCode(accountShop.getShopCode().code())
                .build();
        //TODO : Ajouter un flag "source" dans User pour identifier la source de la creation du compte
        var userRegistered = userService.create(request);
        events.publishEvent(new Messager(request.getEmail(),
                "Activate account", ACTIVATE_NEW_SHOP_ADMIN_USER, Map.of("otp", userRegistered.generatedCode())));
    }
}
