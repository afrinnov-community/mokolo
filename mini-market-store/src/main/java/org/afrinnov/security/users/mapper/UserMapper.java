package org.afrinnov.security.users.mapper;

import org.afrinnov.security.dto.RegUser;
import org.afrinnov.security.entities.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper USER_MAPPER= Mappers.getMapper(UserMapper.class);

    @Mapping(source = "smsNotification", target = "smsSent")
    @Mapping(source = "mailNotification", target = "mailSent")
    @Mapping(source = "mailNotificationDate", target = "mailSentDate")
    @Mapping(source = "smsNotificationDate", target = "smsSentDate")
    @Mapping(source = "status", target = "confirmationPending")
    @Mapping(target = "phoneNumber", ignore = true)
    RegUser entityToRegUser(UserEntity user);
}
