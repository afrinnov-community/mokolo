package org.afrinnov.security.users.dto;

import jakarta.validation.constraints.NotEmpty;


public record UserRegistered(@NotEmpty String generatedCode) {
}
