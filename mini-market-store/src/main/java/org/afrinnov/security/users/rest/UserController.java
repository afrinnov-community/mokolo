package org.afrinnov.security.users.rest;

import jakarta.validation.constraints.Email;
import lombok.RequiredArgsConstructor;
import org.afrinnov.security.dto.RegUser;
import org.afrinnov.security.users.service.UserService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
@Validated
public class UserController {
    private final UserService userService;

    @GetMapping("/{email}")
    public RegUser getUser(@PathVariable @Email String email) {
        return userService.getUser(email);
    }
}
