package org.afrinnov.security.users.service;

import lombok.RequiredArgsConstructor;
import org.afrinnov.security.users.dto.PasswordRequest;
import org.afrinnov.security.Messager;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

import static org.afrinnov.security.MailTemplate.ACCOUNT_ACTIVATED;

@Service
@RequiredArgsConstructor
public class UserPasswordService {
    private final UserService userService;
    private final ApplicationEventPublisher events;
    @Transactional
    public void changePwdFirstTime(PasswordRequest request) {
        userService.changePwd(request);
        events.publishEvent(new Messager(request.email(),
                "Account activated", ACCOUNT_ACTIVATED, Map.of()));
    }
}
