package org.afrinnov.security.users.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

public record PasswordRequest(@NotBlank @Email String email, @NotBlank String otp,
                              @NotBlank @Size(min = 8) String newPassword) {
}
