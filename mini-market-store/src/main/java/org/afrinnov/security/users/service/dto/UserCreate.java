package org.afrinnov.security.users.service.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class UserCreate {
    private String phoneNumber;
    private String email;
    private String firstname;
    private String lastname;
    private String groupCode;
    private String schemaName;
    private String shopCode;
}
