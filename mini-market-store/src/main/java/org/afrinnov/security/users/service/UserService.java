package org.afrinnov.security.users.service;

import com.google.common.base.Strings;
import lombok.RequiredArgsConstructor;
import org.afrinnov.security.ExpiredUserException;
import org.afrinnov.security.ItemNotFoundException;
import org.afrinnov.security.dto.RegUser;
import org.afrinnov.security.dto.UserOtpRequest;
import org.afrinnov.security.entities.UserEntity;
import org.afrinnov.security.entities.UserShopEntity;
import org.afrinnov.security.repository.GroupRepository;
import org.afrinnov.security.repository.ShopRefRepository;
import org.afrinnov.security.repository.UserRepository;
import org.afrinnov.security.repository.UserShopRepository;
import org.afrinnov.security.service.ApplicationSecurityProperties;
import org.afrinnov.security.users.dto.PasswordRequest;
import org.afrinnov.security.users.dto.UserRegistered;
import org.afrinnov.security.users.mapper.UserMapper;
import org.afrinnov.security.users.service.dto.UserCreate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static org.afrinnov.security.entities.Notification.FAILED;
import static org.afrinnov.security.entities.Notification.SENT;
import static org.afrinnov.security.entities.UserStatus.PENDING;
import static org.apache.commons.lang3.RandomStringUtils.random;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Service
@Transactional
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final GroupRepository groupRepository;
    private final ApplicationSecurityProperties applicationProperties;
    private final ShopRefRepository shopRefRepository;
    private final UserShopRepository userShopRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public UserRegistered create(UserCreate request) {
        var group = groupRepository.findByCode(request.getGroupCode())
                .orElseThrow(() -> new ItemNotFoundException("GROUP", request.getGroupCode()));
        UserEntity entity = UserEntity.newEntity(request, group);
        entity.setPassword(passwordEncoder.encode(random(10)));
        userRepository.save(entity);
        if (!Strings.isNullOrEmpty(request.getShopCode())) {
            shopRefRepository.findByCode(request.getShopCode()).ifPresent(shopRef -> {
                UserShopEntity userShop = new UserShopEntity();
                userShop.setShop(shopRef);
                userShop.setUser(entity);
                userShop.setStatus("ACTIVE");
                userShopRepository.save(userShop);
                entity.getUserShops().add(userShop);
                shopRef.getUserShops().add(userShop);
            });
        }
        return new UserRegistered(entity.getOtp());
    }

    public void changePwd(PasswordRequest request) {
        UserEntity user = userRepository.findByEmailIgnoreCaseAndOtp(request.email(), request.otp())
                .orElseThrow(() -> new ExpiredUserException("OTP is not exist"));
        checkExpiredOtp(user);
        user.setPassword(passwordEncoder.encode(request.newPassword()));
        user.setOtp(null);
        user.setOtpCreateDate(null);
        userRepository.save(user);
    }

    private void checkExpiredOtp(UserEntity user) {
        LocalDateTime otpCreateDate = user.getOtpCreateDate()
                .plusDays(applicationProperties.getExpiredOtp().toDays());
        LocalDateTime now = LocalDateTime.now();
        if (now.isAfter(otpCreateDate)) {
            throw new ExpiredUserException("OTP is Expired");
        }
    }

    @Transactional(readOnly = true)
    public RegUser getUser(String email) {
        return userRepository.findByEmailIgnoreCase(email)
                .map(this::mapUser).orElseThrow(() -> new ItemNotFoundException("USER", email));
    }

    private RegUser mapUser(UserEntity user) {
        return UserMapper.USER_MAPPER.entityToRegUser(user);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void saveMailSentStatus(String email, boolean sendMail) {
        UserEntity entity = userRepository.findByEmailIgnoreCase(email)
                .orElseThrow(() -> new ItemNotFoundException("USER", email));
        if (sendMail) {
            entity.setMailNotification(SENT);
        } else {
            entity.setMailNotification(FAILED);
        }
        entity.setMailNotificationDate(LocalDateTime.now());
    }

    public UserRegistered resendOtp(UserOtpRequest request) {
        UserEntity entity = userRepository.findByEmailIgnoreCase(request.email())
                .orElseThrow(() -> new ItemNotFoundException("USER", request.email()));
        String otp = randomNumeric(6);
        entity.setOtp(otp);
        entity.setOtpCreateDate(LocalDateTime.now());
        entity.setStatus(PENDING);
        userRepository.save(entity);
        return new UserRegistered(otp);
    }
}
