package org.afrinnov.security;

public record MessagerSent(String email, boolean sent) {
}
