package org.afrinnov.security.repository;

import org.afrinnov.security.entities.UserShopEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserShopRepository extends JpaRepository<UserShopEntity, String> {
}