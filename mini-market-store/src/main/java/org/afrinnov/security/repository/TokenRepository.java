package org.afrinnov.security.repository;

import org.afrinnov.security.entities.TokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TokenRepository extends JpaRepository<TokenEntity, String> {
    Optional<TokenEntity> findByToken(String token);

    @Query("""
      SELECT token FROM TokenEntity token JOIN token.user user WHERE token.expired = false AND token.revoked = false and  user.username = :username
""")
    List<TokenEntity> findAllValidTokenByUser(@Param("username") String username);
}
