package org.afrinnov.security.repository;

import org.afrinnov.security.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<UserEntity, UUID> {
    Optional<UserEntity> findByEmailIgnoreCaseAndOtp(@NonNull String email, @NonNull String otp);
    Optional<UserEntity> findByEmailIgnoreCase(@NonNull String email);
    Optional<UserEntity> findByUsernameIgnoreCase(String username);
}
