package org.afrinnov.security.repository;

import org.afrinnov.security.entities.ShopRefEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ShopRefRepository extends JpaRepository<ShopRefEntity, UUID> {
    Optional<ShopRefEntity> findByCode(String code);
}