package org.afrinnov.security.utils;

import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import org.afrinnov.security.entities.UserEntity;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.function.Function;

public interface JwtUtil {
    String generateToken(UserEntity user);

    String generateRefreshToken(UserEntity user);

    Claims extractAllClaims(String token);

    boolean isTokenValid(String token, UserDetails userDetails);

    boolean isTokenValidByUser(String token, UserEntity user);

    String extractUsername(String token);

    <T> T extractClaim(String token, Function<Claims, T> claimsResolver);

    String resolveToken(HttpServletRequest request);
}
