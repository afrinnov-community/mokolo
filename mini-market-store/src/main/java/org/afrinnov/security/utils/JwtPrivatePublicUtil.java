package org.afrinnov.security.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import jakarta.servlet.http.HttpServletRequest;
import org.afrinnov.security.entities.UserEntity;
import org.afrinnov.security.service.ApplicationSecurityProperties;
import org.afrinnov.security.spi.JwtLiteUtil;
import org.afrinnov.security.spi.PublicPrivateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.security.Key;
import java.security.PublicKey;
import java.util.Date;
import java.util.UUID;
import java.util.function.Function;

@Component
public class JwtPrivatePublicUtil implements JwtUtil, JwtLiteUtil {
    private final ApplicationSecurityProperties applicationProperties;
    private final Key privateKey;
    private final Key publicKey;

    public JwtPrivatePublicUtil(ApplicationSecurityProperties applicationProperties,
                                @Value("${afrinnov.security.privateKeyFile}") Resource privateKeyFile,
                                @Value("${afrinnov.security.publicKeyFile}") Resource publicKeyFile) throws Exception {
        this.applicationProperties = applicationProperties;
        privateKey = PublicPrivateUtils.makePrivateKeyFromFile(privateKeyFile);
        publicKey = PublicPrivateUtils.makePublicKeyFromFile(publicKeyFile);
    }

    @Transactional(readOnly = true)
    public String generateToken(UserEntity user) {
        Date exp = new Date(System.currentTimeMillis() + applicationProperties.expirationTime());
        return buildToken(user, exp);
    }

    private String buildToken(UserEntity user, Date exp) {
        return Jwts.builder()
                .claims(user.makeClaims())
                .id(UUID.randomUUID().toString())
                .subject(user.getUsername())
                .issuedAt(new Date(System.currentTimeMillis()))
                .expiration(exp)
                .signWith(privateKey)
                .compact();
    }

    public String generateRefreshToken(UserEntity user) {
        Date exp = new Date(System.currentTimeMillis() + applicationProperties.expirationRefreshTokenTime());
        return buildToken(user, exp);
    }

    public Claims extractAllClaims(String token) {
        return Jwts.parser()
                .verifyWith((PublicKey) publicKey)
                .build().parseSignedClaims(token).getPayload();
    }

    public boolean isTokenValid(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername())) && !isTokenExpired(token);
    }

    public boolean isTokenValidByUser(String token, UserEntity user) {
        final String username = extractUsername(token);
        return (username.equals(user.getUsername())) && !isTokenExpired(token);
    }

    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    public String resolveToken(HttpServletRequest request) {
        String TOKEN_HEADER = "Authorization";
        String bearerToken = request.getHeader(TOKEN_HEADER);
        String TOKEN_PREFIX = "Bearer ";
        if (bearerToken != null && bearerToken.startsWith(TOKEN_PREFIX)) {
            return bearerToken.substring(TOKEN_PREFIX.length());
        }
        return null;
    }


}
