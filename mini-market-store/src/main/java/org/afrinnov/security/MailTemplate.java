package org.afrinnov.security;

public enum MailTemplate {
    ACTIVATE_NEW_USER,
    ACCOUNT_ACTIVATED,
    CREATE_NEW_CUSTOMER,
    CREATE_NEW_SHOP,
    CREATE_NEW_EXHIBIT_SHOP,
    ACTIVATE_NEW_SHOP_ADMIN_USER
}
