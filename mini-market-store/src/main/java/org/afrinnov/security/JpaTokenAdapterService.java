package org.afrinnov.security;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.afrinnov.security.service.TokenService;
import org.afrinnov.security.utils.JwtUtil;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class JpaTokenAdapterService implements TokenApiService {
    private final TokenService tokenService;
    private final JwtUtil jwtUtil;

    @Override
    public boolean isTokenValid(String tokenValue) {
        return tokenService.getToken(tokenValue).map(t -> !t.isExpired() && !t.isRevoked())
                .orElse(false);
    }

    @Override
    public String resolveToken(HttpServletRequest request) {
        return jwtUtil.resolveToken(request);
    }

    @Override
    public String extractUsername(String accessToken) {
        return jwtUtil.extractUsername(accessToken);
    }

    @Override
    public boolean hasTokenValid(String accessToken, UserDetails userDetails) {
        return jwtUtil.isTokenValid(accessToken, userDetails);
    }
}
