package org.afrinnov.security.spi;

import org.springframework.core.io.Resource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.function.Function;

import static java.nio.charset.StandardCharsets.UTF_8;

public class PublicPrivateUtils {
    private static final KeyFactory keyFactory;

    static {
        try {
            keyFactory = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private PublicPrivateUtils() {
    }

    private static Key loadKey(Resource resource, Function<String, String> formater,
                               Function<byte[], Key> keyParser) throws IOException {
        String content = resource.getContentAsString(StandardCharsets.UTF_8);
        String formatedContent = formater.apply(content);
        byte[] encoded = Base64.getDecoder().decode(formatedContent.getBytes(UTF_8));
        return keyParser.apply(encoded);
    }

    public static Key makePrivateKeyFromFile(Resource privateKeyFile) throws Exception {
        Function<String, String> formater = p -> p
                .replaceAll("\n", "")
                .replaceAll("\r", "")
                .replace("-----BEGIN PRIVATE KEY-----", "")
                .replace("-----END PRIVATE KEY-----", "");
        return loadKey(privateKeyFile, formater, bytes -> {
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
            try {
                return keyFactory.generatePrivate(keySpec);
            } catch (InvalidKeySpecException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public static Key makePublicKeyFromFile(Resource publicKeyFile) throws Exception {
        Function<String, String> formater = p -> p
                .replaceAll("\n", "")
                .replaceAll("\r", "")
                .replace("-----BEGIN PUBLIC KEY-----", "")
                .replace("-----END PUBLIC KEY-----", "");

        return loadKey(publicKeyFile, formater, bytes -> {
            X509EncodedKeySpec spec = new X509EncodedKeySpec(bytes);
            try {
                return keyFactory.generatePublic(spec);
            } catch (InvalidKeySpecException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
