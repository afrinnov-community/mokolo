package org.afrinnov.security.spi;

import com.google.common.base.Strings;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class AccountShop {
    private String email;
    private String name;
    private String phoneNumber;
    private ShopCode shopCode;
    private ShopSchema shopSchema;

    public AccountShop setEmail(String email) {
        this.email = email;
        return this;
    }

    public AccountShop setName(String name) {
        this.name = name;
        return this;
    }

    public AccountShop setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public AccountShop setShopCode(String shopCode) {
        if(Strings.isNullOrEmpty(shopCode)) {
            throw new IllegalArgumentException("[shop code] Required non null or non empty value");
        }
        this.shopCode = new ShopCode(shopCode);
        return this;
    }

    public AccountShop setShopSchema(String shopSchema) {
        if(Strings.isNullOrEmpty(shopSchema)) {
            throw new IllegalArgumentException("[schema shop] Required non null or non empty value");
        }
        this.shopSchema = new ShopSchema(shopSchema);
        return this;
    }

    public record ShopCode(String code) {
    }

    public record ShopSchema(String name) {
    }

}
