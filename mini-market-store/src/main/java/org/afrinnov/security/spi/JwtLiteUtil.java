package org.afrinnov.security.spi;

import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;

public interface JwtLiteUtil {
    Claims extractAllClaims(String token);

    String resolveToken(HttpServletRequest request);
}
