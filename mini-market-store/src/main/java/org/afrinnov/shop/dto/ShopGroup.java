package org.afrinnov.shop.dto;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class ShopGroup {
    private String code;
    private String name;
    private String description;
    private List<String> roles;
}
