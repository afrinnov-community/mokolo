package org.afrinnov.shop.dto;

public record SchemaTenant(String tenant) {
    public String sqlCreateSchema() {
        return "CREATE SCHEMA IF NOT EXISTS " + tenant;
    }
}
