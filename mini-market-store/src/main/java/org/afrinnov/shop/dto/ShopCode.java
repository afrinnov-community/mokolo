package org.afrinnov.shop.dto;

public record ShopCode(String code) {
    public static ShopCode code(String code) {
        return new ShopCode(code);
    }
}
