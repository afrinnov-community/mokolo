package org.afrinnov.shop.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ShopOwner {
    private String code;
}
