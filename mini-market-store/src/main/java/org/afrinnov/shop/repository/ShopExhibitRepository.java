package org.afrinnov.shop.repository;

import org.afrinnov.shop.entities.ShopExhibitEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ShopExhibitRepository extends JpaRepository<ShopExhibitEntity, UUID> {
}