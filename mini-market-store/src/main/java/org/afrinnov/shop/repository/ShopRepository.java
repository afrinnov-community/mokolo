package org.afrinnov.shop.repository;

import org.afrinnov.shop.entities.ShopEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ShopRepository extends JpaRepository<ShopEntity, UUID> {
}
