package org.afrinnov.shop.repository;

import org.afrinnov.shop.entities.ShopGroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ShopGroupRepository extends JpaRepository<ShopGroupEntity, UUID> {
}