package org.afrinnov.shop.repository;

import org.afrinnov.shop.entities.ShopSchemaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ShopSchemaRepository extends JpaRepository<ShopSchemaEntity, UUID> {
    Optional<ShopSchemaEntity> findByNameSchemaIgnoreCase(String nameSchema);
}