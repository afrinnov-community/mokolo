package org.afrinnov.shop.repository;

import org.afrinnov.shop.entities.ShopRoleRefEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ShopRoleRefRepository extends JpaRepository<ShopRoleRefEntity, String> {
  Optional<ShopRoleRefEntity> findByNameIgnoreCase(String name);
}