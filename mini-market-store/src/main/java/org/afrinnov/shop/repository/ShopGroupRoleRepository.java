package org.afrinnov.shop.repository;

import org.afrinnov.shop.entities.ShopGroupRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ShopGroupRoleRepository extends JpaRepository<ShopGroupRoleEntity, UUID> {
}