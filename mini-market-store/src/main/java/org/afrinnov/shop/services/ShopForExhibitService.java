package org.afrinnov.shop.services;

import liquibase.integration.spring.MultiTenantSpringLiquibase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.afrinnov.security.Messager;
import org.afrinnov.security.spi.AccountShop;
import org.afrinnov.shop.dto.SchemaTenant;
import org.afrinnov.shop.dto.ShopCode;
import org.afrinnov.shop.entities.ShopEntity;
import org.afrinnov.shop.repository.ShopExhibitRepository;
import org.afrinnov.shop.repository.ShopRepository;
import org.afrinnov.shop.repository.ShopSchemaRepository;
import org.afrinnov.shop.rest.input.ShopInput;
import org.afrinnov.shop.services.helper.ShopExhibitHelper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import static org.afrinnov.security.MailTemplate.CREATE_NEW_EXHIBIT_SHOP;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class ShopForExhibitService {
    private final ApplicationEventPublisher events;
    private final ShopExhibitHelper shopExhibitHelper;

    public ShopCode create(ShopInput request) {

        ShopEntity exhibitShop = shopExhibitHelper.createExhibitShop(request);
        String code = exhibitShop.getCode();
        events.publishEvent(new Messager(request.email(),
                "Create shop", CREATE_NEW_EXHIBIT_SHOP, Map.of("shop", code)));
        var accountShop = new AccountShop()
                .setShopCode(code).setShopSchema(code)
                .setName(request.name()).setEmail(request.email())
                .setPhoneNumber(request.phoneNumber());
        events.publishEvent(accountShop);
        events.publishEvent(new SchemaTenant(code.toLowerCase()));
        return new ShopCode(code);
    }


}
