package org.afrinnov.shop.services;

import org.afrinnov.security.Messager;
import org.afrinnov.security.spi.AccountShop;
import org.afrinnov.shop.dto.SchemaTenant;
import org.afrinnov.shop.dto.ShopCode;
import org.afrinnov.shop.entities.ShopEntity;
import org.afrinnov.shop.repository.ShopRepository;
import org.afrinnov.shop.rest.input.ShopInput;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import static org.afrinnov.security.MailTemplate.CREATE_NEW_SHOP;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Service
@Transactional
public class ShopService {
    private final ApplicationEventPublisher events;
    private final ShopRepository shopRepository;

    public ShopService(ApplicationEventPublisher events, ShopRepository shopRepository) {
        this.shopRepository = shopRepository;
        this.events = events;
    }

    public ShopCode create(ShopInput request) {
        var entity = ShopEntity.newEntity(request);
        String code = generateCode();
        entity.setCode(code);
        entity = shopRepository.save(entity);
        String tenant = String.format("SHOP_%s", code);

        events.publishEvent(new Messager(request.email(),
                "Create shop", CREATE_NEW_SHOP, Map.of("shop", entity.getCode())));
        AccountShop accountShop = new AccountShop()
                .setPhoneNumber(request.phoneNumber())
                .setName(request.name()).setEmail(request.email())
                .setShopSchema(tenant).setShopCode(code);
        events.publishEvent(accountShop);
        events.publishEvent(new SchemaTenant(tenant));
        return new ShopCode(code);
    }

    private String generateCode() {
        Year year = Year.now();
        return year.format(DateTimeFormatter.ofPattern("yy")) + randomAlphabetic(3) + randomNumeric(8);
    }


}
