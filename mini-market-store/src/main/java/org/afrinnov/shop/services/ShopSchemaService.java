package org.afrinnov.shop.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.shop.entities.ShopEntity;
import org.afrinnov.shop.entities.ShopSchemaEntity;
import org.afrinnov.shop.repository.ShopSchemaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ShopSchemaService {
    private final ShopSchemaRepository shopSchemaRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void create(ShopEntity exhibitShop, String schemaName) {
        var shopSchema = new ShopSchemaEntity();
        shopSchema.setNameSchema(schemaName);
        shopSchema.setEnable(true);
        shopSchema.setShop(exhibitShop);
        shopSchema.setStatus("CREATED");
        shopSchemaRepository.save(shopSchema);
    }
}
