package org.afrinnov.shop.services.helper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.afrinnov.shop.entities.ShopEntity;
import org.afrinnov.shop.entities.ShopExhibitEntity;
import org.afrinnov.shop.entities.ShopSchemaEntity;
import org.afrinnov.shop.repository.ShopExhibitRepository;
import org.afrinnov.shop.repository.ShopRepository;
import org.afrinnov.shop.repository.ShopSchemaRepository;
import org.afrinnov.shop.rest.input.ShopInput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Service
@RequiredArgsConstructor
@Slf4j
public class ShopExhibitHelper {
    private final ShopRepository shopRepository;
    private final ShopExhibitRepository shopExhibitRepository;
    private final ShopSchemaRepository shopSchemaRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public ShopEntity createExhibitShop(ShopInput request) {
        var entity = ShopEntity.newEntity(request);
        String code = generateCode();
        entity.setCode(code);
        shopRepository.save(entity);
        var shopExhibit = new ShopExhibitEntity();
        shopExhibit.setStartDate(LocalDateTime.now());
        shopExhibit.setEndDate(LocalDateTime.now().plusWeeks(2L));
        shopExhibit.setStatus("CREATED");
        shopExhibit.setShop(entity);
        shopExhibitRepository.save(shopExhibit);
        var shopSchema = new ShopSchemaEntity();
        shopSchema.setNameSchema(code.toLowerCase());
        shopSchema.setEnable(true);
        shopSchema.setShop(entity);
        shopSchema.setStatus("NEW");
        shopSchemaRepository.save(shopSchema);
        return entity;
    }

    private String generateCode() {
        Year year = Year.now();
        return String.format("EXHIBIT%s%s%s",
                year.format(DateTimeFormatter.ofPattern("yy")), randomAlphabetic(2), randomNumeric(2));
    }


}
