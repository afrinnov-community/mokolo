package org.afrinnov.shop.services;

import liquibase.integration.spring.MultiTenantSpringLiquibase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.afrinnov.shop.dto.SchemaTenant;
import org.afrinnov.shop.repository.ShopSchemaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class SchemaService {
    private final DataSource dataSource;
    private final MultiTenantSpringLiquibase liquibaseMT;
    private final ShopSchemaRepository shopSchemaRepository;

    @TransactionalEventListener
    public void on(SchemaTenant tenant) {
        boolean originalShouldRun = liquibaseMT.isShouldRun();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(tenant.sqlCreateSchema())) {
            try {
                connection.setAutoCommit(false);
                int created = statement.executeUpdate();
                connection.commit();
                log.debug("Schema created: {}", created);
                List<String> actualSchemas = liquibaseMT.getSchemas();
                actualSchemas.add(tenant.tenant());
                liquibaseMT.setSchemas(List.of(tenant.tenant()));
                liquibaseMT.setShouldRun(true);
                liquibaseMT.afterPropertiesSet();
                liquibaseMT.setSchemas(actualSchemas);
                shopSchemaRepository.findByNameSchemaIgnoreCase(tenant.tenant())
                        .ifPresent(entity -> entity.setStatus("CREATED"));
            } catch (SQLException e) {
                try {
                    connection.rollback();
                } catch (Exception ex) {
                    log.warn(ex.getMessage(), ex);
                }
            } catch (Exception ex) {
                log.warn(ex.getMessage(), ex);
            }
        } catch (SQLException ex) {
            log.warn(ex.getMessage(), ex);
        } finally {
            liquibaseMT.setShouldRun(originalShouldRun);
        }
    }
}
