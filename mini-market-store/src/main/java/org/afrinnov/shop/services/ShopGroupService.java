package org.afrinnov.shop.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.shop.dto.ShopGroup;
import org.afrinnov.shop.entities.ShopGroupEntity;
import org.afrinnov.shop.entities.ShopGroupRoleEntity;
import org.afrinnov.shop.repository.ShopGroupRepository;
import org.afrinnov.shop.repository.ShopGroupRoleRepository;
import org.afrinnov.shop.repository.ShopRoleRefRepository;
import org.afrinnov.shop.rest.input.ShopGroupCreate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Service
@Transactional
@RequiredArgsConstructor
public class ShopGroupService {

    private final ShopGroupRepository shopGroupRepository;
    private final ShopRoleRefRepository shopRoleRefRepository;
    private final ShopGroupRoleRepository shopGroupRoleRepository;

    public ShopGroup create(ShopGroupCreate create) {
        ShopGroupEntity entity = ShopGroupEntity.newEntity(create);
        entity.setCode(generateCode());
        shopGroupRepository.save(entity);
        if (Objects.nonNull(create.roles())) {
            create.roles().forEach(role -> shopRoleRefRepository.findByNameIgnoreCase(role)
                    .ifPresent(roleEntity -> {
                        var shopGroupRole = new ShopGroupRoleEntity();
                        shopGroupRole.setGroup(entity);
                        shopGroupRole.setRole(roleEntity);
                        shopGroupRoleRepository.save(shopGroupRole);
                    }));
        }

        return ShopGroup.builder().code(entity.getCode()).description(entity.getDescription())
                .name(entity.getName())
                .build();
    }

    private static String generateCode() {
        return randomAlphanumeric(6) + randomNumeric(5);
    }
}
