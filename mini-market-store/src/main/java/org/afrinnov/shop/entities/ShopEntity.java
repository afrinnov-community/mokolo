package org.afrinnov.shop.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.shop.rest.input.ShopInput;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.NaturalId;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.DocumentId;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.type.SqlTypes;

import java.util.UUID;

@Entity
@Table(schema = "eshop", name = "c_shop")
@Getter
@Setter
@Indexed(index = "shop")
public class ShopEntity extends LocalBaseAuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    protected UUID sid;
    @DocumentId
    @NaturalId
    private String code;
    private String name;
    @Column(name = "phone_number")
    private String phoneNumber;
    private String email;
    @Enumerated(EnumType.STRING)
    private ShopStatus status;
    @OneToOne(mappedBy = "shop")
    private ShopSchemaEntity shopSchema;

    public static ShopEntity newEntity(ShopInput request) {
        ShopEntity entity = new ShopEntity();
        entity.setName(request.name());
        entity.setEmail(request.email());
        entity.setPhoneNumber(request.phoneNumber());
        entity.setStatus(ShopStatus.NEW);
        return entity;
    }
}
