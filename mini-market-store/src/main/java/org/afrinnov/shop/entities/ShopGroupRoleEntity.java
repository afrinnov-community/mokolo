package org.afrinnov.shop.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.shop.rest.input.ShopGroupCreate;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.UUID;

@Entity
@Table(name = "sh_role_group")
@Getter
@Setter
public class ShopGroupRoleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    protected UUID sid;
    @ManyToOne
    private ShopGroupEntity group;
    @ManyToOne
    private ShopRoleRefEntity role;
}
