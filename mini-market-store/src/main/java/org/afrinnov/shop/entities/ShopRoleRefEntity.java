package org.afrinnov.shop.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity(name = "ShopRoleRefEntity")
@Table(name = "s_shop_roles", schema = "security")
@Getter
@Setter
public class ShopRoleRefEntity {
    @Id
    private String name;
    private String description;
}
