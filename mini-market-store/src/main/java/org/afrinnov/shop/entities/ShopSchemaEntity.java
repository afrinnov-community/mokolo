package org.afrinnov.shop.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.UUID;

@Entity
@Table(schema = "public", name = "t_shop_schema")
@Getter
@Setter
public class ShopSchemaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    protected UUID sid;
    private String nameSchema;
    private Boolean enable;
    private String status;
    @OneToOne
    private ShopEntity shop;
}
