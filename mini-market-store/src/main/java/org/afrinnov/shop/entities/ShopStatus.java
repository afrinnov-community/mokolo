package org.afrinnov.shop.entities;

public enum ShopStatus {
    NEW, MESSAGE_SEND, DATABASE_INITIALIZE
}
