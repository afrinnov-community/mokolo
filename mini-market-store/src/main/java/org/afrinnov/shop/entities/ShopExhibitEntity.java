package org.afrinnov.shop.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(schema = "eshop", name = "c_shop_exhibit")
@Getter
@Setter
public class ShopExhibitEntity extends LocalBaseAuditEntity {
    @Id
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID sid;
    private String status;
    @Column(name = "start_date")
    private LocalDateTime startDate;
    @Column(name = "end_date")
    private LocalDateTime endDate;
    @OneToOne
    @JoinColumn(name = "sid")
    @MapsId
    private ShopEntity shop;

}
