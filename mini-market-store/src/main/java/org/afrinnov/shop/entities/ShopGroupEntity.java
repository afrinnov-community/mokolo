package org.afrinnov.shop.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.shop.rest.input.ShopGroupCreate;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.UUID;

@Entity
@Table(name = "sh_group")
@Getter
@Setter
public class ShopGroupEntity extends LocalBaseAuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    protected UUID sid;
    private String code;
    private String name;
    private String description;

    public static ShopGroupEntity newEntity(ShopGroupCreate create) {
        ShopGroupEntity entity = new ShopGroupEntity();
        entity.setName(create.name());
        entity.setDescription(create.description());
        return entity;
    }
}
