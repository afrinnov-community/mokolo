package org.afrinnov.shop.rest;

import lombok.RequiredArgsConstructor;
import org.afrinnov.shop.dto.ShopOwner;
import org.afrinnov.shop.shpper.service.ShopperService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/shopper")
@RequiredArgsConstructor
public class ShopperController {
    private final ShopperService shopperService;

    @GetMapping
    public ShopOwner shopOwner() {
       return shopperService.own();
    }
}
