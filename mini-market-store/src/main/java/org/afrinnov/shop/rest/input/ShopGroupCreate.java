package org.afrinnov.shop.rest.input;

import jakarta.validation.constraints.NotEmpty;

import java.util.List;

public record ShopGroupCreate(@NotEmpty String name, @NotEmpty String description, List<String> roles) {
}
