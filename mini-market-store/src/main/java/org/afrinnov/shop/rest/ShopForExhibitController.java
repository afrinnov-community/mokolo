package org.afrinnov.shop.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.shop.dto.ShopCode;
import org.afrinnov.shop.rest.input.ShopInput;
import org.afrinnov.shop.services.ShopForExhibitService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/exhibit/shops")
@RequiredArgsConstructor
public class ShopForExhibitController {

    private final ShopForExhibitService service;
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ShopCode create(@RequestBody @Valid ShopInput request) {
        return service.create(request);
    }
}
