package org.afrinnov.shop.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.shop.dto.ShopCode;
import org.afrinnov.shop.rest.input.ShopInput;
import org.afrinnov.shop.services.ShopService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/shops")
@RequiredArgsConstructor
public class ShopController {
    private final ShopService service;
    @PostMapping
    public ResponseEntity<ShopCode> create(@RequestBody @Valid ShopInput request) {
        return ResponseEntity.ok(service.create(request));
    }
}
