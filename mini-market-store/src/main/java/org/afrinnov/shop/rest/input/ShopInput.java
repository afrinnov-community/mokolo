package org.afrinnov.shop.rest.input;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

public record ShopInput(@NotBlank String name, @NotBlank @Email String email,
                        @NotBlank String phoneNumber) {
}
