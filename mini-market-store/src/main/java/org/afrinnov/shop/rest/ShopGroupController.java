package org.afrinnov.shop.rest;


import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.shop.dto.ShopGroup;
import org.afrinnov.shop.rest.input.ShopGroupCreate;
import org.afrinnov.shop.services.ShopGroupService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/shopper/group")
@RequiredArgsConstructor
public class ShopGroupController {
    private final ShopGroupService shopGroupService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ShopGroup createGroup(@RequestBody @Valid ShopGroupCreate create) {
        return shopGroupService.create(create);
    }
}
