package org.afrinnov.shop.shpper.service;

import org.afrinnov.shop.dto.ShopOwner;
import org.springframework.stereotype.Service;

@Service
public class ShopperService {
    public ShopOwner own() {
        return ShopOwner.builder().build();
    }
}
