package org.afrinnov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.modulith.ApplicationModule;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableScheduling
@EnableAsync
@EnableWebMvc
public class MiniMarketStoreApplication {
    public static void main(String[] args) {
        SpringApplication.run(MiniMarketStoreApplication.class, args);
    }
}
