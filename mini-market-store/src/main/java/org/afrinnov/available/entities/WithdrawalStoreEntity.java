package org.afrinnov.available.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.available.OrderWithdrawalNotify;
import org.afrinnov.available.rest.input.WithdrawalRequest.WithdrawalSize;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "e_withdrawal_store")
@Getter
@Setter
public class WithdrawalStoreEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID sid;
    private String withdrawalNumber;
    private LocalDateTime withdrawalDate;
    private String motif;
    private String sku;
    private String status;
    private String sizeRef;
    private String colorRef;
    private int qty;

    public static WithdrawalStoreEntity newEntity(String sku, String colorRef, LocalDateTime withdrawalDate, WithdrawalSize size) {
        WithdrawalStoreEntity entity = new WithdrawalStoreEntity();
        entity.setQty(size.qty());
        entity.setSku(sku);
        entity.setColorRef(colorRef);
        entity.setSizeRef(size.sizeRef());
        entity.setWithdrawalDate(withdrawalDate);
        return entity;
    }

    public static WithdrawalStoreEntity newItem(OrderWithdrawalNotify.ItemNotify item) {
        WithdrawalStoreEntity entity = new WithdrawalStoreEntity();
        entity.setQty(item.qty());
        entity.setSku(item.sku());
        entity.setColorRef(item.colorRef());
        entity.setSizeRef(item.sizeRef());
        entity.setWithdrawalDate(LocalDateTime.now());
        return entity;
    }
}
