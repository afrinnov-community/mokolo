package org.afrinnov.available.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.available.OrderReturnNotify.ItemNotify;
import org.afrinnov.available.rest.input.EntrySize;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table( name = "e_entry_store")
@Getter
@Setter
public class EntryStoreEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID sid;
    private String entryNumber;
    private LocalDateTime entryDate;
    private String motif;
    private String sku;
    private String sizeRef;
    private String colorRef;
    private int qty;
    private BigDecimal additionalPrice;

    public static EntryStoreEntity newEntity(String sku, String colorRef, LocalDateTime entryDate, EntrySize size) {
        EntryStoreEntity entity = new EntryStoreEntity();
        entity.setQty(size.qty());
        entity.setSku(sku);
        entity.setAdditionalPrice(size.additionalPrice());
        entity.setColorRef(colorRef);
        entity.setSizeRef(size.sizeCode());
        entity.setEntryDate(entryDate);
        return entity;
    }

    public static EntryStoreEntity newItem(ItemNotify item) {
        EntryStoreEntity entity = new EntryStoreEntity();
        entity.setQty(item.qty());
        entity.setSku(item.sku());
        entity.setAdditionalPrice(BigDecimal.ZERO);
        entity.setColorRef(item.colorRef());
        entity.setSizeRef(item.sizeRef());
        entity.setEntryDate(LocalDateTime.now());
        return entity;
    }
}
