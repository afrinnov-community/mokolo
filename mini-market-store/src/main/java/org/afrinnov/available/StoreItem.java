package org.afrinnov.available;

public record StoreItem(String sku, String colorRef, String sizeRef, int qty) {
    public String notAvailableMessage() {
        return sku + ":" + colorRef + ":" + sizeRef + ":" + qty;
    }
}
