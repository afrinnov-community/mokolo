package org.afrinnov.available.adapter;

import lombok.RequiredArgsConstructor;
import org.afrinnov.available.spi.AvailableItemService;
import org.afrinnov.available.StoreItem;
import org.afrinnov.available.entities.EntryStoreEntity;
import org.afrinnov.available.entities.WithdrawalStoreEntity;
import org.afrinnov.available.repository.EntryStoreRepository;
import org.afrinnov.available.repository.WithdrawalStoreRepository;
import org.afrinnov.sheet.spi.ProductSheetApiService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SimpleAvailableItemService implements AvailableItemService {
    private final ProductSheetApiService productSheetApiService;
    private final EntryStoreRepository entryStoreRepository;
    private final WithdrawalStoreRepository withdrawalStoreRepository;

    @Override
    public void checkStore(StoreItem storeItem) {
        productSheetApiService.checkProduct(storeItem.sku());
        var entries = entryStoreRepository.findBySkuAndSizeRefAndColorRef(storeItem.sku(),
                storeItem.sizeRef(), storeItem.colorRef());
        int countEntries = entries.stream().mapToInt(EntryStoreEntity::getQty).sum();
        var withdrawal = withdrawalStoreRepository.findBySkuAndSizeRefAndColorRef(storeItem.sku(),
                storeItem.sizeRef(), storeItem.colorRef());
        int countWithdrawal = withdrawal.stream().mapToInt(WithdrawalStoreEntity::getQty).sum();
        int rest = countEntries - countWithdrawal - storeItem.qty();
        if (rest <= 0) {
            throw new IllegalArgumentException("ITEM_NOT_AVAILABLE:" + rest + ":" + storeItem.notAvailableMessage());
        }
    }

    @Override
    public boolean checkProductAvailability(String sku) {
        List<EntryStoreEntity> entryStoreList = entryStoreRepository.findBySku(sku);
        int entriesSum = entryStoreList.stream().mapToInt(EntryStoreEntity::getQty).sum();

        List<WithdrawalStoreEntity> withdrawalStoreList = withdrawalStoreRepository.findBySku(sku);
        int withdrawalSum = withdrawalStoreList.stream().mapToInt(WithdrawalStoreEntity::getQty).sum();

        return (withdrawalSum - entriesSum) > 0;
    }
}
