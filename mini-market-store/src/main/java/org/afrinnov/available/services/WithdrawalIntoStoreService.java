package org.afrinnov.available.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.available.rest.input.WithdrawalRequest;
import org.afrinnov.sheet.spi.ProductSheetApiService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class WithdrawalIntoStoreService {
    private final ProductSheetApiService productSheetApiService;
    private final WithdrawalService withdrawalService;
    public void withdrawal(String sku, WithdrawalRequest request) {
        productSheetApiService.checkProduct(sku);
        withdrawalService.create(sku, request);

    }
}
