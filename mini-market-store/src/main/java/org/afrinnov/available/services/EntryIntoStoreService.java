package org.afrinnov.available.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.available.OrderReturnNotify;
import org.afrinnov.available.entities.EntryStoreEntity;
import org.afrinnov.available.repository.EntryStoreRepository;
import org.afrinnov.available.rest.input.EntryRequest;
import org.afrinnov.sheet.spi.ProductSheetApiService;
import org.springframework.modulith.events.ApplicationModuleListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Service
@RequiredArgsConstructor
@Transactional
public class EntryIntoStoreService {
    private final ProductSheetApiService productSheetApiService;
    private final EntryStoreRepository entryStoreRepository;


    public void createArrival(String sku, EntryRequest request) {
        productSheetApiService.checkProduct(sku);
        List<EntryStoreEntity> entities = request.getSizes().stream()
                .map(size -> EntryStoreEntity.newEntity(sku, request.getColorRef(),
                        request.getEntryOrCurrentDate(), size)).toList();
        entities.forEach(entity -> {
            entity.setMotif("ENTRY");
            entity.setEntryNumber(generateEntryNumber());
        });
        entryStoreRepository.saveAll(entities);
    }

    private String generateEntryNumber() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern("yyMMdd")) + randomNumeric(8);
    }


    @ApplicationModuleListener
    void on(OrderReturnNotify orderReturnNotify) {
        List<EntryStoreEntity> entities = orderReturnNotify.items().stream().map(EntryStoreEntity::newItem)
                .peek(entity -> {
                    entity.setEntryNumber(generateEntryNumber());
                    entity.setMotif("RETURN:" + orderReturnNotify.orderNumber());
                }).toList();
        entryStoreRepository.saveAll(entities);
    }


}
