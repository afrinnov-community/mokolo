package org.afrinnov.available.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.available.OrderWithdrawalNotify;
import org.afrinnov.available.entities.WithdrawalStoreEntity;
import org.afrinnov.available.repository.WithdrawalStoreRepository;
import org.afrinnov.available.rest.input.WithdrawalRequest;
import org.springframework.modulith.events.ApplicationModuleListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Service
@RequiredArgsConstructor
@Transactional
public class WithdrawalService {
    private final WithdrawalStoreRepository withdrawalStoreRepository;

    public void create(String sku, WithdrawalRequest request) {
        List<WithdrawalStoreEntity> entities = request.getSizes().stream()
                .map(size -> WithdrawalStoreEntity.newEntity(sku, request.getColorRef(),
                        request.getWithdrawalOrCurrentDate(), size))
                .peek(entity -> {
                    entity.setWithdrawalNumber(generateNumber());
                    entity.setMotif("MANUAL");
                })
                .toList();
        withdrawalStoreRepository.saveAll(entities);
    }

    private String generateNumber() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern("yyMMdd")) + randomNumeric(8);
    }

    @ApplicationModuleListener
    void on(OrderWithdrawalNotify orderWithdrawalNotify) {
        List<WithdrawalStoreEntity> entities = orderWithdrawalNotify.items().stream().map(WithdrawalStoreEntity::newItem)
                .peek(entity -> {
                    entity.setWithdrawalNumber(generateNumber());
                    entity.setMotif("COMMAND:" + orderWithdrawalNotify.orderNumber());
                    entity.setStatus(orderWithdrawalNotify.orderStatus());
                }).toList();
        withdrawalStoreRepository.saveAll(entities);
    }
}
