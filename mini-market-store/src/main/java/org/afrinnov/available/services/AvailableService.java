package org.afrinnov.available.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.available.dto.Availables;
import org.afrinnov.available.entities.EntryStoreEntity;
import org.afrinnov.available.entities.WithdrawalStoreEntity;
import org.afrinnov.available.repository.EntryStoreRepository;
import org.afrinnov.available.repository.WithdrawalStoreRepository;
import org.afrinnov.sheet.spi.ProductSheetApiService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class AvailableService {
    private final EntryStoreRepository entryStoreRepository;
    private final WithdrawalStoreRepository withdrawalStoreRepository;
    private final ProductSheetApiService productSheetApiService;

    public Availables getAvailable(String sku) {
        var entryByColor = entryStoreRepository.findBySku(sku).stream()
                .collect(Collectors.groupingBy(EntryStoreEntity::getColorRef));
        var withdrawalByColor = withdrawalStoreRepository.findBySku(sku).stream()
                .collect(Collectors.groupingBy(WithdrawalStoreEntity::getColorRef));
        return new Availables(makeColors(entryByColor, withdrawalByColor));
    }

    private List<Availables.Color> makeColors(Map<String, List<EntryStoreEntity>> availableByColor,
                                              Map<String, List<WithdrawalStoreEntity>> withdrawalByColor) {
        return availableByColor.entrySet().stream().map(t -> makeColor(t, withdrawalByColor.get(t.getKey()))).toList();
    }

    private Availables.Color makeColor(Map.Entry<String, List<EntryStoreEntity>> entry,
                                       List<WithdrawalStoreEntity> withdrawalStores) {
        var color = productSheetApiService.getColor(entry.getKey());
        List<EntryStoreEntity> entryStores = entry.getValue();

        Map<String, Integer> qtyPerWithdrawal = makeQtyPerWithdrawal(withdrawalStores);

        Map<String, Integer> qtyPerEntry = makeQtyPerEntry(entryStores);

        var sizes = entryStores.stream().map(t -> makeSize(t, t.getSizeRef(), compute(t.getSizeRef(), qtyPerEntry, qtyPerWithdrawal))).toList();

        return new Availables.Color(color.code(), color.label(), color.value(), sizes);
    }

    private static Map<String, Integer> makeQtyPerEntry(List<EntryStoreEntity> entryStores) {
        var mapEntry = entryStores.stream().collect(Collectors.groupingBy(EntryStoreEntity::getSizeRef));
        return mapEntry.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                e -> e.getValue().stream().mapToInt(EntryStoreEntity::getQty).sum()));
    }

    private static Map<String, Integer> makeQtyPerWithdrawal(List<WithdrawalStoreEntity> withdrawalStores) {
        var map = Optional.ofNullable(withdrawalStores)
                .map(m -> m.stream().collect(Collectors.groupingBy(WithdrawalStoreEntity::getSizeRef)))
                .orElse(Map.of());
        return map.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                e -> e.getValue().stream().mapToInt(WithdrawalStoreEntity::getQty).sum()));
    }

    private Integer compute(String sizeRef, Map<String, Integer> qtyPerEntry, Map<String, Integer> qtyPerWithdrawal) {
        return getValueOrZero(sizeRef, qtyPerEntry) - getValueOrZero(sizeRef, qtyPerWithdrawal);
    }

    private static Integer getValueOrZero(String sizeRef, Map<String, Integer> map) {
        return Optional.ofNullable(map.get(sizeRef)).orElse(0);
    }

    private Availables.Size makeSize(EntryStoreEntity store, String sizeRef, Integer qty) {
        var size = productSheetApiService.getSize(sizeRef);
        return new Availables.Size(size.code(), size.label(), qty, store.getAdditionalPrice());
    }
}
