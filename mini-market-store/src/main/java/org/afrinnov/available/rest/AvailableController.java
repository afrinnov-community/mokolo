package org.afrinnov.available.rest;

import lombok.RequiredArgsConstructor;
import org.afrinnov.available.dto.Availables;
import org.afrinnov.available.services.AvailableService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/product/{sku}/store")
@RequiredArgsConstructor
public class AvailableController {
    private final AvailableService availableService;
    @GetMapping
    public Availables getAvailable(@PathVariable String sku) {
        return availableService.getAvailable(sku);
    }
}
