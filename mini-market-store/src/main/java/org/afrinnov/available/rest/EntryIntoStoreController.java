package org.afrinnov.available.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.available.rest.input.EntryRequest;
import org.afrinnov.available.services.EntryIntoStoreService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/product/{sku}/store/entry")
@RequiredArgsConstructor
public class EntryIntoStoreController {
    private final EntryIntoStoreService entryIntoStoreService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void arrival(@PathVariable String sku, @RequestBody @Valid EntryRequest request) {
        entryIntoStoreService.createArrival(sku, request);
    }


}
