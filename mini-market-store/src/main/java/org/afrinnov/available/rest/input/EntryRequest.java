package org.afrinnov.available.rest.input;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Getter
@Builder
@Jacksonized
public class EntryRequest {
    @NotEmpty
    private String colorRef;
    @NotEmpty
    private List<EntrySize> sizes;
    @PastOrPresent
    @NotNull
    private LocalDateTime entryDate;

    public LocalDateTime getEntryOrCurrentDate() {
        return Optional.ofNullable(entryDate).orElseGet(LocalDateTime::now);
    }
}
