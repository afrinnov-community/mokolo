package org.afrinnov.available.rest.input;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Getter
@Builder
@Jacksonized
public class WithdrawalRequest {
    @NotEmpty
    private String colorRef;
    @NotEmpty
    private List<WithdrawalSize> sizes;
    private LocalDateTime withdrawalDate;

    public LocalDateTime getWithdrawalOrCurrentDate() {
        return Optional.ofNullable(withdrawalDate).orElseGet(LocalDateTime::now);
    }

    public record WithdrawalSize(@NotEmpty String sizeRef, @NotNull @Min(1) int qty) {
    }
}
