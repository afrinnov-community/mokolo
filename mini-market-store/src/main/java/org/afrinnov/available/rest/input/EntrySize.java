package org.afrinnov.available.rest.input;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;

import java.math.BigDecimal;

public record EntrySize(@NotEmpty String sizeCode, @NotNull @Min(1) int qty,
                        @PositiveOrZero BigDecimal additionalPrice) {
}
