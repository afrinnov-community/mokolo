package org.afrinnov.available.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.available.rest.input.WithdrawalRequest;
import org.afrinnov.available.services.WithdrawalIntoStoreService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/product/{sku}/store/withdrawal")
@RequiredArgsConstructor
public class WithdrawalInStoreController {
    private final WithdrawalIntoStoreService storeService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void withdrawal(@PathVariable String sku, @RequestBody @Valid WithdrawalRequest request) {
        storeService.withdrawal(sku, request);
    }
}
