package org.afrinnov.available.repository;

import org.afrinnov.available.entities.EntryStoreEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.UUID;

public interface EntryStoreRepository extends JpaRepository<EntryStoreEntity, UUID> {
    List<EntryStoreEntity> findBySku(@NonNull String sku);

    List<EntryStoreEntity> findBySkuAndSizeRefAndColorRef(@NonNull String sku, @NonNull String sizeRef, @NonNull String colorRef);
}