package org.afrinnov.available.repository;

import org.afrinnov.available.entities.WithdrawalStoreEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.UUID;

public interface WithdrawalStoreRepository extends JpaRepository<WithdrawalStoreEntity, UUID> {
    List<WithdrawalStoreEntity> findBySku(String sku);

    List<WithdrawalStoreEntity> findBySkuAndSizeRefAndColorRef(@NonNull String sku, @NonNull String sizeRef, @NonNull String colorRef);
}