package org.afrinnov.available.spi;

import org.afrinnov.available.StoreItem;

public interface AvailableItemService {
    void checkStore(StoreItem storeItem);
    boolean checkProductAvailability(String sku);
}
