package org.afrinnov.available;

import java.util.List;

public record OrderWithdrawalNotify(String orderNumber, String orderStatus, List<ItemNotify> items) {

     public static OrderWithdrawalNotify of(String orderNumber, String orderStatus, List<ItemNotify> items) {
         return new OrderWithdrawalNotify(orderNumber, orderStatus, items);
     }
    public record ItemNotify(String sku, String colorRef, String sizeRef, int qty) {
    }
}
