package org.afrinnov.available.dto;

import java.math.BigDecimal;
import java.util.List;

public record Availables(List<Color> colors) {


    public record Color(String colorCode, String label, String colorValue, List<Size> sizes){}
    public record Size(String code, String label, int qty, BigDecimal additionalPrice) {}
}
