package org.afrinnov.available;

import java.util.List;

public record OrderReturnNotify(String orderNumber, List<ItemNotify> items) {

     public static OrderReturnNotify of(String orderNumber, List<ItemNotify> items) {
         return new OrderReturnNotify(orderNumber, items);
     }
    public record ItemNotify(String sku, String colorRef, String sizeRef, int qty) {
    }
}
