package org.afrinnov.customerspace.service;

import lombok.RequiredArgsConstructor;
import org.afrinnov.customer.spi.CustomerApiService;
import org.afrinnov.customerspace.rest.output.CustomerSpace;
import org.afrinnov.order.spi.OrderApiService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerSpaceService {
    private final OrderApiService orderApiService;
    private final CustomerApiService customerApiService;
    public CustomerSpace getCustomer(String code) {
        return CustomerSpace.builder()
                .customer(customerApiService.getCustomer(code))
                .orders(orderApiService.getOrders(code))
                .build();

    }
}
