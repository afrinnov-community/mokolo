package org.afrinnov.customerspace.rest.output;

import lombok.Builder;
import lombok.Getter;
import org.afrinnov.customer.Customer;
import org.afrinnov.order.Order;

import java.util.List;

@Getter
@Builder
public class CustomerSpace {
    private Customer customer;
    private List<Order> orders;
}
