package org.afrinnov.customerspace.rest;

import lombok.RequiredArgsConstructor;
import org.afrinnov.customerspace.rest.output.CustomerSpace;
import org.afrinnov.customerspace.service.CustomerSpaceService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/customer-space")
@RequiredArgsConstructor
public class CustomerSpaceController {
    private final CustomerSpaceService customerSpaceService;

    @GetMapping("/{code}")
    @ResponseStatus(HttpStatus.OK)
    public CustomerSpace getCustomer(@PathVariable String code) {
        return customerSpaceService.getCustomer(code);
    }
}
