package org.afrinnov.order.rest.flow;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.order.rest.flow.input.ItemRequest;
import org.afrinnov.order.rest.flow.service.ItemOrderForService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/orders/for/flow/{orderSessionId}/item")
@RequiredArgsConstructor
public class ItemOrderForController {

    @Autowired
    private final ItemOrderForService itemOrderForService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addItemToOrder(@PathVariable String orderSessionId, @RequestBody @Valid ItemRequest item){
        throw new UnsupportedOperationException("This feature is not available yet");
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void removeItemFromOrder(@PathVariable String orderSessionId, @RequestBody @Valid ItemRequest item){
        throw new UnsupportedOperationException("This feature is not available yet");
    }



}
