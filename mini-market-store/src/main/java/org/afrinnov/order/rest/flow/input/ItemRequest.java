package org.afrinnov.order.rest.flow.input;

import java.math.BigDecimal;

public record ItemRequest(String sku, String sizeCode, String colorCode, BigDecimal qty) {
}
