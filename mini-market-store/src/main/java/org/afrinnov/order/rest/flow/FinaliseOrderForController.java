package org.afrinnov.order.rest.flow;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.order.dto.CommandNumber;
import org.afrinnov.order.rest.flow.input.FinaliseRequest;
import org.afrinnov.order.rest.flow.service.FinaliseOrderForService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/orders/for/flow/{orderSessionId}/finalise")
@RequiredArgsConstructor
public class FinaliseOrderForController {

    @Autowired
    private final FinaliseOrderForService finaliseOrderForService;

    @PostMapping
    public CommandNumber finaliseOrder(@PathVariable String orderSessionId, @RequestBody @Valid FinaliseRequest finalise){
        throw new UnsupportedOperationException("This feature is not available yet");
    }
}
