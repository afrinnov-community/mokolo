package org.afrinnov.order.rest.output;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class CustomerDto {
    private String customerNumber;
    private String fullName;
    private String phone;
    private String email;
}
