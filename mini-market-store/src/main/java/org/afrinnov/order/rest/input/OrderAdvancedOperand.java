package org.afrinnov.order.rest.input;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum OrderAdvancedOperand {
    EQUAL, GREATER_THAN,
    LESS_THAN, GREATER_OR_EQUAL_THAN,
    LESS_OR_EQUAL_THAN, NOT_EQUAL,
    LIKE, BETWEEN
}
