package org.afrinnov.order.rest.flow.input;

import jakarta.validation.constraints.NotEmpty;

public record CustomerOrderId(@NotEmpty String customerId) {
}
