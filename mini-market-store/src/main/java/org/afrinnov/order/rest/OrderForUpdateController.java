package org.afrinnov.order.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.order.dto.CommandNumber;
import org.afrinnov.order.rest.input.CommandForEdit;
import org.afrinnov.order.rest.input.CommandForRequest;
import org.afrinnov.order.services.OrderForValidateService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/orders/for/{commandNumber}")
@RequiredArgsConstructor
public class OrderForUpdateController {
    private final OrderForValidateService orderForService;

    @PostMapping("/validate")
    public CommandNumber validCommand(@PathVariable String commandNumber) {
        return orderForService.validate(new CommandNumber(commandNumber));
    }

    @PostMapping("/cancel")
    public CommandNumber cancelCommand(@PathVariable String commandNumber) {
        return orderForService.cancel(new CommandNumber(commandNumber));
    }

    @PatchMapping
    public CommandNumber editCommand(@PathVariable String commandNumber, @RequestBody @Valid CommandForEdit request) {
        return orderForService.edit(new CommandNumber(commandNumber), request);
    }
}
