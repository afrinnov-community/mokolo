package org.afrinnov.order.rest.input;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;

@Getter
@Builder
@Jacksonized
public class Item {
    @NotEmpty
    private String sku;
    @NotEmpty
    private String colorRef;
    @NotEmpty
    private String sizeRef;
    @NotEmpty
    private String pricingRef;
    @NotNull
    @Min(1)
    private int qty;
    @NotNull
    @Min(0)
    private BigDecimal price;

    public BigDecimal getTotalPrice() {
        return price.multiply(BigDecimal.valueOf(qty));
    }
}
