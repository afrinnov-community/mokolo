package org.afrinnov.order.rest.output;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@Builder
public class ItemDto {
    private String orderNumberRow;
    private BigDecimal price;
    private int qty;
    private String sku;
}
