package org.afrinnov.order.rest.output;

import org.springframework.data.domain.Page;

public record Orders(Page<OrderDto> orders) {
}
