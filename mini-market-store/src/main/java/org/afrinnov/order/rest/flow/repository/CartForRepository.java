package org.afrinnov.order.rest.flow.repository;

import org.afrinnov.order.rest.flow.entity.CartForEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CartForRepository extends JpaRepository<CartForEntity, UUID> {
}
