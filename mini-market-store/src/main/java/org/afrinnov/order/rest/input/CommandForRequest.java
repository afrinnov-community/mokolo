package org.afrinnov.order.rest.input;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Getter
@Builder
@Jacksonized
public class CommandForRequest {
    @NotEmpty
    private List<Item> items;
    @NotNull
    private Shopper shopper;
    private String paymentMode;
    private String deliveryMode;


    public int countItems() {
        if (Objects.isNull(items)) {
            return 0;
        }
        return items.stream().mapToInt(Item::getQty).sum();
    }

    public BigDecimal sumPrices() {
        if (Objects.isNull(items)) {
            return BigDecimal.ZERO;
        }
        return BigDecimal.valueOf(items.stream().map(Item::getTotalPrice).mapToDouble(BigDecimal::doubleValue).sum());
    }
}
