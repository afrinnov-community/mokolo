package org.afrinnov.order.rest.flow.input;

import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FinaliseRequest {
    @NotEmpty
    private String deliveryMode;
    @NotEmpty
    private String paymentMode;
    @NotEmpty
    private String shippingMode;
}
