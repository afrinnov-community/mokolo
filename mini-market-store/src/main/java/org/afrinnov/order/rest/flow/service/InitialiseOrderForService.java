package org.afrinnov.order.rest.flow.service;


import lombok.RequiredArgsConstructor;
import org.afrinnov.order.rest.flow.output.OrderSessionId;
import org.afrinnov.order.services.CartForService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class InitialiseOrderForService {

    private final CartForService cartForService;

    public OrderSessionId init() {
        return new OrderSessionId(UUID.randomUUID().toString());
    }
}
