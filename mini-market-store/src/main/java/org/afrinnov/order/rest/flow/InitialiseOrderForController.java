package org.afrinnov.order.rest.flow;

import lombok.RequiredArgsConstructor;
import org.afrinnov.order.rest.flow.output.OrderSessionId;
import org.afrinnov.order.rest.flow.service.InitialiseOrderForService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/orders/for/flow")
@RequiredArgsConstructor
public class InitialiseOrderForController {

    private final InitialiseOrderForService initialiseOrderForService;

    @PostMapping
    public OrderSessionId initialiseOrder() {

        return initialiseOrderForService.init();

    }
}
