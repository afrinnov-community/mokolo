package org.afrinnov.order.rest.input;

import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class Shopper {
    @NotEmpty
    private String clientNumber;
    private String email;
    private String fullName;
    private String phoneNumber;
    private String currency;
}
