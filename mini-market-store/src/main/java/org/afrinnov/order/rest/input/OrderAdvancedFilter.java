package org.afrinnov.order.rest.input;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.afrinnov.order.entities.OrderEntity;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

public record OrderAdvancedFilter(String alias, Object value1, Object value2, OrderAdvancedOperand operand) {
    public Predicate makePredicate(Root<OrderEntity> entity, CriteriaBuilder builder) {
        return switch (operand) {
            case EQUAL -> builder.equal(entity.get(alias), value1);
            case GREATER_THAN -> makeNumberPredicate(entity, builder);
            case LESS_THAN -> makeLessThanNumberPredicate(entity, builder);
            case GREATER_OR_EQUAL_THAN -> makeGreaterThanOrEqualrPredicate(entity, builder);
            case LESS_OR_EQUAL_THAN -> makeLessThanOrEqualNumberPredicate(entity, builder);
            case NOT_EQUAL -> builder.not(builder.equal(entity.get(alias), value1));
            case LIKE -> builder.like(entity.get(alias), Objects.toString(value1));
            case BETWEEN -> {
                Objects.requireNonNull(value1);
                Objects.requireNonNull(value2);
                yield builder.between(entity.get(alias), Objects.toString(value1), Objects.toString(value2.toString()));
            }
        };
    }

    private Predicate makeLessThanNumberPredicate(Root<OrderEntity> entity, CriteriaBuilder builder) {
        if (value1 instanceof Integer value) {
            return builder.lessThan(entity.get(alias), value);
        }
        if (value1 instanceof BigDecimal value) {
            return builder.lessThan(entity.get(alias), value);
        }

        if (value1 instanceof Instant value) {
            return builder.lessThan(entity.get(alias), value);
        }
        throw new UnsupportedOperationException("le type " + value1.getClass() + " is not supported by lessThan");
    }

    private Predicate makeLessThanOrEqualNumberPredicate(Root<OrderEntity> entity, CriteriaBuilder builder) {
        if (value1 instanceof Integer value) {
            return builder.lessThanOrEqualTo(entity.get(alias), value);
        }
        if (value1 instanceof BigDecimal value) {
            return builder.lessThanOrEqualTo(entity.get(alias), value);
        }
        if (value1 instanceof Instant value) {
            return builder.lessThanOrEqualTo(entity.get(alias), value);
        }
        throw new UnsupportedOperationException("le type " + value1.getClass() + " is not supported by lessThanOrEqualTo");
    }

    private Predicate makeNumberPredicate(Root<OrderEntity> entity, CriteriaBuilder builder) {
        if (value1 instanceof Integer value) {
            return builder.greaterThan(entity.get(alias), value);
        }
        if (value1 instanceof BigDecimal value) {
            return builder.greaterThan(entity.get(alias), value);
        }
        if (value1 instanceof Instant value) {
            return builder.greaterThan(entity.get(alias), value);
        }
        throw new UnsupportedOperationException("le type " + value1.getClass() + " is not supported by greaterThan");
    }

    private Predicate makeGreaterThanOrEqualrPredicate(Root<OrderEntity> entity, CriteriaBuilder builder) {
        if (value1 instanceof Integer value) {
            return builder.greaterThanOrEqualTo(entity.get(alias), value);
        }
        if (value1 instanceof BigDecimal value) {
            return builder.greaterThanOrEqualTo(entity.get(alias), value);
        }
        if (value1 instanceof Instant value) {
            return builder.greaterThanOrEqualTo(entity.get(alias), value);
        }
        throw new UnsupportedOperationException("le type " + value1.getClass() + " is not supported by greaterThanOrEqualTo");
    }
}
