package org.afrinnov.order.rest.flow.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.afrinnov.order.dto.CartFor;
import org.afrinnov.order.rest.flow.input.CustomerOrderId;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class CustomerOrderForService {

    private CartFor cart;

    public void attacheCustomerToOrder(String orderSessionId, CustomerOrderId customerOrderId){
        cart = CartFor.init(new ObjectMapper(), orderSessionId);
        cart.attacheCustom(Objects.requireNonNull(customerOrderId).customerId());
    }

    public void detachCustomerToOrder(String orderSessionId){
        if(this.cart.getSessionIdCartFor().equals(orderSessionId)){
            cart.deattachCustom();
        }
    }
}
