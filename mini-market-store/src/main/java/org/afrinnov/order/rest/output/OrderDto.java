package org.afrinnov.order.rest.output;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;
import org.afrinnov.order.entities.OrderStatus;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Builder
@Jacksonized
public class OrderDto {
    private String orderNumber;
    private LocalDate createdDate;
    private OrderStatus status;
    private LocalDateTime statusDate;
    private List<ItemDto>items;
    private CustomerDto customer;
}
