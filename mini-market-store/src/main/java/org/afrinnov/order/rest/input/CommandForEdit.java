package org.afrinnov.order.rest.input;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class CommandForEdit {
    private String paymentMode;
    private String deliveryMode;
    private String currency;

}
