package org.afrinnov.order.rest.flow;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.order.rest.flow.input.CustomerOrderId;
import org.afrinnov.order.rest.flow.service.CustomerOrderForService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/orders/for/flow/{orderSessionId}/customer")
@RequiredArgsConstructor
public class CustomerOrderForController {

    @Autowired
    private final CustomerOrderForService customerOrderForService;

    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void attacheCustomerToOrder(@PathVariable String orderSessionId, @RequestBody @Valid CustomerOrderId customerOrderId){
        customerOrderForService.attacheCustomerToOrder(orderSessionId, customerOrderId);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void detachCustomerToOrder(@PathVariable String orderSessionId){
        customerOrderForService.detachCustomerToOrder(orderSessionId);
    }
}
