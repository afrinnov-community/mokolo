package org.afrinnov.order.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.order.rest.input.OrderAdvancedCriteria;
import org.afrinnov.order.rest.output.OrderDto;
import org.afrinnov.order.rest.output.Orders;
import org.afrinnov.order.services.OrderFilterService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/orders/filters")
@RequiredArgsConstructor
public class OrderFilterController {
    private final OrderFilterService orderFilterService;

    @GetMapping
    public Orders getOrders() {
        return orderFilterService.getOrders();
    }

    @PostMapping()
    public List<OrderDto> postOrdersFilters(@RequestBody @Valid OrderAdvancedCriteria criteria) {
        return orderFilterService.advanceFilterOrders(criteria);
    }
}
