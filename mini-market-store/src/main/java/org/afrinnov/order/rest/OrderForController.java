package org.afrinnov.order.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.order.dto.CommandNumber;
import org.afrinnov.order.rest.input.CommandForRequest;
import org.afrinnov.order.services.OrderForService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/orders/for")
@RequiredArgsConstructor
public class OrderForController {
    private final OrderForService orderForService;

    @PostMapping
    public CommandNumber createCommand(@RequestBody @Valid CommandForRequest request) {
        return orderForService.create(request);
    }
}
