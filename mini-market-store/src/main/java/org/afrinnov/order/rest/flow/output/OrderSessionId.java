package org.afrinnov.order.rest.flow.output;

public record OrderSessionId(String sessionId) {
}
