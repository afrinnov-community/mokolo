package org.afrinnov.order.rest.input;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Getter
@Builder
@Jacksonized
public class OrderCriteria {
    @Builder.Default
    private int page = 0;
    @Builder.Default
    private int size = 25;
    private List<String> filter;
}
