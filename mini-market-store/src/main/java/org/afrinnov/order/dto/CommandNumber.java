package org.afrinnov.order.dto;

public record CommandNumber(String number) {
}
