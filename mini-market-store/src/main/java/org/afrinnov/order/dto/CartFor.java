package org.afrinnov.order.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import io.swagger.v3.core.util.Json;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;
import org.afrinnov.order.rest.flow.input.ItemRequest;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
public class CartFor {

    private final ObjectMapper objectMapper;
    private final String sessionIdCartFor;
    private ContentCartFor contentCartFor;

    private CartFor(ObjectMapper objectMapper, String cartSessionId) {

        this.objectMapper = Objects.requireNonNull(objectMapper);
        this.sessionIdCartFor = Objects.requireNonNull(cartSessionId);
        contentCartFor = ContentCartFor.builder().build();
    }

    public static CartFor init(ObjectMapper objectMapper, String cartSessionId) {
        return new CartFor(objectMapper, cartSessionId);
    }

    public String getId() {
        return sessionIdCartFor;
    }

    public String getContent() {
        try {
            return objectMapper.writeValueAsString(contentCartFor);
        } catch (JsonProcessingException e) {
            return "{}";
        }
    }

    public void attacheCustom(String customId) {
        contentCartFor = this.contentCartFor.toBuilder().customerId(customId).build();
    }

    public void deattachCustom(){
        contentCartFor = this.contentCartFor.toBuilder().build();
    }

    public void definePaymentMode(String paymentMode) {
        contentCartFor = this.contentCartFor.toBuilder().paymentMode(paymentMode).build();
    }

    public void defineDeliveryMode(String deliveryMode) {
        contentCartFor = this.contentCartFor.toBuilder().deliveryMode(deliveryMode).build();
    }

    public void defineShippingMode(String shippingMode) {
        contentCartFor = this.contentCartFor.toBuilder().shippingMode(shippingMode).build();
    }

    public void addItem(@NotNull ItemRequest itemRequest) {
        Item item = Item.givenItem(itemRequest);
        if (contentCartFor.items == null) {
            contentCartFor = this.contentCartFor.toBuilder().items(new ArrayList<>(List.of(Objects.requireNonNull(item)))).build();
        } else {
            this.contentCartFor.getItems().stream().filter(current -> current.equals(item))
                    .findFirst()
                    .ifPresentOrElse( foundItem -> updateCurrentQty(foundItem, Objects.requireNonNull(item).getQty()),
                                      () -> this.contentCartFor.getItems().add(item)
                    );
        }

    }

    public void removeItem(@NotNull ItemRequest itemRequest) {
        Item item = Item.givenItem(itemRequest);
        this.contentCartFor.getItems().stream().filter(current -> current.equals(Objects.requireNonNull(item)))
                .findFirst()
                .ifPresent(foundItem -> subCurrentQty(foundItem, Objects.requireNonNull(item).getQty()));
    }

    private void updateCurrentQty(Item foundItem, BigDecimal qty) {
        Item build = foundItem.toBuilder().qty(qty.add(foundItem.getQty())).build();
        int index = contentCartFor.items.indexOf(foundItem);
        contentCartFor.items.set(index, build);
    }

    private void subCurrentQty(Item foundItem, BigDecimal qty) {

        int index = contentCartFor.items.indexOf(foundItem);
        BigDecimal qt = foundItem.getQty().subtract(qty);
        if((qt.compareTo(BigDecimal.ZERO) <= 0)){
            contentCartFor.items.remove(index);
        } else {
            Item build = foundItem.toBuilder().qty(foundItem.getQty().subtract(qty)).build();
            contentCartFor.items.set(index, build);
        }
    }

    public List<Item> getItems() {
        return this.contentCartFor.items;
    }

    @Getter
    @Builder(toBuilder = true)
    @Jacksonized
    private static class ContentCartFor {
        private String customerId;
        private List<Item> items;
        private String deliveryMode;

        private String paymentMode;

        private String shippingMode;
    }

    @Builder(toBuilder = true)
    @Getter
    @Jacksonized
    private static class Item {
        private String sku;
        private String sizeCode;
        private String colorCode;
        private BigDecimal qty;

        public static Item givenItem(ItemRequest itemRequest) {
            try {
                return new Gson().fromJson(Json.mapper().writeValueAsString(itemRequest), Item.class);
            } catch (JsonProcessingException e) {
                return null;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Item item = (Item) o;
            return Objects.equals(sku, item.sku) && Objects.equals(sizeCode, item.sizeCode) && Objects.equals(colorCode, item.colorCode);
        }

        @Override
        public int hashCode() {
            return Objects.hash(sku, sizeCode, colorCode);
        }
    }

}
