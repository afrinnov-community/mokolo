package org.afrinnov.order;

import org.afrinnov.order.Order.OrderRow;
import org.afrinnov.order.entities.OrderEntity;
import org.afrinnov.order.entities.OrderRowEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderMapper {
    OrderMapper ORDER_MAPPER = Mappers.getMapper(OrderMapper.class);

    Order mapEntityToOrder(OrderEntity entity);

    OrderRow mapEntityToOrderRow(OrderRowEntity entity);
}
