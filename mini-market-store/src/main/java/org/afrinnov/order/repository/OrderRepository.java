package org.afrinnov.order.repository;

import org.afrinnov.order.entities.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OrderRepository extends JpaRepository<OrderEntity, UUID>,
        JpaSpecificationExecutor<OrderEntity>, PagingAndSortingRepository<OrderEntity, UUID> {
    Optional<OrderEntity> findByOrderNumber(@NonNull String orderNumber);

    List<OrderEntity> findByShopperRef(@NonNull String shopperRef);
}