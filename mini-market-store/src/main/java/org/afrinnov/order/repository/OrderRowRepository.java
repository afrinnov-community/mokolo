package org.afrinnov.order.repository;

import org.afrinnov.order.entities.OrderRowEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OrderRowRepository extends JpaRepository<OrderRowEntity, UUID> {
}