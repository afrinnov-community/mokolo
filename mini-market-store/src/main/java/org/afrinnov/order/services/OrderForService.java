package org.afrinnov.order.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.available.spi.AvailableItemService;
import org.afrinnov.available.StoreItem;
import org.afrinnov.customer.CustomerNotify;
import org.afrinnov.order.dto.CommandNumber;
import org.afrinnov.order.entities.OrderEntity;
import org.afrinnov.order.entities.OrderRowEntity;
import org.afrinnov.order.repository.OrderRepository;
import org.afrinnov.order.repository.OrderRowRepository;
import org.afrinnov.order.rest.input.CommandForRequest;
import org.afrinnov.order.rest.input.Item;
import org.afrinnov.pricing.PricingItem;
import org.afrinnov.pricing.spi.PricingApiService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import static com.google.common.base.Strings.padStart;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderForService {
    private final OrderRepository orderRepository;
    private final OrderRowRepository orderRowRepository;
    private final ApplicationEventPublisher events;
    private final AvailableItemService availableItemService;
    private final PricingApiService pricingApiService;

    public CommandNumber create(CommandForRequest request) {
        request.getItems().forEach(this::checkItemValid);
        request.getItems().forEach(this::checkPricingValid);
        if(Objects.nonNull(request.getShopper().getCurrency())) {
            pricingApiService.checkCurrency(request.getShopper().getCurrency());
        }
        OrderEntity order = OrderEntity.newEntity(request);
        order.setOrderNumber(generateOrderNumber());
        orderRepository.save(order);
        var index = new AtomicInteger(0);
        var rowEntities = request.getItems().stream().map(OrderRowEntity::newEntity)
                .peek(row -> {
                    row.setOrder(order);
                    var orderNumberRow = padStart(String.valueOf(index.incrementAndGet()), 3, '0');
                    row.setOrderNumberRow(order.getOrderNumber() + "." + orderNumberRow);
                }).toList();
        orderRowRepository.saveAll(rowEntities);

        Map<String, Object> data = Map.of("orderNumber", order.getOrderNumber());
        events.publishEvent(new CustomerNotify(request.getShopper().getClientNumber(), "ORDER_FOR", data));
        return new CommandNumber(order.getOrderNumber());
    }

    private void checkPricingValid(Item item) {
        pricingApiService.checkPrice(new PricingItem(item.getSku(), item.getPricingRef(), item.getPrice()));
    }

    private void checkItemValid(Item item) {
        availableItemService
                .checkStore(new StoreItem(item.getSku(), item.getColorRef(), item.getSizeRef(), item.getQty()));
    }

    private String generateOrderNumber() {
        return LocalDate.now().format(ofPattern("yyMMdd")) + randomNumeric(3) + randomNumeric(4);
    }
}
