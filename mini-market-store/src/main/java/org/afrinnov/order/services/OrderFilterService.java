package org.afrinnov.order.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.order.entities.OrderEntity;
import org.afrinnov.order.entities.OrderRowEntity;
import org.afrinnov.order.filter.OrderFindAllBuilders;
import org.afrinnov.order.repository.OrderRepository;
import org.afrinnov.order.rest.input.OrderAdvancedCriteria;
import org.afrinnov.order.rest.output.CustomerDto;
import org.afrinnov.order.rest.output.ItemDto;
import org.afrinnov.order.rest.output.OrderDto;
import org.afrinnov.order.rest.output.Orders;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class OrderFilterService {
    private final OrderRepository orderRepository;

    public Orders getOrders() {
        Page<OrderDto> map = orderRepository.findAll(PageRequest.of(0, 25))
                .map(this::mapEntityToDto);
        return new Orders(map);
    }

    private OrderDto mapEntityToDto(OrderEntity entity) {
        return OrderDto.builder()
                .orderNumber(entity.getOrderNumber())
                .createdDate(convert(entity.getCreateDate()))
                .status(entity.getStatus())
                .statusDate(entity.getStatusDate())
                .items(entity.getOrderRows().stream().map(this::mapRowEntityToDto).toList())
                .customer(mapToCustomer(entity))
                .build();
    }

    private static CustomerDto mapToCustomer(OrderEntity entity) {
        return CustomerDto.builder()
                .customerNumber(entity.getShopperRef())
                .email(entity.getShopperEmail())
                .phone(entity.getShopperPhone())
                .fullName(entity.getShopperFullName())
                .build();
    }

    private ItemDto mapRowEntityToDto(OrderRowEntity entity) {
        return ItemDto.builder()
                .orderNumberRow(entity.getOrderNumberRow())
                .sku(entity.getSku())
                .qty(entity.getQty())
                .price(entity.getPrice())
                .build();
    }

    private LocalDate convert(Instant instant) {
        ZoneId zoneId = ZoneId.systemDefault();
        return LocalDate.ofInstant(instant, zoneId);
    }

    public List<OrderDto> advanceFilterOrders(OrderAdvancedCriteria criteria) {
        return OrderFindAllBuilders.usingRepository(orderRepository).filterBy(criteria.getFilters())
                .findAll(criteria.getPage(), criteria.getSize()).stream().map(this::mapEntityToDto).toList();
    }
}
