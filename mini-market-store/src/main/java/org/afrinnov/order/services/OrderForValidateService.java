package org.afrinnov.order.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.available.OrderReturnNotify;
import org.afrinnov.available.OrderReturnNotify.ItemNotify;
import org.afrinnov.available.OrderWithdrawalNotify;
import org.afrinnov.customer.CustomerNotify;
import org.afrinnov.order.dto.CommandNumber;
import org.afrinnov.order.entities.OrderEntity;
import org.afrinnov.order.entities.OrderRowEntity;
import org.afrinnov.order.repository.OrderRepository;
import org.afrinnov.order.rest.input.CommandForEdit;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;

import static org.afrinnov.order.entities.OrderStatus.CANCELED;
import static org.afrinnov.order.entities.OrderStatus.VALIDATE;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderForValidateService {
    private final OrderRepository orderRepository;
    private final ApplicationEventPublisher events;

    public CommandNumber validate(CommandNumber commandNumber) {
        OrderEntity order = orderRepository.findByOrderNumber(commandNumber.number())
                .orElseThrow(() -> new IllegalArgumentException("ORDER_NOT_FOUND:" + commandNumber.number()));
        order.checkOrderBeforeValidate();
        order.setStatus(VALIDATE);
        order.setStatusDate(LocalDateTime.now());
        orderRepository.save(order);
        Map<String, Object> data = Map.of("orderNumber", commandNumber.number());
        events.publishEvent(OrderWithdrawalNotify.of(order.getOrderNumber(), VALIDATE.name(),
                order.getOrderRows().stream().map(this::mapEntityToWithdrawalDto).toList()));
        events.publishEvent(new CustomerNotify(order.getShopperRef(), "VALIDATE_ORDER_FOR", data));
        return commandNumber;
    }

    public CommandNumber cancel(CommandNumber commandNumber) {
        OrderEntity order = orderRepository.findByOrderNumber(commandNumber.number())
                .orElseThrow(() -> new IllegalArgumentException("ORDER_NOT_FOUND:" + commandNumber.number()));
        order.checkOrderBeforeCancel();
        order.setStatus(CANCELED);
        order.setStatusDate(LocalDateTime.now());
        orderRepository.save(order);
        if (order.isAlreadyValidate()) {
            events.publishEvent(OrderReturnNotify.of(order.getOrderNumber(), order.getOrderRows().stream().map(this::mapEntityToDto).toList()));
        }
        Map<String, Object> data = Map.of("orderNumber", commandNumber.number());

        events.publishEvent(new CustomerNotify(order.getShopperRef(), "CANCELED_ORDER_FOR", data));
        return commandNumber;
    }

    private ItemNotify mapEntityToDto(OrderRowEntity item) {
        return new ItemNotify(item.getSku(), item.getColorRef(), item.getSizeRef(), item.getQty());
    }


    private OrderWithdrawalNotify.ItemNotify mapEntityToWithdrawalDto(OrderRowEntity item) {
        return new OrderWithdrawalNotify.ItemNotify(item.getSku(), item.getColorRef(), item.getSizeRef(), item.getQty());
    }

    public CommandNumber edit(CommandNumber commandNumber, CommandForEdit request) {
        OrderEntity order = orderRepository.findByOrderNumber(commandNumber.number())
                .orElseThrow(() -> new IllegalArgumentException("ORDER_NOT_FOUND:" + commandNumber.number()));
        Optional.ofNullable(request.getCurrency()).ifPresent(order::setShopperCurrency);
        Optional.ofNullable(request.getPaymentMode()).ifPresent(order::setPaymentmode);
        Optional.ofNullable(request.getDeliveryMode()).ifPresent(order::setDeliverymode);
        orderRepository.save(order);
        return commandNumber;
    }
}
