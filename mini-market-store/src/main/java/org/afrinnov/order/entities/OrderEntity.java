package org.afrinnov.order.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.order.rest.input.CommandForRequest;
import org.afrinnov.order.rest.input.Shopper;
import org.hibernate.envers.Audited;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

import static org.afrinnov.order.entities.OrderStatus.*;

@Entity
@Table(schema = "eshop", name = "c_order")
@Getter
@Setter
@Audited
public class OrderEntity extends LocalBaseAuditEntity {
    private String orderNumber;
    private BigDecimal totalPrice;
    private int countItems;
    private String shopperRef;
    private String shopperEmail;
    private String shopperPhone;
    private String shopperCurrency;
    private String shopperFullName;
    private String paymentmode;
    private String deliverymode;
    @Enumerated(EnumType.STRING)
    private OrderStatus status;
    private LocalDateTime statusDate;

    @OneToMany(mappedBy = "order")
    private Set<OrderRowEntity> orderRows = new HashSet<>();

    public static OrderEntity newEntity(CommandForRequest request) {
        var order = new OrderEntity();
        order.setCountItems(request.countItems());
        Shopper shopper = request.getShopper();
        order.setShopperEmail(shopper.getEmail());
        order.setShopperPhone(shopper.getPhoneNumber());
        order.setShopperRef(shopper.getClientNumber());
        order.setShopperCurrency(shopper.getCurrency());
        order.setShopperFullName(shopper.getFullName());
        order.setTotalPrice(request.sumPrices());
        order.setStatus(NEW);
        order.setStatusDate(LocalDateTime.now());
        order.setDeliverymode(request.getDeliveryMode());
        order.setPaymentmode(request.getPaymentMode());
        return order;
    }

    public void checkOrderBeforeValidate() {
        if (CANCELED.equals(status)) {
            throw new UnsupportedOperationException("Unable to validate canceled order:" + orderNumber + " date:" + formateDate());
        }

        if (VALIDATE.equals(status)) {
            throw new UnsupportedOperationException("Order is already validate:" + orderNumber + " date:" + formateDate());
        }
    }

    private String formateDate() {
        return statusDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss"));
    }

    public void checkOrderBeforeCancel() {
        if (CANCELED.equals(status)) {
            throw new UnsupportedOperationException("Order is already canceled:" + orderNumber + " date:" + formateDate());
        }
    }

    public boolean isAlreadyValidate() {
        return VALIDATE.equals(status);
    }
}
