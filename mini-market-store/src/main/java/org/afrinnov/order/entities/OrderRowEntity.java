package org.afrinnov.order.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.order.rest.input.Item;
import org.hibernate.envers.Audited;

import java.math.BigDecimal;

@Entity
@Table(schema = "eshop", name = "c_order_row")
@Getter
@Setter
@Audited
public class OrderRowEntity extends LocalBaseAuditEntity {
    private String orderNumberRow;
    @ManyToOne
    private OrderEntity order;
    private BigDecimal price;
    private int qty;
    private String sku;
    private String colorRef;
    private String sizeRef;
    private String pricingRef;


    public static OrderRowEntity newEntity(Item item) {
        var entity = new OrderRowEntity();
        entity.setQty(item.getQty());
        entity.setPrice(item.getPrice());
        entity.setSku(item.getSku());
        entity.setColorRef(item.getColorRef());
        entity.setSizeRef(item.getSizeRef());
        entity.setPricingRef(item.getPricingRef());
        return entity;
    }
}
