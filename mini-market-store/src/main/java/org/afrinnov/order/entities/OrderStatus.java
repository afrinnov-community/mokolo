package org.afrinnov.order.entities;

public enum OrderStatus {
    VALIDATE, CANCELED, PENDING, NEW
}
