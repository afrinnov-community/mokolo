package org.afrinnov.order.spi;

import org.afrinnov.order.Order;

import java.util.List;

public interface OrderApiService {
    List<Order> getOrders(String customerRef);
}
