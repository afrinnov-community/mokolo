package org.afrinnov.order;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Builder(toBuilder = true)
@EqualsAndHashCode(of = "orderNumber")
public class Order {
    private String orderNumber;
    private BigDecimal totalPrice;
    private int countItems;
    private String shopperRef;
    private String shopperEmail;
    private String shopperPhone;
    private String shopperCurrency;
    private String shopperFullName;
    private String status;
    private LocalDateTime statusDate;
    private List<OrderRow> orderRows;

    @Getter
    @Builder(toBuilder = true)
    @EqualsAndHashCode(of = "orderNumberRow")
    public static class OrderRow {
        private String orderNumberRow;
        private BigDecimal price;
        private int qty;
        private String sku;
        private String colorRef;
        private String sizeRef;
        private String pricingRef;
    }
}
