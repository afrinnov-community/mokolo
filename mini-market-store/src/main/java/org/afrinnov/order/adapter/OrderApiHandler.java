package org.afrinnov.order.adapter;

import lombok.RequiredArgsConstructor;
import org.afrinnov.order.Order;
import org.afrinnov.order.spi.OrderApiService;
import org.afrinnov.order.entities.OrderEntity;
import org.afrinnov.order.repository.OrderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.afrinnov.order.OrderMapper.ORDER_MAPPER;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class OrderApiHandler implements OrderApiService {
    private final OrderRepository orderRepository;
    @Override
    public List<Order> getOrders(String customerRef) {
        return orderRepository.findByShopperRef(customerRef)
                .stream().map(this::mapEntityToDto).toList();
    }

    private Order mapEntityToDto(OrderEntity entity) {
        return ORDER_MAPPER.mapEntityToOrder(entity);
    }
}
