package org.afrinnov.order.filter;

import org.afrinnov.order.entities.OrderEntity;
import org.afrinnov.order.repository.OrderRepository;
import org.afrinnov.order.rest.input.OrderAdvancedFilter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.LinkedList;
import java.util.List;


public class OrderFindOrderOrderAllBuilder implements PrepareOrderAllBuilder, ProcessOrderAllBuilder {
    private final OrderRepository repository;
    private Specification<OrderEntity> filters;
    private Sort sort = Sort.unsorted();



    OrderFindOrderOrderAllBuilder(OrderRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<OrderEntity> findAll(int page, int size) {
        return new LinkedList<>(repository.findAll(filters, PageRequest.of(page, size, sort)).toList());
    }

    @Override
    public ProcessOrderAllBuilder filterBy(List<OrderAdvancedFilter> listFilters) {
        for (OrderAdvancedFilter filter : listFilters) {
            Specification<OrderEntity> fct = (root, query, builder) -> filter.makePredicate(root, builder);
            if (filters == null) {
                filters = Specification.where(fct);
            } else {
                filters.and(fct);
            }
        }
        return this;
    }

    @Override
    public ProcessOrderAllBuilder sortBy(String orderBy, String orderDir) {
        if (StringUtils.isNotEmpty(orderBy)) {
            sort = Sort.by(Sort.Direction.fromOptionalString(orderDir).orElse(Sort.Direction.ASC), orderBy);
        }

        return this;
    }
}
