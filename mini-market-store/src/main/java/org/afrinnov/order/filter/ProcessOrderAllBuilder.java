package org.afrinnov.order.filter;

import org.afrinnov.order.entities.OrderEntity;

import java.util.List;

public interface ProcessOrderAllBuilder {
    List<OrderEntity> findAll(int page, int size);
}
