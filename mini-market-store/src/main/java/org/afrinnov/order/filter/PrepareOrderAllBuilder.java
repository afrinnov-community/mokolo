package org.afrinnov.order.filter;

import org.afrinnov.order.rest.input.OrderAdvancedFilter;

import java.util.List;

public interface PrepareOrderAllBuilder {
    ProcessOrderAllBuilder filterBy(List<OrderAdvancedFilter> listFilters);

    ProcessOrderAllBuilder sortBy(String orderBy, String orderDir);
}
