package org.afrinnov.order.filter;

import org.afrinnov.order.repository.OrderRepository;

public class OrderFindAllBuilders {
    public static PrepareOrderAllBuilder usingRepository(OrderRepository repository) {
        return new OrderFindOrderOrderAllBuilder(repository);
    }
}
