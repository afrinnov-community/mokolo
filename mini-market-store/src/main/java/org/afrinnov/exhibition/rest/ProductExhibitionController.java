package org.afrinnov.exhibition.rest;

import lombok.RequiredArgsConstructor;
import org.afrinnov.exhibition.rest.output.ProductSummary;
import org.afrinnov.exhibition.services.ProductExhibitionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/exhibition/products")
@RequiredArgsConstructor
public class ProductExhibitionController {
    private final ProductExhibitionService productExhibitionService;
    @GetMapping()
    public Map<String, List<ProductSummary>> getProductsToExhibit() throws ExecutionException, InterruptedException {
        return productExhibitionService.findAllProductByPricing();
    }
}
