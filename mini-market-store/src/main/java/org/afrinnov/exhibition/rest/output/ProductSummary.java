package org.afrinnov.exhibition.rest.output;

import lombok.Builder;

@Builder
public class ProductSummary {
    private String sku;
    private String title;
    private String description;
    private String previewImageUrl;
    private double price;
    private String nature;
}
