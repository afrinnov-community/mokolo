package org.afrinnov.exhibition.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.available.spi.AvailableItemService;
import org.afrinnov.exhibition.rest.output.ProductSummary;
import org.afrinnov.pricing.spi.PricingApiService;
import org.afrinnov.pricing.ProductPricingDto;
import org.afrinnov.sheet.spi.ProductSheetApiService;
import org.afrinnov.sheet.spi.model.ProductBasic;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class ProductExhibitionService {
    private final ProductSheetApiService productSheetApiService;
    private final AvailableItemService availableItemService;
    private final PricingApiService pricingApiService;

    public Map<String, List<ProductSummary>> findAllProductByPricing() throws ExecutionException, InterruptedException {
        var classicFeature = findClassicPricings("CLASSIC");
        var saleOrderFeature = findClassicPricings("SALE_PERIOD");
        var preOrderFeature = findClassicPricings("PRE_ORDER");

        CompletableFuture.allOf(classicFeature, saleOrderFeature, preOrderFeature).join();

        return Map.of("CLASSIC", classicFeature.get().stream().map(mapDTOToView()).toList(),
                "SALE_PERIOD", saleOrderFeature.get().stream().map(mapDTOToView()).toList(),
                "PRE_ORDER", preOrderFeature.get().stream().map(mapDTOToView()).toList());
    }
            
    private Function<ProductPricingDto, ProductSummary> mapDTOToView() {
        return pricingDto -> {
            ProductBasic productBasic = productSheetApiService.getProductInfo(pricingDto.getSku())
                    .orElseThrow(() -> new NoSuchElementException("Product not found for SKU: " + pricingDto.getSku()));

            return ProductSummary.builder()
                    .sku(pricingDto.getSku())
                    .price(pricingDto.getPrice())
                    .title(productBasic.getTitle())
                    .description(productBasic.getDescription())
                    .previewImageUrl(productBasic.getPreviewImageUrl())
                    .nature(pricingDto.getNature())
                    .build();
        };
    }

    @Async
    public CompletableFuture<List<ProductPricingDto>> findClassicPricings(String nature) {
        List<ProductPricingDto> classics = new ArrayList<>();
        int page = 0;

        do {
            Page<ProductPricingDto> pricingProducts = pricingApiService.getAllProductPricing(nature, page, 10);

            List<ProductPricingDto> productPricingDtos = pricingProducts.get().filter(productSummary -> availableItemService.checkProductAvailability(productSummary.getSku()))
                    .toList();
            classics.addAll(productPricingDtos);
            if (pricingProducts.getTotalPages() < 1) {
                break;
            }
            page++;
        }
        while (classics.size() < 10);
        return CompletableFuture.completedFuture(classics);
    }
}
