package org.afrinnov.sheet;

public class TechnicalException extends RuntimeException {
    public TechnicalException(Throwable cause) {
        super(cause);
    }

}
