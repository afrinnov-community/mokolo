package org.afrinnov.sheet.dto;

public record PaymentMode(String mode, String description, Boolean enable) {
}
