package org.afrinnov.sheet.dto;

import lombok.Builder;
import lombok.Getter;
import org.afrinnov.sheet.spi.model.ProductBasic;

@Builder
@Getter
public class ProductBasicDto implements ProductBasic {
    private String sku;
    private String title;
    private String description;
    private String previewImageUrl;
}
