package org.afrinnov.sheet.dto;

public record FeatureDto(String code, String label, String description) {
}
