package org.afrinnov.sheet.dto;

public record ShippingMode(String mode, String description) {
}
