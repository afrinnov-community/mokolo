package org.afrinnov.sheet.dto;

import lombok.Builder;
import lombok.Getter;
import org.afrinnov.sheet.spi.model.ProductShare;

@Getter
@Builder
public class ProductShareDto implements ProductShare {
    private String sku;
    private String title;
    private String description;
    private String previewImageUrl;
}
