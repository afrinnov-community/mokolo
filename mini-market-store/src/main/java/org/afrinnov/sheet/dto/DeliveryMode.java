package org.afrinnov.sheet.dto;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/12/2023 -- 18:14<br></br>
 * By : @author alexk<br></br>
 * Project : mokolo<br></br>
 * Package : org.afrinnov.common.delivry.entity<br></br>
 */
public record DeliveryMode (String mode, String description) {
}
