package org.afrinnov.sheet.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class Brand {

    private String code;
    private String label;
    private String imageLogoUrl;
}
