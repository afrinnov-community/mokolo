package org.afrinnov.sheet.dto;

public record CategoryType(String code, String name, String status) {
}
