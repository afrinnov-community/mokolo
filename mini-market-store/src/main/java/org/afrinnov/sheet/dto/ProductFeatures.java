package org.afrinnov.sheet.dto;

import java.util.List;

public record ProductFeatures(List<Feature> features) {

    public record Feature(String code, String label, String description, List<ProductFeature> details){}
}
