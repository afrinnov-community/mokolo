package org.afrinnov.sheet.dto;

import org.afrinnov.sheet.spi.model.Color;

public record ColorDto(String code, String label, String value) implements Color {
}
