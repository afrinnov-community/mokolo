package org.afrinnov.sheet.dto;

public record BrandImageLogo(String urlImageLogo) {
}
