package org.afrinnov.sheet.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;
import org.afrinnov.sheet.dto.ProductFeatures.Feature;

import java.util.List;

@Getter
@Builder(toBuilder = true)
@Jacksonized
public class ProductSheet {
    private String sku;
    private String title;
    private String description;
    private Category category;
    private Brand brand;
    private FileContent previewImageId;
    private String previewImageUrl;
    private List<FileContent> wizardImageIds;
    private List<SizeDto> sizes;
    private List<String> wizardImageUrls;
    private List<ShippingMode> supportedShippingModes;
    private List<DeliveryMode> deliveryModes;
    private List<PaymentMode> paymentModes;
    private List<Feature> features;

}
