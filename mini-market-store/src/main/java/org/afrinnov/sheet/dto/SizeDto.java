package org.afrinnov.sheet.dto;

import org.afrinnov.sheet.spi.model.Size;

public record SizeDto(String code, String label, String description, String categoryCode) implements Size {
}
