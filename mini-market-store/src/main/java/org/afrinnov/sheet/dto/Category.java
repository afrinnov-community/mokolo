package org.afrinnov.sheet.dto;

import org.afrinnov.sheet.entities.CategoryEntity;

import java.util.Objects;

public record Category(String code, String label, String description) {
    public static Category map(CategoryEntity category) {
        if(Objects.isNull(category)) {
            return null;
        }
        return new Category(category.getCode(), category.getLabel(), category.getDescription());
    }
}
