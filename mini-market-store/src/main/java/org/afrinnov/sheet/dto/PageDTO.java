package org.afrinnov.sheet.dto;

import java.util.List;


public record PageDTO<T>(List<T> content, long total) {
}
