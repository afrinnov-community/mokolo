package org.afrinnov.sheet.dto;

import java.util.UUID;

import static org.afrinnov.sheet.tools.upload.ContentTypeUtils.fileExtension;

public record FileContent(UUID sid, String contentType) {

    public String makeFileName() {
        return sid.toString() + fileExtension(contentType).map(t -> "." + t).orElse("");
    }
}
