package org.afrinnov.sheet.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class FileDto {
    private byte[] image;
    private String imageContentType;
    private String originalFilename;
    private long size;
    private boolean imageMarked;
}
