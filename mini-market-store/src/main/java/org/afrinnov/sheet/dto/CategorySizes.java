package org.afrinnov.sheet.dto;

import java.util.List;

public record CategorySizes(String code, String label, String description, List<SizeDto> sizes, CategoryType categoryType) {
}
