package org.afrinnov.sheet.dto;

public record ProductFeature(String clef, String label) {
}
