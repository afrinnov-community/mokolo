package org.afrinnov.sheet.adapter;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.*;
import org.afrinnov.sheet.dto.FileContent;
import org.afrinnov.sheet.dto.ProductBasicDto;
import org.afrinnov.sheet.dto.ProductShareDto;
import org.afrinnov.sheet.dto.ProductSheet;
import org.afrinnov.sheet.entities.ProductEntity;
import org.afrinnov.sheet.id.ProductSku;
import org.afrinnov.sheet.rest.FilesController;
import org.afrinnov.sheet.services.ColorService;
import org.afrinnov.sheet.services.ProductSizeService;
import org.afrinnov.sheet.services.product.ProductService;
import org.afrinnov.sheet.spi.ProductSheetApiService;
import org.afrinnov.sheet.spi.model.Color;
import org.afrinnov.sheet.spi.model.ProductBasic;
import org.afrinnov.sheet.spi.model.ProductShare;
import org.afrinnov.sheet.spi.model.Size;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class SimpleProductSheetApiService implements ProductSheetApiService {
    private final ProductService productService;
    private final ColorService colorService;
    private final ProductSizeService productSizeService;

    @Override
    public void checkProduct(String sku) {
        Optional<ProductEntity> product = productService.getEntity(new ProductSku(sku));
        if (product.isEmpty()) {
            throw new IllegalArgumentException("PRODUCT_NOT_FOUND_WITH_SKU:" + sku);
        }
    }

    @Override
    public Color getColor(String key) {
        return colorService.getColor(key);
    }

    @Override
    public Size getSize(String sizeRef) {
        return productSizeService.getSize(sizeRef);
    }

    @Override
    public Optional<ProductShareId> getProductIdentifier(String sku) {
        return productService.getEntity(ProductSku.of(sku))
                .map(entity -> new ProductShareId(entity.getSid(), entity.getSku()));
    }

    @Override
    public Optional<ProductShare> getProduct(String sku) {
        return productService.getEntity(ProductSku.of(sku))
                .map(entity -> ProductShareDto.builder()
                        .sku(sku)
                        .title(entity.getTitle())
                        .description(entity.getDescription())
                        .previewImageUrl(makePreviewImageUrl(entity))
                        .build());

    }

    private static String makePreviewImageUrl(ProductEntity entity) {
        return entity.getPreviewImages().stream().map(file -> new FileContent(file.getSid(), file.getImageContentType()))
                .map(FileContent::makeFileName).findFirst().orElse(null);
    }

    @Override
    public Optional<ProductBasic> getProductInfo(String sku) {
        ProductSheet sheet = productService.getProduct(new ProductSku(sku));

        return Optional.of(mapProductToPricingProduct(sheet));
    }

    private ProductBasicDto mapProductToPricingProduct(ProductSheet product) {
        return ProductBasicDto.builder()
                .title(product.getTitle())
                .description(product.getDescription())
                .previewImageUrl(makePreviewUri(product.getPreviewImageId()))
                .build();
    }

    private String makePreviewUri(FileContent previewImageId) {
        if (Objects.isNull(previewImageId)) {
            return null;
        }
        return makeUri(previewImageId);
    }

    private static String makeUri(FileContent fileContent) {
        return MvcUriComponentsBuilder.fromMethodName(FilesController.class,
                "openServeFile", fileContent.makeFileName()).build().toUri().toString();
    }
}
