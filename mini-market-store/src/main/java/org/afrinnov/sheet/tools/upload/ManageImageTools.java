package org.afrinnov.sheet.tools.upload;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.afrinnov.sheet.TechnicalException;
import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.net.URL;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ManageImageTools {

    public static final int DEGREE = 0;
    public static final float TRANSPARENCY = 0.5f;
    public static final int S_FONT_SIZE = 20;

    public static byte[] addWaterMark(URL image, String text) {
        int sWidth = 60;
        int sHeight = 90;
        String sfontbold = "0";
        return markByText(text, image, sWidth, sHeight, sfontbold);
    }

    private static byte[] markByText(String logoText, URL url,
                                     Integer sWidth, Integer sHeight, String sfontbold) {

        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            Image srcImg = ImageIO.read(url);

            BufferedImage buffImg = new BufferedImage(srcImg.getWidth(null),
                    srcImg.getHeight(null), BufferedImage.TYPE_INT_RGB);

            // get the brush object
            Graphics2D g = buffImg.createGraphics();

            // Set the jagged edge processing of the line segment
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

            g.drawImage(srcImg.getScaledInstance(srcImg.getWidth(null), srcImg
                    .getHeight(null), Image.SCALE_SMOOTH), 0, 0, null);

            g.rotate(Math.toRadians(DEGREE),
                    (double) buffImg.getWidth() / 2, (double) buffImg
                            .getHeight() / 2);
            // set the label
            g.setColor(Color.blue);
            // Set Font (font, font style, font size) such as font (" ", Font.BOLD, 20)
            if (sfontbold != null && sfontbold.equals("0")) {
                g.setFont(new Font(" ", Font.BOLD, S_FONT_SIZE));
            } else {
                g.setFont(new Font(" ", Font.ITALIC, S_FONT_SIZE));
            }
            // Set the transparency 1f opaque, 0.5f translucent, 0f completely transparent
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, TRANSPARENCY));
            ImageIcon imgIcon = new ImageIcon(url);
            Image theImg = imgIcon.getImage();
            int width = theImg.getWidth(null);
            int height = theImg.getHeight(null);
            // The first parameter -> the content of the setting, the last two parameters -> the coordinate position of the text on the image (x, y).
            g.drawString(logoText, width * ((float) sWidth / 100f), height * (float) sHeight / 100f);

            g.dispose();
            // generate image
            ImageIO.write(buffImg, "JPG", os);
            return os.toByteArray();
        } catch (Exception e) {
            throw new TechnicalException(e);
        }
    }
}
