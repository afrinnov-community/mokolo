package org.afrinnov.sheet.tools.upload;

import java.util.Optional;

public interface ContentTypeUtils {

    static Optional<String> fileExtension(String contentType) {
        return Optional.ofNullable(contentType)
                .map(type -> {
                    if (type.startsWith("image")) {
                        return type.split("/")[1];
                    }
                    return null;
                });
    }

     static String removeFileExtension(String filename, boolean removeAllExtensions) {
        if (filename == null || filename.isEmpty()) {
            return filename;
        }

        String extPattern = "(?<!^)[.]" + (removeAllExtensions ? ".*" : "[^.]*$");
        return filename.replaceAll(extPattern, "");
    }
}
