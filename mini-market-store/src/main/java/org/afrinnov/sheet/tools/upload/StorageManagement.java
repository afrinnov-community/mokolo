package org.afrinnov.sheet.tools.upload;

import lombok.RequiredArgsConstructor;
import org.afrinnov.config.InitFolderDto;
import org.afrinnov.sheet.services.upload.StorageService;
import org.springframework.modulith.events.ApplicationModuleListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class StorageManagement {
    private final StorageService storageService;

    @ApplicationModuleListener
    void on(InitFolderDto initFolder) {
        storageService.init();
    }
}
