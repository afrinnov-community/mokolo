package org.afrinnov.sheet.services.product;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.ItemNotFoundException;
import org.afrinnov.sheet.dto.*;
import org.afrinnov.sheet.entities.*;
import org.afrinnov.sheet.id.FileId;
import org.afrinnov.sheet.id.ProductSku;
import org.afrinnov.sheet.repositories.*;
import org.afrinnov.sheet.rest.input.ProductSheetDelete;
import org.afrinnov.sheet.rest.input.ProductSheetRequest;
import org.afrinnov.sheet.rest.input.ProductSheetUpdate;
import org.afrinnov.sheet.services.ProductSizeService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ShippingModeRepository shippingModeRepository;
    private final FileRepository fileRepository;
    private final BrandRepository brandRepository;
    private final CategoryRepository categoryRepository;
    private final ProductSizeService productSizeService;
    private final DeliveryModeRepository deliveryModeRepository;
    private final PaymentModeRepository paymentModeRepository;


    public ProductSku create(ProductSheetRequest request) {
        ProductEntity entity = ProductEntity.newEntity(request);
        if (Objects.nonNull(request.getShippingModes())) {
            Set<ShippingModeEntity> shippingModes = request.getShippingModes().stream()
                    .map(mode -> shippingModeRepository.findByModeIgnoreCase(mode).orElseThrow())
                    .collect(Collectors.toSet());
            entity.setShippingModes(shippingModes);
        }
        if (Objects.nonNull(request.getDeliveryModes())) {
            Set<DeliveryModeEntity> shippingModes = request.getDeliveryModes().stream()
                    .map(mode -> deliveryModeRepository.findByModeIgnoreCase(mode).orElseThrow())
                    .collect(Collectors.toSet());
            entity.setDeliveryModes(shippingModes);
        }
        if (Objects.nonNull(request.getDeliveryModes())) {
            Set<DeliveryModeEntity> shippingModes = request.getDeliveryModes().stream()
                    .map(mode -> deliveryModeRepository.findByModeIgnoreCase(mode).orElseThrow())
                    .collect(Collectors.toSet());
            entity.setDeliveryModes(shippingModes);
        }
        String sku = generateSku();
        entity.setSku(sku);
        productRepository.save(entity);
        return new ProductSku(sku);
    }

    private String generateSku() {
        Year year = Year.now();
        return year.format(DateTimeFormatter.ofPattern("yy")) + RandomStringUtils.randomNumeric(10);
    }

    @Transactional(readOnly = true)
    public ProductSheet getProduct(ProductSku productSku) {
        return productRepository.findBySku(productSku.sku())
                .map(this::mapEntityToDto)
                .orElseThrow(() -> new ItemNotFoundException("Product", productSku.sku()));
    }

    private ProductSheet mapEntityToDto(ProductEntity product) {
        return ProductSheet.builder()
                .description(product.getDescription())
                .title(product.getTitle())
                .sku(product.getSku())
                .sizes(productSizeService.getSizes(new ProductSku(product.getSku())))
                .category(Category.map(product.getCategory()))
                .brand(mapBrandEntityToDto(product.getBrand()))
                .supportedShippingModes(product.getShippingModes().stream().map(this::mapShippingToDto).toList())
                .previewImageId(product.getPreviewImages().stream().findFirst().map(this::mapFileToDto).orElse(null))
                .wizardImageIds(product.getWizardImages().stream().map(this::mapFileToDto).toList())
                .deliveryModes(product.getDeliveryModes().stream().map(this::mapDeliveryToDto).toList())
                .paymentModes(product.getPaymentModes().stream().map(ProductService::mapPayementToDto).toList())
                .build();
    }

    private static PaymentMode mapPayementToDto(PaymentModeEntity entity) {
        return new PaymentMode(entity.getMode(), entity.getDescription(), entity.isEnable());
    }

    private DeliveryMode mapDeliveryToDto(DeliveryModeEntity entity) {
        return new DeliveryMode(entity.getMode(), entity.getDescription());
    }

    private Brand mapBrandEntityToDto(BrandEntity brand) {
        return ofNullable(brand)
                .map(entity -> Brand.builder().code(brand.getCode()).label(brand.getLabel()).build())
                .orElse(null);
    }

    private FileContent mapFileToDto(FileEntity file) {
        return new FileContent(file.getSid(), file.getImageContentType());
    }

    private ShippingMode mapShippingToDto(ShippingModeEntity mode) {
        return new ShippingMode(mode.getMode(), mode.getDescription());
    }

    public void attachPreviewImage(ProductSku productSku, FileId fileId) {
        var product = productRepository.findBySku(productSku.sku())
                .orElseThrow(() -> new ItemNotFoundException("PRODUCT", productSku.sku()));
        var fileEntity = fileRepository.getReferenceById(fileId.id());
        product.getPreviewImages().add(fileEntity);
    }

    public void attachWizardImage(ProductSku productSku, FileId fileId) {
        ProductEntity product = productRepository.findBySku(productSku.sku())
                .orElseThrow(() -> new ItemNotFoundException("PRODUCT", productSku.sku()));
        FileEntity fileEntity = fileRepository.getReferenceById(fileId.id());
        product.getWizardImages().add(fileEntity);
    }

    public Optional<ProductEntity> getEntity(ProductSku productSku) {
        return productRepository.findBySku(productSku.sku());
    }

    public void update(ProductSku productSku, ProductSheetUpdate request) {
        var product = productRepository.findBySku(productSku.sku())
                .orElseThrow(() -> new ItemNotFoundException("PRODUCT", productSku.sku()));
        ofNullable(request.getBrandCode())
                .flatMap(brandRepository::findByCode).ifPresent(product::setBrand);
        ofNullable(request.getCategoryCode())
                .flatMap(categoryRepository::findByCode).ifPresent(product::setCategory);
        if (Objects.nonNull(request.getShippingModes())) {
            Set<ShippingModeEntity> shippingModes = request.getShippingModes().stream()
                    .map(mode -> shippingModeRepository.findByModeIgnoreCase(mode).orElseThrow())
                    .collect(Collectors.toSet());
            product.getShippingModes().addAll(shippingModes);
        }

        if (Objects.nonNull(request.getDeliveryModes())) {
            Set<DeliveryModeEntity> deliveryMode = request.getDeliveryModes().stream()
                    .map(mode -> deliveryModeRepository.findByModeIgnoreCase(mode).orElseThrow())
                    .collect(Collectors.toSet());
            product.getDeliveryModes().addAll(deliveryMode);
        }


        if (Objects.nonNull(request.getPaymentModes())) {
            Set<PaymentModeEntity> payementModes = request.getPaymentModes().stream()
                    .map(mode -> paymentModeRepository.findByModeIgnoreCase(mode).orElseThrow())
                    .collect(Collectors.toSet());
            product.getPaymentModes().addAll(payementModes);
        }

    }

    public void delete(ProductSku productSku, ProductSheetDelete request) {
        var product = productRepository.findBySku(productSku.sku())
                .orElseThrow(() -> new ItemNotFoundException("PRODUCT", productSku.sku()));

        if (Objects.nonNull(request.getShippingModes())) {
            Set<ShippingModeEntity> shippingModes = request.getShippingModes().stream()
                    .map(mode -> shippingModeRepository.findByModeIgnoreCase(mode).orElseThrow())
                    .collect(Collectors.toSet());
            product.getShippingModes().removeAll(shippingModes);
        }

        if (Objects.nonNull(request.getDeliveryModes())) {
            Set<DeliveryModeEntity> deliveryMode = request.getDeliveryModes().stream()
                    .map(mode -> deliveryModeRepository.findByModeIgnoreCase(mode).orElseThrow())
                    .collect(Collectors.toSet());
            product.getDeliveryModes().removeAll(deliveryMode);
        }


        if (Objects.nonNull(request.getPaymentModes())) {
            Set<PaymentModeEntity> payementModes = request.getPaymentModes().stream()
                    .map(mode -> paymentModeRepository.findByModeIgnoreCase(mode).orElseThrow())
                    .collect(Collectors.toSet());
            product.getPaymentModes().removeAll(payementModes);
        }
    }
}
