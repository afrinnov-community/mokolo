package org.afrinnov.sheet.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.ItemNotFoundException;
import org.afrinnov.sheet.dto.Brand;
import org.afrinnov.sheet.dto.BrandImageLogo;
import org.afrinnov.sheet.entities.BrandEntity;
import org.afrinnov.sheet.id.BrandCode;
import org.afrinnov.sheet.repositories.BrandRepository;
import org.afrinnov.sheet.rest.support.input.BrandRequest;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class BrandService {

    private final BrandRepository brandRepository;


    public Brand create(BrandRequest brandRequest) {


        BrandCode brandCode = new BrandCode(generateSku());
        var entity = brandRepository.save(BrandEntity.newEntity(brandRequest, brandCode));

        return mapEntityToDto(entity);
    }

    public Brand addBrandImageLogo(BrandCode brandCode, BrandImageLogo brandImageLogo) {
        BrandEntity brand = brandRepository.findByCode(brandCode.codeBrand())
                .orElseThrow(() -> new ItemNotFoundException("BRAND", brandCode.codeBrand()));

        brand.setImageLogoUrl(brandImageLogo.urlImageLogo());
        brandRepository.save(brand);

        return mapEntityToDto(brand);

    }

    private Brand mapEntityToDto(BrandEntity brand) {
        return Brand.builder()
                .code(brand.getCode())
                .label(brand.getLabel())
                .imageLogoUrl(brand.getImageLogoUrl())
                .build();
    }

    private String generateSku() {
        Year year = Year.now();
        return year.format(DateTimeFormatter.ofPattern("yy")) + RandomStringUtils.randomNumeric(5);
    }

    public List<Brand> getBrands() {
        return brandRepository.findAll().stream()
                .map(this::mapEntityToDto).toList();
    }
}
