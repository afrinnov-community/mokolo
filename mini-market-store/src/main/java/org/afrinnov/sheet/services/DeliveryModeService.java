package org.afrinnov.sheet.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.ItemNotFoundException;
import org.afrinnov.sheet.dto.DeliveryMode;
import org.afrinnov.sheet.id.DeliveryModeId;
import org.afrinnov.sheet.rest.support.input.DeliveryModeRequest;
import org.afrinnov.sheet.entities.DeliveryModeEntity;
import org.afrinnov.sheet.repositories.DeliveryModeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/12/2023 -- 18:03<br></br>
 * By : @author alexk<br></br>
 * Project : mokolo<br></br>
 * Package : org.afrinnov.common.delivry.seervice<br></br>
 */
@Service
@RequiredArgsConstructor
public class DeliveryModeService {
    private final DeliveryModeRepository repository;

    public DeliveryModeId create(DeliveryModeRequest request) {
        DeliveryModeEntity entity = repository.save(DeliveryModeEntity.newEntity(request));
        return new DeliveryModeId(entity.getMode());
    }

    public DeliveryMode getDeliveryMode(String mode) {
        return repository.findByModeIgnoreCase(mode)
                .map(DeliveryModeEntity::deliveryMode)
                .orElseThrow(() -> new ItemNotFoundException("DeliveryMode", mode));
    }

    public List<DeliveryMode> getAllDeliveryMode() {
        return repository.findAll()
                .stream()
                .map(DeliveryModeEntity::deliveryMode)
                .toList();
    }
}
