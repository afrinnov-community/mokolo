package org.afrinnov.sheet.services.upload.adapter;

import org.afrinnov.sheet.FileUploadException;
import org.afrinnov.sheet.DownloadException;
import org.afrinnov.sheet.services.upload.StorageService;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

public abstract class AbstractLocalStorageService implements StorageService {
    protected final Path rootLocation;

    protected AbstractLocalStorageService(Path rootLocation) {
        this.rootLocation = rootLocation;
    }

    @Override
    public List<Path> loadAll() {
        try (Stream<Path> walk = Files.walk(this.rootLocation, 1)) {
            List<Path> paths = walk.toList();
            return paths.stream()
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(this.rootLocation::relativize).toList();
        } catch (IOException e) {
            throw new DownloadException("Failed to read stored files", e);
        }
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new DownloadException("Could not read file: " + filename);

            }
        } catch (MalformedURLException e) {
            throw new DownloadException("Could not read file: " + filename, e);
        }
    }
    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        } catch (IOException e) {
            throw new FileUploadException("Could not initialize storage", e);
        }
    }
}
