package org.afrinnov.sheet.services;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.ItemNotFoundException;
import org.afrinnov.sheet.entities.TelephoneCodeEntity;
import org.afrinnov.sheet.id.TelephoneCode;
import org.afrinnov.sheet.id.TelephoneCodeId;
import org.afrinnov.sheet.repositories.TelephoneCodeRepository;
import org.afrinnov.sheet.rest.support.input.TelephoneCodeRequest;
import org.afrinnov.sheet.rest.input.TelephoneCodeValidRequest;
import org.afrinnov.sheet.rest.output.TelephoneCodeValidatedResponse;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
@Transactional
@RequiredArgsConstructor
public class TelephoneCodeService {
    private final TelephoneCodeRepository repository;

    public TelephoneCodeId create(TelephoneCodeRequest telephoneCode) {
        TelephoneCodeEntity entity = TelephoneCodeEntity.newEntity(telephoneCode);
        repository.save(entity);
        return new TelephoneCodeId(entity.getSid().toString());
    }

    public void delete(TelephoneCodeId codeId) {
        repository.findOneByCode(codeId.id()).ifPresent(entity -> entity.setEnable(false));

    }

    public TelephoneCode getTelephoneCode(TelephoneCodeId codeId) {
        return repository.findOneByCode(codeId.id()).map(TelephoneCodeService::mapEntityToDto)
                .orElseThrow(() -> new ItemNotFoundException("TelephoneCode", codeId.id()));
    }

    private static TelephoneCode mapEntityToDto(TelephoneCodeEntity entity) {
        return TelephoneCode.builder()
                .code(entity.getCode())
                .country(entity.getIsoCountryCode())
                .enable(entity.isEnable())
                .format(entity.getFormat())
                .build();
    }

    public TelephoneCodeValidatedResponse validate(TelephoneCodeId codeId, TelephoneCodeValidRequest validRequest) {
        TelephoneCode telephoneCode = repository.findOneByCode(codeId.id()).map(TelephoneCodeService::mapEntityToDto)
                .orElseThrow(() -> new ItemNotFoundException("TelephoneCode", codeId.id()));
        Pattern pattern=Pattern.compile(telephoneCode.getFormat());
        boolean matches = pattern.matcher(validRequest.phoneNumber()).matches();
        return TelephoneCodeValidatedResponse.builder()
                .format(telephoneCode.getFormat())
                .matches(matches)
                .phoneNumber(validRequest.phoneNumber())
                .build();
    }
}
