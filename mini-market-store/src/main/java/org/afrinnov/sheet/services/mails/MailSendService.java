package org.afrinnov.sheet.services.mails;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

public interface MailSendService {
    void ping(String to, String subject, String body);

    @Component
    @ConditionalOnProperty(havingValue = "NOOP", name = "afrinnov.notification.provider")
    @Slf4j
    class NoopMailSendService implements MailSendService {
        @Override
        public void ping(String to, String subject, String body) {
            log.info("Mail send with NooP provider");
            log.info("to:{} Subject: {} Body:{}", to, subject, body);
        }
    }
}
