package org.afrinnov.sheet.services.upload.adapter;

import lombok.extern.slf4j.Slf4j;
import org.afrinnov.sheet.DownloadException;
import org.afrinnov.sheet.FileUploadException;
import org.afrinnov.sheet.tools.upload.ContentTypeUtils;
import org.afrinnov.sheet.tools.upload.ManageImageTools;
import org.afrinnov.sheet.dto.FileDto;
import org.afrinnov.sheet.id.FileId;
import org.afrinnov.sheet.rest.input.FileRequest;
import org.afrinnov.sheet.services.FileService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.UUID;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Service
@ConditionalOnProperty(name = "afrinnov.upload.location", havingValue = "FROM_DATABASE")
@Slf4j
public class DatabaseStorageService extends AbstractLocalStorageService {
    private final FileService fileService;

    public DatabaseStorageService(FileService fileService,
                                  @Value("${afrinnov.upload.database-storage-folder}") String folder) {
        super(Paths.get(folder));
        this.fileService = fileService;
    }

    @Override
    public FileId save(MultipartFile file) {
        byte[] content;
        FileId fileId;
        try {
            content = file.getBytes();
            String contentType = file.getContentType();
            long size = file.getSize();
            String originalFilename = file.getOriginalFilename();
            FileRequest fileRequest = FileRequest.builder()
                    .content(content)
                    .originalFilename(originalFilename)
                    .contentType(contentType)
                    .size(size)
                    .build();
            fileId = fileService.create(fileRequest);
            log.debug("file saved code {}", fileId.id());
        } catch (IOException ex) {
            throw new FileUploadException(ex);
        }
        init();
        saveFile(file, content, fileId);
        return fileId;
    }

    @Override
    public FileId saveWithMark(MultipartFile file) {
        byte[] content;
        FileId fileId;
        try {
            Path tempFile = Files.createTempFile("mokolo", "marked-image");
            Files.copy(file.getInputStream(), tempFile, REPLACE_EXISTING);
            content = ManageImageTools.addWaterMark(tempFile.toFile().toURI().toURL(), "Mark by afrinnov");
            String contentType = file.getContentType();
            long size = file.getSize();
            String originalFilename = file.getOriginalFilename();
            FileRequest fileRequest = FileRequest.builder()
                    .content(content)
                    .originalFilename(originalFilename)
                    .contentType(contentType)
                    .size(size)
                    .marked(true)
                    .build();
            fileId = fileService.create(fileRequest);
            log.debug("file saved code {}", fileId.id());
        } catch (Exception ex) {
            throw new FileUploadException(ex);
        }

        init();

        saveFile(file, content, fileId);
        return fileId;
    }

    @Override
    public Path load(String filename) {
        Path resolve = rootLocation.resolve(filename);
        if (Files.exists(resolve)) {
            return resolve;
        }
        String fileId = ContentTypeUtils.removeFileExtension(filename, true);
        FileId id = new FileId(UUID.fromString(fileId));
        FileDto fileDto = fileService.getFile(id);
        String fileName = makeFileName(fileDto.getImageContentType(), id);
        try (InputStream is = new ByteArrayInputStream(fileDto.getImage())) {
            storeFile(is, fileName);
        } catch (IOException ex) {
            throw new DownloadException("Could not read file: " + filename, ex);
        }
        return resolve;
    }

    private void storeFile(InputStream inputStream, String filename) throws IOException {
        Path destinationFile = this.rootLocation.resolve(
                        Paths.get(Objects.requireNonNull(filename)))
                .normalize().toAbsolutePath();
        Files.copy(inputStream, destinationFile, REPLACE_EXISTING);
    }

    private static String makeFileName(String contentType, FileId fileId) {
        return ContentTypeUtils.fileExtension(contentType)
                .map(ext -> fileId.id().toString() + "." + ext).orElse(fileId.id().toString());
    }

    private void saveFile(MultipartFile file, byte[] content, FileId fileId) {
        String filename = makeFileName(file.getContentType(), fileId);
        try (InputStream inputStream = new ByteArrayInputStream(content)) {
            storeFile(inputStream, filename);
        } catch (IOException e) {
            throw new FileUploadException(e);
        }
    }
}
