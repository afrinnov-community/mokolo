package org.afrinnov.sheet.services;

import com.google.common.base.Strings;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.entities.CategoryTypeEntity;
import org.afrinnov.sheet.repositories.CategoryTypeRepository;
import org.afrinnov.sheet.rest.support.input.CategoryTypeInput;
import org.afrinnov.sheet.rest.support.output.CategoryType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Year;
import java.util.List;
import java.util.Optional;

import static java.time.format.DateTimeFormatter.ofPattern;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Service
@RequiredArgsConstructor
public class CategoryTypeService {

    private final CategoryTypeRepository categoryTypeRepository;


    @Transactional(readOnly = true)
    public List<CategoryType> getCategoryTypes() {
        return categoryTypeRepository.findAll().stream().map(CategoryType::make).toList();
    }

    public CategoryType createOrUpdate(CategoryTypeInput input) {
        if (Strings.isNullOrEmpty(input.code())) {
            CategoryTypeEntity entity = new CategoryTypeEntity();
            entity.setCode(generateCode());
            entity.setName(input.name());
            entity.setStatus(CategoryTypeEntity.CategoryTypeStatus.ACTIVE);
            return CategoryType.make(categoryTypeRepository.save(entity));
        }
        CategoryTypeEntity categoryType = categoryTypeRepository.findByCode(input.code()).orElseThrow();
        categoryType.setName(input.name());
        Optional.ofNullable(input.status()).map(Enum::name).ifPresent(categoryType::defineStatus);
        categoryTypeRepository.save(categoryType);
        return CategoryType.make(categoryType);
    }

    public static String generateCode() {
        return Year.now().format(ofPattern("yy")) + randomNumeric(3);
    }
}
