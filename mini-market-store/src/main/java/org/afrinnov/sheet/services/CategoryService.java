package org.afrinnov.sheet.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.ItemNotFoundException;
import org.afrinnov.sheet.dto.CategorySizes;
import org.afrinnov.sheet.dto.CategoryType;
import org.afrinnov.sheet.entities.CategoryTypeEntity;
import org.afrinnov.sheet.id.CategoryTypeCode;
import org.afrinnov.sheet.entities.CategoryEntity;
import org.afrinnov.sheet.entities.SizeEntity;
import org.afrinnov.sheet.id.CategoryCode;
import org.afrinnov.sheet.id.SizeCode;
import org.afrinnov.sheet.repositories.CategoryRepository;
import org.afrinnov.sheet.repositories.CategoryTypeRepository;
import org.afrinnov.sheet.repositories.SizeRepository;
import org.afrinnov.sheet.rest.support.input.CategoryRequest;
import org.afrinnov.sheet.rest.support.input.SizeRequest;
import org.afrinnov.sheet.services.mapper.ProductCategoryMapper;
import org.afrinnov.sheet.services.mapper.ProductSizeMapper;
import org.afrinnov.sheet.utils.ProductUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;
    private final SizeRepository sizeRepository;
    private final CategoryTypeRepository categoryTypeRepository;

    @Transactional
    public CategorySizes createCategory(CategoryRequest categoryRequest) {
        CategoryEntity category = new CategoryEntity();
        category.setDescription(categoryRequest.description());
        category.setLabel(categoryRequest.label());
        category.setCode(ProductUtil.generateCode());
        if (categoryRequest.isSent()) {
            categoryTypeRepository.findByCode(categoryRequest.categoryTypeCode())
                    .ifPresentOrElse(category::setCategoryType,
                            () -> category.setCategoryType(categoryTypeRepository.findDefaultType()));
        } else {
            category.setCategoryType(categoryTypeRepository.findDefaultType());
        }
        categoryRepository.save(category);

        return new CategorySizes(category.getCode(), category.getLabel(), category.getDescription(), List.of(),
                mapCategoryType(category.getCategoryType()));
    }

    @Transactional
    public CategorySizes updateCategory(String code, CategorySizes categorySizes) {
        CategoryEntity category = categoryRepository.findByCode(code)
                .orElseThrow(() -> new ItemNotFoundException("ProductCategory", categorySizes.code()));

        category.setLabel(categorySizes.label());
        category.setDescription(categorySizes.description());

        categoryRepository.save(category);
        return new CategorySizes(category.getCode(), category.getLabel(),
                category.getDescription(),
                category.getSizes().stream().map(ProductSizeMapper::mapProductSizeToDto).toList(),
                mapCategoryType(category.getCategoryType()));
    }

    private CategoryType mapCategoryType(CategoryTypeEntity category) {
        if(Objects.isNull(category)) {
            return null;
        }
        return new CategoryType(category.getCode(), category.getName(), category.getStatus().name());
    }

    @Transactional(readOnly = true)
    public List<CategorySizes> getAllCategories() {
        return categoryRepository.findAll().stream().map(ProductCategoryMapper::mapEntityToDto).toList();
    }

    @Transactional(readOnly = true)
    public CategorySizes getCategory(String code) {
        return categoryRepository.findByCode(code)
                .map(ProductCategoryMapper::mapEntityToDto)
                .orElseThrow(() -> new ItemNotFoundException("ProductCategory", code));
    }

    @Transactional
    public void deleteCategory(String code) {
        var category = categoryRepository.findByCode(code);
        category.ifPresent(entity -> categoryRepository.deleteById(entity.getSid()));
    }

    @Transactional
    public CategorySizes addSizeToCategory(String code, SizeRequest size) {
        var category = categoryRepository.findByCode(code)
                .orElseThrow(() -> new ItemNotFoundException("ProductCategory", code));

        SizeEntity productSize = new SizeEntity();
        productSize.setDescription(size.description());
        productSize.setLabel(size.description());
        productSize.setCategory(category);
        productSize.setCode(ProductUtil.generateCode());

        sizeRepository.save(productSize);
        return ProductCategoryMapper.mapEntityToDto(category);
    }

    @Transactional
    public CategorySizes addSizesToCategory(CategoryCode categoryCode, List<SizeCode> sizeCodes) {
        var category = categoryRepository.findByCode(categoryCode.code())
                .orElseThrow(() -> new ItemNotFoundException("ProductCategory", categoryCode.code()));
        sizeCodes.forEach(sizeCode -> sizeRepository.findByCode(sizeCode.code())
                .ifPresent(size -> {
                    size.setCategory(category);
                    category.getSizes().add(size);
                }));
        categoryRepository.save(category);
        return ProductCategoryMapper.mapEntityToDto(category);
    }

    @Transactional
    public void updateCategoryType(CategoryCode categoryCode, CategoryTypeCode categoryTypeCode) {
        var category = categoryRepository.findByCode(categoryCode.code())
                .orElseThrow(() -> new ItemNotFoundException("ProductCategory", categoryCode.code()));
        CategoryTypeEntity categoryType = categoryTypeRepository.findByCode(categoryTypeCode.code())
                .orElseThrow(() -> new ItemNotFoundException("CategoryType", categoryTypeCode.code()));
        category.setCategoryType(categoryType);
    }
}
