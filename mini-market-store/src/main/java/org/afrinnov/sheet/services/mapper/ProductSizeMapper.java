package org.afrinnov.sheet.services.mapper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.afrinnov.sheet.dto.SizeDto;
import org.afrinnov.sheet.entities.CategoryEntity;
import org.afrinnov.sheet.entities.SizeEntity;

import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductSizeMapper {
    public static SizeDto mapProductSizeToDto(SizeEntity sizeEntity) {
        String categoryCode = Optional.ofNullable(sizeEntity.getCategory())
                .map(CategoryEntity::getCode).orElse(null);
        return new SizeDto(sizeEntity.getCode(), sizeEntity.getLabel(),
                sizeEntity.getDescription(), categoryCode);
    }
}
