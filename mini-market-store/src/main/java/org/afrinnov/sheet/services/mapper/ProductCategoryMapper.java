package org.afrinnov.sheet.services.mapper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.afrinnov.sheet.dto.CategorySizes;
import org.afrinnov.sheet.dto.CategoryType;
import org.afrinnov.sheet.entities.CategoryEntity;
import org.afrinnov.sheet.entities.CategoryTypeEntity;

import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductCategoryMapper {
    public static CategorySizes mapEntityToDto(CategoryEntity entity) {
        return new CategorySizes(entity.getCode(), entity.getLabel(), entity.getDescription(),
                entity.getSizes().stream().map(ProductSizeMapper::mapProductSizeToDto).toList(),
                mapCategoryType(entity.getCategoryType()));
    }

    private static CategoryType mapCategoryType(CategoryTypeEntity categoryType) {
        if (Objects.isNull(categoryType)) {
            return null;
        }
        return new CategoryType(categoryType.getCode(), categoryType.getName(), categoryType.getStatus().name());
    }
}
