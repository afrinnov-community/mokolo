package org.afrinnov.sheet.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.FileDto;
import org.afrinnov.sheet.id.FileId;
import org.afrinnov.sheet.rest.input.FileRequest;
import org.afrinnov.sheet.entities.FileEntity;
import org.afrinnov.sheet.repositories.FileRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class FileService {
    private final FileRepository fileRepository;

    public FileId create(FileRequest fileRequest) {
        FileEntity entity= FileEntity.newEntity(fileRequest);
        fileRepository.save(entity);
        return new FileId(entity.getSid());
    }

    @Transactional(readOnly = true)
    public FileDto getFile(FileId id) {
        return mapEntityToDto(fileRepository.getReferenceById(id.id()));
    }

    private FileDto mapEntityToDto(FileEntity entity) {
        return FileDto.builder()
                .image(entity.getImage())
                .imageContentType(entity.getImageContentType())
                .imageMarked(entity.isImageMarked())
                .originalFilename(entity.getOriginalFilename())
                .size(entity.getSize())
                .build();
    }
}
