package org.afrinnov.sheet.services.notify.processor.adapter;

import lombok.RequiredArgsConstructor;
import org.afrinnov.security.MailTemplate;
import org.afrinnov.security.Messager;
import org.afrinnov.sheet.services.mails.MailSendService;
import org.afrinnov.sheet.services.notify.processor.MessageProcessor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GenericMessageProcessor implements MessageProcessor {
    private final MailSendService mailSendService;


    @Override
    public void send(Messager messager) {
        mailSendService.ping(messager.to(), messager.subject(),
                String.format("GENERIC: %s", messager.data().getOrDefault("otp", "-1-1-1-1")));
    }

    @Override
    public boolean isSupported(MailTemplate mailTemplate) {
        return switch (mailTemplate) {
            case CREATE_NEW_SHOP, ACCOUNT_ACTIVATED, CREATE_NEW_CUSTOMER, CREATE_NEW_EXHIBIT_SHOP,
                 ACTIVATE_NEW_SHOP_ADMIN_USER -> true;
            default -> false;
        };
    }
}
