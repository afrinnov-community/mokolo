package org.afrinnov.sheet.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.entities.ProductSizeEntity;
import org.afrinnov.sheet.id.ProductSku;
import org.afrinnov.sheet.repositories.ProductRepository;
import org.afrinnov.sheet.repositories.ProductSizeRepository;
import org.afrinnov.sheet.repositories.SizeRepository;
import org.afrinnov.sheet.rest.input.ProductSizeRequest;
import org.afrinnov.sheet.rest.input.SizeCodeRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductSizeSheetService {
    private final ProductSizeRepository productSizeRepository;
    private final ProductRepository productRepository;
    private final SizeRepository sizeRepository;

    public void addSizes(ProductSku productSku, List<ProductSizeRequest> requests) {
        var product = productRepository.findBySku(productSku.sku())
                .orElseThrow(() -> new IllegalArgumentException("PRODUCT_NOT_FOUND:[" + productSku.sku() + "]"));
        var entities = requests.stream()
                .map(dto -> sizeRepository.findByCode(dto.sizeCode())
                        .map(size -> ProductSizeEntity.newEntity(product, size, dto)))
                .filter(Optional::isPresent).map(Optional::get).toList();
        productSizeRepository.saveAll(entities);
    }

    public void removeSizes(ProductSku productSku, List<SizeCodeRequest> requests) {
        var product = productRepository.findBySku(productSku.sku())
                .orElseThrow(() -> new IllegalArgumentException("PRODUCT_NOT_FOUND:[" + productSku.sku() + "]"));
        requests.forEach(sizeCode -> {
            Optional<ProductSizeEntity> productSize = productSizeRepository.findByProductAndSize_Code(product, sizeCode.code());
            productSize.ifPresent(productSizeRepository::delete);
        });
    }
}
