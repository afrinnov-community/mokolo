package org.afrinnov.sheet.services.search;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class IndexException extends RuntimeException {

    public IndexException(String message, Throwable cause) {
        super(message, cause);
    }

}
