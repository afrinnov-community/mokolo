package org.afrinnov.sheet.services.upload;

import org.afrinnov.sheet.id.FileId;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;

public interface StorageService {
    FileId save(MultipartFile file);

    List<Path> loadAll();

    Resource loadAsResource(String filename);

    Path load(String filename);

    void init();

    FileId saveWithMark(MultipartFile file);
}
