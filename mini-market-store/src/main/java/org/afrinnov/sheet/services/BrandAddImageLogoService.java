package org.afrinnov.sheet.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.Brand;
import org.afrinnov.sheet.dto.BrandImageLogo;
import org.afrinnov.sheet.id.BrandCode;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class BrandAddImageLogoService {

    private final BrandService brandService;

    public Brand addImageLogo(BrandCode brandCode, MultipartFile file) {
        BrandImageLogo logo = new BrandImageLogo(file.getOriginalFilename());
        return  brandService.addBrandImageLogo(brandCode, logo);
    }
}
