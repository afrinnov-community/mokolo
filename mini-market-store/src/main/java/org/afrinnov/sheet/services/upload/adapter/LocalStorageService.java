package org.afrinnov.sheet.services.upload.adapter;

import lombok.extern.slf4j.Slf4j;
import org.afrinnov.sheet.FileUploadException;
import org.afrinnov.sheet.tools.upload.ManageImageTools;
import org.afrinnov.sheet.id.FileId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static org.afrinnov.sheet.tools.upload.ContentTypeUtils.fileExtension;

@Component
@ConditionalOnProperty(name = "afrinnov.upload.location", havingValue = "FROM_DISK")
@Slf4j
public class LocalStorageService extends AbstractLocalStorageService {

    public LocalStorageService(@Value("${afrinnov.upload.local-storage-folder}")String folder) {
        super(Paths.get(folder));
    }

    @Override
    public FileId save(MultipartFile file) {
        log.debug("Upload file: {}", file.getOriginalFilename());
        UUID uuid = UUID.randomUUID();
        String filename = uuid + fileExtension(file.getContentType()).map(ext -> "." + ext).orElse("");
        try (InputStream inputStream = file.getInputStream()) {
            Path destinationFile = this.rootLocation.resolve(
                            Paths.get(filename))
                    .normalize().toAbsolutePath();
            Files.copy(inputStream, destinationFile, REPLACE_EXISTING);
        } catch (IOException e) {
            throw new FileUploadException(e);
        }
        return new FileId(uuid);
    }


    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public FileId saveWithMark(MultipartFile file) {
        log.debug("Upload file with mark: {}", file.getOriginalFilename());
        UUID uuid = UUID.randomUUID();
        String filename = uuid + fileExtension(file.getContentType()).map(ext -> "." + ext).orElse("");
        try {
            Path tempFile = Files.createTempFile("mokolo", "marked-image");
            Files.copy(file.getInputStream(), tempFile, REPLACE_EXISTING);
            byte[] flux = ManageImageTools.addWaterMark(tempFile.toFile().toURI().toURL(), "Mark by afrinnov");
            try (InputStream inputStream = new ByteArrayInputStream(flux)) {
                Path destinationFile = this.rootLocation.resolve(
                                Paths.get(filename))
                        .normalize().toAbsolutePath();
                Files.copy(inputStream, destinationFile, REPLACE_EXISTING);
            }
        } catch (IOException e) {
            throw new FileUploadException(e);
        }
        return new FileId(uuid);
    }
}
