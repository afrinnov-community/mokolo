package org.afrinnov.sheet.services.adapter;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.ItemNotFoundException;
import org.afrinnov.sheet.dto.ProductFeature;
import org.afrinnov.sheet.dto.ProductFeatures;
import org.afrinnov.sheet.entities.FeatureEntity;
import org.afrinnov.sheet.entities.ProductFeatureEntity;
import org.afrinnov.sheet.id.FeatureCode;
import org.afrinnov.sheet.id.ProductSku;
import org.afrinnov.sheet.repositories.FeatureRepository;
import org.afrinnov.sheet.repositories.ProductFeatureRepository;
import org.afrinnov.sheet.repositories.ProductRepository;
import org.afrinnov.sheet.rest.input.ProductFeaturesRequest;
import org.afrinnov.sheet.services.ProductFeatureService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class SimpleProductFeatureService implements ProductFeatureService {
    private final ProductRepository productRepository;
    private final FeatureRepository featureRepository;
    private final ProductFeatureRepository productFeatureRepository;

    public void addFeatures(ProductSku productSku, ProductFeaturesRequest request) {
        var product = productRepository.findBySku(productSku.sku())
                .orElseThrow(() -> new ItemNotFoundException("Product", productSku.sku()));
        var feature = featureRepository.findByCodeIgnoreCase(request.featureCode())
                .orElseThrow(() -> new ItemNotFoundException("Feature", request.featureCode()));
        var entities = request.features().stream()
                .map(item -> ProductFeatureEntity.newEntity(item, product, feature)).toList();
        productFeatureRepository.saveAll(entities);
    }

    @Transactional(readOnly = true)
    public ProductFeatures getProductFeatures(ProductSku productSku) {
        var entities = productFeatureRepository.findDistinctByProduct_Sku(productSku.sku());
        Map<String, FeatureEntity> features = entities.stream().collect(Collectors.toMap(this::extractFeatureCode,
                ProductFeatureEntity::getFeature, (oldItem, newItem) -> newItem));
        var map = entities.stream().collect(Collectors.groupingBy(
                this::extractFeatureCode,
                Collectors.mapping(entity -> new ProductFeature(entity.getClef(), entity.getLabel()), Collectors.toList())));
        var list = features.entrySet().stream().map(entry -> {
            var productFeatures = map.get(entry.getKey());
            var feature = entry.getValue();
            return new ProductFeatures.Feature(feature.getCode(), feature.getLabel(), feature.getDescription(), productFeatures);
        }).toList();
        return new ProductFeatures(list);
    }

    @Override
    public void removeFeature(ProductSku productSku, FeatureCode featureCode) {
        var product = productRepository.findBySku(productSku.sku())
                .orElseThrow(() -> new ItemNotFoundException("Product", productSku.sku()));
        var feature = featureRepository.findByCodeIgnoreCase(featureCode.code())
                .orElseThrow(() -> new ItemNotFoundException("Feature", featureCode.code()));
        productFeatureRepository.deleteByProductAndFeature(product, feature);
    }

    @Override
    public void removeDetailsFeature(ProductSku productSku, FeatureCode featureCode, List<String> clefs) {
        var product = productRepository.findBySku(productSku.sku())
                .orElseThrow(() -> new ItemNotFoundException("Product", productSku.sku()));
        var feature = featureRepository.findByCodeIgnoreCase(featureCode.code())
                .orElseThrow(() -> new ItemNotFoundException("Feature", featureCode.code()));
        clefs.forEach(clef -> productFeatureRepository.deleteByProductAndFeatureAndClefIgnoreCase(product, feature, clef));
    }

    private String extractFeatureCode(ProductFeatureEntity entity) {
        return entity.getFeature().getCode();
    }
}
