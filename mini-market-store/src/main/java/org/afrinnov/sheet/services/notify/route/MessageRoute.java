package org.afrinnov.sheet.services.notify.route;

import lombok.RequiredArgsConstructor;
import org.afrinnov.security.Messager;
import org.afrinnov.sheet.services.notify.processor.MessageProcessor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class MessageRoute {
    private final List<MessageProcessor> messageProcessors;

    public void route(Messager messager) {
        messageProcessors.stream().filter(processor -> processor.isSupported(messager.mailTemplate()))
                .forEach(processor -> processor.send(messager));
    }
}
