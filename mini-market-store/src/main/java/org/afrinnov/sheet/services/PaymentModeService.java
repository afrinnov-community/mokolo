package org.afrinnov.sheet.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.entities.PaymentModeEntity;
import org.afrinnov.sheet.repositories.PaymentModeRepository;
import org.afrinnov.sheet.rest.support.input.PaymentModeInput;
import org.afrinnov.sheet.rest.support.output.PaymentModeResponse;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class PaymentModeService {
    private final PaymentModeRepository paymentModeRepository;

    @Transactional
    public PaymentModeResponse create(PaymentModeInput request) {

        Optional<PaymentModeEntity> paymentMode = paymentModeRepository.findByModeIgnoreCase(request.mode());

        if (paymentMode.isEmpty()) {
            PaymentModeEntity entity = PaymentModeEntity.newEntity(request);
            paymentModeRepository.save(entity);
            return mapEntityToResponse(entity);
        }
        PaymentModeEntity entity = paymentMode.get();
        entity.setDescription(request.description());
        paymentModeRepository.save(entity);
        return mapEntityToResponse(entity);
    }

    public PaymentModeResponse getPaymentMode(String mode) {
        return paymentModeRepository.findByModeIgnoreCase(mode)
                .map(PaymentModeService::mapEntityToResponse)
                .orElseThrow(() -> new IllegalArgumentException("PAYMENT_MODE_NOT_FOUND:" + mode));
    }

    private static PaymentModeResponse mapEntityToResponse(PaymentModeEntity entity) {
        return new PaymentModeResponse(entity.getMode(), entity.getDescription(), entity.isEnable());
    }

    public List<PaymentModeResponse> getPaymentModes() {
        return paymentModeRepository.findAll().stream().map(PaymentModeService::mapEntityToResponse).toList();
    }
}
