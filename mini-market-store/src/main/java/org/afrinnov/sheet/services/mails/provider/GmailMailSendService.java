package org.afrinnov.sheet.services.mails.provider;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.services.mails.MailSendService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@ConditionalOnProperty(havingValue = "GMAIL", name = "afrinnov.notification.provider")
public class GmailMailSendService implements MailSendService {
    private final JavaMailSender mailSender;

    public void ping(String to, String subject, String body) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(body);
        mailSender.send(message);
    }
}
