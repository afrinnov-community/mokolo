package org.afrinnov.sheet.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.afrinnov.security.MessagerSent;
import org.afrinnov.sheet.services.mails.MailSendService;
import org.afrinnov.security.Messager;
import org.afrinnov.sheet.services.notify.route.MessageRoute;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.modulith.events.ApplicationModuleListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
@RequiredArgsConstructor
@Slf4j
public class NotifyService {
    private final MailSendService mailSendService;
    private final MessageRoute messageRoute;


    public boolean sendMail(Messager messager) {
        mailSendService.ping(messager.to(), messager.subject(),
                String.format("OTP: %s", messager.data().getOrDefault("otp", "-1-1-1-1")));
        return true;
    }

    @Async
    @TransactionalEventListener
    void on(Messager messager) {
        log.info("my first listener");
        messageRoute.route(messager);
    }
}
