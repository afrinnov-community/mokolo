package org.afrinnov.sheet.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.ItemNotFoundException;
import org.afrinnov.sheet.dto.ColorDto;
import org.afrinnov.sheet.entities.ColorEntity;
import org.afrinnov.sheet.repositories.ColorRepository;
import org.afrinnov.sheet.rest.support.input.ColorRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.afrinnov.sheet.utils.ProductUtil.generateCode;

@Service
@RequiredArgsConstructor
@Transactional
public class ColorService {
    private final ColorRepository colorRepository;

    private ColorDto mapEntityToDto(ColorEntity entity) {
        return new ColorDto(entity.getCode(), entity.getLabel(), entity.getColorValue());
    }

    public ColorDto createColor(ColorRequest request) {
        if (request.hasCode()) {
            var entity = colorRepository.findByCode(request.code())
                    .orElseThrow(() -> new ItemNotFoundException("Color", request.code()));
            entity.setLabel(request.label());
            entity.setColorValue(request.value());
            colorRepository.save(entity);
            return mapEntityToDto(entity);
        }
        Optional<ColorEntity> result = colorRepository.findByLabelIgnoreCase(request.label());
        if (result.isPresent()) {
            ColorEntity entity = result.get();
            entity.setLabel(request.label());
            entity.setColorValue(request.value());
            colorRepository.save(entity);
            return mapEntityToDto(entity);
        }
        var entity = ColorEntity.newEntity(request);
        entity.setCode(generateCode());
        colorRepository.save(entity);
        return mapEntityToDto(entity);
    }

    @Transactional(readOnly = true)
    public List<ColorDto> getColors() {
        return colorRepository.findAll().stream()
                .map(this::mapEntityToDto).toList();
    }

    public ColorDto getColor(String code) {
        var entity = colorRepository.findByCode(code)
                .orElseThrow(() -> new ItemNotFoundException("Color", code));
        return mapEntityToDto(entity);
    }

    public void deleteColor(String code) {
        var entity = colorRepository.findByCode(code)
                .orElseThrow(() -> new ItemNotFoundException("Color", code));
        colorRepository.delete(entity);
    }
}
