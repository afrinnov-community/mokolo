package org.afrinnov.sheet.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.ItemNotFoundException;
import org.afrinnov.sheet.id.FeatureCode;
import org.afrinnov.sheet.dto.FeatureDto;
import org.afrinnov.sheet.rest.support.input.FeatureRequest;
import org.afrinnov.sheet.entities.FeatureEntity;
import org.afrinnov.sheet.repositories.FeatureRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class FeatureService {
    private final FeatureRepository featureRepository;

    @Transactional
    public FeatureCode create(FeatureRequest request) {
        FeatureEntity entity = FeatureEntity.newEntity(request);
        entity.setCode(generateCode());
        featureRepository.save(entity);
        return new FeatureCode(entity.getCode());
    }

    public FeatureDto getFeature(String code) {
        return featureRepository.findByCodeIgnoreCase(code)
                .map(FeatureService::mapEntityToDto)
                .orElseThrow(() -> new ItemNotFoundException("Feature", code));
    }

    public List<FeatureDto> getFeatures() {
        return featureRepository.findAll().stream().map(FeatureService::mapEntityToDto).toList();
    }


    private static FeatureDto mapEntityToDto(FeatureEntity entity) {
        return new FeatureDto(entity.getCode(), entity.getLabel(), entity.getDescription());
    }

    private String generateCode() {
        Year year = Year.now();
        return year.format(DateTimeFormatter.ofPattern("yy")) + RandomStringUtils.randomNumeric(5);
    }
}
