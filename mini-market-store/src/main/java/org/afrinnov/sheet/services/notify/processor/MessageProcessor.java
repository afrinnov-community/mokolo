package org.afrinnov.sheet.services.notify.processor;

import org.afrinnov.security.MailTemplate;
import org.afrinnov.security.Messager;

public interface MessageProcessor {
    void send(Messager messager);
    boolean isSupported(MailTemplate mailTemplate);
}
