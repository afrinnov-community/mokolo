package org.afrinnov.sheet.services.product;

import org.afrinnov.sheet.dto.PageDTO;
import org.afrinnov.sheet.rest.input.search.PageableSearchRequest;
import org.afrinnov.sheet.rest.input.search.SearchRequest;
import org.afrinnov.sheet.dto.ProductSearchResult;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductSearchService {
    public List<ProductSearchResult> search(SearchRequest searchRequest) {
        return List.of();
    }

    public PageDTO<ProductSearchResult> searchPage(PageableSearchRequest searchRequest) {
        return new PageDTO<>(List.of(), 0);
    }
}
