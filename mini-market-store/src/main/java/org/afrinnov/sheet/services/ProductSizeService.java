package org.afrinnov.sheet.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.ItemNotFoundException;
import org.afrinnov.sheet.dto.SizeDto;
import org.afrinnov.sheet.entities.CategoryEntity;
import org.afrinnov.sheet.entities.ProductSizeEntity;
import org.afrinnov.sheet.entities.SizeEntity;
import org.afrinnov.sheet.id.ProductSku;
import org.afrinnov.sheet.repositories.CategoryRepository;
import org.afrinnov.sheet.repositories.ProductSizeRepository;
import org.afrinnov.sheet.repositories.SizeRepository;
import org.afrinnov.sheet.rest.support.input.SizeRequest;
import org.afrinnov.sheet.services.mapper.ProductSizeMapper;
import org.afrinnov.sheet.utils.ProductUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductSizeService {
    private final SizeRepository sizeRepository;
    private final CategoryRepository categoryRepository;
    private final ProductSizeRepository productSizeRepository;

    @Transactional
    public SizeDto createSize(SizeRequest sizeRequest) {
        if (sizeRequest.hasCode()) {
            var entity = sizeRepository.findByCode(sizeRequest.code())
                    .orElseThrow(() -> new ItemNotFoundException("Size", sizeRequest.code()));
            entity.setLabel(sizeRequest.label());
            entity.setDescription(sizeRequest.description());
            sizeRepository.save(entity);
            return new SizeDto(entity.getCode(), entity.getDescription(),
                    entity.getLabel(), null);
        }
        var result = sizeRepository.findByLabelIgnoreCase(sizeRequest.label());
        if (result.isPresent()) {
            var entity = result.get();
            entity.setLabel(sizeRequest.label());
            entity.setDescription(sizeRequest.description());
            sizeRepository.save(entity);
            return new SizeDto(entity.getCode(), entity.getDescription(),
                    entity.getLabel(), null);
        }
        var entity = new SizeEntity();
        entity.setLabel(sizeRequest.label());
        entity.setDescription(sizeRequest.description());
        entity.setCode(ProductUtil.generateCode());
        sizeRepository.save(entity);

        return new SizeDto(entity.getCode(), entity.getDescription(),
                entity.getLabel(), null);
    }

    @Transactional
    public void updateSize(String code, SizeDto productSize) {
        SizeEntity size = sizeRepository.findByCode(code)
                .orElseThrow(() -> new ItemNotFoundException("Size", productSize.code()));

        size.setLabel(productSize.label());
        size.setDescription(productSize.description());

        CategoryEntity category = categoryRepository.findByCode(productSize.categoryCode())
                .orElseThrow(() -> new ItemNotFoundException("ProductCategory", productSize.categoryCode()));

        size.setCategory(category);
        sizeRepository.save(size);
    }

    @Transactional(readOnly = true)
    public List<SizeDto> getAllSizes() {
        return sizeRepository.findAll()
                .stream()
                .map(ProductSizeMapper::mapProductSizeToDto)
                .toList();
    }

    @Transactional(readOnly = true)
    public SizeDto getSize(String code) {
        return sizeRepository.findByCode(code)
                .map(ProductSizeMapper::mapProductSizeToDto)
                .orElseThrow(() -> new ItemNotFoundException("ProductSize", code));
    }

    @Transactional
    public void deleteSize(String code) {
        var productSize = sizeRepository.findByCode(code);
        productSize.ifPresent(entity -> sizeRepository.deleteById(productSize.get().getSid()));
    }

    @Transactional(readOnly = true)
    public List<SizeDto> getSizes(ProductSku productSku) {
        return productSizeRepository.findByProduct_Sku(productSku.sku())
                .stream().map(ProductSizeEntity::getSize).map(ProductSizeMapper::mapProductSizeToDto).toList();
    }
}
