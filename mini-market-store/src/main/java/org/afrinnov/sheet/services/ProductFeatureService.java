package org.afrinnov.sheet.services;

import org.afrinnov.sheet.dto.ProductFeatures;
import org.afrinnov.sheet.id.FeatureCode;
import org.afrinnov.sheet.id.ProductSku;
import org.afrinnov.sheet.rest.input.ProductFeaturesRequest;

import java.util.List;


public interface ProductFeatureService {
    void addFeatures(ProductSku productSku, ProductFeaturesRequest request);

    ProductFeatures getProductFeatures(ProductSku productSku);

    void removeFeature(ProductSku productSku, FeatureCode featureCode);

    void removeDetailsFeature(ProductSku productSku, FeatureCode featureCode, List<String> clefs);
}
