package org.afrinnov.sheet.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.Country;
import org.afrinnov.sheet.id.CountryId;
import org.afrinnov.sheet.rest.support.input.CountryRequest;
import org.afrinnov.sheet.entities.CountryEntity;
import org.afrinnov.sheet.id.TelephoneCode;
import org.afrinnov.sheet.entities.TelephoneCodeEntity;
import org.afrinnov.sheet.repositories.TelephoneCodeRepository;
import org.afrinnov.sheet.repositories.CountryRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class CountryService {
    private final CountryRepository repository;
    private final TelephoneCodeRepository telephoneCodeRepository;

    public CountryId create(CountryRequest country) {
        var entity = repository.save(CountryEntity.newEntity(country));
        return new CountryId(entity.getSid().toString());
    }

    @Transactional(readOnly = true)
    @Cacheable("countries")
    public List<Country> getCountries() {
        return repository.findAll().stream().map(this::mapEntityToDto).toList();
    }

    private Country mapEntityToDto(CountryEntity country) {
        return Country.builder().isoCode(country.getIsoCode())
                .name(country.getName())
                .nameUs(country.getNameUs())
                .telephoneCode(getTelephoneCode(country))
                .build();

    }

    private TelephoneCode getTelephoneCode(CountryEntity country) {
        return telephoneCodeRepository
                .findOneByIsoCountryCode(country.getIsoCode()).map(this::mapTelephoneCodeToDto).orElse(null);
    }

    private TelephoneCode mapTelephoneCodeToDto(TelephoneCodeEntity telephone) {
        return TelephoneCode.builder()
                .code(telephone.getCode())
                .country(telephone.getIsoCountryCode())
                .format(telephone.getFormat())
                .enable(telephone.isEnable())
                .build();
    }
}
