package org.afrinnov.sheet.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.ItemNotFoundException;
import org.afrinnov.sheet.entities.ShippingModeEntity;
import org.afrinnov.sheet.repositories.ShippingModeRepository;
import org.afrinnov.sheet.dto.ShippingMode;
import org.afrinnov.sheet.id.ShippingModeId;
import org.afrinnov.sheet.rest.support.input.ShippingModeRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ShippingModeService {
    private final ShippingModeRepository shippingModeRepository;

    public ShippingModeId create(ShippingModeRequest request) {
        ShippingModeEntity saved = shippingModeRepository.save(ShippingModeEntity.newEntity(request));
        return new ShippingModeId(saved.getMode());
    }

    public ShippingMode getShippingMode(String mode) {
        return shippingModeRepository.findByModeIgnoreCase(mode)
                .map(ShippingModeService::mapEntityToDto)
                .orElseThrow(() -> new ItemNotFoundException("ShippingMode", mode));
    }

    private static ShippingMode mapEntityToDto(ShippingModeEntity entity) {
        return new ShippingMode(entity.getMode(), entity.getDescription());
    }

    public List<ShippingMode> getShippingModes() {
        return shippingModeRepository.findAll().stream().map(ShippingModeService::mapEntityToDto).toList();
    }
}
