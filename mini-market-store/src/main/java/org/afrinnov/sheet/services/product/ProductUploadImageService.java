package org.afrinnov.sheet.services.product;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.id.FileId;
import org.afrinnov.sheet.services.upload.StorageService;
import org.afrinnov.sheet.id.ProductSku;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class ProductUploadImageService {
    private final StorageService storageService;
    private final ProductService productService;

    public void savePreviewImage(ProductSku productSku, MultipartFile file) {
        FileId fileId = storageService.saveWithMark(file);
        productService.attachPreviewImage(productSku, fileId);
    }

    public void addWizardImage(ProductSku productSku, MultipartFile file) {
        FileId fileId = storageService.saveWithMark(file);
        productService.attachWizardImage(productSku, fileId);
    }
}
