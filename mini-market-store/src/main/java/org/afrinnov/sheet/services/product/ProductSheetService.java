package org.afrinnov.sheet.services.product;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.ProductSheet;
import org.afrinnov.sheet.rest.input.ProductSheetDelete;
import org.afrinnov.sheet.rest.input.ProductSheetRequest;
import org.afrinnov.sheet.id.ProductSku;
import org.afrinnov.sheet.rest.input.ProductSheetUpdate;
import org.afrinnov.sheet.services.ProductFeatureService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductSheetService {
    private final ProductService productService;
    private final ProductFeatureService productFeatureService;

    public ProductSku initialize(ProductSheetRequest request) {
        return productService.create(request);
    }

    public ProductSheet getProduct(ProductSku productSku) {
        ProductSheet product = productService.getProduct(productSku);
        return product.toBuilder().features(productFeatureService.getProductFeatures(productSku).features()).build();
    }

    public void update(ProductSku productSku, ProductSheetUpdate request) {
        productService.update(productSku, request);
    }

    public void remove(ProductSku productSku, ProductSheetDelete request) {
        productService.delete(productSku, request);
    }
}
