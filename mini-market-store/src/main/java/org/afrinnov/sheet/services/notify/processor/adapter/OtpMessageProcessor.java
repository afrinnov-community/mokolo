package org.afrinnov.sheet.services.notify.processor.adapter;

import lombok.RequiredArgsConstructor;
import org.afrinnov.security.MailTemplate;
import org.afrinnov.security.Messager;
import org.afrinnov.security.MessagerSent;
import org.afrinnov.sheet.services.mails.MailSendService;
import org.afrinnov.sheet.services.notify.processor.MessageProcessor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OtpMessageProcessor implements MessageProcessor {
    private final MailSendService mailSendService;
    private final ApplicationEventPublisher events;


    @Override
    public void send(Messager messager) {
        mailSendService.ping(messager.to(), messager.subject(),
                String.format("OTP: %s", messager.data().getOrDefault("otp", "-1-1-1-1")));
        events.publishEvent(new MessagerSent(messager.to(), true));
    }

    @Override
    public boolean isSupported(MailTemplate mailTemplate) {
        return MailTemplate.ACTIVATE_NEW_USER.equals(mailTemplate);
    }
}
