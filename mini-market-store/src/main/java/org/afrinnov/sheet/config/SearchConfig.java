package org.afrinnov.sheet.config;

import org.afrinnov.sheet.services.search.Indexer;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SearchConfig {
    @Bean
    public ApplicationRunner buildIndex(Indexer ignored) {
        return (ApplicationArguments args) -> {
            //TODO uncomment when product entity available
            //indexer.indexPersistedData("org.afrinnov.product.entities.ProductEntity");
        };
    }
}
