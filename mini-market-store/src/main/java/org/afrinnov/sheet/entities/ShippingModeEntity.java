package org.afrinnov.sheet.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.sheet.rest.support.input.ShippingModeRequest;

@Entity
@Table(schema = "eshop", name = "e_shipping_mode")
@Getter
@Setter
public class ShippingModeEntity extends BaseIdentityEntity {
    private String mode;
    private String description;

    public static ShippingModeEntity newEntity(ShippingModeRequest request) {
        var entity = new ShippingModeEntity();
        entity.setMode(request.mode());
        entity.setDescription(request.description());
        return entity;
    }
}
