package org.afrinnov.sheet.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.sheet.rest.support.input.ColorRequest;

@Entity
@Table(name = "e_color")
@Getter
@Setter
public class ColorEntity extends BaseIdentityEntity {
    @Column(name = "code", nullable = false, unique = true)
    private String code;
    @Column(name = "label", nullable = false, unique = true)
    private String label;
    private String colorValue;

    public static ColorEntity newEntity(ColorRequest request) {
        ColorEntity entity = new ColorEntity();
        entity.setColorValue(request.value());
        entity.setLabel(request.label());
        return entity;
    }
}
