package org.afrinnov.sheet.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.sheet.dto.DeliveryMode;
import org.afrinnov.sheet.rest.support.input.PaymentModeInput;

import java.util.Optional;

@Entity
@Table(schema = "eshop", name = "e_payment_mode")
@Getter
@Setter
public class PaymentModeEntity extends BaseIdentityEntity {
    private String mode;
    private String description;
    private boolean enable;

    public static PaymentModeEntity newEntity(PaymentModeInput request) {
        PaymentModeEntity entity = new PaymentModeEntity();
        entity.setMode(request.mode());
        entity.setDescription(request.description());
        Optional.ofNullable(request.enable()).ifPresent(entity::setEnable);
        return entity;
    }

    public DeliveryMode deliveryMode() {
        return new DeliveryMode(this.getMode(), this.getDescription());
    }
}
