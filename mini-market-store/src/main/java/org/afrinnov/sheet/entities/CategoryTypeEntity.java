package org.afrinnov.sheet.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "tools", name = "c_category_type")
@Getter
@Setter
public class CategoryTypeEntity extends BaseIdentityEntity {
    @Column(name = "code", nullable = false, unique = true)
    private String code;
    @Column(name = "name", nullable = false, unique = true)
    private String name;
    @Enumerated(EnumType.STRING)
    private CategoryTypeStatus status;

    public void defineStatus(String status) {
        try {
            this.status = CategoryTypeStatus.valueOf(status);
        } catch (IllegalArgumentException exception) {
            this.status = CategoryTypeStatus.DEACTIVATE;
        }
    }


    public enum CategoryTypeStatus {
        ACTIVE, DEACTIVATE
    }
}
