package org.afrinnov.sheet.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "e_size")
@Getter
@Setter
public class SizeEntity extends BaseIdentityEntity {
    @Column(name = "code", nullable = false, unique = true)
    private String code;
    @Column(name = "label", nullable = false, unique = true)
    private String label;
    @Column(name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable( name = "E_CATEGORY_SIZE_LINK" , joinColumns = @JoinColumn(name = "SIZE_SID"),
    inverseJoinColumns = @JoinColumn(name = "CATEGORY_SID"))
    private CategoryEntity category;
}
