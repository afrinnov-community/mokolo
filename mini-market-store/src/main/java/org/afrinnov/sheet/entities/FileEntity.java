package org.afrinnov.sheet.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.sheet.rest.input.FileRequest;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(schema = "upload", name = "f_files")
@Getter
@Setter
public class FileEntity extends BaseIdentityEntity {
    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] image;
    private String imageContentType;
    private String originalFilename;
    @Column(name = "image_size")
    private long size;
    private boolean imageMarked;

    @ManyToMany(mappedBy = "previewImages")
    private Set<ProductEntity> products = new HashSet<>();

    public static FileEntity newEntity(FileRequest fileRequest) {
        FileEntity entity = new FileEntity();
        entity.setImage(fileRequest.getContent());
        entity.setSize(fileRequest.getSize());
        entity.setImageMarked(fileRequest.isMarked());
        entity.setOriginalFilename(fileRequest.getOriginalFilename());
        entity.setImageContentType(fileRequest.getContentType());
        return entity;
    }
}
