package org.afrinnov.sheet.entities;

import io.micrometer.common.util.StringUtils;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.sheet.dto.BrandImageLogo;
import org.afrinnov.sheet.id.BrandCode;
import org.afrinnov.sheet.rest.support.input.BrandRequest;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.UUID;

@Entity
@Table(name = "e_brand")
@Getter
@Setter
public class BrandEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID sid;
    private String code;
    private String label;
    private String imageLogoUrl;


    public static BrandEntity newEntity(BrandRequest brandRequest, BrandCode codeBrand) {

        BrandEntity entity = new BrandEntity();
        entity.setCode(codeBrand.codeBrand());
        entity.setLabel(brandRequest.label());
        entity.setImageLogoUrl("");
        return entity;
    }

    public static BrandEntity newEntityWithLogo(BrandRequest brandRequest, BrandCode codeBrand, BrandImageLogo brandImageLogo) {

        BrandEntity entity = new BrandEntity();
        entity.setCode(codeBrand.codeBrand());
        entity.setLabel(brandRequest.label());
        entity.setImageLogoUrl(((StringUtils.isEmpty(brandImageLogo.urlImageLogo()) ? "" : brandImageLogo.urlImageLogo())));
        return entity;
    }
}
