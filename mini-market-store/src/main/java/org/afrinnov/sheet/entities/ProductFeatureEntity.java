package org.afrinnov.sheet.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.sheet.rest.input.ProductFeatureRequest;

import java.util.Objects;

@Entity
@Table(name = "e_product_feature")
@Getter
@Setter
public class ProductFeatureEntity extends BaseIdentityEntity {
    private String clef;
    private String label;

    @ManyToOne
    private FeatureEntity feature;
    @ManyToOne
    private ProductEntity product;

    public static ProductFeatureEntity newEntity(ProductFeatureRequest request, ProductEntity product, FeatureEntity feature) {
        Objects.requireNonNull(product);
        Objects.requireNonNull(feature);
        ProductFeatureEntity entity = new ProductFeatureEntity();
        entity.setFeature(feature);
        entity.setProduct(product);
        entity.setClef(request.clef());
        entity.setLabel(request.label());
        return entity;
    }
}
