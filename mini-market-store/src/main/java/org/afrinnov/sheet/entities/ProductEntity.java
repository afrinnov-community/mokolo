package org.afrinnov.sheet.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.sheet.rest.input.ProductSheetRequest;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "e_product")
@Getter
@Setter
public class ProductEntity extends LocalBaseAuditEntity {
    private String sku;
    private String title;
    private String description;

    @ManyToOne
    private CategoryEntity category;
    @ManyToOne
    private BrandEntity brand;

    @ManyToMany
    @JoinTable(name = "E_PRODUCT_EXPEDITION_MODE", joinColumns = @JoinColumn(name = "PRODUCT_SID"),
            inverseJoinColumns = @JoinColumn(name = "SHIPPING_MODE_SID"))
    private Set<ShippingModeEntity> shippingModes = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "e_product_payment_mode", joinColumns = @JoinColumn(name = "PRODUCT_SID"),
            inverseJoinColumns = @JoinColumn(name = "payment_mode_sid"))
    private Set<PaymentModeEntity> paymentModes = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "e_product_delivery_mode", schema = "eshop", joinColumns = @JoinColumn(name = "PRODUCT_SID"),
            inverseJoinColumns = @JoinColumn(name = "delivery_mode_sid"))
    private Set<DeliveryModeEntity> deliveryModes = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "F_FILES_PRODUCT_PREVIEW", joinColumns = @JoinColumn(name = "E_PRODUCT_SID"),
            inverseJoinColumns = @JoinColumn(name = "F_FILES_SID"))
    private Set<FileEntity> previewImages = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "F_FILES_PRODUCT_WIZARD", joinColumns = @JoinColumn(name = "E_PRODUCT_SID"),
            inverseJoinColumns = @JoinColumn(name = "F_FILES_SID"))
    private Set<FileEntity> wizardImages = new HashSet<>();

    public static ProductEntity newEntity(ProductSheetRequest request) {
        ProductEntity entity = new ProductEntity();
        entity.setTitle(request.getTitle());
        entity.setDescription(request.getDescription());
        return entity;
    }
}
