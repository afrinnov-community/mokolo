package org.afrinnov.sheet.entities;

public enum CurrencyType {
    EUR,
    XAF,
    XOF
}
