package org.afrinnov.sheet.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "e_category")
@Getter
@Setter
public class CategoryEntity extends BaseIdentityEntity {
    @Column(name = "code", nullable = false, unique = true)
    private String code;
    @Column(name = "label", nullable = false, unique = true)
    private String label;
    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "category")
    private List<SizeEntity> sizes = new ArrayList<>();
    @ManyToOne(fetch = FetchType.LAZY)
    private CategoryTypeEntity categoryType;
}
