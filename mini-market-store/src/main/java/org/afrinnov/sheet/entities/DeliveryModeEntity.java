package org.afrinnov.sheet.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;
import org.afrinnov.sheet.dto.DeliveryMode;
import org.afrinnov.sheet.rest.support.input.DeliveryModeRequest;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/12/2023 -- 18:26<br></br>
 * By : @author alexk<br></br>
 * Project : mokolo<br></br>
 * Package : org.afrinnov.common.delivry.entity<br></br>
 */

@Entity
@Table(schema = "eshop", name = "e_delivery_mode")
@Getter
@Setter
public class DeliveryModeEntity extends BaseIdentityEntity {
    private String mode;
    private String description;

    public static DeliveryModeEntity newEntity(DeliveryModeRequest request) {
        DeliveryModeEntity entity = new DeliveryModeEntity();
        entity.setMode(request.mode());
        entity.setDescription(request.description());
        return entity;
    }

    public DeliveryMode deliveryMode() {
        return new DeliveryMode(this.getMode(), this.getDescription());
    }
}
