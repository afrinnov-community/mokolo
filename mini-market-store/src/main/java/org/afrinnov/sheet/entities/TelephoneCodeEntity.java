package org.afrinnov.sheet.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.sheet.rest.support.input.TelephoneCodeRequest;
import org.hibernate.envers.Audited;

@Entity
@Table(schema = "tools", name = "telephone_code")
@Getter
@Setter
@Audited
public class TelephoneCodeEntity extends LocalBaseAuditEntity {
    private String code;
    private String isoCountryCode;
    private boolean enable;
    private String format;


    public static TelephoneCodeEntity newEntity(TelephoneCodeRequest telephoneCode) {
        TelephoneCodeEntity entity = new TelephoneCodeEntity();
        entity.setCode(telephoneCode.code());
        entity.setIsoCountryCode(telephoneCode.countryCode());
        entity.setEnable(true);
        entity.setFormat(telephoneCode.format());
        return entity;
    }
}
