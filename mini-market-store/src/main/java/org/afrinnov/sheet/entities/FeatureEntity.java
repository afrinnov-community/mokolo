package org.afrinnov.sheet.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.sheet.rest.support.input.FeatureRequest;

@Entity
@Table( name = "e_feature")
@Getter
@Setter
public class FeatureEntity extends BaseIdentityEntity {
    private String code;
    private String label;
    private String description;

    public static FeatureEntity newEntity(FeatureRequest request) {
        FeatureEntity entity = new FeatureEntity();
        entity.setDescription(request.description());
        entity.setLabel(request.label());
        return entity;
    }
}
