package org.afrinnov.sheet.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.sheet.rest.support.input.CountryRequest;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.UUID;

@Entity
@Table(schema = "tools", name = "country")
@Getter
@Setter
//@org.hibernate.annotations.Cache(usage= CacheConcurrencyStrategy.READ_ONLY)
public class CountryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID sid;
    private String isoCode;
    private String name;
    private String nameUs;

    public static CountryEntity newEntity(CountryRequest country) {
        CountryEntity entity = new CountryEntity();
        entity.setName(country.name());
        entity.setNameUs(country.nameUs());
        entity.setIsoCode(country.code());
        return entity;
    }
}
