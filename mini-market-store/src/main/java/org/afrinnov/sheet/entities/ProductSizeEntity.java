package org.afrinnov.sheet.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.sheet.rest.input.ProductSizeRequest;

import java.math.BigDecimal;

@Entity
@Table(name = "e_product_size")
@Getter
@Setter
public class ProductSizeEntity extends BaseIdentityEntity {
    private BigDecimal additionalFees;
    @ManyToOne
    private ProductEntity product;
    @ManyToOne
    private SizeEntity size;


    public static ProductSizeEntity newEntity(ProductEntity product, SizeEntity size, ProductSizeRequest dto) {
        ProductSizeEntity entity = new ProductSizeEntity();
        entity.setAdditionalFees(dto.additionalFees());
        entity.setSize(size);
        entity.setProduct(product);
        return entity;
    }
}
