package org.afrinnov.sheet.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.afrinnov.sheet.dto.FileContent;
import org.afrinnov.sheet.rest.FilesController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.time.Year;

import static java.time.format.DateTimeFormatter.ofPattern;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductUtil {
    public static String generateCode() {
        return Year.now().format(ofPattern("yy"))+ randomNumeric(5);
    }

    public static String makeUri(FileContent fileContent) {
        return MvcUriComponentsBuilder.fromMethodName(FilesController.class,
                "openServeFile", fileContent.makeFileName()).build().toUri().toString();
    }
}
