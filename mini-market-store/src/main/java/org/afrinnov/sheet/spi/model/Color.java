package org.afrinnov.sheet.spi.model;

public interface Color {
    String code();
    String label();
    String value();
}
