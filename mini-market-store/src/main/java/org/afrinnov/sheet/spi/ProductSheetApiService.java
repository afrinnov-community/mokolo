package org.afrinnov.sheet.spi;


import org.afrinnov.sheet.*;
import org.afrinnov.sheet.spi.model.Color;
import org.afrinnov.sheet.spi.model.ProductBasic;
import org.afrinnov.sheet.spi.model.ProductShare;
import org.afrinnov.sheet.spi.model.Size;

import java.util.Optional;

public interface ProductSheetApiService {
    void checkProduct(String sku);

    Color getColor(String key);

    Size getSize(String sizeRef);

    Optional<ProductShareId> getProductIdentifier(String sku);

    Optional<ProductShare> getProduct(String sku);
    Optional<ProductBasic> getProductInfo(String sku);
}
