package org.afrinnov.sheet.spi.model;

public interface ProductShare {
     String getSku();
     String getTitle();
     String getDescription();
     String getPreviewImageUrl();
}
