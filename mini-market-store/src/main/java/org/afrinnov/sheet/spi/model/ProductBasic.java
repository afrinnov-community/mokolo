package org.afrinnov.sheet.spi.model;

public interface ProductBasic {
    String getSku();
    String getTitle();
    String getDescription();
    String getPreviewImageUrl();
}
