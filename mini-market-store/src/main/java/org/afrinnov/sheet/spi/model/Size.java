package org.afrinnov.sheet.spi.model;

public interface Size {
    String code();
    String label();
    String description();
    String categoryCode();
}
