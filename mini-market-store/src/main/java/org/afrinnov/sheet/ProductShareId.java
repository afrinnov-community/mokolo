package org.afrinnov.sheet;

import java.util.UUID;

public record ProductShareId(UUID sid, String sku) {
}
