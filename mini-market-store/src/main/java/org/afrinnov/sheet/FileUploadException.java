package org.afrinnov.sheet;

public class FileUploadException extends RuntimeException {
    public FileUploadException(Throwable cause) {
        super(cause);
    }

    public FileUploadException(String message, Throwable cause) {
        super(message, cause);
    }
}
