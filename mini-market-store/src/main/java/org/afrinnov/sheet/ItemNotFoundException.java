package org.afrinnov.sheet;

import lombok.Getter;

@Getter
public class ItemNotFoundException extends RuntimeException {

    private final String type;
    private final Object reference;

    public ItemNotFoundException(String type, Object reference) {
        this.type = type;
        this.reference = reference;
    }
}
