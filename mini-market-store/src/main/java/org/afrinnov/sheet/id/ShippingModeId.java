package org.afrinnov.sheet.id;

public record ShippingModeId(String mode) {
}
