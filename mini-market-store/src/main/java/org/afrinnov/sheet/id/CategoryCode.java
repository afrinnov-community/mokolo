package org.afrinnov.sheet.id;

import java.util.Objects;

public record CategoryCode(String code) {

    public static CategoryCode code(String code) {
        Objects.requireNonNull(code);
        return new CategoryCode(code);
    }
}
