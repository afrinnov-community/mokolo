package org.afrinnov.sheet.id;

import java.util.UUID;

public record FileId(UUID id) {
}
