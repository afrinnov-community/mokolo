package org.afrinnov.sheet.id;

public record ProductSku(String sku) {

    public static ProductSku of(String sku) {
        return new ProductSku(sku);
    }
}
