package org.afrinnov.sheet.id;

public record TelephoneCodeId(String id) {
}
