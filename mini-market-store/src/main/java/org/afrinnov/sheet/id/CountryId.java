package org.afrinnov.sheet.id;

public record CountryId(String id) {
}
