package org.afrinnov.sheet.id;

public record SizeCode(String code) {

    public static SizeCode code(String code) {
        return new SizeCode(code);
    }
}
