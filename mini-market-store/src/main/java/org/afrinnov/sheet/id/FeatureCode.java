package org.afrinnov.sheet.id;

public record FeatureCode(String code) {
}
