package org.afrinnov.sheet.id;

public record CategoryTypeCode(String code) {
}
