package org.afrinnov.sheet.rest.support;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.Brand;
import org.afrinnov.sheet.rest.support.input.BrandRequest;
import org.afrinnov.sheet.rest.support.output.BrandResponse;
import org.afrinnov.sheet.services.BrandService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/brands")
@RequiredArgsConstructor
public class BrandController {

    private final BrandService service;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public BrandResponse save(@RequestBody BrandRequest brandRequest){
        Brand brand =  service.create(brandRequest);
        return new BrandResponse(brand.getLabel(), brand.getCode(), "CREATED", "Le code de la marque est : "+brand.getCode());
    }

    @GetMapping
    public List<Brand> allBrands() {
        return service.getBrands();
    }

}
