package org.afrinnov.sheet.rest.input;


import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Getter
@Builder
@Jacksonized
public class ProductSheetDelete {
    private List<String> shippingModes;
    private List<String> deliveryModes;
    private List<String> paymentModes;

}
