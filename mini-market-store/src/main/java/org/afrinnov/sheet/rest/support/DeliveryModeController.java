package org.afrinnov.sheet.rest.support;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.DeliveryMode;
import org.afrinnov.sheet.id.DeliveryModeId;
import org.afrinnov.sheet.rest.support.input.DeliveryModeRequest;
import org.afrinnov.sheet.rest.support.output.DeliveryModeResponse;
import org.afrinnov.sheet.services.DeliveryModeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/12/2023 -- 17:47<br></br>
 * By : @author alexk<br></br>
 * Project : mokolo<br></br>
 * Package : org.afrinnov.common.delivry.rest<br></br>
 */

@RestController
@RequestMapping("/api/delivery-mode")
@RequiredArgsConstructor
public class DeliveryModeController {
    private final DeliveryModeService deliveryModeService;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public DeliveryModeResponse save(@RequestBody @Valid DeliveryModeRequest request) {
        DeliveryModeId deliveryModeId = deliveryModeService.create(request);
        return new DeliveryModeResponse(request.mode(), "CREATED", "Id:" + deliveryModeId.mode());
    }

    @GetMapping("/{mode}")
    @ResponseStatus(HttpStatus.OK)
    public DeliveryMode getOneMode(@PathVariable String mode) {
        return deliveryModeService.getDeliveryMode(mode);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<DeliveryMode> getAllMode() {
        return deliveryModeService.getAllDeliveryMode();
    }


}
