package org.afrinnov.sheet.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.ProductFeatures;
import org.afrinnov.sheet.id.FeatureCode;
import org.afrinnov.sheet.rest.input.ProductFeaturesRequest;
import org.afrinnov.sheet.id.ProductSku;
import org.afrinnov.sheet.services.ProductFeatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromMethodName;

@RestController
@RequestMapping("/api/product/{sku}/features")
@RequiredArgsConstructor
public class ProductFeatureController {
    private final ProductFeatureService productSheetService;

    @PostMapping
    public ResponseEntity<Void> addFeatures(@PathVariable String sku, @RequestBody @Valid ProductFeaturesRequest request) {
        productSheetService.addFeatures(new ProductSku(sku), request);
        URI uri = fromMethodName(ProductFeatureController.class, "productFeatures", sku)
                .build()
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping
    public ProductFeatures productFeatures(@PathVariable String sku) {
        return productSheetService.getProductFeatures(new ProductSku(sku));
    }

    @DeleteMapping("/{featureCode}")
    public void removeFeature(@PathVariable String sku, @PathVariable String featureCode) {
        productSheetService.removeFeature(new ProductSku(sku), new FeatureCode(featureCode));
    }

    @DeleteMapping("/{featureCode}/details")
    public void removeFeatureDetails(@PathVariable String sku, @PathVariable String featureCode, @RequestBody List<String> clefs) {
        productSheetService.removeDetailsFeature(new ProductSku(sku), new FeatureCode(featureCode), clefs);
    }
}
