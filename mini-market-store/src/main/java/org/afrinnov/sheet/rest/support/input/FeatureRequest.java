package org.afrinnov.sheet.rest.support.input;

import jakarta.validation.constraints.NotBlank;

public record FeatureRequest(@NotBlank String label, String description) {
}
