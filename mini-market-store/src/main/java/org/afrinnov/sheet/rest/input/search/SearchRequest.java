package org.afrinnov.sheet.rest.input.search;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class SearchRequest {
    @NotBlank
    private String text;
    private List<String> fields;
    @Min(0)
    private int limit;


}
