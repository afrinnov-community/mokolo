package org.afrinnov.sheet.rest.input;

import jakarta.validation.constraints.NotEmpty;

public record TelephoneCodeValidRequest(@NotEmpty String phoneNumber) {
}
