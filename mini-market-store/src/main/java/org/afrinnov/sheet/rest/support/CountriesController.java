package org.afrinnov.sheet.rest.support;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.Country;
import org.afrinnov.sheet.id.CountryId;
import org.afrinnov.sheet.rest.support.input.CountryRequest;
import org.afrinnov.sheet.rest.support.output.CountryResponse;
import org.afrinnov.sheet.services.CountryService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/countries")
@RequiredArgsConstructor
public class CountriesController {
    private final CountryService service;

    @GetMapping
    public List<Country> countries() {
        return service.getCountries();
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public CountryResponse save(@RequestBody CountryRequest country){
        CountryId countryId = service.create(country);
        return new CountryResponse(country.code(), "CREATED", "Id:"+countryId.id());
    }
}
