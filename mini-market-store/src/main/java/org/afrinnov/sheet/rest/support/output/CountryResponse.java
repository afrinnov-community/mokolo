package org.afrinnov.sheet.rest.support.output;

public record CountryResponse(String code, String status, String message) {
}
