package org.afrinnov.sheet.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.afrinnov.sheet.id.ProductSku;
import org.afrinnov.sheet.services.product.ProductUploadImageService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
@RestController
@RequestMapping("/api/product/sheet/{sku}")
@RequiredArgsConstructor
@Slf4j
public class ProductUploadImageController {
    private final ProductUploadImageService uploadImageService;

    @PostMapping(value = "/preview-image", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void uploadPreviewImage(@PathVariable String sku, @RequestParam("file") MultipartFile file) {
      uploadImageService.savePreviewImage(new ProductSku(sku), file);
    }

    @PostMapping(value = "/wizard-image", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void uploadWizardImage(@PathVariable String sku, @RequestParam("file") MultipartFile file) {
        uploadImageService.addWizardImage(new ProductSku(sku), file);
    }
}
