package org.afrinnov.sheet.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.services.mails.MailSendService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/mail/ping")
@RequiredArgsConstructor
public class MailPingController {
    private final MailSendService mailSendService;

    @PostMapping
    public void sendMessage(@RequestBody @Valid PingMailMessage message) {
        mailSendService.ping(message.getTo(), message.getSubject(), message.getBody());
    }
}
