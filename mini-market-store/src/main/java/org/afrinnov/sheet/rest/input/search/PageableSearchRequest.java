package org.afrinnov.sheet.rest.input.search;

import jakarta.validation.constraints.Min;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class PageableSearchRequest extends SearchRequest{
    @Min(0)
    private int pageOffset;
}
