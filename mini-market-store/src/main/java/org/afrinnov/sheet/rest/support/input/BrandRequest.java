package org.afrinnov.sheet.rest.support.input;

import jakarta.validation.constraints.NotEmpty;

public record BrandRequest (@NotEmpty String label) {

}
