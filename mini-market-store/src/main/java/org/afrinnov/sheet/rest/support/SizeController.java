package org.afrinnov.sheet.rest.support;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.SizeDto;
import org.afrinnov.sheet.rest.support.input.SizeRequest;
import org.afrinnov.sheet.services.ProductSizeService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.net.URI;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/sizes")
@RequiredArgsConstructor
@Validated
public class SizeController {
    private final ProductSizeService productSizeService;

    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<SizeDto> createSize(@RequestBody @Valid SizeRequest sizeRequest) {
        SizeDto size = productSizeService.createSize(sizeRequest);
        URI uri = MvcUriComponentsBuilder.fromMethodName(SizeController.class, "getSize", size.code()).build().toUri();
        return ResponseEntity.created(uri).body(size);
    }
    @GetMapping()
    public ResponseEntity<List<SizeDto>> getSizes() {
        return ResponseEntity.ok(productSizeService.getAllSizes());
    }

    @GetMapping("/{code}")
    public ResponseEntity<SizeDto> getSize(@PathVariable String code) {
        return ResponseEntity.ok(productSizeService.getSize(code));
    }

    @DeleteMapping("/{code}")
    public void deleteSize(@PathVariable String code) {
        productSizeService.deleteSize(code);
    }
}
