package org.afrinnov.sheet.rest.support.input;

import com.google.common.base.Strings;
import jakarta.validation.constraints.NotEmpty;

public record CategoryRequest(@NotEmpty String label, String description, String categoryTypeCode) {
    public boolean isSent() {
        return !Strings.isNullOrEmpty(categoryTypeCode);
    }
}
