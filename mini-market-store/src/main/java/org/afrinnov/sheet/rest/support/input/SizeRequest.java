package org.afrinnov.sheet.rest.support.input;

import jakarta.validation.constraints.NotEmpty;

import java.util.Objects;

public record SizeRequest(String code, @NotEmpty String label, String description) {
    public boolean hasCode() {
        return Objects.nonNull(code);
    }
}
