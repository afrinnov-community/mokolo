package org.afrinnov.sheet.rest.support.output;

public record ShippingModeResponse(String code, String status, String message) {
}
