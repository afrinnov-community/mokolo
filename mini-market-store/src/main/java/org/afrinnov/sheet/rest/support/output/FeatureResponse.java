package org.afrinnov.sheet.rest.support.output;

public record FeatureResponse(String code, String status, String message) {
}
