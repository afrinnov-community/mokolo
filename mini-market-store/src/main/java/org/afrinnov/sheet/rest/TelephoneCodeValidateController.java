package org.afrinnov.sheet.rest;


import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.id.TelephoneCodeId;
import org.afrinnov.sheet.rest.input.TelephoneCodeValidRequest;
import org.afrinnov.sheet.rest.output.TelephoneCodeValidatedResponse;
import org.afrinnov.sheet.services.TelephoneCodeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/telephone-code/{code}/validate-format")
@RequiredArgsConstructor
public class TelephoneCodeValidateController {
    private final TelephoneCodeService service;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public TelephoneCodeValidatedResponse defineCode(@PathVariable String code, @RequestBody @Valid TelephoneCodeValidRequest validRequest) {
        return service.validate(new TelephoneCodeId(code), validRequest);
    }

}
