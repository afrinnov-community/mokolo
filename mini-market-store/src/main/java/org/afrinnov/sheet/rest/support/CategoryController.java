package org.afrinnov.sheet.rest.support;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.CategorySizes;
import org.afrinnov.sheet.id.CategoryTypeCode;
import org.afrinnov.sheet.id.CategoryCode;
import org.afrinnov.sheet.id.SizeCode;
import org.afrinnov.sheet.rest.support.input.CategoryRequest;
import org.afrinnov.sheet.rest.support.input.SizeRequest;
import org.afrinnov.sheet.services.CategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/categories")
@RequiredArgsConstructor
@Validated
public class CategoryController {

    private final CategoryService categoryService;

    @PostMapping()
    public ResponseEntity<CategorySizes> createCategory(@RequestBody @Valid CategoryRequest categoryRequest) {
        CategorySizes category = categoryService.createCategory(categoryRequest);
        URI uri = MvcUriComponentsBuilder.fromMethodName(CategoryController.class, "getCategory", category.code()).build().toUri();
        return ResponseEntity.created(uri).body(category);
    }

    @PutMapping("/{code}/change-category-type/{categoryTypeCode}")
    public ResponseEntity<Map<String, Object>> updateCategoryType(@PathVariable String code, @PathVariable String categoryTypeCode) {
        categoryService.updateCategoryType(new CategoryCode(code), new CategoryTypeCode(categoryTypeCode));
        return ResponseEntity.ok(Map.of("result", "success"));
    }

    @PutMapping("/{code}")
    public ResponseEntity<CategorySizes> updateCategory(@PathVariable String code, @RequestBody CategorySizes category) {
        CategorySizes updatedCategory = categoryService.updateCategory(code, category);
        return ResponseEntity.ok(updatedCategory);
    }

    @GetMapping()
    public ResponseEntity<List<CategorySizes>> getProductCategories() {
        return ResponseEntity.ok(categoryService.getAllCategories());
    }

    @GetMapping("/{code}")
    public ResponseEntity<CategorySizes> getCategory(@PathVariable String code) {
        return ResponseEntity.ok(categoryService.getCategory(code));
    }

    @DeleteMapping("/{code}")
    public void deleteCategory(@PathVariable String code) {
        categoryService.deleteCategory(code);
    }

    @PostMapping("/{code}/sizes")
    public ResponseEntity<CategorySizes> addSizeToCategory(@PathVariable String code, @RequestBody SizeRequest sizeRequest) {
        return ResponseEntity.ok(categoryService.addSizeToCategory(code, sizeRequest));
    }

    @PostMapping("/{categoryCode}/add-sizes")
    public ResponseEntity<CategorySizes> addSizesToCategory(@PathVariable String categoryCode,
                                                            @RequestBody List<String> sizeCodes) {
        return ResponseEntity.ok(categoryService
                .addSizesToCategory(new CategoryCode(categoryCode), sizeCodes.stream().map(SizeCode::code).toList()));
    }
}
