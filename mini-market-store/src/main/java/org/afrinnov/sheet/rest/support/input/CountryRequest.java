package org.afrinnov.sheet.rest.support.input;

import jakarta.validation.constraints.NotEmpty;

public record CountryRequest(@NotEmpty String code, @NotEmpty String name, String nameUs) {
}
