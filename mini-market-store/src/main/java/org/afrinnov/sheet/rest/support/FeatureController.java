package org.afrinnov.sheet.rest.support;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.id.FeatureCode;
import org.afrinnov.sheet.dto.FeatureDto;
import org.afrinnov.sheet.rest.support.input.FeatureRequest;
import org.afrinnov.sheet.rest.support.output.FeatureResponse;
import org.afrinnov.sheet.services.FeatureService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/feature")
@RequiredArgsConstructor
public class FeatureController {
    private final FeatureService featureService;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public FeatureResponse save(@RequestBody @Valid FeatureRequest request) {
        FeatureCode featureCode = featureService.create(request);
        return new FeatureResponse(featureCode.code(), "CREATED", "Id:" + featureCode.code());
    }

    @GetMapping("/{code}")
    @ResponseStatus(HttpStatus.OK)
    public FeatureDto getFeature(@PathVariable String code) {
        return featureService.getFeature(code);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<FeatureDto> getFeatures() {
        return featureService.getFeatures();
    }
}
