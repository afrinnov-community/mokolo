package org.afrinnov.sheet.rest.support;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.ColorDto;
import org.afrinnov.sheet.rest.support.input.ColorRequest;
import org.afrinnov.sheet.services.ColorService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromMethodName;

@RestController
@RequestMapping("/api/colors")
@RequiredArgsConstructor
@Validated
public class ColorsController {
    private final ColorService colorService;

    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ColorDto> createSize(@RequestBody @Valid ColorRequest request) {
        var color = colorService.createColor(request);
        var uri = fromMethodName(ColorsController.class, "getColor", color.code()).build().toUri();
        return ResponseEntity.created(uri).body(color);
    }

    @GetMapping()
    public ResponseEntity<List<ColorDto>> getColors() {
        return ResponseEntity.ok(colorService.getColors());
    }

    @GetMapping("/{code}")
    public ResponseEntity<ColorDto> getColor(@PathVariable String code) {
        return ResponseEntity.ok(colorService.getColor(code));
    }

    @DeleteMapping("/{code}")
    public void deleteSize(@PathVariable String code) {
        colorService.deleteColor(code);
    }
}
