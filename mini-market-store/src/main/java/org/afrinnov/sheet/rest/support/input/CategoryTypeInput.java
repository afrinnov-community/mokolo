package org.afrinnov.sheet.rest.support.input;

import jakarta.validation.constraints.NotEmpty;

public record CategoryTypeInput(String code, @NotEmpty String name, Status status) {

    public enum Status {
        ACTIVE, DEACTIVATE
    }
}
