package org.afrinnov.sheet.rest.support;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.rest.support.input.PaymentModeInput;
import org.afrinnov.sheet.rest.support.output.PaymentModeResponse;
import org.afrinnov.sheet.services.PaymentModeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/payment-modes")
@RequiredArgsConstructor
public class PaymentModeController {
    private final PaymentModeService paymentModeService;

    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public PaymentModeResponse save(@RequestBody @Valid PaymentModeInput request) {
        return paymentModeService.create(request);
    }

    @GetMapping("/{mode}")
    @ResponseStatus(HttpStatus.OK)
    public PaymentModeResponse getPaymentMode(@PathVariable String mode) {
        return paymentModeService.getPaymentMode(mode);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<PaymentModeResponse> getPaymentModes() {
        return paymentModeService.getPaymentModes();
    }
}
