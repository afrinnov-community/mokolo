package org.afrinnov.sheet.rest.input;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;

import java.math.BigDecimal;

public record ProductSizeRequest(@NotEmpty String sizeCode, @Min(0) BigDecimal additionalFees) {
}
