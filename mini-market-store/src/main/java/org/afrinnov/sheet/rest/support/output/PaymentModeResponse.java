package org.afrinnov.sheet.rest.support.output;

public record PaymentModeResponse(String mode, String description, Boolean enable) {
}
