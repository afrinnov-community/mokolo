package org.afrinnov.sheet.rest.input;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;

import java.util.List;

public record ProductFeaturesRequest(@NotBlank String featureCode, @NotEmpty List<ProductFeatureRequest> features) {
}
