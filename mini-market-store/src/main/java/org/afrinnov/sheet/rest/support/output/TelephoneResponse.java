package org.afrinnov.sheet.rest.support.output;

public record TelephoneResponse(String code, String status, String message) {

}
