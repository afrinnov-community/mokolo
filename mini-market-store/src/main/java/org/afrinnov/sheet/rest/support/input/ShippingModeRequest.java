package org.afrinnov.sheet.rest.support.input;

import jakarta.validation.constraints.NotEmpty;

public record ShippingModeRequest(@NotEmpty String mode, @NotEmpty String description) {
}
