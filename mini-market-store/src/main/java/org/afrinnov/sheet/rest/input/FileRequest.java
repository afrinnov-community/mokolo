package org.afrinnov.sheet.rest.input;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public final class FileRequest {
    private byte[] content;
    private String contentType;
    private long size;
    private String originalFilename;
    private boolean marked;



}
