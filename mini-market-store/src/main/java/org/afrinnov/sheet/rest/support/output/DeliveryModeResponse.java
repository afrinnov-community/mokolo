package org.afrinnov.sheet.rest.support.output;

import lombok.Builder;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/12/2023 -- 18:12<br></br>
 * By : @author alexk<br></br>
 * Project : mokolo<br></br>
 * Package : org.afrinnov.common.delivry.dto<br></br>
 */
@Builder(setterPrefix = "with")
public record DeliveryModeResponse(String mode, String status, String message) {
}
