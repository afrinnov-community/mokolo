package org.afrinnov.sheet.rest.support.input;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public record PaymentModeInput(@NotEmpty String mode, String description, @NotNull Boolean enable) {

}
