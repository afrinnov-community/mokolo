package org.afrinnov.sheet.rest;

import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class PingMailMessage {
    @NotEmpty
    private String to;
    @NotEmpty
    private String subject;
    @NotEmpty
    private String body;
}
