package org.afrinnov.sheet.rest;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.PageDTO;
import org.afrinnov.sheet.rest.input.search.PageableSearchRequest;
import org.afrinnov.sheet.rest.input.search.SearchRequest;
import org.afrinnov.sheet.dto.ProductSearchResult;
import org.afrinnov.sheet.services.product.ProductSearchService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/product/search")
@RequiredArgsConstructor
public class ProductSearchController {
    private final ProductSearchService service;

    @GetMapping
    public List<ProductSearchResult> search(SearchRequest searchRequest) {
        return service.search(searchRequest);
    }

    @GetMapping("/page")
    public PageDTO<ProductSearchResult> search(PageableSearchRequest searchRequest) {
        return service.searchPage(searchRequest);
    }
}
