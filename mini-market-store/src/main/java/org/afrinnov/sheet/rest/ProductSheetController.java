package org.afrinnov.sheet.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.FileContent;
import org.afrinnov.sheet.dto.ProductSheet;
import org.afrinnov.sheet.id.ProductSku;
import org.afrinnov.sheet.rest.input.ProductSheetDelete;
import org.afrinnov.sheet.rest.input.ProductSheetRequest;
import org.afrinnov.sheet.rest.input.ProductSheetUpdate;
import org.afrinnov.sheet.services.product.ProductSheetService;
import org.afrinnov.sheet.utils.ProductUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromMethodName;

@RestController
@RequestMapping("/api/product/sheet")
@RequiredArgsConstructor
public class ProductSheetController {
    private final ProductSheetService productSheetService;

    @PostMapping
    public ResponseEntity<ProductSheet> initialize(@RequestBody @Valid ProductSheetRequest request) {
        ProductSku sku = productSheetService.initialize(request);
        URI uri = fromMethodName(ProductSheetController.class, "productSheet", sku.sku())
                .build()
                .toUri();
        ProductSheet productSheet = ProductSheet.builder().sku(sku.sku()).title(request.getTitle())
                .description(request.getDescription())
                .build();
        return ResponseEntity.created(uri).body(productSheet);
    }

    @GetMapping("/{sku}")
    public ProductSheet productSheet(@PathVariable String sku) {
        ProductSheet product = productSheetService.getProduct(new ProductSku(sku));

        return product.toBuilder()
                .previewImageUrl(makePreviewUri(product.getPreviewImageId()))
                .wizardImageUrls(makeWizardUri(product.getWizardImageIds()))
                .build();
    }

    @PatchMapping("/{sku}")
    public ResponseEntity<ProductSheet> update(@PathVariable String sku, @RequestBody @Valid ProductSheetUpdate request) {
        productSheetService.update(new ProductSku(sku), request);
        URI uri = fromMethodName(ProductSheetController.class, "productSheet", sku)
                .build()
                .toUri();
        ProductSheet productSheet = ProductSheet.builder().sku(sku).title(request.getTitle())
                .description(request.getDescription())
                .build();
        return ResponseEntity.created(uri).body(productSheet);
    }

    private List<String> makeWizardUri(List<FileContent> wizardImageIds) {
        if (Objects.isNull(wizardImageIds)) {
            return List.of();
        }
        return wizardImageIds.stream().map(ProductUtil::makeUri).toList();
    }

    private String makePreviewUri(FileContent previewImageId) {
        if (Objects.isNull(previewImageId)) {
            return null;
        }
        return ProductUtil.makeUri(previewImageId);
    }

    @DeleteMapping("/{sku}/remove")
    public void remove(@PathVariable String sku, @RequestBody @Valid ProductSheetDelete request) {
        productSheetService.remove(new ProductSku(sku), request);
    }
}
