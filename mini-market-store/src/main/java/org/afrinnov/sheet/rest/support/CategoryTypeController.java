package org.afrinnov.sheet.rest.support;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.rest.support.input.CategoryTypeInput;
import org.afrinnov.sheet.rest.support.output.CategoryType;
import org.afrinnov.sheet.services.CategoryTypeService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/category-types")
@RequiredArgsConstructor
@Validated
public class CategoryTypeController {
    private final CategoryTypeService categoryTypeService;

    @PutMapping
    public CategoryType createOrUpdate(@RequestBody @Valid CategoryTypeInput input) {
        return categoryTypeService.createOrUpdate(input);
    }

    @GetMapping
    public List<CategoryType> categoryTypes() {
        return categoryTypeService.getCategoryTypes();
    }
}
