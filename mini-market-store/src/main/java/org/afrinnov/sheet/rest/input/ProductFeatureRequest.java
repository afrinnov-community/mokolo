package org.afrinnov.sheet.rest.input;

import jakarta.validation.constraints.NotBlank;

public record ProductFeatureRequest(@NotBlank String clef, @NotBlank String label) {
}
