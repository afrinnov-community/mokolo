package org.afrinnov.sheet.rest.support.input;

import jakarta.validation.constraints.NotEmpty;

public record TelephoneCodeRequest(@NotEmpty String code, @NotEmpty String countryCode, String format) {
}
