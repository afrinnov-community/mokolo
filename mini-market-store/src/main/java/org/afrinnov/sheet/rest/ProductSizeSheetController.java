package org.afrinnov.sheet.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.id.ProductSku;
import org.afrinnov.sheet.rest.input.ProductSizeRequest;
import org.afrinnov.sheet.rest.input.SizeCodeRequest;
import org.afrinnov.sheet.services.ProductSizeSheetService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/product/sheet/{sku}/sizes")
@RequiredArgsConstructor
public class ProductSizeSheetController {
    private final ProductSizeSheetService service;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addSizes(@PathVariable String sku,
                                                 @RequestBody @Valid List<ProductSizeRequest> requests) {
       service.addSizes(new ProductSku(sku), requests);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void removeSizes(@PathVariable String sku,
                         @RequestBody @Valid List<SizeCodeRequest> requests) {
        service.removeSizes(new ProductSku(sku), requests);
    }
}
