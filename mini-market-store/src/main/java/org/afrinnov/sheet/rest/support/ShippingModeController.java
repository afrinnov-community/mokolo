package org.afrinnov.sheet.rest.support;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.ShippingMode;
import org.afrinnov.sheet.id.ShippingModeId;
import org.afrinnov.sheet.rest.support.input.ShippingModeRequest;
import org.afrinnov.sheet.rest.support.output.ShippingModeResponse;
import org.afrinnov.sheet.services.ShippingModeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/shipping-mode")
@RequiredArgsConstructor
public class ShippingModeController {
    private final ShippingModeService shippingModeService;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ShippingModeResponse save(@RequestBody @Valid ShippingModeRequest request) {
        ShippingModeId countryId = shippingModeService.create(request);
        return new ShippingModeResponse(request.mode(), "CREATED", "Id:" + countryId.mode());
    }

    @GetMapping("/{mode}")
    @ResponseStatus(HttpStatus.OK)
    public ShippingMode getChippingMode(@PathVariable String mode) {
        return shippingModeService.getShippingMode(mode);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ShippingMode> getShippingModes() {
        return shippingModeService.getShippingModes();
    }
}
