package org.afrinnov.sheet.rest.support.output;

import org.afrinnov.sheet.entities.CategoryTypeEntity;

public record CategoryType(String code, String name, String status) {
    public static CategoryType make(CategoryTypeEntity entity) {
        return new CategoryType(entity.getCode(), entity.getName(), entity.getStatus().name());
    }
}
