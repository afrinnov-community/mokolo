package org.afrinnov.sheet.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.afrinnov.sheet.services.upload.StorageService;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;

@RestController
@RequestMapping("/api/files")
@RequiredArgsConstructor
@Slf4j
public class FilesController {
    private final StorageService storageService;

    @PostMapping(value = "/upload", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<Map<String, Object>> upload(@RequestParam("file") MultipartFile file) {

        storageService.save(file);
        return makeResponse(file);
    }

    private static ResponseEntity<Map<String, Object>> makeResponse(MultipartFile file) {
        Map<String, Object> map = new HashMap<>();

        // Populate the map with file details
        map.put("fileName", file.getOriginalFilename());
        map.put("fileSize", file.getSize());
        map.put("fileContentType", file.getContentType());

        // File upload is successful
        map.put("message", "File upload done");
        return ResponseEntity.ok(map);
    }

    @PostMapping(value = "/upload-with-mark", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<Map<String, Object>> uploadWithMark(@RequestParam("file") MultipartFile file) {

        storageService.saveWithMark(file);
        return makeResponse(file);
    }

    @GetMapping("/")
    public List<String> listUploadedFiles() {
        return storageService.loadAll().stream().map(
                        path -> MvcUriComponentsBuilder.fromMethodName(FilesController.class,
                                "openServeFile", path.getFileName().toString()).build().toUri().toString())
                .toList();
    }

    @GetMapping("/files/{filename:.+}")
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);

        if (file == null)
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok().header(CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @GetMapping("/open/{filename:.+}")
    public ResponseEntity<Resource> openServeFile(@PathVariable String filename) throws IOException {

        Resource file = storageService.loadAsResource(filename);

        if (file == null)
            return ResponseEntity.notFound().build();
        String contentType = Files.probeContentType(file.getFile().toPath());

        return ResponseEntity.ok()
                .contentType(makeContentType(contentType))
                .body(file);
    }

    private static MediaType makeContentType(String contentType) {
        try {
            return MediaType.parseMediaType(contentType);
        } catch (Exception ex) {
            log.warn(ex.getMessage());
            return MediaType.APPLICATION_OCTET_STREAM;
        }
    }


}
