package org.afrinnov.sheet.rest.input;


import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Getter
@Builder
@Jacksonized
public class ProductSheetRequest {
    @NotEmpty
    private String title;
    private String description;
    private String categoryCode;
    private String brandCode;
    private List<String> shippingModes;
    private List<String> deliveryModes;
    private List<String> paymentModes;

}
