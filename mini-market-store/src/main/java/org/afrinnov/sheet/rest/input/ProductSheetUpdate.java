package org.afrinnov.sheet.rest.input;


import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Getter
@Builder
@Jacksonized
public class ProductSheetUpdate {
    private String title;
    private String description;
    private String categoryCode;
    private String brandCode;
    private List<String> shippingModes;
    private List<String> deliveryModes;
    private List<String> paymentModes;

}
