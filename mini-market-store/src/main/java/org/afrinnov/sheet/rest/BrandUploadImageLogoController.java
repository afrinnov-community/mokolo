package org.afrinnov.sheet.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.afrinnov.sheet.dto.Brand;
import org.afrinnov.sheet.id.BrandCode;
import org.afrinnov.sheet.rest.support.output.BrandResponse;
import org.afrinnov.sheet.services.BrandAddImageLogoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/brands/{brandCode}")
@RequiredArgsConstructor
@Slf4j
public class BrandUploadImageLogoController {
    private final BrandAddImageLogoService brandImageLogo;

    @PostMapping(value = "/add-image-logo", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public BrandResponse addBrandImageLogo(@PathVariable String brandCode, @RequestParam("file") MultipartFile file) {
           Brand brand = brandImageLogo.addImageLogo(new BrandCode(brandCode), file);
           return new BrandResponse(brand.getLabel(),  brand.getCode(), "CREATED", "URL Image logo "+file.getOriginalFilename());

    }
}
