package org.afrinnov.sheet.rest.output;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class TelephoneCodeValidatedResponse {
    private boolean matches;
    private String format;
    private String phoneNumber;
}
