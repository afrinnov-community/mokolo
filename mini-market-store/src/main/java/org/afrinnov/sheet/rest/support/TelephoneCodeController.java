package org.afrinnov.sheet.rest.support;


import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.id.TelephoneCode;
import org.afrinnov.sheet.id.TelephoneCodeId;
import org.afrinnov.sheet.rest.support.input.TelephoneCodeRequest;
import org.afrinnov.sheet.rest.support.output.TelephoneResponse;
import org.afrinnov.sheet.services.TelephoneCodeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/telephone-code")
@RequiredArgsConstructor
public class TelephoneCodeController {
    private final TelephoneCodeService service;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public TelephoneResponse defineCode(@RequestBody @Valid TelephoneCodeRequest telephoneCode) {
        TelephoneCodeId id = service.create(telephoneCode);
        return new TelephoneResponse(telephoneCode.code(), "CREATED", "Id=" + id.id());
    }

    @DeleteMapping("/{code}")
    public TelephoneResponse removeTelephoneCode(@PathVariable String code) {
        service.delete(new TelephoneCodeId(code));
        return new TelephoneResponse(code, "DELETED", "");
    }

    @GetMapping("/{code}")
    public TelephoneCode getTelephoneCode(@PathVariable String code) {
        return service.getTelephoneCode(new TelephoneCodeId(code));
    }


}
