package org.afrinnov.sheet.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/api/countries/files")
public class CountriesFileController {
    private final Resource countries;

    public CountriesFileController(@Value("classpath:db/countries.json") Resource countries) {
        this.countries = countries;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public String countries() throws IOException {
        return countries.getContentAsString(StandardCharsets.UTF_8);
    }
}
