package org.afrinnov.sheet.rest.support.input;

import jakarta.validation.constraints.NotEmpty;

import java.util.Objects;

public record ColorRequest(String code, @NotEmpty String label, @NotEmpty String value) {
    public boolean hasCode() {
        return Objects.nonNull(code);
    }
}
