package org.afrinnov.sheet.rest.support.output;

public record BrandResponse(String label, String brandCode, String status, String message) {


}
