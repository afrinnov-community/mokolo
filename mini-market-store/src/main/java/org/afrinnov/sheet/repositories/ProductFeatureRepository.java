package org.afrinnov.sheet.repositories;

import org.afrinnov.sheet.entities.FeatureEntity;
import org.afrinnov.sheet.entities.ProductEntity;
import org.afrinnov.sheet.entities.ProductFeatureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.UUID;

public interface ProductFeatureRepository extends JpaRepository<ProductFeatureEntity, UUID> {
    List<ProductFeatureEntity> findDistinctByProduct_Sku(@NonNull String sku);

    void deleteByProductAndFeature(@NonNull ProductEntity product, @NonNull FeatureEntity feature);

    void deleteByProductAndFeatureAndClefIgnoreCase(ProductEntity product, FeatureEntity feature, @NonNull String clef);
}
