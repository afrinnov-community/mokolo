package org.afrinnov.sheet.repositories.search.adapter;

import jakarta.persistence.EntityManager;
import org.afrinnov.sheet.repositories.search.SearchRepository;
import org.afrinnov.sheet.dto.PageDTO;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.hibernate.search.engine.search.query.SearchResult;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Transactional
public class SimpleSearchRepository<T, I extends Serializable> extends SimpleJpaRepository<T, I>
        implements SearchRepository<T, I> {
    private final EntityManager entityManager;

    public SimpleSearchRepository(Class<T> domainClass, EntityManager entityManager) {
        super(domainClass, entityManager);
        this.entityManager = entityManager;
    }

    public SimpleSearchRepository(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

    @Override
    public List<T> searchBy(String text, int limit, String... fields) {
        SearchResult<T> result = getSearchResult(text, limit, 0, fields);
        return result.hits();
    }

    @Override
    public PageDTO<T> searchPageBy(String text, int limit, int offset, String... fields) {
        SearchResult<T> result = getSearchResult(text, limit, offset, fields);
        return new PageDTO<>(result.hits(), result.total().hitCount());
    }

    private SearchResult<T> getSearchResult(String text, int limit, int offset, String[] fields) {
        SearchSession session = Search.session(entityManager);
        return session.search(getDomainClass())
                .where(f -> f.match().fields(fields).matching(text).fuzzy(2))
                .fetch(offset, limit);
    }


}
