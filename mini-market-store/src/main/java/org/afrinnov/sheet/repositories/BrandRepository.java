package org.afrinnov.sheet.repositories;

import org.afrinnov.sheet.entities.BrandEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.UUID;

public interface BrandRepository extends JpaRepository<BrandEntity, UUID> {

    Optional<BrandEntity> findByCode(@NonNull String codeBrand);

}
