package org.afrinnov.sheet.repositories;

import org.afrinnov.sheet.entities.CategoryTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface CategoryTypeRepository extends JpaRepository<CategoryTypeEntity, UUID> {
    Optional<CategoryTypeEntity> findByCode(String code);

    @Query("SELECT categoryType FROM CategoryTypeEntity categoryType WHERE categoryType.code='0005'")
    CategoryTypeEntity findDefaultType();
}