package org.afrinnov.sheet.repositories;

import org.afrinnov.sheet.entities.TelephoneCodeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface TelephoneCodeRepository extends JpaRepository<TelephoneCodeEntity, UUID> {
    Optional<TelephoneCodeEntity> findOneByCode(String code);

    Optional<TelephoneCodeEntity> findOneByIsoCountryCode(String isoCode);
}
