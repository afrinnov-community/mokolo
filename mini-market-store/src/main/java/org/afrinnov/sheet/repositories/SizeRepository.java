package org.afrinnov.sheet.repositories;

import org.afrinnov.sheet.entities.SizeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;
@Repository
public interface SizeRepository extends JpaRepository<SizeEntity, UUID> {
    Optional<SizeEntity> findByCode(String code);

    Optional<SizeEntity> findByLabelIgnoreCase(@NonNull String label);
}
