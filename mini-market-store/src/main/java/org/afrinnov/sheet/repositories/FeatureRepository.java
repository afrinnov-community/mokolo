package org.afrinnov.sheet.repositories;

import org.afrinnov.sheet.entities.FeatureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.UUID;

public interface FeatureRepository extends JpaRepository<FeatureEntity, UUID> {
    Optional<FeatureEntity> findByCodeIgnoreCase(@NonNull String code);
}
