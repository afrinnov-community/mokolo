package org.afrinnov.sheet.repositories;

import org.afrinnov.sheet.entities.FileEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface FileRepository extends JpaRepository<FileEntity, UUID> {
}
