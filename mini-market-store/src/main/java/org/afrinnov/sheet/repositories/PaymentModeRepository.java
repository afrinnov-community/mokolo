package org.afrinnov.sheet.repositories;

import org.afrinnov.sheet.entities.PaymentModeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.UUID;

public interface PaymentModeRepository extends JpaRepository<PaymentModeEntity, UUID> {
    Optional<PaymentModeEntity> findByModeIgnoreCase(@NonNull String mode);
}