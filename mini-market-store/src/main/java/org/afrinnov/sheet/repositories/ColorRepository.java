package org.afrinnov.sheet.repositories;

import org.afrinnov.sheet.entities.ColorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.UUID;

public interface ColorRepository extends JpaRepository<ColorEntity, UUID> {
    Optional<ColorEntity> findByCode(String code);

    Optional<ColorEntity> findByLabelIgnoreCase(@NonNull String label);
}