package org.afrinnov.sheet.repositories;

import org.afrinnov.sheet.entities.ShippingModeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.UUID;

public interface ShippingModeRepository extends JpaRepository<ShippingModeEntity, UUID> {
    Optional<ShippingModeEntity> findByModeIgnoreCase(@NonNull String mode);
}
