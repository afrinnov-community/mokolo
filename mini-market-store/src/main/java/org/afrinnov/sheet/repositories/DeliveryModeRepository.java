package org.afrinnov.sheet.repositories;

import org.afrinnov.sheet.entities.DeliveryModeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.UUID;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/12/2023 -- 18:28<br></br>
 * By : @author alexk<br></br>
 * Project : mokolo<br></br>
 * Package : org.afrinnov.common.delivry.service<br></br>
 */
public interface DeliveryModeRepository extends JpaRepository<DeliveryModeEntity, UUID> {
    Optional<DeliveryModeEntity> findByModeIgnoreCase(@NonNull String mode);
}
