package org.afrinnov.sheet.repositories;

import org.afrinnov.sheet.entities.ProductEntity;
import org.afrinnov.sheet.entities.ProductSizeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProductSizeRepository extends JpaRepository<ProductSizeEntity, UUID> {
    List<ProductSizeEntity> findByProduct_Sku(@NonNull String sku);

    Optional<ProductSizeEntity> findByProductAndSize_Code(@NonNull ProductEntity product, @NonNull String code);
}