package org.afrinnov.sheet.repositories;

import org.afrinnov.sheet.entities.CountryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CountryRepository extends JpaRepository<CountryEntity, UUID> {
}
