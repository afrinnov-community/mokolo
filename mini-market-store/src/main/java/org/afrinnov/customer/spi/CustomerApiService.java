package org.afrinnov.customer.spi;

import org.afrinnov.customer.Customer;

public interface CustomerApiService {
    Customer getCustomer(String code);
}
