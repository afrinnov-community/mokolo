package org.afrinnov.customer.dto;

public record CustomerCode(String code) {
    public static CustomerCode code(String code) {
        return new CustomerCode(code);
    }
}
