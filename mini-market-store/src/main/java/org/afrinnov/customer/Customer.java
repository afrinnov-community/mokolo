package org.afrinnov.customer;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class Customer {
    private String code;
    private String phoneNumber;
    private String email;
    private String firstName;
    private String lastName;
}
