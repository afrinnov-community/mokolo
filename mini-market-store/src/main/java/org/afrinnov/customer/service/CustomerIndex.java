package org.afrinnov.customer.service;

import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.afrinnov.customer.entity.CustomerEntity;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.massindexing.MassIndexer;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.hibernate.search.mapper.pojo.massindexing.impl.PojoMassIndexingLoggingMonitor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
@Transactional
@Slf4j
public class CustomerIndex implements ApplicationRunner {
    private final EntityManager entityManager;

    @Override
    public void run(ApplicationArguments args) {
        /*SearchSession searchSession = Search.session(entityManager);
        searchSession.schemaManager().createOrUpdate();
        searchSession.workspace(CustomerEntity.class).refresh();
        MassIndexer indexer = searchSession.massIndexer(CustomerEntity.class)
                .monitor(new PojoMassIndexingLoggingMonitor(1))
                .threadsToLoadObjects(3);
        indexer.start();*/
    }
}
