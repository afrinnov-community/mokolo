package org.afrinnov.customer.service;

import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import org.afrinnov.customer.ItemNotFoundException;
import org.afrinnov.customer.Customer;
import org.afrinnov.customer.dto.CustomerCode;
import org.afrinnov.customer.entity.CustomerEntity;
import org.afrinnov.customer.repository.CustomerRepository;
import org.afrinnov.customer.rest.input.CustomerPatch;
import org.hibernate.search.engine.search.query.SearchResult;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.afrinnov.customer.mapper.CustomerMapper.CUSTOMER_MAPPER;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class CustomersService {
    private final CustomerRepository customerRepository;
    private final EntityManager entityManager;

    public Customer getCustomer(String code) {
        return customerRepository.findByCode(code).map(this::mapEntityToDto)
                .orElseThrow(() -> new ItemNotFoundException("CUSTOMER", code));
    }

    public Page<Customer> getCustomers(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return customerRepository.findAll(pageable)
                .map(this::mapEntityToDto);
    }

    private Customer mapEntityToDto(CustomerEntity entity) {
        return Customer.builder()
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .phoneNumber(entity.getPhoneNumber())
                .email(entity.getEmail())
                .code(entity.getCode())
                .build();
    }

    public List<Customer> searchCustomers(int offset, int limit, String query) {
        SearchSession searchSession = Search.session(entityManager);
        SearchResult<CustomerEntity> result = searchSession.search(CustomerEntity.class)
                .where(f -> f.or().add(f.match().fields("phonenumber", "email").matching(query))
                        .add(f
                                .wildcard()
                                .fields("firstname", "lastname")
                                .matching("*" + query + "*")))
                .fetch(offset , limit);
        List<CustomerEntity> entities = result.hits();

        return entities.stream().map(this::mapEntityToDto).toList();
    }

    @Transactional
    public Customer updateCustomer(CustomerCode code, CustomerPatch patch) {
        CustomerEntity customer = customerRepository.findByCode(code.code())
                .orElseThrow(() -> new IllegalArgumentException("CUSTOMER_NOT_FOUND:" + code.code()));
        CUSTOMER_MAPPER.patch(customer, patch);
        return mapEntityToDto(customer);
    }
}
