package org.afrinnov.customer.service;

import lombok.RequiredArgsConstructor;
import org.afrinnov.security.Messager;
import org.afrinnov.customer.dto.CustomerCode;
import org.afrinnov.customer.rest.input.CustomerInput;
import org.afrinnov.customer.entity.CustomerEntity;
import org.afrinnov.customer.repository.CustomerRepository;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import static org.afrinnov.security.MailTemplate.CREATE_NEW_CUSTOMER;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Service
@RequiredArgsConstructor
@Transactional
public class CustomerCreateService {
    private final ApplicationEventPublisher events;
    private final CustomerRepository customerRepository;

    public CustomerCode create(CustomerInput request) {
        var entity = CustomerEntity.newEntity(request);
        String code = generateCode();
        entity.setCode(code);
        customerRepository.save(entity);
        events.publishEvent(new Messager(request.email(),
                "Create customer", CREATE_NEW_CUSTOMER, Map.of("matricule", code)));
        return new CustomerCode(code);
    }

    private String generateCode() {
        Year year = Year.now();
        return year.format(DateTimeFormatter.ofPattern("yy")) + randomAlphabetic(3) + randomNumeric(8);
    }
}
