package org.afrinnov.customer.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.afrinnov.customer.CustomerNotify;
import org.springframework.modulith.events.ApplicationModuleListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerService {

    @ApplicationModuleListener
    void on(CustomerNotify customerNotify) {
        log.info("{}", customerNotify);
    }
}
