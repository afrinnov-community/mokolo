package org.afrinnov.customer.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.customer.rest.input.CustomerInput;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.NaturalId;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.DocumentId;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;
import org.hibernate.type.SqlTypes;

import java.util.UUID;

@Entity
@Table(name = "c_customer")
@Getter
@Setter
@Indexed(index = "customer")
public class CustomerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    protected UUID sid;
    @DocumentId
    @NaturalId
    private String code;
    @KeywordField(name = "phonenumber")
    private String phoneNumber;
    @KeywordField
    private String email;
    @FullTextField(name = "firstname")
    private String firstName;
    @FullTextField(name = "lastname")
    private String lastName;
    public static CustomerEntity newEntity(CustomerInput request) {
        CustomerEntity entity = new CustomerEntity();
        entity.setEmail(request.email());
        entity.setFirstName(request.firstName());
        entity.setLastName(request.lastName());
        entity.setPhoneNumber(request.phoneNumber());
        return entity;
    }
}
