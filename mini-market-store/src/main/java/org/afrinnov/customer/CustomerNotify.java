package org.afrinnov.customer;

import java.util.Map;

public record CustomerNotify(String customerNumber, String motif, Map<String, Object> data) {
}
