package org.afrinnov.customer.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.customer.dto.CustomerCode;
import org.afrinnov.customer.rest.input.CustomerInput;
import org.afrinnov.customer.service.CustomerCreateService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/customers")
@RequiredArgsConstructor
public class CustomerCreateController {
    private final CustomerCreateService customerService;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, String> save(@RequestBody @Valid CustomerInput customerInput) {
        CustomerCode code = customerService.create(customerInput);
        return Map.of("id", code.code(), "status", "CREATED");
    }
}
