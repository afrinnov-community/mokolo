package org.afrinnov.customer.rest.input;

import jakarta.validation.constraints.Email;

public record CustomerPatch(String phoneNumber,
                            @Email String email, String firstName, String lastName) {
}
