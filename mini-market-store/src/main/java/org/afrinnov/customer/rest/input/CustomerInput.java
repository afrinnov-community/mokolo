package org.afrinnov.customer.rest.input;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

public record CustomerInput(@NotBlank String phoneNumber,
                            @Email String email, String firstName, String lastName) {
}
