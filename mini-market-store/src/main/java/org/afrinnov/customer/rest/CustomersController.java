package org.afrinnov.customer.rest;

import lombok.RequiredArgsConstructor;
import org.afrinnov.customer.Customer;
import org.afrinnov.customer.dto.CustomerCode;
import org.afrinnov.customer.rest.input.CustomerPatch;
import org.afrinnov.customer.service.CustomersService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customers")
@RequiredArgsConstructor
public class CustomersController {
    private final CustomersService customerService;

    @PatchMapping("/{code}")
    @ResponseStatus(HttpStatus.OK)
    public Customer patchCustomer(@PathVariable String code, @RequestBody CustomerPatch customer) {
        return customerService.updateCustomer(CustomerCode.code(code), customer);
    }
    @GetMapping("/{code}")
    @ResponseStatus(HttpStatus.OK)
    public Customer getCustomer(@PathVariable String code) {
        return customerService.getCustomer(code);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<Customer> getCustomers(@RequestParam(name = "pageNumbeer", defaultValue = "0") int pageNumber,
                                       @RequestParam(name = "pageSize", defaultValue = "20") int pageSize) {
        return customerService.getCustomers(pageNumber, pageSize);
    }

    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<Customer> searchCustomers(@RequestParam(name = "offset", defaultValue = "0") int offset,
                                          @RequestParam(name = "limit", defaultValue = "20") int limit, String query) {
        return customerService.searchCustomers(offset, limit, query);
    }
}
