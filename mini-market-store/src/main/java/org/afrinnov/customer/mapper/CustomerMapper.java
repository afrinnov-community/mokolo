package org.afrinnov.customer.mapper;

import org.afrinnov.customer.entity.CustomerEntity;
import org.afrinnov.customer.rest.input.CustomerPatch;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import static org.mapstruct.NullValueCheckStrategy.ALWAYS;
import static org.mapstruct.NullValuePropertyMappingStrategy.IGNORE;

@Mapper(nullValueCheckStrategy = ALWAYS, nullValuePropertyMappingStrategy = IGNORE)
public interface CustomerMapper {
    CustomerMapper CUSTOMER_MAPPER = Mappers.getMapper(CustomerMapper.class);

    @Mapping(target = "sid", ignore = true)
    @Mapping(target = "code", ignore = true)
    void patch(@MappingTarget CustomerEntity customer, CustomerPatch patch);
}
