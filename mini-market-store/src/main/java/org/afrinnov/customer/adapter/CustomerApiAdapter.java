package org.afrinnov.customer.adapter;

import lombok.RequiredArgsConstructor;
import org.afrinnov.customer.spi.CustomerApiService;
import org.afrinnov.customer.ItemNotFoundException;
import org.afrinnov.customer.Customer;
import org.afrinnov.customer.entity.CustomerEntity;
import org.afrinnov.customer.repository.CustomerRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class CustomerApiAdapter implements CustomerApiService {
    private final CustomerRepository customerRepository;

    public Customer getCustomer(String code) {
        return customerRepository.findByCode(code).map(this::mapEntityToDto)
                .orElseThrow(() -> new ItemNotFoundException("CUSTOMER", code));
    }

    private Customer mapEntityToDto(CustomerEntity entity) {
        return Customer.builder()
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .phoneNumber(entity.getPhoneNumber())
                .email(entity.getEmail())
                .code(entity.getCode())
                .build();
    }
}
