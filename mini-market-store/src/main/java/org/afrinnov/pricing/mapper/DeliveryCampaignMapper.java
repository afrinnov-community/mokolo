package org.afrinnov.pricing.mapper;

import org.afrinnov.pricing.entities.DeliveryCampaignEntity;
import org.afrinnov.pricing.rest.input.DeliveryCampaignEdit;
import org.afrinnov.pricing.rest.input.DeliveryCampaignInput;
import org.afrinnov.pricing.rest.output.DeliveryCampaign;
import org.afrinnov.pricing.rest.output.DeliveryCampaignDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper
public interface DeliveryCampaignMapper {
    DeliveryCampaignMapper DELIVERY_CAMPAIGN_MAPPER = Mappers.getMapper(DeliveryCampaignMapper.class);

    @Mapping(target = "sid", ignore = true)
    @Mapping(target = "code", ignore = true)
    @Mapping(target = "productIds", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "createDate", ignore = true)
    @Mapping(target = "lastModifiedBy", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    DeliveryCampaignEntity newEntity(DeliveryCampaignInput input);

    @Mapping(target = "sid", ignore = true)
    @Mapping(target = "code", ignore = true)
    @Mapping(target = "productIds", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "createDate", ignore = true)
    @Mapping(target = "lastModifiedBy", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    void patch(@MappingTarget DeliveryCampaignEntity entity, DeliveryCampaignEdit edit);
    @Mapping(target = "countProducts", ignore = true)
    DeliveryCampaign mapToDto(DeliveryCampaignEntity entity);
    @Mapping(target = "products", ignore = true)
    @Mapping(target = "supportedCurrencies", ignore = true)
    DeliveryCampaignDetails mapToDetails(DeliveryCampaignEntity entity);
}
