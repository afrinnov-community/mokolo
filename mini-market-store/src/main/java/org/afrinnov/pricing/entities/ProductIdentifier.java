package org.afrinnov.pricing.entities;

import jakarta.persistence.Embeddable;

import java.util.UUID;

@Embeddable
public record ProductIdentifier(UUID sid, String sku) {
}
