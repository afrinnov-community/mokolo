package org.afrinnov.pricing.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(schema = "eshop", name = "c_delivery_campaign")
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class DeliveryCampaignEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    protected UUID sid;
    private String code;
    private String currency;
    private String designation;
    private String description;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "c_delivery_campaign_product", schema = "eshop", joinColumns = @JoinColumn(name = "delivery_campaign_sid"))
    @AttributeOverrides({
            @AttributeOverride(name = "sid", column = @Column(name = "product_sid")),
            @AttributeOverride(name = "sku", column = @Column(name = "sku"))
    })
    private Set<ProductIdentifier> productIds = new HashSet<>();


    @CreatedBy
    @Column(name = "created_by", updatable = false)
    private String createdBy;

    @CreatedDate
    @Column(name = "created_date", nullable = false, updatable = false)
    private Instant createDate = Instant.now();

    @LastModifiedBy
    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "last_modified_date", nullable = false)
    private Instant lastModifiedDate = Instant.now();

}
