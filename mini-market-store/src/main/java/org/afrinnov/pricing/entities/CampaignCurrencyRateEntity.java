package org.afrinnov.pricing.entities;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(schema = "eshop", name = "c_delivery_campaign_currency_rate")
@Getter
@Setter
@NoArgsConstructor
public class CampaignCurrencyRateEntity {
    @EmbeddedId
    private CampaignCurrencyRatePK id;
    private BigDecimal rate;

    public CampaignCurrencyRateEntity(DeliveryCampaignEntity deliveryCampaign, CurrencyEntity currency) {
        id = new CampaignCurrencyRatePK(deliveryCampaign, currency);
    }

    public String getCurrencyIsoCode() {
        return id.getCurrency().getIsoCode();
    }

    public String getCurrencyName() {
        return id.getCurrency().getName();
    }

    @Embeddable
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode
    public static class CampaignCurrencyRatePK implements Serializable {
        @Serial
        private static final long serialVersionUID = 42L;
        @ManyToOne
        @JoinColumn(name = "delivery_campaign_sid")
        private DeliveryCampaignEntity deliveryCampaign;

        @ManyToOne
        @JoinColumn(name = "currency_iso_code")
        private CurrencyEntity currency;

    }
}
