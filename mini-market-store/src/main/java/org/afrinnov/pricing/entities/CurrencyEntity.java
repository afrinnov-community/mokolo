package org.afrinnov.pricing.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.pricing.rest.input.CurrentInput;

@Entity
@Table(schema = "eshop", name = "c_currency")
@Getter
@Setter
public class CurrencyEntity {
    @Id
    private String isoCode;
    private String symbol;
    private String name;

    public static CurrencyEntity newEntity(String isoCode, CurrentInput currentInput) {
        var entity = new CurrencyEntity();
        entity.setSymbol(currentInput.symbole());
        entity.setName(currentInput.name());
        entity.setIsoCode(isoCode);
        return entity;
    }
}
