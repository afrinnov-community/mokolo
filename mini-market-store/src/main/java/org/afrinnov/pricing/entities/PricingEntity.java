package org.afrinnov.pricing.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.pricing.dto.PricingNature;
import org.afrinnov.pricing.rest.input.PriceRequest;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table( name = "e_pricing")
@Getter
@Setter
public class PricingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID sid;
    private String code;
    private String sku;
    @Enumerated(EnumType.STRING)
    private PricingNature nature;
    private boolean current;
    private double purchasePrice;
    private String currency;
    private double sellingPrice;
    private LocalDateTime startDate;
    private LocalDateTime dueDate;


    public static PricingEntity newPrice(PriceRequest request) {
        var entity = new PricingEntity();
        entity.setSellingPrice(request.getSellingPrice());
        entity.setDueDate(request.getDueDate());
        entity.setCurrency(request.getCurrency());
        entity.setPurchasePrice(request.getPurchasePrice());
        entity.setStartDate(request.getStartDate());
        return entity;
    }
}
