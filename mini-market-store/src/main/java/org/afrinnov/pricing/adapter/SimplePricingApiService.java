package org.afrinnov.pricing.adapter;

import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.PricingItem;
import org.afrinnov.pricing.spi.PricingApiService;
import org.afrinnov.pricing.entities.PricingEntity;
import org.afrinnov.pricing.repository.CurrencyRepository;
import org.afrinnov.pricing.repository.PricingRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.afrinnov.pricing.PricingSpecs;
import org.afrinnov.pricing.ProductPricingDto;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SimplePricingApiService implements PricingApiService {
    private final PricingRepository pricingRepository;
    private final CurrencyRepository currencyRepository;

    @Override
    public void checkPrice(PricingItem pricingItem) {
        PricingEntity pricing = pricingRepository.findBySkuAndCode(pricingItem.sku(), pricingItem.pricingRef())
                .orElseThrow(() -> new IllegalArgumentException("PRICING_NOT_DEFINE:" + pricingItem.notDefineMsg()));
        if (!pricing.isCurrent()) {
            throw new IllegalArgumentException("PRICING_NOT_CURRENT:" + pricingItem.notDefineMsg());
        }
    }

    @Override
    public void checkCurrency(String currency) {
        currencyRepository.findByIsoCodeIgnoreCase(currency)
                .orElseThrow(() -> new IllegalArgumentException("CURRENCY_NOT_FOUND:" + currency));
    }

    public List<ProductPricingDto> getAllProductPricing() {
        return pricingRepository.findAll(PricingSpecs.getAllPricingByNatureSpecification()).stream().map(pricingEntity ->
                        ProductPricingDto.builder()
                                .sku(pricingEntity.getSku())
                                .nature(pricingEntity.getNature().name())
                                .price(pricingEntity.getSellingPrice())
                                .build())
                .toList();
    }

    @Override
    public Page<ProductPricingDto> getAllProductPricing(String nature, int page, int size) {
        return pricingRepository.findAll(PricingSpecs.getAllPricingByNatureSpecification(nature), PageRequest.of(page, size)).map(pricingEntity ->
                ProductPricingDto.builder()
                        .sku(pricingEntity.getSku())
                        .nature(pricingEntity.getNature().name())
                        .price(pricingEntity.getSellingPrice())
                        .build());
    }
}
