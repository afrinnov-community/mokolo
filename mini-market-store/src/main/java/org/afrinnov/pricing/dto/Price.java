package org.afrinnov.pricing.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;

@Getter
@Builder
@Jacksonized
public class Price {
    private String code;
    private String sku;
    private String nature;
    private boolean current;
    private double purchasePrice;
    private String currency;
    private double sellingPrice;
    private LocalDateTime startDate;
    private LocalDateTime dueDate;
}
