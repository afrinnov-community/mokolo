package org.afrinnov.pricing.dto;

public enum PricingNature {
    CLASSIC,SALE_PERIOD, BLACK_FRIDAY, PRE_ORDER, PROMOTION
}
