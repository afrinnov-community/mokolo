package org.afrinnov.pricing.dto;

import java.util.List;

public record Prices(List<Price>prices) {
}
