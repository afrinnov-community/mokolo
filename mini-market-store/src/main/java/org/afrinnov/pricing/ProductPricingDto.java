package org.afrinnov.pricing;

import lombok.Builder;
import lombok.Getter;

@Builder(toBuilder = true)
@Getter
public class ProductPricingDto {
    private String sku;
    private String nature;
    private double price;
}
