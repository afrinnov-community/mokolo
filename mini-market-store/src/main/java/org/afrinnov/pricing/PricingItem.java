package org.afrinnov.pricing;

import java.math.BigDecimal;

public record PricingItem(String sku, String pricingRef, BigDecimal price) {
    public String notDefineMsg() {
        return sku + ":" + pricingRef;
    }
}
