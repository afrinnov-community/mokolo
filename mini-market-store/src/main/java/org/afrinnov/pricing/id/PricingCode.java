package org.afrinnov.pricing.id;

public record PricingCode(String code) {
}
