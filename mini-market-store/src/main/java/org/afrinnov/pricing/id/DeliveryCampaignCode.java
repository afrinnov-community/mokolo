package org.afrinnov.pricing.id;

public record DeliveryCampaignCode(String code) {
}
