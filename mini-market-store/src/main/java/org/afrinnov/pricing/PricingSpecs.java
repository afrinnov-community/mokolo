package org.afrinnov.pricing;

import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;
import org.afrinnov.pricing.dto.PricingNature;
import org.afrinnov.pricing.entities.PricingEntity;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public class PricingSpecs {
    public static Specification<PricingEntity> getAllPricingByNatureSpecification() {
        return (root, query, builder) -> {
            List<PricingNature> pricingNatures = List.of(PricingNature.CLASSIC,
                    PricingNature.PRE_ORDER, PricingNature.SALE_PERIOD);

            Expression<PricingNature> statusExpression = root.get("nature");
            Predicate pricingNaturePredicate = statusExpression.in(pricingNatures);

            Predicate pricingCurrentPredicate = builder.isTrue(root.get("current"));

            return builder.and(pricingNaturePredicate, pricingCurrentPredicate);
        };
    }

    public static Specification<PricingEntity> getAllPricingByNatureSpecification(String nature) {
        return (root, query, builder) -> {
            Predicate pricingCurrentPredicate = builder.isTrue(root.get("current"));
            Predicate pricingNaturePredicate = builder.equal(root.<PricingNature>get("nature"), PricingNature.valueOf(nature));

            return builder.and(pricingNaturePredicate, pricingCurrentPredicate);
        };
    }
}
