package org.afrinnov.pricing.services;

import org.afrinnov.pricing.dto.Price;
import org.afrinnov.pricing.entities.PricingEntity;
import org.afrinnov.pricing.rest.input.PricingPatch;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;
import static org.mapstruct.NullValueCheckStrategy.ALWAYS;
import static org.mapstruct.NullValuePropertyMappingStrategy.IGNORE;

@Mapper(implementationPackage = "org.afrinnov.pricing.services.mapper",
        nullValueCheckStrategy = ALWAYS,
        nullValuePropertyMappingStrategy = IGNORE,
        implementationName = "Simple<CLASS_NAME>",
        componentModel = SPRING)
public interface PricingMapper {

    @Mapping(target = "sid", ignore = true)
    @Mapping(target = "sku", ignore = true)
    @Mapping(target = "code", ignore = true)
    @Mapping(target = "current", ignore = true)
    void patchDtoToEntity(@MappingTarget PricingEntity entity, PricingPatch patch);

    Price mapEntityToDto(PricingEntity pricing);
}
