package org.afrinnov.pricing.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.ItemNotFoundException;
import org.afrinnov.pricing.dto.Price;
import org.afrinnov.pricing.dto.Prices;
import org.afrinnov.pricing.dto.PricingNature;
import org.afrinnov.pricing.id.PricingCode;
import org.afrinnov.pricing.rest.input.PriceRequest;
import org.afrinnov.pricing.rest.input.PricingPatch;
import org.afrinnov.pricing.rest.output.ActivatingPricingResult;
import org.afrinnov.pricing.schedule.service.PricingScheduleService;
import org.afrinnov.sheet.spi.ProductSheetApiService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
@Transactional
@RequiredArgsConstructor
public class PricingDefineService {
    private final ProductSheetApiService productSheetApiService;
    private final PricingService pricingService;
    private final PricingScheduleService pricingScheduleService;


    public void update(String sku, PricingCode pricingCode, PricingPatch request) {
        productSheetApiService.checkProduct(sku);
        pricingService.update(sku, pricingCode, request);
    }

    public Price getPricing(String sku, PricingCode pricingCode) {
        productSheetApiService.checkProduct(sku);
        return pricingService.getPricing(sku, pricingCode);
    }

    public Prices getPricings(String sku) {
        productSheetApiService.checkProduct(sku);
        return new Prices(pricingService.getPricings(sku));
    }

    public ActivatingPricingResult activatePricing(String sku, PricingCode pricingCode) {
        productSheetApiService.checkProduct(sku);
        AtomicReference<ActivatingPricingResult> result = new AtomicReference<>();
        pricingService.getActivePricing(sku)
                .ifPresentOrElse(active -> {
                            pricingService.deactivatePricing(new PricingCode(active.getCode()));
                            pricingService.activatePricing(pricingCode);
                            result.set(new ActivatingPricingResult(pricingCode.code(), null, "CHANGED"));
                        },
                        () -> {
                            pricingService.activatePricing(pricingCode);
                            result.set(new ActivatingPricingResult(pricingCode.code(), null, "DEFINE"));
                        });
        return result.get();
    }

    public ActivatingPricingResult deactivatePricing(String sku, PricingCode pricingCode) {
        productSheetApiService.checkProduct(sku);
        Price price = pricingService.getPricing(pricingCode)
                .orElseThrow(() -> new ItemNotFoundException("PRICING", pricingCode.code()));
        checkDefaultPricingPricing(price);
        checkActivatePricing(price);
        pricingService.deactivatePricing(pricingCode);
        PricingCode classicPricingCode = pricingService.activateClassicPricing(sku);
        return new ActivatingPricingResult(classicPricingCode.code(), "CLASSIC", "CHANGED");
    }

    private void checkDefaultPricingPricing(Price price) {
        if ("CLASSIC".equals(price.getNature())) {
            throw new IllegalArgumentException("[CLASSIC] cannot be deactivate manually");
        }
    }

    private void checkActivatePricing(Price price) {
        if (!price.isCurrent()) {
            throw new IllegalArgumentException("[" + price.getCode() + "] is not current activate pricing");
        }
    }

    public PricingCode createPrice(String sku, PricingNature nature, PriceRequest request) {
        productSheetApiService.checkProduct(sku);
        List<Price> price = pricingService.findPricings(sku, nature);
        if(!price.isEmpty()) {
            throw new IllegalArgumentException("PRICE_NATURE_ALREADY_DEFINE");
        }
        PricingCode pricingCode = pricingService.createPrice(sku, nature, request);
        if (request.hasStartDate() && request.hasDueDate()) {
            pricingScheduleService.createStartDate(pricingCode, request.getStartDate().toLocalDate());
            pricingScheduleService.createDueDate(pricingCode, request.getDueDate().toLocalDate());
        }
        return pricingCode;
    }
}
