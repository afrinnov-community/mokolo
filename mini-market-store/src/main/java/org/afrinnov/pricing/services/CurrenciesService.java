package org.afrinnov.pricing.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.entities.CurrencyEntity;
import org.afrinnov.pricing.repository.CurrencyRepository;
import org.afrinnov.pricing.rest.input.CurrentInput;
import org.afrinnov.pricing.rest.output.Currency;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class CurrenciesService {

    private final CurrencyRepository currencyRepository;

    public List<Currency> getCurrencies() {
        return currencyRepository.findAll().stream()
                .map(this::mapEntityToDto).toList();
    }

    private Currency mapEntityToDto(CurrencyEntity entity) {
        return new Currency(entity.getIsoCode(), entity.getSymbol(), entity.getName());
    }

    public Currency update(String isoCode, CurrentInput currentInput) {
        Optional<CurrencyEntity> currency = currencyRepository.findByIsoCode(isoCode);
        if (currency.isPresent()) {
            CurrencyEntity entity = currency.get();
            entity.setName(currentInput.name());
            entity.setSymbol(currentInput.symbole());
            currencyRepository.save(entity);
            return mapEntityToDto(entity);
        }
        CurrencyEntity entity = CurrencyEntity.newEntity(isoCode, currentInput);
        return mapEntityToDto(currencyRepository.save(entity));
    }

    public Optional<Currency> getCurrency(String isoCode) {
        return currencyRepository.findByIsoCode(isoCode)
                .map(this::mapEntityToDto);
    }
}
