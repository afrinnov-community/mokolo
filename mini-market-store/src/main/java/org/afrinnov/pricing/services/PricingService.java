package org.afrinnov.pricing.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.ItemNotFoundException;
import org.afrinnov.pricing.dto.Price;
import org.afrinnov.pricing.dto.PricingNature;
import org.afrinnov.pricing.entities.PricingEntity;
import org.afrinnov.pricing.id.PricingCode;
import org.afrinnov.pricing.repository.PricingRepository;
import org.afrinnov.pricing.rest.input.PriceRequest;
import org.afrinnov.pricing.rest.input.PricingPatch;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.Year;
import java.util.List;
import java.util.Optional;

import static java.time.format.DateTimeFormatter.ofPattern;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Service
@RequiredArgsConstructor
public class PricingService {
    private final PricingRepository pricingRepository;
    private final PricingMapper pricingMapper;


    public PricingCode createPrice(String sku, PricingNature nature, PriceRequest request) {
        var entity = PricingEntity.newPrice(request);
        entity.setSku(sku);
        entity.setNature(nature);
        entity.setCode(generateCode());
        pricingRepository.save(entity);
        return new PricingCode(entity.getCode());
    }

    private String generateCode() {
        Year year = Year.now();
        return year.format(ofPattern("yy")) + "PRG" + randomNumeric(8) + randomAlphabetic(4);
    }

    public void update(String sku, PricingCode pricingCode, PricingPatch request) {
        PricingEntity pricing = pricingRepository.findBySkuAndCodeIgnoreCase(sku, pricingCode.code())
                .orElseThrow(() -> new ItemNotFoundException("PRICING", pricingCode.code()));
        pricingMapper.patchDtoToEntity(pricing, request);
        pricingRepository.save(pricing);
    }

    public Price getPricing(String sku, PricingCode pricingCode) {
        PricingEntity pricing = pricingRepository.findBySkuAndCodeIgnoreCase(sku, pricingCode.code())
                .orElseThrow(() -> new ItemNotFoundException("PRICING", pricingCode.code()));
        return pricingMapper.mapEntityToDto(pricing);
    }

    public List<Price> getPricings(String sku) {
        return pricingRepository.findBySku(sku).stream().map(pricingMapper::mapEntityToDto).toList();
    }

    public Optional<Price> getActivePricing(String sku) {
        return pricingRepository.findBySkuAndCurrentTrue(sku)
                .map(pricingMapper::mapEntityToDto);
    }

    public void deactivatePricing(PricingCode pricingCode) {
        pricingRepository.findByCode(pricingCode.code())
                .ifPresent(entity -> {
                    entity.setCurrent(false);
                    pricingRepository.save(entity);
                });
    }

    public void activatePricing(PricingCode pricingCode) {
        pricingRepository.findByCode(pricingCode.code())
                .ifPresent(entity -> {
                    entity.setCurrent(true);
                    pricingRepository.save(entity);
                });
    }

    public Optional<Price> getPricing(PricingCode pricingCode) {
        return pricingRepository.findByCode(pricingCode.code()).map(pricingMapper::mapEntityToDto);
    }

    public PricingCode activateClassicPricing(String sku) {
        PricingEntity classicPricing = pricingRepository.findOneBySkuAndNature(sku, PricingNature.CLASSIC)
                .orElseThrow(() -> new ItemNotFoundException("CLASSIC PRICING", sku));
        classicPricing.setCurrent(true);
        return new PricingCode(classicPricing.getCode());
    }

    public List<Price> findPricings(String sku, PricingNature nature) {
        return pricingRepository.findBySkuAndNature(sku, nature)
                .stream().map(pricingMapper::mapEntityToDto).toList();
    }

    public void defineCurrentPricing(PricingCode pricingCode) {
        pricingRepository.findByCode(pricingCode.code()).filter(this::isDueDateUpperThanNow).ifPresent(
                entity -> {
                    Optional<PricingEntity> currentPricing = pricingRepository.findBySkuAndCurrentTrue(entity.getSku());
                    entity.setCurrent(true);
                    pricingRepository.save(entity);
                    currentPricing.ifPresent(cp -> {
                        cp.setCurrent(false);
                        pricingRepository.save(cp);
                    });
                }
        );
    }

    public void unDefineCurrentPricing(PricingCode pricingCode) {
        pricingRepository.findByCode(pricingCode.code()).filter(this::isDueDateLessThanNow).ifPresent(
                entity -> {
                    Optional<PricingEntity> currentPricing = pricingRepository.findBySkuAndCurrentTrue(entity.getSku());
                    entity.setCurrent(false);
                    getActivePricing(entity.getSku());
                    pricingRepository.save(entity);
                    currentPricing.ifPresent(cp -> {
                        cp.setCurrent(true);
                        pricingRepository.save(cp);
                    });
                }
        );
    }

    private boolean isDueDateUpperThanNow(PricingEntity pricing) {
        return (pricing.getDueDate()).isAfter(LocalDateTime.now());
    }

    private boolean isDueDateLessThanNow(PricingEntity pricing) {
        return !isDueDateUpperThanNow(pricing);
    }
}
