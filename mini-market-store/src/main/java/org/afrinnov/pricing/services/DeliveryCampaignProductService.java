package org.afrinnov.pricing.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.entities.DeliveryCampaignEntity;
import org.afrinnov.pricing.entities.ProductIdentifier;
import org.afrinnov.pricing.repository.DeliveryCampaignRepository;
import org.afrinnov.sheet.spi.ProductSheetApiService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class DeliveryCampaignProductService {
    private final ProductSheetApiService productSheetApiService;
    private final DeliveryCampaignRepository deliveryCampaignRepository;



    public void addProducts(String campaignCode, List<String> skus) {
        Set<ProductIdentifier> identifiers = skus.stream().map(productSheetApiService::getProductIdentifier).filter(Optional::isPresent)
                .map(Optional::get).map(id -> new ProductIdentifier(id.sid(), id.sku())).collect(Collectors.toSet());
        DeliveryCampaignEntity entity = deliveryCampaignRepository.findByCode(campaignCode)
                .orElseThrow(() -> new IllegalArgumentException("Campaign not found:" + campaignCode));
        entity.getProductIds().addAll(identifiers);
        deliveryCampaignRepository.save(entity);
    }
}
