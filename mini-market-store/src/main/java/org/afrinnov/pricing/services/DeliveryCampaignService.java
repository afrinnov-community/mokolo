package org.afrinnov.pricing.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.entities.CampaignCurrencyRateEntity;
import org.afrinnov.pricing.entities.DeliveryCampaignEntity;
import org.afrinnov.pricing.id.DeliveryCampaignCode;
import org.afrinnov.pricing.repository.CampaignCurrencyRateRepository;
import org.afrinnov.pricing.repository.DeliveryCampaignRepository;
import org.afrinnov.pricing.rest.input.DeliveryCampaignEdit;
import org.afrinnov.pricing.rest.input.DeliveryCampaignInput;
import org.afrinnov.pricing.rest.output.CurrencyRate;
import org.afrinnov.pricing.rest.output.DeliveryCampaign;
import org.afrinnov.pricing.rest.output.DeliveryCampaignDetails;
import org.afrinnov.sheet.spi.ProductSheetApiService;
import org.afrinnov.sheet.spi.model.ProductShare;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static java.time.format.DateTimeFormatter.ofPattern;
import static org.afrinnov.pricing.mapper.DeliveryCampaignMapper.DELIVERY_CAMPAIGN_MAPPER;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Service
@Transactional
@RequiredArgsConstructor
public class DeliveryCampaignService {

    private final DeliveryCampaignRepository deliveryCampaignRepository;
    private final CampaignCurrencyRateRepository campaignCurrencyRateRepository;
    private final ProductSheetApiService productSheetApiService;

    public DeliveryCampaignCode initialize(DeliveryCampaignInput deliveryCampaignInput) {
        var entity = DELIVERY_CAMPAIGN_MAPPER.newEntity(deliveryCampaignInput);
        var generatedCode = generateCode();
        entity.setCode(generatedCode);
        deliveryCampaignRepository.save(entity);
        return new DeliveryCampaignCode(generatedCode);
    }

    private String generateCode() {
        return LocalDate.now().format(ofPattern("yyMMdd")) + randomNumeric(5) + randomNumeric(5);
    }

    public DeliveryCampaignCode edit(String campaignCode, DeliveryCampaignEdit deliveryCampaignEdit) {
        DeliveryCampaignEntity entity = deliveryCampaignRepository.findByCode(campaignCode)
                .orElseThrow(() -> new IllegalArgumentException("Campaign not found:" + campaignCode));
        DELIVERY_CAMPAIGN_MAPPER.patch(entity, deliveryCampaignEdit);
        return new DeliveryCampaignCode(campaignCode);
    }

    public Page<DeliveryCampaign> allDeliveryCampaigns(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createDate"));
        return deliveryCampaignRepository.findAll(request)
                .map(DeliveryCampaignService::buildDeliveryCampaign);
    }

    private static DeliveryCampaign buildDeliveryCampaign(DeliveryCampaignEntity entity) {
        return DELIVERY_CAMPAIGN_MAPPER.mapToDto(entity).toBuilder().countProducts(entity.getProductIds().size()).build();
    }

    public DeliveryCampaignDetails details(String campaignCode) {
        DeliveryCampaignEntity deliveryCampaign = deliveryCampaignRepository.findByCode(campaignCode)
                .orElseThrow();
        return DELIVERY_CAMPAIGN_MAPPER.mapToDetails(deliveryCampaign).toBuilder()
                .products(makeProduct(deliveryCampaign))
                .supportedCurrencies(mapEntityToCurrencyRate(deliveryCampaign))
                .build();
    }

    private List<CurrencyRate> mapEntityToCurrencyRate(DeliveryCampaignEntity deliveryCampaign) {
        return campaignCurrencyRateRepository.findById_DeliveryCampaign(deliveryCampaign)
                .stream().map(DeliveryCampaignService::buildCurrencyRate).toList();
    }

    private static CurrencyRate buildCurrencyRate(CampaignCurrencyRateEntity entity) {
        return new CurrencyRate(entity.getCurrencyIsoCode(), entity.getCurrencyName(), entity.getRate());
    }

    private List<ProductShare> makeProduct(DeliveryCampaignEntity entity) {
        return entity.getProductIds().stream()
                .map(identifier -> productSheetApiService.getProduct(identifier.sku()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
    }
}
