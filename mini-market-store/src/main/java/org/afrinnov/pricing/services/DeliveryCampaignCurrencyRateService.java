package org.afrinnov.pricing.services;

import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.entities.CampaignCurrencyRateEntity;
import org.afrinnov.pricing.entities.DeliveryCampaignEntity;
import org.afrinnov.pricing.repository.CampaignCurrencyRateRepository;
import org.afrinnov.pricing.repository.CurrencyRepository;
import org.afrinnov.pricing.repository.DeliveryCampaignRepository;
import org.afrinnov.pricing.rest.input.CurrencyRateInput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class DeliveryCampaignCurrencyRateService {
    private final DeliveryCampaignRepository deliveryCampaignRepository;
    private final CampaignCurrencyRateRepository campaignCurrencyRateRepository;
    private final CurrencyRepository currencyRepository;

    public void defineCurrencyRates(String campaignCode, List<CurrencyRateInput> currencyRateInputs) {
        DeliveryCampaignEntity deliveryCampaign = deliveryCampaignRepository.findByCode(campaignCode)
                .orElseThrow(() -> new IllegalArgumentException("Campaign not found:" + campaignCode));
        currencyRateInputs.forEach(currencyRateInput -> {
            currencyRepository.findByIsoCode(currencyRateInput.isoCode())
                    .ifPresent(currency -> {
                        campaignCurrencyRateRepository.findById_CurrencyAndId_DeliveryCampaign(currency, deliveryCampaign)
                                .ifPresentOrElse(rateEntity -> {
                                    rateEntity.setRate(currencyRateInput.rate());
                                    campaignCurrencyRateRepository.save(rateEntity);
                                }, () -> {
                                    CampaignCurrencyRateEntity rateEntity = new CampaignCurrencyRateEntity(deliveryCampaign, currency);
                                    rateEntity.setRate(currencyRateInput.rate());
                                    campaignCurrencyRateRepository.save(rateEntity);
                                });

                    });
        });

    }
}
