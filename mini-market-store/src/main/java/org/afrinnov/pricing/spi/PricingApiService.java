package org.afrinnov.pricing.spi;

import org.afrinnov.pricing.PricingItem;
import org.afrinnov.pricing.ProductPricingDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface PricingApiService {
    void checkPrice(PricingItem pricingItem);

    void checkCurrency(String currency);
    List<ProductPricingDto> getAllProductPricing();
    Page<ProductPricingDto> getAllProductPricing(String nature, int page, int size);
}
