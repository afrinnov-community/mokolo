package org.afrinnov.pricing.schedule.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.pricing.schedule.dto.PricingScheduleDueDate;
import org.afrinnov.pricing.schedule.dto.PricingScheduleStartDate;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "pricing_schedule")
@Getter
@Setter
public class PricingScheduleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID sid;
    @Enumerated(EnumType.STRING)
    private PricingScheduleNature nature;
    private boolean scheduled;
    private LocalDate triggerDate;

    @OneToMany(mappedBy = "pricingSchedule")
    List<PricingScheduleRowEntity> pricingsScheduleRows;

    public static PricingScheduleEntity newEntity(PricingScheduleStartDate pricingScheduleStart) {
        PricingScheduleEntity entity = new PricingScheduleEntity();
        entity.setSid(UUID.randomUUID());
        entity.setScheduled(false);
        entity.setNature(PricingScheduleNature.START);
        entity.setTriggerDate(pricingScheduleStart.startDate());
        return entity;
    }

    public static PricingScheduleEntity newEntity(PricingScheduleDueDate pricingScheduleDue) {
        PricingScheduleEntity entity = new PricingScheduleEntity();
        entity.setSid(UUID.randomUUID());
        entity.setScheduled(false);
        entity.setNature(PricingScheduleNature.END);
        entity.setTriggerDate(pricingScheduleDue.dueDate());
        return entity;
    }
}
