package org.afrinnov.pricing.schedule.entities;

public enum PricingScheduleNature {
    START,
    END;
}
