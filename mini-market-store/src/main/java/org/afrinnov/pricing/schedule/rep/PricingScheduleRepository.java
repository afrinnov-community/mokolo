package org.afrinnov.pricing.schedule.rep;

import jakarta.validation.constraints.NotNull;
import org.afrinnov.pricing.schedule.entities.PricingScheduleEntity;
import org.afrinnov.pricing.schedule.entities.PricingScheduleNature;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PricingScheduleRepository extends JpaRepository<PricingScheduleEntity, UUID> {
    Optional<PricingScheduleEntity> findByTriggerDateAndNature(@NotNull LocalDate startDate, @NotNull PricingScheduleNature pricingScheduleNature);

    @Query("""
            SELECT pse
            FROM PricingScheduleEntity pse
            WHERE pse.triggerDate <=:startDate
                 AND pse.nature =:pricingScheduleNature
                 AND pse.scheduled =:scheduled
            """)
    List<PricingScheduleEntity> findAllPricingScheduleByStartDateAndNatureAndScheduled(@Param("startDate") LocalDate startDate, @Param("pricingScheduleNature") PricingScheduleNature pricingScheduleNature, @Param("scheduled") boolean scheduled);

    @Query("""
            SELECT pse
            FROM PricingScheduleEntity pse
            WHERE pse.triggerDate <:dueDate
                 AND pse.nature =:pricingScheduleNature
                 AND pse.scheduled =:scheduled
            """)
    List<PricingScheduleEntity> findAllPricingScheduleByDueDateAndNatureAndScheduled(@Param("dueDate") LocalDate dueDate, @Param("pricingScheduleNature") PricingScheduleNature pricingScheduleNature, @Param("scheduled") boolean scheduled);
}
