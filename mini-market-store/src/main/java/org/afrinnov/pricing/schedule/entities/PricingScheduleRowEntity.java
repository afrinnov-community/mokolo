package org.afrinnov.pricing.schedule.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.pricing.id.PricingCode;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.UUID;

@Entity
@Table(name = "pricing_schedule_row")
@Getter
@Setter
public class PricingScheduleRowEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID sid;
    private String priceCode;
    @ManyToOne
    private PricingScheduleEntity pricingSchedule;

    public static PricingScheduleRowEntity newEntity(PricingCode code, PricingScheduleEntity entitySchedule) {
        PricingScheduleRowEntity entityRow = new PricingScheduleRowEntity();
        entityRow.setSid(UUID.randomUUID());
        entityRow.setPricingSchedule(entitySchedule);
        entityRow.setPriceCode(code.code());
        return entityRow;
    }
}
