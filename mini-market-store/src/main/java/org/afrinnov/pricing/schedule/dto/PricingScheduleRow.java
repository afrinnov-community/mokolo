package org.afrinnov.pricing.schedule.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class PricingScheduleRow {
    private String priceCode;
}
