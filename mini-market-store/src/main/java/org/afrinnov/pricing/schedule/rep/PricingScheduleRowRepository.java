package org.afrinnov.pricing.schedule.rep;

import org.afrinnov.pricing.schedule.entities.PricingScheduleRowEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PricingScheduleRowRepository extends JpaRepository<PricingScheduleRowEntity, UUID> {
}
