package org.afrinnov.pricing.schedule.service;

import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.id.PricingCode;
import org.afrinnov.pricing.schedule.dto.PricingScheduleDueDate;
import org.afrinnov.pricing.schedule.dto.PricingScheduleStartDate;
import org.afrinnov.pricing.schedule.entities.PricingScheduleEntity;
import org.afrinnov.pricing.schedule.entities.PricingScheduleRowEntity;
import org.afrinnov.pricing.schedule.rep.PricingScheduleRepository;
import org.afrinnov.pricing.schedule.rep.PricingScheduleRowRepository;
import org.afrinnov.pricing.services.PricingService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.afrinnov.pricing.schedule.entities.PricingScheduleNature.END;
import static org.afrinnov.pricing.schedule.entities.PricingScheduleNature.START;

@Service
@RequiredArgsConstructor
public class PricingScheduleService {

    private final PricingScheduleRepository pricingScheduleRepository;
    private final PricingScheduleRowRepository pricingScheduleRowRepository;
    private final PricingService pricingService;

    public void createStartDate(PricingCode code, LocalDate startDate) {
        Optional<PricingScheduleEntity> pricingSchedule = pricingScheduleRepository.findByTriggerDateAndNature(startDate, START);
        if (pricingSchedule.isPresent()) {
            pricingScheduleRowRepository.save(PricingScheduleRowEntity.newEntity(code, pricingSchedule.get()));
        } else {
            PricingScheduleEntity entity = PricingScheduleEntity.newEntity(new PricingScheduleStartDate(startDate));
            pricingScheduleRepository.save(entity);
            pricingScheduleRowRepository.save(PricingScheduleRowEntity.newEntity(code, entity));
        }
    }

    public void createDueDate(PricingCode code, LocalDate dueDate) {
        Optional<PricingScheduleEntity> pricingSchedule = pricingScheduleRepository
                .findByTriggerDateAndNature(dueDate, END);
        if (pricingSchedule.isPresent()) {
            pricingScheduleRowRepository.save(PricingScheduleRowEntity.newEntity(code, pricingSchedule.get()));
        } else {
            PricingScheduleEntity entity = PricingScheduleEntity.newEntity(new PricingScheduleDueDate(dueDate));
            pricingScheduleRepository.save(entity);
            pricingScheduleRowRepository.save(PricingScheduleRowEntity.newEntity(code, entity));
        }

    }

    public void process() {
        findAllPricingScheduleStartDate().forEach(pse -> {
            pse.getPricingsScheduleRows().forEach(psre -> pricingService.defineCurrentPricing(new PricingCode(psre.getPriceCode())));
            pse.setScheduled(true);
            pricingScheduleRepository.save(pse);
        });
    }

    public void stopProcessing() {
        findAllPricingScheduleDuetDate().forEach(pse -> {
            pse.getPricingsScheduleRows().forEach(psre -> pricingService.unDefineCurrentPricing(new PricingCode(psre.getPriceCode())));
            pse.setScheduled(true);
            pricingScheduleRepository.save(pse);
        });
    }

    private List<PricingScheduleEntity> findAllPricingScheduleStartDate() {
        return pricingScheduleRepository.
                findAllPricingScheduleByStartDateAndNatureAndScheduled(LocalDate.now(), START, false);
    }

    private List<PricingScheduleEntity> findAllPricingScheduleDuetDate() {
        return pricingScheduleRepository.
                findAllPricingScheduleByDueDateAndNatureAndScheduled(LocalDate.now(), END, false);
    }

}
