package org.afrinnov.pricing.schedule.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;
import org.afrinnov.pricing.schedule.entities.PricingScheduleNature;

import java.time.LocalDate;

@Getter
@Builder
@Jacksonized
public class PricingSchedule {

    private PricingScheduleNature nature;
    private boolean scheduled;
    private LocalDate triggerDate;
}
