package org.afrinnov.pricing.schedule.service;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ScheduleTaskPricingByDate {
    private final PricingScheduleService pricingScheduleService;

    @Scheduled(cron = "0 0 0/4 * * *")
    public void scheduleTaskPricingToTrigge( ) {
        pricingScheduleService.process();
    }

    @Scheduled(cron = "0 0 0/4 * * *")
    public void scheduleTaskPricingToStop( ) {
        pricingScheduleService.stopProcessing();
    }
}
