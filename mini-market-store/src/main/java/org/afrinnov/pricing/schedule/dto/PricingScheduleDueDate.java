package org.afrinnov.pricing.schedule.dto;

import java.time.LocalDate;

public record PricingScheduleDueDate(LocalDate dueDate) {
}
