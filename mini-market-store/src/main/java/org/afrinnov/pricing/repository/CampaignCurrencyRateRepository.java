package org.afrinnov.pricing.repository;

import org.afrinnov.pricing.entities.CampaignCurrencyRateEntity;
import org.afrinnov.pricing.entities.CampaignCurrencyRateEntity.CampaignCurrencyRatePK;
import org.afrinnov.pricing.entities.CurrencyEntity;
import org.afrinnov.pricing.entities.DeliveryCampaignEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Optional;

public interface CampaignCurrencyRateRepository extends JpaRepository<CampaignCurrencyRateEntity, CampaignCurrencyRatePK> {
    Optional<CampaignCurrencyRateEntity> findById_CurrencyAndId_DeliveryCampaign(@NonNull CurrencyEntity currency, @NonNull DeliveryCampaignEntity deliveryCampaign);

    List<CampaignCurrencyRateEntity> findById_DeliveryCampaign(DeliveryCampaignEntity deliveryCampaign);
}