package org.afrinnov.pricing.repository;

import org.afrinnov.pricing.entities.DeliveryCampaignEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.UUID;

public interface DeliveryCampaignRepository extends JpaRepository<DeliveryCampaignEntity, UUID> {
    Optional<DeliveryCampaignEntity> findByCode(@NonNull String code);
}