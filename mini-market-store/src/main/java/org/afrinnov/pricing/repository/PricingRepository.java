package org.afrinnov.pricing.repository;

import org.afrinnov.pricing.dto.PricingNature;
import org.afrinnov.pricing.entities.PricingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PricingRepository extends JpaRepository<PricingEntity, UUID>, JpaSpecificationExecutor<PricingEntity> {
    Optional<PricingEntity> findBySkuAndCodeIgnoreCase(@NonNull String sku, @NonNull String code);

    List<PricingEntity> findBySku(@NonNull String sku);

    Optional<PricingEntity> findBySkuAndCurrentTrue(@NonNull String sku);

    Optional<PricingEntity> findByCode(@NonNull String code);

    Optional<PricingEntity> findOneBySkuAndNature(String sku, PricingNature nature);

    List<PricingEntity> findBySkuAndNature(String sku, PricingNature nature);

    Optional<PricingEntity> findBySkuAndCode(@NonNull String sku, @NonNull String code);
}
