package org.afrinnov.pricing.repository;

import org.afrinnov.pricing.entities.CurrencyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;

import java.util.Optional;

public interface CurrencyRepository extends JpaRepository<CurrencyEntity, String> {
    Optional<CurrencyEntity> findByIsoCode(@NonNull String isoCode);

    @Query("select c from CurrencyEntity c where upper(c.isoCode) = upper(?1)")
    Optional<CurrencyEntity> findByIsoCodeIgnoreCase(String currency);
}