package org.afrinnov.pricing.rest.input;

import static com.google.common.base.Strings.isNullOrEmpty;

public record CurrentInput(String symbole, String name) {
    public boolean hasNoContent() {
        return isNullOrEmpty(symbole) && isNullOrEmpty(name);
    }
}
