package org.afrinnov.pricing.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.id.DeliveryCampaignCode;
import org.afrinnov.pricing.rest.input.DeliveryCampaignEdit;
import org.afrinnov.pricing.rest.input.DeliveryCampaignInput;
import org.afrinnov.pricing.rest.output.DeliveryCampaign;
import org.afrinnov.pricing.rest.output.DeliveryCampaignDetails;
import org.afrinnov.pricing.services.DeliveryCampaignService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/delivery-campaign")
@RequiredArgsConstructor
public class DeliveryCampaignController {
    private final DeliveryCampaignService deliveryCampaignService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DeliveryCampaignCode initializeDeliveryCampaign(@RequestBody @Valid DeliveryCampaignInput deliveryCampaignInput) {
        return deliveryCampaignService.initialize(deliveryCampaignInput);
    }

    @PatchMapping("/{campaignCode}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public DeliveryCampaignCode editDeliveryCampaign(@PathVariable String campaignCode,
                                                     @RequestBody @Valid DeliveryCampaignEdit deliveryCampaignEdit) {
        return deliveryCampaignService.edit(campaignCode, deliveryCampaignEdit);
    }

    @GetMapping
    public Page<DeliveryCampaign> allDeliveryCampaigns(@RequestParam int page, @RequestParam int size) {
        return deliveryCampaignService.allDeliveryCampaigns(page, size);
    }

    @GetMapping("/{campaignCode}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public DeliveryCampaignDetails deliveryCampaignDetails(@PathVariable String campaignCode) {
        return deliveryCampaignService.details(campaignCode);
    }


}
