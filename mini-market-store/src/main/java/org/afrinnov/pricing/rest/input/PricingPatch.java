package org.afrinnov.pricing.rest.input;

import jakarta.validation.constraints.Min;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Builder
@Jacksonized
public class PricingPatch {
    private String nature;
    @Min(0)
    private BigDecimal purchasePrice;
    private String currency;
    @Min(0)
    private BigDecimal sellingPrice;
    private LocalDateTime startDate;
    private LocalDateTime dueDate;
}
