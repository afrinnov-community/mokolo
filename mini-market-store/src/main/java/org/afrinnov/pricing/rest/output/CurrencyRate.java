package org.afrinnov.pricing.rest.output;

import java.math.BigDecimal;

public record CurrencyRate(String isoCode, String name, BigDecimal rate) {
}
