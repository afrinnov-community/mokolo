package org.afrinnov.pricing.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.dto.PricingNature;
import org.afrinnov.pricing.id.PricingCode;
import org.afrinnov.pricing.rest.input.PriceRequest;
import org.afrinnov.pricing.services.PricingDefineService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/product/{sku}/pricing")
@RequiredArgsConstructor
public class PriceController {
    private final PricingDefineService pricingDefineService;

    @PostMapping(value = "/{nature}/nature", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public PricingCode definePricingNature(@PathVariable String sku,
                                           @PathVariable PricingNature nature,
                                           @RequestBody @Valid PriceRequest request) {
        return pricingDefineService.createPrice(sku, nature, request);
    }
}
