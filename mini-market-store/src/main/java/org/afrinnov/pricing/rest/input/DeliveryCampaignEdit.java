package org.afrinnov.pricing.rest.input;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class DeliveryCampaignEdit {
    private String currency;
    private String designation;
    private String description;
}
