package org.afrinnov.pricing.rest;

import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.rest.input.CurrentInput;
import org.afrinnov.pricing.rest.output.Currency;
import org.afrinnov.pricing.services.CurrenciesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/currencies")
@RequiredArgsConstructor
public class CurrenciesController {
    private final CurrenciesService currenciesService;

    @PutMapping("/{isoCode}")
    public ResponseEntity<Currency> putCurrency(@PathVariable String isoCode, @RequestBody CurrentInput currentInput) {
        if(currentInput.hasNoContent()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(currenciesService.update(isoCode, currentInput));
    }
    @GetMapping
    public List<Currency> getCurrencies() {
        return currenciesService.getCurrencies();
    }

    @GetMapping("/{isoCode}")
    public ResponseEntity<Currency> getCurrency(@PathVariable String isoCode) {
        return ResponseEntity.of(currenciesService.getCurrency(isoCode));
    }
}
