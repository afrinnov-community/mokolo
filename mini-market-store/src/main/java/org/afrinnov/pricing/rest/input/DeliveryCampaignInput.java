package org.afrinnov.pricing.rest.input;

import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class DeliveryCampaignInput {
    @NotEmpty
    private String currency;
    @NotEmpty
    private String designation;
    private String description;
}
