package org.afrinnov.pricing.rest;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.dto.Price;
import org.afrinnov.pricing.dto.Prices;
import org.afrinnov.pricing.id.PricingCode;
import org.afrinnov.pricing.rest.input.PricingPatch;
import org.afrinnov.pricing.rest.output.ActivatingPricingResult;
import org.afrinnov.pricing.services.PricingDefineService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/product/{sku}/pricing")
@RequiredArgsConstructor
public class PricingDefineController {
    private final PricingDefineService pricingDefineService;


    @PostMapping("/{code}/activate")
    public ActivatingPricingResult activatePricing(@PathVariable String sku, @PathVariable String code) {
        return pricingDefineService.activatePricing(sku, new PricingCode(code));
    }

    @PostMapping("/{code}/deactivate")
    public ActivatingPricingResult deactivatePricing(@PathVariable String sku, @PathVariable String code) {
        return pricingDefineService.deactivatePricing(sku, new PricingCode(code));
    }

    @PatchMapping("/{code}")
    @ResponseStatus(HttpStatus.OK)
    public void updatePricing(@PathVariable String sku, @PathVariable String code,
                              @RequestBody @Valid PricingPatch request) {
        pricingDefineService.update(sku, new PricingCode(code), request);
    }

    @GetMapping("/{code}")
    @ResponseStatus(HttpStatus.OK)
    public Price getPricing(@PathVariable String sku, @PathVariable String code) {
        return pricingDefineService.getPricing(sku, new PricingCode(code));
    }

    @PatchMapping
    @ResponseStatus(HttpStatus.OK)
    public Prices getPricings(@PathVariable String sku) {
        return pricingDefineService.getPricings(sku);
    }
}
