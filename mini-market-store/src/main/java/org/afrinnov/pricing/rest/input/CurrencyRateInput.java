package org.afrinnov.pricing.rest.input;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;

public record CurrencyRateInput(@NotEmpty String isoCode, @NotNull BigDecimal rate) {
}
