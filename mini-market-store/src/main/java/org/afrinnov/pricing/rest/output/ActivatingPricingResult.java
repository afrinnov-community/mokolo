package org.afrinnov.pricing.rest.output;

public record ActivatingPricingResult(String code, String nature, String message) {
}
