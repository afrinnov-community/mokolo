package org.afrinnov.pricing.rest.output;

import lombok.Builder;
import lombok.Getter;
import org.afrinnov.sheet.spi.model.ProductShare;

import java.util.List;

@Getter
@Builder(toBuilder = true)
public class DeliveryCampaignDetails {
    private String code;
    private String currency;
    private String designation;
    private String description;
    private List<ProductShare> products;
    private List<CurrencyRate> supportedCurrencies;
}
