package org.afrinnov.pricing.rest;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.rest.input.CurrencyRateInput;
import org.afrinnov.pricing.services.DeliveryCampaignCurrencyRateService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/delivery-campaign/{campaignCode}/currency-rates")
@RequiredArgsConstructor
public class DeliveryCampaignCurrencyRateController {
    private final DeliveryCampaignCurrencyRateService currencyRateService;


    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void defineCurrencyRates(@PathVariable String campaignCode,
                                    @RequestBody @Valid List<CurrencyRateInput> currencyRateInputs) {
        currencyRateService.defineCurrencyRates(campaignCode, currencyRateInputs);
    }


}
