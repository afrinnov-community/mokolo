package org.afrinnov.pricing.rest.output;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder(toBuilder = true)
public class DeliveryCampaign {
    private String code;
    private String currency;
    private String designation;
    private String description;
    private int countProducts;
}
