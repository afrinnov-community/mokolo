package org.afrinnov.pricing.rest;

import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.services.DeliveryCampaignProductService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/delivery-campaign/{campaignCode}/add-products")
@RequiredArgsConstructor
public class DeliveryCampaignProductController {
    private final DeliveryCampaignProductService service;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addProducts(@PathVariable String campaignCode, @RequestBody List<String> skus) {
        service.addProducts(campaignCode, skus);
    }


}
