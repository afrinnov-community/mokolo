package org.afrinnov.pricing.rest.input;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Builder
@Jacksonized
public class PriceRequest {
    @NotNull
    private Double purchasePrice;
    @NotEmpty
    private String currency;
    @NotNull
    private Double sellingPrice;
    private String otherNature;
    private LocalDateTime startDate;
    private LocalDateTime dueDate;

    public boolean hasStartDate() {
        return Objects.nonNull(startDate);
    }

    public boolean hasDueDate() {
        return Objects.nonNull(dueDate);
    }
}
