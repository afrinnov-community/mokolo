package org.afrinnov.pricing.rest;

import lombok.RequiredArgsConstructor;
import org.afrinnov.pricing.dto.Prices;
import org.afrinnov.pricing.services.PricingService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/product/{sku}/pricing")
@RequiredArgsConstructor
public class PricingController {
    private final PricingService pricingService;

    @GetMapping
    public Prices getPrices(@PathVariable String sku) {
        return new Prices(pricingService.getPricings(sku));
    }
}
