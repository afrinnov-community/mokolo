package org.afrinnov.pricing.rest.output;

public record Currency(String isoCode, String symbol, String name) {
}
