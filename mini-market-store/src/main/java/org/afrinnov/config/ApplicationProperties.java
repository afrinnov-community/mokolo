package org.afrinnov.config;

import io.jsonwebtoken.io.Decoders;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.time.Duration;

@ConfigurationProperties("afrinnov")
@Component
@Getter
public class ApplicationProperties {
    private final Security security = new Security();
    private Upload upload = new Upload();

    @Getter
    @Setter
    public static class Upload {
        private String location;
        private String localStorageFolder;
        private String databaseStorageFolder;
    }

    @Getter
    public static class Security {
        private final JwtProperties jwt = new JwtProperties();
        @Setter
        private Duration expiredOtp;
        @Setter
        private String type;

        public long expirationTime() {
            return System.currentTimeMillis() + jwt.getExpirationTime().toMillis();
        }

        public byte[] key() {
            return Decoders.BASE64.decode(jwt.getKey());
        }

        public long expirationRefreshTokenTime() {
            return System.currentTimeMillis() + jwt.getRefreshExpirationTime().toMillis();
        }
    }

    @Getter
    @Setter
    public static class JwtProperties {
        private String key;
        private Duration expirationTime;
        private Duration refreshExpirationTime;
    }
}
