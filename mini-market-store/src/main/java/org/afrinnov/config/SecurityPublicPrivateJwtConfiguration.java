package org.afrinnov.config;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import org.afrinnov.config.security.validator.JwtTokenALiveValidator;
import org.afrinnov.security.TokenApiService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

import java.security.Key;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.List;

import static org.afrinnov.security.spi.PublicPrivateUtils.makePrivateKeyFromFile;
import static org.afrinnov.security.spi.PublicPrivateUtils.makePublicKeyFromFile;

@Configuration
public class SecurityPublicPrivateJwtConfiguration {
    public static final String AUTHORITIES_KEY = "auth";
    private final TokenApiService tokenService;
    private final Key publicKey;
    private final Key privateKey;

    public SecurityPublicPrivateJwtConfiguration(TokenApiService tokenService,
                                                 @Value("${afrinnov.security.privateKeyFile}") Resource privateKeyFile,
                                                 @Value("${afrinnov.security.publicKeyFile}") Resource publicKeyFile) throws Exception {
        this.tokenService = tokenService;
        publicKey = makePublicKeyFromFile(publicKeyFile);
        privateKey = makePrivateKeyFromFile(privateKeyFile);
    }

    @Bean
    public JwtDecoder jwtDecoder() {
        NimbusJwtDecoder jwtDecoder = NimbusJwtDecoder.withPublicKey((RSAPublicKey) publicKey).build();
        jwtDecoder.setJwtValidator(createDefault());
        return jwtDecoder;
    }

    @Bean
    public JwtEncoder jwtEncoder() {
        JWK jwk = new RSAKey.Builder((RSAPublicKey) publicKey).privateKey((RSAPrivateKey) privateKey).build();
        JWKSource<SecurityContext> jwks = new ImmutableJWKSet<>(new JWKSet(jwk));
        return new NimbusJwtEncoder(jwks);
    }

    @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverter() {
        JwtGrantedAuthoritiesConverter grantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        grantedAuthoritiesConverter.setAuthorityPrefix("");
        grantedAuthoritiesConverter.setAuthoritiesClaimName(AUTHORITIES_KEY);

        JwtAuthenticationConverter authenticationConverter = new JwtAuthenticationConverter();
        authenticationConverter.setJwtGrantedAuthoritiesConverter(grantedAuthoritiesConverter);
        return authenticationConverter;
    }


    private OAuth2TokenValidator<Jwt> createDefault() {
        return new DelegatingOAuth2TokenValidator<>(List.of(new JwtTimestampValidator(), new JwtTokenALiveValidator(tokenService)));
    }
}
