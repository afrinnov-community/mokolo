package org.afrinnov.config.security.audit;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;

@Component
public class AfrinnovAuditorAware implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .filter(Authentication::isAuthenticated)
                .map(this::extractUserName)
                ;
    }

    private String extractUserName(Authentication authentication) {
        if (authentication instanceof AnonymousAuthenticationToken token) {
            return Objects.toString(token.getPrincipal(), "System");
        }
        if (authentication instanceof UsernamePasswordAuthenticationToken token) {
            return ((User) token.getPrincipal()).getUsername();
        }
        return authentication.getPrincipal().toString();
    }
}
