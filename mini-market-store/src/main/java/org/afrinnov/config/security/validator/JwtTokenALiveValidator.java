package org.afrinnov.config.security.validator;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.afrinnov.security.TokenApiService;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.security.oauth2.jwt.Jwt;

import static org.springframework.security.oauth2.core.OAuth2ErrorCodes.INVALID_TOKEN;

@RequiredArgsConstructor
@Slf4j
public class JwtTokenALiveValidator implements OAuth2TokenValidator<Jwt> {
    private final TokenApiService tokenService;
    @Override
    public OAuth2TokenValidatorResult validate(Jwt token) {
        var isTokenValid = tokenService.isTokenValid(token.getTokenValue());
        if (isTokenValid) {
            return OAuth2TokenValidatorResult.success();
        }
        var oAuth2Error = createOAuth2Error(String.format("Jwt is already revoked %s", token.getNotBefore()));
        return OAuth2TokenValidatorResult.failure(oAuth2Error);
    }

    private OAuth2Error createOAuth2Error(String reason) {
        log.debug("Oauth2: {}", reason);
        return new OAuth2Error(INVALID_TOKEN, reason,
                "https://tools.ietf.org/html/rfc6750#section-3.1");
    }
}
