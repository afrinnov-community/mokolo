package org.afrinnov.config.tenant;

import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.afrinnov.security.spi.JwtLiteUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class SecurityDomain {
    private static final String PRIVATE_TENANT_HEADER = "X-PrivateTenant";
    private final JwtLiteUtil jwtLiteUtil;

    public String getTenantIdFromJwt(HttpServletRequest request) {
        String token = jwtLiteUtil.resolveToken(request);
        if (Objects.isNull(token)) {
            return "public";
        }
        Claims claims = jwtLiteUtil.extractAllClaims(token);
        List<String> tenants = claims.get("tenants", List.class);
        if (tenants.size() == 1) {
            return tenants.getFirst();
        }
        String privateTenant = request.getHeader(PRIVATE_TENANT_HEADER);
        if (Objects.nonNull(privateTenant)) {
            if (tenants.contains(privateTenant)) {
                return privateTenant;
            }
        }
        return "public";
    }
}
