package org.afrinnov.config.tenant;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class TenantRequestInterceptor implements AsyncHandlerInterceptor {
    private final SecurityDomain securityDomain;

    @Override
    public boolean preHandle(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response,
                             @NonNull Object handler) {
        return Optional.of(request).map(securityDomain::getTenantIdFromJwt)
                .map(this::setTenant).orElse(false);
    }

    private boolean setTenant(String tenant) {
        TenantContext.setCurrentTenant(tenant);
        return true;
    }

    @Override
    public void postHandle(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response,
                           @NonNull Object handler, ModelAndView modelAndView) {
      TenantContext.clear();
    }
}
