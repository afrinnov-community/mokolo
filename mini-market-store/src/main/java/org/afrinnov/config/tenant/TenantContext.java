package org.afrinnov.config.tenant;

import org.slf4j.MDC;

import java.util.Objects;

public class TenantContext {
    private static final String LOGGER_TENANT_ID = "tenant_id";
    private static final String DEFAULT_TENANT = "public";
    private static final ThreadLocal<String> currentTenant = new InheritableThreadLocal<>();

    public static void setCurrentTenant(String tenant) {
        MDC.put(LOGGER_TENANT_ID, tenant);
        currentTenant.set(tenant);
    }

    public static String getCurrentTenant() {
        String tenant = currentTenant.get();
        MDC.put(LOGGER_TENANT_ID, tenant);
        return Objects.requireNonNullElse(tenant, DEFAULT_TENANT);
    }

    public static void clear() {
        MDC.clear();
        currentTenant.remove();
    }
}
