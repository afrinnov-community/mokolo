package org.afrinnov.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InitFolderDto {
    private String info;
}
