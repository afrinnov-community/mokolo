package org.afrinnov.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationProvider;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
@RequiredArgsConstructor
@EnableAsync
public class ApplicationConfig implements ApplicationRunner {
    private final ApplicationEventPublisher events;
    @Value("${afrinnov.upload.local-storage-folder}")
    private String localFolder;
    @Value("${afrinnov.upload.database-storage-folder}")
    private String databaseFolder;
    @Value("${afrinnov.upload.location}")
    private String uploadLocation;


    @Bean
    public AuthenticationProvider authenticationProvider(UserDetailsService userDetailsService) {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    //@Bean
    public AuthenticationProvider bearerTokenAuthenticationToken() {
        //BearerTokenAuthenticationToken
        //AuthenticationProvider provider=new AuthenticationProvider
                return null;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Transactional
    public void run(ApplicationArguments args) {
        if ("FROM_DATABASE".equals(uploadLocation)) {
            actToCreateFolder(Paths.get(databaseFolder));
        } else if ("FROM_DISK".equals(uploadLocation)) {
            actToCreateFolder(Paths.get(localFolder));
        }
    }

    private void actToCreateFolder(Path path) {
        if (!Files.exists(path)) {
            events.publishEvent(new InitFolderDto());
        }
    }
    @Bean
    public TaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);
        executor.setMaxPoolSize(100);
        executor.initialize();

        return executor;
    }

}
