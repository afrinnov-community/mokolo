package org.afrinnov.sms.dto;

public enum ProviderType {
    INFOBIP,
    TWILIO,
    NOOP
}
