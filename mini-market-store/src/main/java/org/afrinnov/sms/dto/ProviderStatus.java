package org.afrinnov.sms.dto;

public enum ProviderStatus {
    CURRENT,
    NONE
}
