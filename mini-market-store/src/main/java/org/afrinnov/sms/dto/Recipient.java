package org.afrinnov.sms.dto;

public record Recipient(String phone, String txtSMS) {
}
