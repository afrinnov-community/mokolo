package org.afrinnov.sms.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class SmsDataConfig {
    @NotNull
    private ProviderType providerName;
    @NotNull
    private String url;
    private String apiKey;
    private String token;
    private String accountSid;
    private String senderPhoneNumber;
    @NotNull
    private boolean deactivated;
    private ProviderStatus status;
}
