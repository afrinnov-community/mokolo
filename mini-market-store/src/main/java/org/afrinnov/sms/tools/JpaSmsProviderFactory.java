package org.afrinnov.sms.tools;

import lombok.RequiredArgsConstructor;
import org.afrinnov.sms.dto.ProviderStatus;
import org.afrinnov.sms.dto.ProviderType;
import org.afrinnov.sms.entities.SmsDataConfigEntity;
import org.afrinnov.sms.repository.SmsDataConfigRepository;
import org.afrinnov.sms.service.client.SMSClient;
import org.afrinnov.sms.service.client.impl.InfoBipSMSClientProvider;
import org.afrinnov.sms.service.client.impl.NoOPTSMSClient;
import org.afrinnov.sms.service.client.impl.TwilioSMSClientProvider;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class JpaSmsProviderFactory implements SmsProviderFactory {

    private final List<SMSClient> smsClients;


    private final SmsDataConfigRepository dataConfigRepository;

    @Override
    public SMSClient get() {
        return getSmsClient();
    }

    private SMSClient getSmsClient() {
        Optional<SmsDataConfigEntity> activeDataSmsConfig = dataConfigRepository.findByDeactivatedIsFalseAndStatus(ProviderStatus.CURRENT);
        if(activeDataSmsConfig.isEmpty()){
            return smsClients.stream().filter(client->client instanceof NoOPTSMSClient).findFirst().orElseThrow();
        }
        SmsDataConfigEntity entity = activeDataSmsConfig.get();
        if (entity.getProviderName() == ProviderType.INFOBIP) {
            return smsClients.stream().filter(client->client instanceof InfoBipSMSClientProvider).findFirst().orElseThrow();
        }
        if (entity.getProviderName() == ProviderType.TWILIO) {
            return smsClients.stream().filter(client->client instanceof TwilioSMSClientProvider).findFirst().orElseThrow();
        }
        return smsClients.stream().filter(client->client instanceof NoOPTSMSClient).findFirst().orElseThrow();
    }
}
