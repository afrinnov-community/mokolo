package org.afrinnov.sms.tools;

import org.afrinnov.sms.service.client.SMSClient;

public interface SmsProviderFactory {
    SMSClient get();
}
