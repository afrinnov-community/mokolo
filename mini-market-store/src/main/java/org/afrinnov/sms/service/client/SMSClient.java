package org.afrinnov.sms.service.client;

import org.afrinnov.sms.dto.Recipient;

public interface SMSClient {

    public String sendSMS(Recipient recipient);
}
