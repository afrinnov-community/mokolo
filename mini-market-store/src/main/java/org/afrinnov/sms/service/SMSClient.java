package org.afrinnov.sms.service;

import org.afrinnov.sms.service.dto.Recipient;

public interface SMSClient {

    public String sendSMS(Recipient recipient);
}
