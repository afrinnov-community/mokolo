package org.afrinnov.sms.service.client.impl;

import org.afrinnov.sms.dto.Recipient;
import org.afrinnov.sms.service.client.SMSClient;
import org.springframework.stereotype.Service;

import static org.hibernate.search.backend.lucene.search.sort.impl.LuceneSearchSort.log;

@Service
public class NoOPTSMSClient implements SMSClient {

    @Override
    public String sendSMS(Recipient recipient) {
        log.info("NoOPSMSClient message envoyé !");
        return null;
    }
}
