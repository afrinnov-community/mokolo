package org.afrinnov.sms.service.client.impl;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sms.dto.ProviderType;
import org.afrinnov.sms.dto.Recipient;
import org.afrinnov.sms.entities.SmsDataConfigEntity;
import org.afrinnov.sms.repository.SmsDataConfigRepository;
import org.afrinnov.sms.service.client.SMSClient;
import org.springframework.stereotype.Service;

import static org.hibernate.search.backend.lucene.search.sort.impl.LuceneSearchSort.log;

@Service
@RequiredArgsConstructor
public class TwilioSMSClientProvider implements SMSClient {

    private final SmsDataConfigRepository dataConfigRepository;

    @Override
    public String sendSMS(Recipient recipient) {

        SmsDataConfigEntity provider = dataConfigRepository.findByProviderName(ProviderType.TWILIO).orElseThrow();
        Twilio.init(provider.getAccountSid(), provider.getToken());
        Message message = Message.creator(
                        new com.twilio.type.PhoneNumber(recipient.phone()),
                        new com.twilio.type.PhoneNumber(provider.getSenderPhoneNumber()),
                        recipient.txtSMS())
                .create();

        log.info(message.getSid());

        var responseCode = "200";

        if (message.getErrorCode() != null) {
            return message.getErrorCode() + "";
        }
        return responseCode;
    }
}
