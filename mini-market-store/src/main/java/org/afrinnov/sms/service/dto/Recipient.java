package org.afrinnov.sms.service.dto;

public record Recipient(String phone, String txtSMS) {
}
