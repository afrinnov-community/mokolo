package org.afrinnov.sms.service.client.impl;

import com.infobip.ApiClient;
import com.infobip.ApiException;
import com.infobip.ApiKey;
import com.infobip.BaseUrl;
import com.infobip.api.SmsApi;
import com.infobip.model.SmsAdvancedTextualRequest;
import com.infobip.model.SmsDestination;
import com.infobip.model.SmsTextualMessage;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sms.dto.ProviderType;
import org.afrinnov.sms.dto.Recipient;
import org.afrinnov.sms.entities.SmsDataConfigEntity;
import org.afrinnov.sms.repository.SmsDataConfigRepository;
import org.afrinnov.sms.service.client.SMSClient;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.hibernate.search.backend.lucene.search.sort.impl.LuceneSearchSort.log;

@Service
@RequiredArgsConstructor
public class InfoBipSMSClientProvider implements SMSClient {

    private final SmsDataConfigRepository dataConfigRepository;

    @Override
    public String sendSMS(Recipient recipient) {

        var responseCode = "200";
        SmsDataConfigEntity provider = dataConfigRepository.findByProviderName(ProviderType.INFOBIP).orElseThrow();
        var client = ApiClient.forApiKey(ApiKey.from(provider.getApiKey()))
                .withBaseUrl(BaseUrl.from(provider.getUrl()))
                .build();

        var api = new SmsApi(client);

        var message = new SmsTextualMessage()
                .from("InfoSMSMokolo")
                .destinations(List.of(new SmsDestination().to(recipient.phone())))
                .text(recipient.txtSMS());

        var request = new SmsAdvancedTextualRequest()
                .messages(List.of(message));

        try {
            var response = api.sendSmsMessage(request).execute();
        } catch (ApiException exception) {
            log.error("Error occured : {}", exception);
            log.error("Error occurred: { " + "\nReceived status " + exception.responseStatusCode() + " when calling the Infobip API.%n" + " \n}");
            if (exception.details() != null) {
                log.error("Error occurred: { " + "\nError details: " + exception.details().getText() + " \n}");
            }
            throw new IllegalArgumentException(exception);
        }
        return responseCode;
    }
}
