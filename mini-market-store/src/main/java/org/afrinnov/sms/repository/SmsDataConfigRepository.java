package org.afrinnov.sms.repository;

import jakarta.validation.constraints.NotNull;
import org.afrinnov.sms.dto.ProviderStatus;
import org.afrinnov.sms.entities.SmsDataConfigEntity;
import org.afrinnov.sms.dto.ProviderType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface SmsDataConfigRepository extends JpaRepository<SmsDataConfigEntity, UUID> {
    Optional<SmsDataConfigEntity> findByProviderName(@NotNull ProviderType providerType);

    Optional<SmsDataConfigEntity> findByDeactivatedIsFalseAndStatus(ProviderStatus status);
}
