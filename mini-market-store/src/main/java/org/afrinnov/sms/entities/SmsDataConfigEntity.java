package org.afrinnov.sms.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.afrinnov.sms.dto.ProviderStatus;
import org.afrinnov.sms.dto.ProviderType;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.UUID;

@Entity
@Table(schema = "tools", name = "c_sms_config_provider")
@Getter
@Setter
public class SmsDataConfigEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID sid;
    @Enumerated(EnumType.STRING)
    private ProviderType providerName;
    private String url;
    private String apiKey;
    private String token;
    private String accountSid;
    private String senderPhoneNumber;
    private boolean deactivated;
    private ProviderStatus status;

}
