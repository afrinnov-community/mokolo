package org.afrinnov.rest.advice;

public class TechnicalException extends RuntimeException {
    public TechnicalException(Throwable cause) {
        super(cause);
    }

    public TechnicalException(String message) {
        super(message);
    }
}
