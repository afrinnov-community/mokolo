package org.afrinnov.rest.advice;

import lombok.Getter;

@Getter
public class ElementNotFoundException extends RuntimeException {

    private final String type;
    private final Object reference;

    public ElementNotFoundException(String type, Object reference) {
        this.type = type;
        this.reference = reference;
    }
}
