Feature: Manage telephone code

  @AsAdminUser
  Scenario: Add new telephone code of country
    Given  Record of iso code "SEN" english name "Senegal" and name "Senegal" of new country
    When I post request of country
    And I select "SEN" country
    And I prepare a record with code "237" and country
    When I post request to save telephone code
    Then I must check new telephone code added

  @AsAdminUser
  Scenario: Disable a telephone code of country
    Given  Record of iso code "MAU" english name "Mauritius" and name "Ile Maurice" of new country
    When I post request of country
    And I select "MAU" country
    And I prepare a record with code "238" and country
    And I post request to save telephone code
    When I send to delete "238" telephone code
    Then I get 200 status code when i find "238" telephone code
    And I check current telephone code with enable "false"

  @AsAdminUser
  Scenario: Validate telephone code with given format
    Given  Record of iso code "NIG" english name "Nigeria" and name "Nigeria" of new country
    When I post request of country
    And I select "NIG" country
    And I prepare a record with code "33", country and validation format "^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}"
    And I post request to save telephone code
    Then I request check phone numbers are valide
      | 0033756881732 |true  |
      | +33756881732  |true  |
      | 0756881732    |true  |
      | 756881732     |false |

