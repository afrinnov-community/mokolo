Feature: Manage Delivery mode

  @AsAdminUser
  Scenario: Add new delivery mode
    Given  Record of "BICYCLE" delivery mode and his description "RAS"
    When I post request of delivery mode
    Then I find this delivery mode by "BICYCLE" value
    And  I check "BICYCLE" delivery mode when I retrieve all the delivery mode

