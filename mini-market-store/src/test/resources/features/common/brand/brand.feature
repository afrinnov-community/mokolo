

Feature: Managed Brand

  @AsAdminUser
  Scenario: Add new brand without image of logo
    Given Record of label "NIKE" of a new brand
    When I post request of brand
    And I must get the label of brand

  @AsAdminUser
  Scenario: Add image logo to saved brand
    Given  Record of label "ADIDAS" of a new brand
    When I post request of brand
    And I add image logo to brand
    Then I must get label and image logo of brand