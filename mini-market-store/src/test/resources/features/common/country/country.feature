# new feature
# Tags: optional

Feature: Managed Country

  @AsAdminUser
  Scenario: Add new country
    Given  Record of iso code "CMR" english name "Cameroon" and name "Cameroun" of new country
    When I post request of country
    Then I must get the iso code of country

  @AsAdminUser
  Scenario: As admin i need to retrieve countries with telephone code
    Given  Record of iso code "CIV" english name "Ivory coast" and name "Côte d'ivoire" of new country
    And I select this country
    And I prepare a record with code "225" and country
    When I post request of country
    And I post request to save telephone code
    Then Load countries
    And this element content "225" telephone code of country