Feature: Manage product sheet

  @AsAdminUser
  Scenario: Create exhibit shop
    Given  A prospect informations
    When I send to initialize shop
    Then A mail sent to confirm shop is created
    And  An account with shop role is created
    And  A shop has two validity week
    When A shopper define new password "shuuut2024" with his otp
    And A shopper logs in with new password "shuuut2024"
    When A shopper create user group "seller"
    And  A shopper add "CREATE_COMMAND,VALIDATE_COMMAND" roles for "seller" group
    Then A shopper add "aboubakar" as "seller"

