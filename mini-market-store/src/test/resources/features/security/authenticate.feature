
  Feature: Authenticate User

    Scenario: Generation of user's token after authenticated
      Given I have a user with user name "admin" and password "admin"
      When I post a login request
      Then I get the jwt token
      And I get the refresh token
      When I say hello
      Then I get "ok" as response

    Scenario: Generation token with valid refresh token
      Given I have a user with user name "admin" and password "admin"
      When I post a login request
      And I post refresh token
      Then I check the new jwt token

    @AsAdminUser
    Scenario: Create new account and send code by mail
      Given I have new admin user data information
      When I send a new user registration request
      Then I check if new user request is pending and mail sent to new user address mail

    Scenario: Activate account after receive OTP code
      Given Admin create my account with my user data information
      When I say hello
      Then I receive 401 http status error message
      When After receiving OTP code, i send request with new password and otp code to activate an account
      Then I receive new mail witch confirm account is activated
      When I use my account to connect
      And I say hello
      Then I receive 200 message

    @AsAdminUser
    Scenario: Resend new OTP for user
      Given I have new admin user data information
      And I send a new user registration request
      When I resend a new OTP code
      Then I check if new user receive a otp mail and user status is in pending mode


