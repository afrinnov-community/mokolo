Feature: Manage product categories and sizes

  @AsAdminUser
  Scenario: Initialize a category
    Given  I have the "label" and the "description" of the category
    When I post request to initialize the category
    Then I get category link which content the code of this category
    And  I get request to retrieve the new category

  @AsAdminUser
  Scenario: Initialize category with size
    Given  I record the "label" and the "description" of a size
    When I add the new size to the category
    Then I check the list of sizes in the category