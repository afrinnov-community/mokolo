Feature: Manage product sheet

  @AsAdminUser
  Scenario: Arrival on box of articles
    Given  I have articles in the box
    When I have quantity(3) for each "red" shoes in the box with "24,26,34,35" type of size
    And I have quantity(3) for each "blue" shoes in the box with "36,40,45" type of size
    When I entry "red" product into store
    And  I entry "blue" product into store
    When I request available product
    Then I check two colors list
    And  I check list size(4) for "red" color
    And  I check list size(3) for "blue" color

  @AsAdminUser
  Scenario: Entry and withdrawal in store
    Given  I have articles in the box
    When I have quantity(1) for each "red" shoes in the box with "24" type of size
    And I entry "red" product into store
    When I request available product
    Then  I check list size(1) for "red" color
    When I withdrawal "red" shoes with size "24"
    And I request available product
    Then  I check list size(1) for "red" color
    And I check quantity(0) of "red" and size "24" shoe
