Feature: Manage product sheet

  @AsAdminUser
  Scenario: Define classic pricing
    Given  I have articles in the box
    And I have "classic" pricing with purchase price 10, selling price 15 and currency "EUR"
    When I post a "classic" pricing for an article
    When I request prices of product
    Then I check "CLASSIC" pricing with purchase price 10, selling price 15 and "EUR" currency

