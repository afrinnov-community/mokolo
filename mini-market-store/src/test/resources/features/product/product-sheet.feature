Feature: Manage product sheet

  @AsAdminUser
  Scenario: Initialize product sheet
    Given  I have "base" product sheet data information
    When I post request to initialize the product
    Then I get product sheet link witch content sku of this product
    And  I get request to retrieve the new product

  @AsAdminUser
  Scenario: Initialize product sheet with shipping mode
    Given  I have "product with shipping mode" product sheet data information
    When I post request to initialize the product
    Then I get product sheet link witch content sku of this product
    And  I get request to retrieve the new product
    And I check a supported shipping mode of product

  @AsAdminUser
  Scenario: Add images for product sheet
    Given  I have "base" product sheet data information
    And I post request to initialize the product
    When  I add preview image for product sheet
    And Id add wizard images for product sheet
    Then I check a preview image url of product sheet
    And I check a list of others images url of product sheet