package org.afrinnov.order.rest.input;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.afrinnov.order.entities.OrderEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class OrderAdvancedFilterTest {

    @Test
    void test(@Autowired TestEntityManager entityManager) {
        //Act
        CriteriaBuilder criteriaBuilder = entityManager.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<OrderEntity> criteriaQuery = criteriaBuilder.createQuery(OrderEntity.class);
        Root<OrderEntity> root=criteriaQuery.from(OrderEntity.class);
        Predicate totalPrice = new OrderAdvancedFilter("totalPrice", BigDecimal.valueOf(10_000),
                null, OrderAdvancedOperand.EQUAL).makePredicate(root, criteriaBuilder);
        //Assert
        assertThat(totalPrice).isNotNull();
    }

}