package org.afrinnov.order.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.afrinnov.order.rest.flow.input.ItemRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class CartForTest {
    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void shouldInitialiseCartFor() {
        //Act
        CartFor cartFor = CartFor.init(objectMapper, "123");
        //Assert
        assertThat(cartFor.getId()).isEqualTo("123");
        assertThat(cartFor.getContent()).isEqualTo("{\"customerId\":null,\"items\":null,\"deliveryMode\":null,\"paymentMode\":null,\"shippingMode\":null}");
    }

    @Test
    void shouldDefineGivenData() {
        //Arrange
        CartFor cartFor = CartFor.init(objectMapper, "123");

        //Act
        cartFor.attacheCustom("abc");
        cartFor.definePaymentMode("cash");
        cartFor.defineDeliveryMode("Home");
        cartFor.defineShippingMode("boat");
        //Assert
        assertThat(cartFor.getId()).isEqualTo("123");
        assertThat(cartFor.getContent()).isEqualTo("{\"customerId\":\"abc\",\"items\":null,\"deliveryMode\":\"Home\",\"paymentMode\":\"cash\",\"shippingMode\":\"boat\"}");
    }

    @Test
    void shouldAddAnItem(){

        //Act
        CartFor cartFor = CartFor.init(objectMapper, "123");
        cartFor.addItem(new ItemRequest("abcd", "12zq", "#1523B", BigDecimal.TWO));
        //Assert
        assertThat(cartFor.getId()).isEqualTo("123");
        assertThat(cartFor.getContent()).isEqualTo("{\"customerId\":null,\"items\":[{\"sku\":\"abcd\",\"sizeCode\":\"12zq\",\"colorCode\":\"#1523B\",\"qty\":2}],\"deliveryMode\":null,\"paymentMode\":null,\"shippingMode\":null}");

    }

    @Test
    void shouldAddMoreThanOneItem(){

        //Act
        CartFor cartFor = CartFor.init(objectMapper, "123");
        cartFor.addItem(new ItemRequest("abcd", "12zq", "#1523B", BigDecimal.TWO));
        cartFor.addItem(new ItemRequest("abcde", "12zqr", "#1523BC", BigDecimal.TEN));
        //Assert
        assertThat(cartFor.getId()).isEqualTo("123");
        assertThat(cartFor.getContent()).isEqualTo("{\"customerId\":null,\"items\":[{\"sku\":\"abcd\",\"sizeCode\":\"12zq\",\"colorCode\":\"#1523B\",\"qty\":2},{\"sku\":\"abcde\",\"sizeCode\":\"12zqr\",\"colorCode\":\"#1523BC\",\"qty\":10}],\"deliveryMode\":null,\"paymentMode\":null,\"shippingMode\":null}");

    }
    @Test
    void shouldUpdateQtyForSameItemToAdd(){
        //Act
        CartFor cartFor = CartFor.init(objectMapper, "123");
        cartFor.addItem(new ItemRequest("abcd", "12zq", "#1523B", BigDecimal.TWO));
        cartFor.addItem(new ItemRequest("abcd", "12zq", "#1523B", BigDecimal.TWO));
        //Assert
        assertThat(cartFor.getItems()).isNotEmpty();
        assertThat(cartFor.getId()).isEqualTo("123");
        assertThat(cartFor.getContent()).isEqualTo("{\"customerId\":null,\"items\":[{\"sku\":\"abcd\",\"sizeCode\":\"12zq\",\"colorCode\":\"#1523B\",\"qty\":4}],\"deliveryMode\":null,\"paymentMode\":null,\"shippingMode\":null}");

    }

    @Test
    void shouldUpdateExistingItem(){
        //Act
        CartFor cartFor = CartFor.init(objectMapper, "123");
        cartFor.addItem(new ItemRequest("abcd", "12zq", "#1523B", BigDecimal.TWO));
        cartFor.addItem(new ItemRequest("abcde", "12zqr", "#1523BC", BigDecimal.TEN));
        cartFor.addItem(new ItemRequest("abcd", "12zq", "#1523B", BigDecimal.valueOf(3)));
        //Assert
        cartFor.getItems();
        assertThat(cartFor.getItems()).isNotEmpty();
        assertThat(cartFor.getId()).isEqualTo("123");
        assertThat(cartFor.getContent()).isEqualTo("{\"customerId\":null,\"items\":[{\"sku\":\"abcd\",\"sizeCode\":\"12zq\",\"colorCode\":\"#1523B\",\"qty\":5},{\"sku\":\"abcde\",\"sizeCode\":\"12zqr\",\"colorCode\":\"#1523BC\",\"qty\":10}],\"deliveryMode\":null,\"paymentMode\":null,\"shippingMode\":null}");

    }

    @Test
    void shouldRemoveItemAtFirstPosition(){
        //Act
        CartFor cartFor = CartFor.init(objectMapper, "123");
        cartFor.addItem(new ItemRequest("abcd", "12zq", "#1523B", BigDecimal.TWO));
        cartFor.removeItem(new ItemRequest("abcd", "12zq", "#1523B", BigDecimal.TWO));
        //Assert
        assertThat(cartFor.getItems()).isEmpty();
        assertThat(cartFor.getId()).isEqualTo("123");
        assertThat(cartFor.getContent()).isEqualTo("{\"customerId\":null,\"items\":[],\"deliveryMode\":null,\"paymentMode\":null,\"shippingMode\":null}");

    }

    @Test
    void shouldRemoveItemAtAnyPosition(){
        //Act
        CartFor cartFor = CartFor.init(objectMapper, "123");
        cartFor.addItem(new ItemRequest("abcd", "12zq", "#1523B", BigDecimal.TWO));
        cartFor.addItem(new ItemRequest("abcde", "12zqr", "#1523BC", BigDecimal.TEN));
        cartFor.removeItem(new ItemRequest("abcde", "12zqr", "#1523BC", BigDecimal.TEN));
        //Assert
        assertThat(cartFor.getId()).isEqualTo("123");
        assertThat(cartFor.getContent()).isEqualTo("{\"customerId\":null,\"items\":[{\"sku\":\"abcd\",\"sizeCode\":\"12zq\",\"colorCode\":\"#1523B\",\"qty\":2}],\"deliveryMode\":null,\"paymentMode\":null,\"shippingMode\":null}");

    }

    @Test
    void shouldUpdateQuantityItemWhenReduce(){
        //Act
        CartFor cartFor = CartFor.init(objectMapper, "123");
        cartFor.addItem(new ItemRequest("abcd", "12zq", "#1523B", BigDecimal.TWO));
        cartFor.removeItem(new ItemRequest("abcd", "12zq", "#1523B", BigDecimal.ONE));
        //Assert
        assertThat(cartFor.getId()).isEqualTo("123");
        assertThat(cartFor.getContent()).isEqualTo("{\"customerId\":null,\"items\":[{\"sku\":\"abcd\",\"sizeCode\":\"12zq\",\"colorCode\":\"#1523B\",\"qty\":1}],\"deliveryMode\":null,\"paymentMode\":null,\"shippingMode\":null}");

    }

    @Test
    void shouldAddRemoveUpdateItem(){
        //Act
        CartFor cartFor = CartFor.init(objectMapper, "123");
        cartFor.addItem(new ItemRequest("abcd", "12zq", "#1523B", BigDecimal.TWO));
        cartFor.addItem(new ItemRequest("abcde", "12zqr", "#1523BC", BigDecimal.TEN));
        cartFor.removeItem(new ItemRequest("abcde", "12zqr", "#1523BC", BigDecimal.valueOf(8)));
        cartFor.addItem(new ItemRequest("abcdef", "12zqrf", "#1523BCM", BigDecimal.TEN));
        cartFor.removeItem(new ItemRequest("abcdef", "12zqrf", "#1523BCM", BigDecimal.TEN));
        //Assert
        assertThat(cartFor.getId()).isEqualTo("123");
        assertThat(cartFor.getContent()).isEqualTo("{\"customerId\":null,\"items\":[{\"sku\":\"abcd\",\"sizeCode\":\"12zq\",\"colorCode\":\"#1523B\",\"qty\":2},{\"sku\":\"abcde\",\"sizeCode\":\"12zqr\",\"colorCode\":\"#1523BC\",\"qty\":2}],\"deliveryMode\":null,\"paymentMode\":null,\"shippingMode\":null}");

    }

    @Test
    void shouldReturnCartFor() {
        //Arrange
        CartFor cartFor = CartFor.init(objectMapper, "123");

        //Act
        cartFor.attacheCustom("abc");
        cartFor.definePaymentMode("cash");
        cartFor.defineDeliveryMode("Home");
        cartFor.defineShippingMode("boat");
        cartFor.addItem(new ItemRequest("abcdefghijklm", "co-300", "color-14", BigDecimal.valueOf(5)));
        //Assert
        assertThat(cartFor.getId()).isEqualTo("123");
        assertThat(cartFor.getContent()).isEqualTo("{\"customerId\":\"abc\",\"items\":[{\"sku\":\"abcdefghijklm\",\"sizeCode\":\"co-300\",\"colorCode\":\"color-14\",\"qty\":5}],\"deliveryMode\":\"Home\",\"paymentMode\":\"cash\",\"shippingMode\":\"boat\"}");
    }
}