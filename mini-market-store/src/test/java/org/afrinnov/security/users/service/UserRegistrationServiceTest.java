package org.afrinnov.security.users.service;

import org.afrinnov.security.Messager;
import org.afrinnov.security.users.dto.UserRegistered;
import org.afrinnov.security.dto.RegistrationRequest;
import org.afrinnov.security.users.service.dto.UserCreate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Map;

import static org.afrinnov.security.MailTemplate.ACTIVATE_NEW_USER;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserRegistrationServiceTest {
    @InjectMocks
    private UserRegistrationService userRegistrationService;
    @Mock
    private UserService userService;
    @Mock
    private ApplicationEventPublisher events;

    @Test
    void createUserAndSendMailNotification() {
        //Arrange
        String email = "toto@localhost.com";
        UserCreate request = UserCreate.builder().email(email).build();
        when(userService.create(eq(request))).thenReturn(new UserRegistered("000000"));
        RegistrationRequest registrationRequest = RegistrationRequest.builder().email(email).build();
        //Act
        userRegistrationService.create(registrationRequest);

        //Assert
        verify(events).publishEvent(givenAMessage(email));
    }

    private static Messager givenAMessage(String email) {
        return new Messager(email, "Activate account", ACTIVATE_NEW_USER, Map.of("otp", "000000"));
    }

}