package org.afrinnov.security.users.service;

import org.afrinnov.config.ApplicationProperties;
import org.afrinnov.security.ExpiredUserException;
import org.afrinnov.security.dto.RegistrationRequest;
import org.afrinnov.security.entities.GroupEntity;
import org.afrinnov.security.entities.UserEntity;
import org.afrinnov.security.entities.UserStatus;
import org.afrinnov.security.repository.GroupRepository;
import org.afrinnov.security.repository.UserRepository;
import org.afrinnov.security.users.dto.PasswordRequest;
import org.afrinnov.security.users.service.dto.UserCreate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    @InjectMocks
    private UserService userService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private GroupRepository groupRepository;
    @Mock
    private ApplicationProperties applicationProperties;
    @Captor
    private ArgumentCaptor<UserEntity> userCaptor;

    @Test
    void shouldFailedWhenOtpIsNotExist() {
        //Arrange6
        when(userRepository.findByEmailIgnoreCaseAndOtp("un@email.com", "123456")).thenReturn(Optional.empty());
        var passwordRequest = new PasswordRequest("un@email.com", "123456", "L25mp@TYP");
        //Act
        var exception = assertThrows(ExpiredUserException.class, () -> userService.changePwd(passwordRequest));
        //Assert
        assertThat(exception).isNotNull();
        assertThat(exception.getMessage()).isEqualTo("OTP is not exist");
    }

    @Test
    void shouldFailedWhenOtpCreatedDateTimeIsExpired() {
        //Arrange
        var security = new ApplicationProperties.Security();
        security.setExpiredOtp(Duration.ofDays(7));
        when(applicationProperties.getSecurity()).thenReturn(security);
        UserEntity entity = new UserEntity();
        entity.setOtpCreateDate(LocalDateTime.now().minusDays(8));
        when(userRepository.findByEmailIgnoreCaseAndOtp("un@email.com", "123456")).thenReturn(Optional.of(entity));
        var passwordRequest = new PasswordRequest("un@email.com", "123456", "L25mp@TYP");
        //Act
        var exception = assertThrows(ExpiredUserException.class, () -> userService.changePwd(passwordRequest));
        //Assert
        assertThat(exception).isNotNull();
        assertThat(exception.getMessage()).isEqualTo("OTP is Expired");
    }

    @Test
    void shouldChangePassword() {
        //Arrange
        var security = new ApplicationProperties.Security();
        security.setExpiredOtp(Duration.ofDays(7));
        when(applicationProperties.getSecurity()).thenReturn(security);
        UserEntity entity = new UserEntity();
        entity.setOtpCreateDate(LocalDateTime.now().minusDays(6));
        String otp = "123456";
        String userEmail = "un@email.com";
        when(userRepository.findByEmailIgnoreCaseAndOtp(userEmail, otp)).thenReturn(Optional.of(entity));
        String encodedPwd = "25633251145";
        String password = "L25mp@TYP";
        when(passwordEncoder.encode(password)).thenReturn(encodedPwd);
        var passwordRequest = new PasswordRequest(userEmail, otp, password);
        //Act
        userService.changePwd(passwordRequest);
        //Assert
        verify(userRepository).save(userCaptor.capture());
        var updatedUser = userCaptor.getValue();
        assertThat(updatedUser).isNotNull();
        assertThat(updatedUser.getPassword()).isEqualTo(encodedPwd);
        assertThat(updatedUser.getOtp()).isNull();
        assertThat(updatedUser.getOtpCreateDate()).isNull();
    }

    @Test
    void shouldCreateUser() {
        //Arrange
        when(groupRepository.findByCode("123")).thenReturn(Optional.of(new GroupEntity()));
        when(passwordEncoder.encode(ArgumentMatchers.anyString())).thenReturn("ras_1254632");
        UserCreate request = UserCreate.builder().email("toto").groupCode("123")
                .phoneNumber("2525").firstname("first").lastname("last").build();
        //Act
        var userRegistered = userService.create(request);
        //Assert
        assertThat(userRegistered.generatedCode()).isNotEmpty();
        verify(userRepository).save(userCaptor.capture());
        UserEntity user = userCaptor.getValue();
        assertThat(user.getFirstname()).isEqualTo("first");
        assertThat(user.getLastname()).isEqualTo("last");
        assertThat(user.getEmail()).isEqualTo("toto");
        assertThat(user.getPhone()).isEqualTo("2525");
        assertThat(user.getPassword()).isEqualTo("ras_1254632");
        assertThat(user.getStatus()).isEqualTo(UserStatus.PENDING);
        assertThat(user.getOtp()).isEqualTo(userRegistered.generatedCode());
        assertThat(user.getOtpCreateDate()).isEqualToIgnoringSeconds(LocalDateTime.now());
        assertThat(user.getGroup()).isNotNull();
    }

}