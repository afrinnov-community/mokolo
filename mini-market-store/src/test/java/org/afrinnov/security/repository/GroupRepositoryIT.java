package org.afrinnov.security.repository;

import org.afrinnov.security.entities.GroupEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
//@DataJpaTest(excludeAutoConfiguration = {DelegatingWebMvcConfiguration.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Testcontainers
class GroupRepositoryIT {
    @Autowired
    private GroupRepository groupRepository;

    @Container
    @ServiceConnection
    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>(
            "postgres:15.7"
    );

    @Test
    void test() {
        //Act
        GroupEntity entity = new GroupEntity();
        entity.setCode("001");
        entity.setName("OneGroupe");
        entity.setDescription("Hello");
        groupRepository.save(entity);
        String sid = entity.getSid();
        System.out.println();
    }

}