package org.afrinnov.security.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.security.Key;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.time.Duration;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class JwtPrivatePublicUtilTest {

    @Test
    void testJWTWithRsa() {

        KeyPair kp = Jwts.SIG.RS512.keyPair().build();
        PublicKey publicKey = kp.getPublic();
        PrivateKey privateKey = kp.getPrivate();

        String encodedPublicKey = Base64.getEncoder().encodeToString(publicKey.getEncoded());
        String encodedPrivateKey = Base64.getEncoder().encodeToString(privateKey.getEncoded());
        log.info("Public Key:");
        log.info(convertToPublicKey(encodedPublicKey));
        log.info("Private Key:");
        log.info(convertToPrivateKey(encodedPrivateKey));
        String token = generateJwtToken(privateKey);
        log.info("TOKEN:");
        log.info(token);
        printStructure(token, publicKey);
    }

    @Test
    void testGenerateToken() throws Exception {
        Key privateKey = JwtKeyToolTest.makePrivateKeyFromFile();

        Key publicKey = JwtKeyToolTest.makePublicKeyFromFile();

        String token = generateJwtToken(privateKey);
        log.info("TOKEN");
        log.info(token);
        printStructure(token, (PublicKey) publicKey);
        assertClaims(token, (PublicKey) publicKey);
        assertThat(publicKey).isInstanceOf(RSAPublicKey.class);
    }

    private String generateJwtToken(Key privateKey) {
        Date exp = new Date(System.currentTimeMillis() + Duration.ofDays(30).toMillis());
        Map<String, Object> claims = new HashMap<>();
        claims.put("roles", List.of("ADMIN", "USER"));
        claims.put("group", "Home");
        claims.put("firstName", "Paul");
        return Jwts.builder()
                .subject("adam")
                .expiration(exp)
                .id(UUID.randomUUID().toString())
                .issuedAt(new Date())
                .issuer("info@wstutorial.com")
                .claims(claims)
                .signWith(privateKey)
                .header().add("typ", "JWT").and()
                .compact();
    }

    private void printStructure(String token, PublicKey publicKey) {
        Jws<Claims> parseClaimsJws = Jwts.parser().verifyWith(publicKey).build().parseSignedClaims(token);
        log.info("Header     :{}", parseClaimsJws.getHeader());
        log.info("Body       : {}", parseClaimsJws.getPayload());
    }

    private void assertClaims(String token, PublicKey publicKey) {
        Claims payload = Jwts.parser().verifyWith(publicKey).build().parseSignedClaims(token).getPayload();
        assertThat(payload.getSubject()).isEqualTo("adam");
        assertThat(payload.get("roles", List.class)).isEqualTo(List.of("ADMIN", "USER"));
    }


    // Add BEGIN and END comments
    private String convertToPublicKey(String key) {
        return String.format("-----BEGIN PUBLIC KEY-----\n%s\n-----END PUBLIC KEY-----", key);
    }

    // Add BEGIN and END comments
    private String convertToPrivateKey(String key) {
        return String.format("-----BEGIN PRIVATE KEY-----\n%s\n-----END PRIVATE KEY-----", key);
    }

}