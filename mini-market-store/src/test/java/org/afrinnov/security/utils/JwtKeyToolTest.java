package org.afrinnov.security.utils;

import org.afrinnov.security.spi.PublicPrivateUtils;
import org.springframework.core.io.InputStreamResource;

import java.io.InputStream;
import java.security.Key;

public interface JwtKeyToolTest {

    static InputStream privateCertificate() {
        InputStream stream = JwtKeyToolTest.class.getResourceAsStream("/secret/local/private.txt");
        assert stream != null;
        return stream;
    }

    static InputStream publicCertificate() {
        InputStream stream = JwtKeyToolTest.class.getResourceAsStream("/secret/local/public.txt");
        assert stream != null;
        return stream;
    }

    static Key makePrivateKeyFromFile() throws Exception {
        try (InputStream stream = privateCertificate()) {
            return PublicPrivateUtils.makePrivateKeyFromFile(new InputStreamResource(stream));
        }
    }

    static Key makePublicKeyFromFile() throws Exception {
        try (InputStream stream = publicCertificate()) {
            return PublicPrivateUtils.makePublicKeyFromFile(new InputStreamResource(stream));
        }
    }
}
