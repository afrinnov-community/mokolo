package org.afrinnov.security.utils;

import io.jsonwebtoken.Claims;
import org.afrinnov.security.entities.*;
import org.afrinnov.security.service.ApplicationSecurityProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.InputStreamResource;

import java.io.InputStream;
import java.time.Duration;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JwtUtilTest {

    private JwtUtil jwtUtil;
    @Mock
    private ApplicationSecurityProperties applicationProperties;

    @BeforeEach
    void setUp() throws Exception {
        try(InputStream privateCertificate = JwtKeyToolTest.privateCertificate();
            InputStream publicCertificate = JwtKeyToolTest.publicCertificate()) {
            jwtUtil = new JwtPrivatePublicUtil(applicationProperties, new InputStreamResource(privateCertificate),
                    new InputStreamResource(publicCertificate));
        }
    }

    @Test
    void shouldGenerateToken() {
        //Arrange
        when(applicationProperties.expirationTime()).thenReturn(Duration.ofDays(3).toMillis());
        var user = new UserEntity();
        user.setEmail("toto@sysbio.cm");
        GroupEntity group = new GroupEntity();
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setName("SHOPPER");
        group.setRoles(List.of(roleEntity));
        user.setGroup(group);
        UserShopEntity userShopEntity = new UserShopEntity();
        ShopRefEntity shop = new ShopRefEntity();
        shop.setName("Boutique");
        shop.setCode("0001");
        ShopSchemaRefEntity shopSchema = new ShopSchemaRefEntity();
        shopSchema.setNameSchema("shop123456789");
        shop.setShopSchema(shopSchema);
        userShopEntity.setShop(shop);
        user.setUserShops(List.of(userShopEntity));
        //Act
        String token = jwtUtil.generateToken(user);
        Claims claims = jwtUtil.extractAllClaims(token);
        //Assert
        assertThat(claims).hasSize(7);
    }
}