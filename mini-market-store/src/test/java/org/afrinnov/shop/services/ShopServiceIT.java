package org.afrinnov.shop.services;

import org.afrinnov.shop.dto.ShopCode;
import org.afrinnov.shop.rest.input.ShopInput;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLSyntaxErrorException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ShopServiceIT {
    @Autowired
    private ShopService shopService;

    @Autowired
    private DataSource dataSource;

    @Test
    @WithMockUser
    void should_create_a_new_shop() {
        ShopInput shop = new ShopInput("Brandol's Shop","brandol.kuete@gmail.com","0757489321");
        ShopCode shopCode = shopService.create(shop);

        SQLSyntaxErrorException exception = Assertions.assertThrows(SQLSyntaxErrorException.class, () -> {
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement statement = connection.prepareStatement("CREATE SCHEMA " + ("SHOP_" + shopCode.code()))
            ) {
                connection.setAutoCommit(false);
                statement.executeUpdate();
                connection.commit();
            }
        });
        assertThat(exception.getMessage()).contains("object name already exists: SHOP_");
    }
}