package org.afrinnov.shop.services;

import org.afrinnov.security.Messager;
import org.afrinnov.security.spi.AccountShop;
import org.afrinnov.shop.entities.ShopEntity;
import org.afrinnov.shop.repository.ShopRepository;
import org.afrinnov.shop.rest.input.ShopInput;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;

import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ShopServiceTest {

    @InjectMocks
    private ShopService shopService;

    @Mock
    private ShopRepository shopRepository;

    @Mock
    private DataSource dataSource;

    @Mock
    private ApplicationEventPublisher events;

    @Test
    void should_create_shop_and_shop_schema() throws SQLException {
        //Arrange
        ShopEntity shopEntity = new ShopEntity();
        shopEntity.setCode("001");
        when(shopRepository.save(any())).thenReturn(shopEntity);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);

        //Act
        shopService.create(new ShopInput("Brandol's Shop","brandol.kuete@gmail.com","0757489321"));

        //Assert
        InOrder inOrder = inOrder(connection, preparedStatement);
        inOrder.verify(connection).setAutoCommit(false);
        inOrder.verify(preparedStatement).executeLargeUpdate();
        inOrder.verify(connection).commit();
        verify(events).publishEvent(any(Messager.class));
        verify(events).publishEvent(any(AccountShop.class));
    }

    @Test
    void should_throw_exception_on_schema_creation() throws SQLException {
        //Arrange
        ShopEntity shopEntity = new ShopEntity();
        shopEntity.setCode("001");
        when(shopRepository.save(any())).thenReturn(shopEntity);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        doThrow(new SQLException("ERROR")).when(preparedStatement).executeLargeUpdate();
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);

        //Act
        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                shopService.create(new ShopInput("Brandol's Shop", "brandol.kuete@gmail.com", "0757489321")));

        //Assert
        assertThat(exception.getMessage()).isEqualTo("java.sql.SQLException: ERROR");
        InOrder inOrder = inOrder(connection, preparedStatement);
        inOrder.verify(connection).setAutoCommit(false);
        inOrder.verify(preparedStatement).executeLargeUpdate();
        inOrder.verify(connection).rollback();
        verifyNoInteractions(events);
    }
}