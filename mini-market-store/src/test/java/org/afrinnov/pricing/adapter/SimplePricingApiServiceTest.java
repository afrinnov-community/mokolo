package org.afrinnov.pricing.adapter;

import org.afrinnov.pricing.PricingSpecs;
import org.afrinnov.pricing.ProductPricingDto;
import org.afrinnov.pricing.dto.PricingNature;
import org.afrinnov.pricing.entities.PricingEntity;
import org.afrinnov.pricing.repository.PricingRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Stream.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SimplePricingApiServiceTest {

    @InjectMocks
    private SimplePricingApiService pricingApiService;
    @Mock
    private PricingRepository pricingRepository;

    @Test
    void shouldGetProductsGroupedByPricing() {
        var spec = PricingSpecs.getAllPricingByNatureSpecification();

        when(pricingRepository.findAll(any(spec.getClass()))).thenReturn(mergeLists(getClassicPricings(), getPreOrderPricings(), getSalePeriodPricings()));

        List<ProductPricingDto> productPricingDtos = pricingApiService.getAllProductPricing();

        assertThat(productPricingDtos.size()).isEqualTo(4);
    }

    @Test
    void shouldGetPagingClassicPricing() {
        int pageNumber = 0;
        int pageSize = 10;
        var spec = PricingSpecs.getAllPricingByNatureSpecification("CLASSIC");

        when(pricingRepository.findAll(any(spec.getClass()), eq(PageRequest.of(pageNumber, pageSize)))).thenReturn(createPage(getClassicPricings(), pageNumber, pageSize));
        Page<ProductPricingDto> productPricingDtos = pricingApiService.getAllProductPricing("CLASSIC", pageNumber, pageSize);

        assertEquals(2, productPricingDtos.getTotalElements());
    }

    private static List<PricingEntity> getClassicPricings() {
        PricingEntity pricing1 = new PricingEntity();
        pricing1.setSid(UUID.randomUUID());
        pricing1.setSku("sku1");
        pricing1.setCode("123");
        pricing1.setCurrency("EUR");
        pricing1.setNature(PricingNature.CLASSIC);
        pricing1.setCurrent(false);
        pricing1.setPurchasePrice(1500);
        pricing1.setSellingPrice(1800);
        pricing1.setDueDate(LocalDateTime.of(2023, 12, 25, 12, 30));
        pricing1.setStartDate(LocalDateTime.of(2023, 12, 29, 11, 10));

        PricingEntity pricing2 = new PricingEntity();
        pricing2.setSid(UUID.randomUUID());
        pricing2.setSku("sku4");
        pricing2.setCode("177");
        pricing2.setCurrency("XAF");
        pricing2.setNature(PricingNature.CLASSIC);
        pricing2.setCurrent(false);
        pricing2.setPurchasePrice(6500);
        pricing2.setSellingPrice(8200);
        pricing2.setDueDate(LocalDateTime.of(2024, 1, 26, 2, 30));
        pricing2.setStartDate(LocalDateTime.of(2023, 2, 26, 13, 10));

        return List.of(pricing1, pricing2);
    }

    private static List<PricingEntity> getPreOrderPricings() {
        PricingEntity pricing1 = new PricingEntity();
        pricing1.setSid(UUID.randomUUID());
        pricing1.setSku("sku2");
        pricing1.setCode("143");
        pricing1.setCurrency("EUR");
        pricing1.setNature(PricingNature.PRE_ORDER);
        pricing1.setCurrent(true);
        pricing1.setPurchasePrice(200);
        pricing1.setSellingPrice(300);
        pricing1.setDueDate(LocalDateTime.of(2023, 1, 25, 12, 30));
        pricing1.setStartDate(LocalDateTime.of(2023, 2, 12, 11, 10));

        return List.of(pricing1);
    }

    private static List<PricingEntity> getSalePeriodPricings() {
        PricingEntity pricing1 = new PricingEntity();
        pricing1.setSid(UUID.randomUUID());
        pricing1.setSku("sku3");
        pricing1.setCode("128");
        pricing1.setCurrency("RON");
        pricing1.setNature(PricingNature.SALE_PERIOD);
        pricing1.setCurrent(false);
        pricing1.setPurchasePrice(150);
        pricing1.setSellingPrice(200);
        pricing1.setDueDate(LocalDateTime.of(2024, 1, 26, 2, 30));
        pricing1.setStartDate(LocalDateTime.of(2023, 2, 26, 13, 10));

        return List.of(pricing1);
    }

    @SafeVarargs
    public static List<PricingEntity> mergeLists(List<PricingEntity>... lists) {
        List<PricingEntity> mergedList = new ArrayList<>();
        of(lists).forEach(mergedList::addAll);

        return mergedList;
    }

    public static Page<PricingEntity> createPage(List<PricingEntity> pricingList, int pageNumber, int pageSize) {
        int start = Math.min((int) (long) pageNumber * pageSize, pricingList.size());
        int end = Math.min(start + pageSize, pricingList.size());

        List<PricingEntity> pageContent = pricingList.subList(start, end);

        return new PageImpl<>(pageContent, PageRequest.of(pageNumber, pageSize), pricingList.size());
    }
}