package org.afrinnov.pricing.schedule.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ScheduleTaskPricingByDateTest {
    @InjectMocks
    private ScheduleTaskPricingByDate taskPricingUpToDate;
    @Mock
    private PricingScheduleService pricingScheduleService;

    @Test
    void shouldCallScheduleService() {
        //Act
        taskPricingUpToDate.scheduleTaskPricingToTrigge();
        //Assert
        verify(pricingScheduleService).process();
    }

    @Test
    void shouldCallScheduleServiceStoppingProcess(){
        //Act
        taskPricingUpToDate.scheduleTaskPricingToStop();

        //Assert
        verify(pricingScheduleService).stopProcessing();
    }

}