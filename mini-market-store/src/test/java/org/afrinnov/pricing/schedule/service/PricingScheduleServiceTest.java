package org.afrinnov.pricing.schedule.service;

import org.afrinnov.pricing.id.PricingCode;
import org.afrinnov.pricing.schedule.entities.PricingScheduleEntity;
import org.afrinnov.pricing.schedule.entities.PricingScheduleRowEntity;
import org.afrinnov.pricing.schedule.rep.PricingScheduleRepository;
import org.afrinnov.pricing.services.PricingService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

import static org.afrinnov.pricing.schedule.entities.PricingScheduleNature.END;
import static org.afrinnov.pricing.schedule.entities.PricingScheduleNature.START;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PricingScheduleServiceTest {
    @InjectMocks
    private PricingScheduleService service;
    @Mock
    private PricingScheduleRepository pricingScheduleRepository;
    @Mock
    private PricingService pricingService;
    @Captor
    private ArgumentCaptor<PricingScheduleEntity> scheduleEntityCaptor;

    @Test
    void shouldNotCallPricingAndScheduleServiceWhenScheduleNotAvailable() {
        //Arrange
        when(pricingScheduleRepository.
                findAllPricingScheduleByStartDateAndNatureAndScheduled(LocalDate.now(),START, false))
                .thenReturn(List.of());
        //Act
        service.process();
        //Assert
        verifyNoInteractions(pricingService);
        verify(pricingScheduleRepository, never()).save(any());
    }

    @Test
    void shouldNotCallPricingAndScheduleService() {
        //Arrange
        PricingScheduleEntity pricingSchedule = getPricingScheduleEntity();
        when(   pricingScheduleRepository
                .findAllPricingScheduleByStartDateAndNatureAndScheduled(LocalDate.now(),START, false))
                .thenReturn(List.of(pricingSchedule)
             );
        //Act
        service.process();
        //Assert
        verify(pricingService).defineCurrentPricing(new PricingCode("01"));
        verify(pricingScheduleRepository).save(scheduleEntityCaptor.capture());
        PricingScheduleEntity captorValue = scheduleEntityCaptor.getValue();
        assertThat(captorValue.isScheduled()).isTrue();
    }

    @Test
    void shouldNotCallPricingAndScheduleServiceWhenScheduleNotAvailableDueDateCase() {
        //Arrange
        when(pricingScheduleRepository.
                findAllPricingScheduleByDueDateAndNatureAndScheduled(LocalDate.now(),END, false))
                .thenReturn(List.of());
        //Act
        service.stopProcessing();
        //Assert
        verifyNoInteractions(pricingService);
        verify(pricingScheduleRepository, never()).save(any());
    }

    @Test
    void shouldNotCallPricingAndScheduleServiceDueDateCase() {
        //Arrange
        PricingScheduleEntity pricingSchedule = getPricingScheduleEntity();
        when(   pricingScheduleRepository
                .findAllPricingScheduleByDueDateAndNatureAndScheduled(LocalDate.now(),END, false))
                .thenReturn(List.of(pricingSchedule)
                );
        //Act
        service.stopProcessing();
        //Assert
        verify(pricingService).unDefineCurrentPricing(new PricingCode("01"));
        verify(pricingScheduleRepository).save(scheduleEntityCaptor.capture());
        PricingScheduleEntity captorValue = scheduleEntityCaptor.getValue();
        assertThat(captorValue.isScheduled()).isTrue();
    }

    private static PricingScheduleEntity getPricingScheduleEntity() {
        PricingScheduleEntity pricingSchedule = new PricingScheduleEntity();
        PricingScheduleRowEntity scheduleRowEntity = new PricingScheduleRowEntity();
        scheduleRowEntity.setPriceCode("01");
        pricingSchedule.setPricingsScheduleRows(List.of(scheduleRowEntity));
        return pricingSchedule;
    }

}