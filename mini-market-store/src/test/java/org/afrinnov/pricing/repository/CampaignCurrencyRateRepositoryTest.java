package org.afrinnov.pricing.repository;

import org.afrinnov.pricing.entities.CampaignCurrencyRateEntity;
import org.afrinnov.pricing.entities.DeliveryCampaignEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Transactional(propagation = Propagation.REQUIRES_NEW)
@TestPropertySource(properties = {
        "spring.liquibase.change-log=classpath:db/changelog/master.xml"
})
class CampaignCurrencyRateRepositoryTest {
    @Autowired
    private CampaignCurrencyRateRepository repository;
    @Autowired
    private DeliveryCampaignRepository deliveryCampaignRepository;
    @Autowired
    private CurrencyRepository currencyRepository;

    @Test
    void createCurrencyRate() {
        var deliveryCampaign = new DeliveryCampaignEntity();
        deliveryCampaign.setCode("123456");
        deliveryCampaign.setCurrency("EUR");
        deliveryCampaign.setDesignation("01/02/2024 livraison");
        deliveryCampaign.setCreatedBy("Tester");
        deliveryCampaign.setCreateDate(Instant.now());
        deliveryCampaign.setLastModifiedBy("Tester");
        deliveryCampaign.setLastModifiedDate(Instant.now());
        deliveryCampaignRepository.saveAndFlush(deliveryCampaign);
        var currency = currencyRepository.findByIsoCode("XAF").orElseThrow();
        var entity=new CampaignCurrencyRateEntity(deliveryCampaign, currency);
        entity.setRate(BigDecimal.valueOf(25000));
        repository.saveAndFlush(entity);
    }


}