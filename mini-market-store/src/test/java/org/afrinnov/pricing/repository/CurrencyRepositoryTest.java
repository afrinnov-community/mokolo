package org.afrinnov.pricing.repository;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Transactional(propagation = Propagation.REQUIRES_NEW)
@TestPropertySource(properties = {
        "spring.liquibase.change-log=classpath:db/changelog/master.xml"
})
class CurrencyRepositoryTest {
    @Autowired
    private CurrencyRepository repository;

    @ParameterizedTest
    @ValueSource(strings = {"XAF", "XOF", "USD"})
    void checkCurrencyIsoCode(String currency) {
        //Act & Assert
        assertThat(repository.findByIsoCodeIgnoreCase(currency)).isPresent();
    }

}