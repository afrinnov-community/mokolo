package org.afrinnov.pricing.repository;

import org.afrinnov.pricing.entities.DeliveryCampaignEntity;
import org.afrinnov.pricing.entities.ProductIdentifier;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Transactional(propagation = Propagation.REQUIRES_NEW)
@TestPropertySource(properties = {
        "spring.liquibase.change-log=classpath:db/changelog/master.xml"
})
class DeliveryCampaignRepositoryTest {

    @Autowired
    private DeliveryCampaignRepository repository;
    @Autowired
    private TestEntityManager entityManager;

    @Test
    void createCampaign() {
        String sql = """
                INSERT INTO PUBLIC.ESHOP.E_PRODUCT
                (SID, SKU, TITLE, DESCRIPTION, CREATED_BY, CREATED_DATE, LAST_MODIFIED_BY, LAST_MODIFIED_DATE)
                VALUES('f7dc32fb-0353-4943-b56f-2db15978ef7f', '245612624293', 'Chaussures', 'Une chaussure', 'admin', '2024-01-14 20:22:27.490', 'admin', '2024-01-14 21:54:53.976');
                """;
        entityManager.getEntityManager().createNativeQuery(sql).executeUpdate();
        DeliveryCampaignEntity entity = new DeliveryCampaignEntity();
        entity.setCode("123456");
        entity.setCurrency("EUR");
        entity.setDesignation("01/02/2024 livraison");
        entity.setCreatedBy("Tester");
        entity.setCreateDate(Instant.now());
        entity.setLastModifiedBy("Tester");
        entity.setLastModifiedDate(Instant.now());
        repository.saveAndFlush(entity);
        entity.getProductIds().add(new ProductIdentifier(UUID.fromString("f7dc32fb-0353-4943-b56f-2db15978ef7f"), "245612624293"));
        repository.saveAndFlush(entity);

    }

}