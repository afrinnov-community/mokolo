package org.afrinnov.pricing.services;

import org.afrinnov.pricing.id.PricingCode;
import org.afrinnov.pricing.rest.input.PriceRequest;
import org.afrinnov.pricing.schedule.service.PricingScheduleService;
import org.afrinnov.sheet.spi.ProductSheetApiService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.afrinnov.pricing.dto.PricingNature.CLASSIC;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PricingDefineServiceTest {

    @InjectMocks
    private PricingDefineService pricingDefineService;
    @Mock
    private ProductSheetApiService productSheetApiService;
    @Mock
    private PricingService pricingService;

    @Mock
    private PricingScheduleService pricingScheduleService;

    @Test
    void shouldNotCallScheduleServiceWhenStartDateNull(){
        //Act
        pricingDefineService.createPrice("00001", CLASSIC, PriceRequest.builder().build());
        //Assert
        verifyNoInteractions(pricingScheduleService);
    }
    @Test
    void shouldInitializeSchedulePricingWithGivenStartDate(){
        //Arrange
        LocalDateTime now = LocalDateTime.now();
        PriceRequest request = PriceRequest.builder().startDate(now).build();
        when(pricingService.createPrice("00001", CLASSIC, request)).thenReturn(new PricingCode("C123"));
        //Act
        pricingDefineService.createPrice("00001", CLASSIC, request);
        //Assert
        verifyNoInteractions(pricingScheduleService);
    }
    @Test
    void shouldInitializeSchedulePricingWithGivenDueDate(){
        //Arrange
        LocalDateTime now = LocalDateTime.now();
        PriceRequest request = PriceRequest.builder().dueDate(now).build();
        when(pricingService.createPrice("00001", CLASSIC, request)).thenReturn(new PricingCode("C123"));
        //Act
        pricingDefineService.createPrice("00001", CLASSIC, request);
        //Assert
        verifyNoInteractions(pricingScheduleService);
    }

    @Test
    void shouldNotCallScheduleServiceWhenStartDateDefined(){
        //Arrange
        LocalDateTime now = LocalDateTime.now();
        PriceRequest request = PriceRequest.builder().startDate(now).build();
        when(pricingService.createPrice("00001", CLASSIC, request)).thenReturn(new PricingCode("C123"));

        //Act
        pricingDefineService.createPrice("00001", CLASSIC, request);

        //Assert
        verifyNoInteractions(pricingScheduleService);
    }
    @Test
    void shouldNotCallScheduleServiceWhenDueDateDefined(){
        //Arrange
        LocalDateTime now = LocalDateTime.now();
        PriceRequest request = PriceRequest.builder().dueDate(now).build();
        when(pricingService.createPrice("00001", CLASSIC, request)).thenReturn(new PricingCode("C123"));

        //Act
        pricingDefineService.createPrice("00001", CLASSIC, request);

        //Assert
        verifyNoInteractions(pricingScheduleService);
    }

    @Test
    void shouldNotCallScheduleService(){
        //Arrange
        LocalDateTime now = LocalDateTime.now();
        PriceRequest request = PriceRequest.builder().startDate(now).dueDate(now).build();
        when(pricingService.createPrice("00001", CLASSIC, request)).thenReturn(new PricingCode("C123"));

        //Act
        PricingCode pricingCode = pricingDefineService.createPrice("00001", CLASSIC, request);

        //Assert
        verify(pricingScheduleService).createStartDate(pricingCode, now.toLocalDate());
        verify(pricingScheduleService).createDueDate(pricingCode, now.toLocalDate());
    }
}