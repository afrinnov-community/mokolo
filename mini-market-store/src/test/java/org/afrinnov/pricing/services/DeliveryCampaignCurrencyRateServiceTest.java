package org.afrinnov.pricing.services;

import org.afrinnov.pricing.entities.CampaignCurrencyRateEntity;
import org.afrinnov.pricing.entities.CurrencyEntity;
import org.afrinnov.pricing.entities.DeliveryCampaignEntity;
import org.afrinnov.pricing.repository.CampaignCurrencyRateRepository;
import org.afrinnov.pricing.repository.CurrencyRepository;
import org.afrinnov.pricing.repository.DeliveryCampaignRepository;
import org.afrinnov.pricing.rest.input.CurrencyRateInput;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.math.BigDecimal.ONE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DeliveryCampaignCurrencyRateServiceTest {
    @InjectMocks
    private DeliveryCampaignCurrencyRateService service;
    @Mock
    private DeliveryCampaignRepository deliveryCampaignRepository;
    @Mock
    private CampaignCurrencyRateRepository campaignCurrencyRateRepository;
    @Mock
    private CurrencyRepository currencyRepository;

    @Test
    void shouldFailWhenDeliveryCampaignNotFound() {
        //Arrange
        when(deliveryCampaignRepository.findByCode("123")).thenReturn(Optional.empty());
        //Act & Assert
        var error = assertThrows(IllegalArgumentException.class,
                () -> service.defineCurrencyRates("123", List.of(new CurrencyRateInput("XAF", ONE))));
        assertThat(error.getMessage()).isEqualTo("Campaign not found:123");
    }

    @Test
    void shouldNotCreateRateWhenCurrencyNotFound() {
        //Arrange
        DeliveryCampaignEntity deliveryCampaign = new DeliveryCampaignEntity();
        deliveryCampaign.setCode("123");
        deliveryCampaign.setSid(UUID.randomUUID());
        when(deliveryCampaignRepository.findByCode("123")).thenReturn(Optional.of(deliveryCampaign));
        when(currencyRepository.findByIsoCode("XAF")).thenReturn(Optional.empty());
        //Act
        service.defineCurrencyRates("123", List.of(new CurrencyRateInput("XAF", ONE)));
        //Assert
        verifyNoInteractions(campaignCurrencyRateRepository);
    }

    @Test
    void shouldUpdateRateWhenCurrencyRateExist() {
        //Arrange
        var deliveryCampaign = givenADeliveryCampaign();
        when(deliveryCampaignRepository.findByCode("123")).thenReturn(Optional.of(deliveryCampaign));
        var currency = givenACurrency();
        when(currencyRepository.findByIsoCode("XAF")).thenReturn(Optional.of(currency));
        var rateEntity = givenACampaignCurrencyRate(deliveryCampaign, currency);
        when(campaignCurrencyRateRepository.findById_CurrencyAndId_DeliveryCampaign(currency, deliveryCampaign))
                .thenReturn(Optional.of(rateEntity));
        //Act
        service.defineCurrencyRates("123", List.of(new CurrencyRateInput("XAF", ONE)));
        //Assert
        var argumentCaptor = ArgumentCaptor.forClass(CampaignCurrencyRateEntity.class);
        verify(campaignCurrencyRateRepository).save(argumentCaptor.capture());
        var updatedRateEntity = argumentCaptor.getValue();
        assertThat(updatedRateEntity.getRate()).isEqualTo(ONE);
    }

    @Test
    void shouldCreateCurrencyRate() {
        //Arrange
        var deliveryCampaign = givenADeliveryCampaign();
        when(deliveryCampaignRepository.findByCode("123")).thenReturn(Optional.of(deliveryCampaign));
        var currency = givenACurrency();
        when(currencyRepository.findByIsoCode("XAF")).thenReturn(Optional.of(currency));
        when(campaignCurrencyRateRepository.findById_CurrencyAndId_DeliveryCampaign(currency, deliveryCampaign))
                .thenReturn(Optional.empty());
        //Act
        service.defineCurrencyRates("123", List.of(new CurrencyRateInput("XAF", ONE)));
        //Assert
        var argumentCaptor = ArgumentCaptor.forClass(CampaignCurrencyRateEntity.class);
        verify(campaignCurrencyRateRepository).save(argumentCaptor.capture());
        var updatedRateEntity = argumentCaptor.getValue();
        assertThat(updatedRateEntity.getRate()).isEqualTo(ONE);
        assertThat(updatedRateEntity.getId()).isNotNull();
        assertThat(updatedRateEntity.getId().getCurrency()).isSameAs(currency);
        assertThat(updatedRateEntity.getId().getDeliveryCampaign()).isSameAs(deliveryCampaign);

    }

    private CampaignCurrencyRateEntity givenACampaignCurrencyRate(DeliveryCampaignEntity deliveryCampaign, CurrencyEntity currency) {
        var rateEntity = new CampaignCurrencyRateEntity();
        rateEntity.setRate(BigDecimal.ZERO);
        rateEntity.setId(new CampaignCurrencyRateEntity.CampaignCurrencyRatePK(deliveryCampaign, currency));
        return rateEntity;
    }

    private CurrencyEntity givenACurrency() {
        var currency = new CurrencyEntity();
        currency.setIsoCode("XAF");
        return currency;
    }

    private DeliveryCampaignEntity givenADeliveryCampaign() {
        var deliveryCampaign = new DeliveryCampaignEntity();
        deliveryCampaign.setCode("123");
        deliveryCampaign.setSid(UUID.randomUUID());
        return deliveryCampaign;
    }

}