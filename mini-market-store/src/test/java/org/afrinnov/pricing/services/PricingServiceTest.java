package org.afrinnov.pricing.services;

import org.afrinnov.pricing.dto.PricingNature;
import org.afrinnov.pricing.entities.PricingEntity;
import org.afrinnov.pricing.id.PricingCode;
import org.afrinnov.pricing.repository.PricingRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PricingServiceTest {

    @InjectMocks
    private PricingService pricingService;

    @Mock
    private PricingRepository pricingRepository;

    @Captor
    private ArgumentCaptor<PricingEntity> scheduleEntityCaptor;

    @Test
    void shouldCallPricingServiceForDefineCurrentPricing(){
        //Arrange
        PricingCode pricingCode = new PricingCode("00002");
        Optional<PricingEntity> opt = Optional.of(givePricing());
        when(pricingRepository.findByCode("00002")).thenReturn(opt);
        when(pricingRepository.findBySkuAndCurrentTrue("01CL2024")).thenReturn(opt);
        //Act
        pricingService.defineCurrentPricing(pricingCode);

        //Assert
        verify(pricingRepository).findByCode("00002");
        verify(pricingRepository).findBySkuAndCurrentTrue("01CL2024");
    }

    private static PricingEntity givePricing(){
        PricingEntity entity = new PricingEntity();
        entity.setCurrent(false);
        entity.setNature(PricingNature.CLASSIC);
        entity.setCode("00002");
        entity.setSid(UUID.randomUUID());
        entity.setSku("01CL2024");
        entity.setStartDate(LocalDateTime.now());
        entity.setDueDate(LocalDateTime.MAX);
        return entity;
    }

}