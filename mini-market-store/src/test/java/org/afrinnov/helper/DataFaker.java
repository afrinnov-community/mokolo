package org.afrinnov.helper;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/12/2023 -- 18:47<br></br>
 * By : @author alexk<br></br>
 * Project : mokolo<br></br>
 * Package : org.afrinnov.helper<br></br>
 */
public class DataFaker {
    final static ObjectMapper MAPPER = new ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL)
            .registerModule(new JavaTimeModule());
    private final static String RESOURCES_PATH = "src/test/resources/";

    public static <T> T getObject(String path, Class<T> clazz) throws IOException {
        final String jsonContent = Files.readString(Path.of(RESOURCES_PATH+path));
        return  MAPPER.readValue(jsonContent, clazz);
    }
}
