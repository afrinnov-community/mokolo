package org.afrinnov.exhibition.services;

import org.afrinnov.pricing.dto.PricingNature;
import org.afrinnov.pricing.entities.PricingEntity;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class ProductExhibitionServiceTest {

    @Test
    void findAllProductByPricing() {
    }

    private static List<PricingEntity> getSavedPricings() {
        PricingEntity pricing1 = new PricingEntity();
        pricing1.setSid(UUID.randomUUID());
        pricing1.setSku("sku1");
        pricing1.setCode("123");
        pricing1.setCurrency("EUR");
        pricing1.setNature(PricingNature.CLASSIC);
        pricing1.setCurrent(false);
        pricing1.setPurchasePrice(1500);
        pricing1.setSellingPrice(1800);
        pricing1.setDueDate(LocalDateTime.of(2023, 12, 25, 12, 30));
        pricing1.setStartDate(LocalDateTime.of(2023, 12, 29, 11, 10));

        PricingEntity pricing2 = new PricingEntity();
        pricing2.setSid(UUID.randomUUID());
        pricing2.setSku("sku2");
        pricing2.setCode("143");
        pricing2.setCurrency("EUR");
        pricing2.setNature(PricingNature.PRE_ORDER);
        pricing2.setCurrent(true);
        pricing2.setPurchasePrice(200);
        pricing2.setSellingPrice(300);
        pricing2.setDueDate(LocalDateTime.of(2023, 1, 25, 12, 30));
        pricing2.setStartDate(LocalDateTime.of(2023, 2, 12, 11, 10));

        PricingEntity pricing3 = new PricingEntity();
        pricing3.setSid(UUID.randomUUID());
        pricing3.setSku("sku3");
        pricing3.setCode("128");
        pricing3.setCurrency("RON");
        pricing3.setNature(PricingNature.SALE_PERIOD);
        pricing3.setCurrent(false);
        pricing3.setPurchasePrice(150);
        pricing3.setSellingPrice(200);
        pricing3.setDueDate(LocalDateTime.of(2024, 1, 26, 2, 30));
        pricing3.setStartDate(LocalDateTime.of(2023, 2, 26, 13, 10));

        PricingEntity pricing4 = new PricingEntity();
        pricing4.setSid(UUID.randomUUID());
        pricing4.setSku("sku4");
        pricing4.setCode("177");
        pricing4.setCurrency("XAF");
        pricing4.setNature(PricingNature.CLASSIC);
        pricing4.setCurrent(false);
        pricing4.setPurchasePrice(6500);
        pricing4.setSellingPrice(8200);
        pricing4.setDueDate(LocalDateTime.of(2024, 1, 26, 2, 30));
        pricing4.setStartDate(LocalDateTime.of(2023, 2, 26, 13, 10));

        return List.of(pricing1, pricing2, pricing3, pricing4);
    }
}