package org.afrinnov.cucumber.config;

import org.afrinnov.cucumber.tools.StoreSession;
import org.springdoc.core.providers.ObjectMapperProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class MiniConfig {
    @Bean
    @Scope("cucumber-glue")
    public StoreSession storeSession(ObjectMapperProvider objectMapperProvider) {
        return new StoreSession(objectMapperProvider.jsonMapper());
    }


}
