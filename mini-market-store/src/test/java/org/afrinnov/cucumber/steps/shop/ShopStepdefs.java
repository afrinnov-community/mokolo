package org.afrinnov.cucumber.steps.shop;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.RequiredArgsConstructor;
import org.afrinnov.cucumber.tools.StoreSession;
import org.afrinnov.security.dto.LoginRequest;
import org.afrinnov.security.dto.LoginResponse;
import org.afrinnov.security.dto.RegUser;
import org.afrinnov.security.entities.UserEntity;
import org.afrinnov.security.repository.UserRepository;
import org.afrinnov.security.users.dto.PasswordRequest;
import org.afrinnov.security.users.service.UserService;
import org.afrinnov.sheet.services.mails.MailSendService;
import org.afrinnov.shop.dto.ShopCode;
import org.afrinnov.shop.dto.ShopGroup;
import org.afrinnov.shop.rest.input.ShopInput;
import org.assertj.core.api.Assertions;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RequiredArgsConstructor
public class ShopStepdefs {
    private final WebTestClient webTestClient;
    private final StoreSession storeSession;
    private final MailSendService mailSendService;
    private final UserRepository userRepository;


    @Given("A prospect informations")
    public void aProspectInformations() {
        ShopInput shopInput = new ShopInput("Yaya", "yaya@afrinnov.net", "+23512457896");
        storeSession.saveResult("shop", shopInput);
    }

    @When("I send to initialize shop")
    public void iSendToInitializeShop() {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        var request = storeSession.get("shop", ShopInput.class);
        FluxExchangeResult<ShopCode> result = webTestClient.post().uri("/api/exhibit/shops")
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(request)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .returnResult(ShopCode.class);
        var response = result.getResponseBody().blockFirst();
        storeSession.saveResult("createdShop", response);
    }

    @Then("A mail sent to confirm shop is created")
    public void aMailSentToConfirmShopIsCreated() {
        verify(mailSendService).ping("yaya@afrinnov.net", "Create shop", "GENERIC: -1-1-1-1");
    }

    @And("An account with shop role is created")
    public void anAccountWithShopRoleIsCreated() {
        Optional<UserEntity> userEntity = userRepository.findByUsernameIgnoreCase("yaya@afrinnov.net");
        assertThat(userEntity).isPresent();
        storeSession.saveResult("generatedOtp", userEntity.get().getOtp());
    }

    @And("A shop has two validity week")
    public void aShopHasTwoValidityWeek() {

    }

    @When("A shopper define new password {string} with his otp")
    public void aShopperDefineNewPasswordWithHisOtp(String password) {
        String generatedOtp = storeSession.get("generatedOtp", String.class);
        PasswordRequest passwordRequest = new PasswordRequest("yaya@afrinnov.net", generatedOtp, password);

        webTestClient.post().uri("/api/users/first-change-password")
                .contentType(APPLICATION_JSON)
                .bodyValue(passwordRequest)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated();
    }

    @And("A shopper logs in with new password {string}")
    public void aShopperLogsInWithNewPassword(String password) {
        LoginRequest loginRequest = new LoginRequest("yaya@afrinnov.net", password);

        LoginResponse loginResponse = webTestClient.post().uri("/api/authenticate")
                .contentType(APPLICATION_JSON)
                .bodyValue(loginRequest)
                .accept(APPLICATION_JSON).exchange().expectStatus().isOk()
                .returnResult(LoginResponse.class).getResponseBody().blockFirst();
        storeSession.saveResult("shopperLoginResponse", loginResponse);
    }

    @When("A shopper create user group {string}")
    public void aShopperCreateUserGroup(String group) {
        var loginResponse = storeSession.get("shopperLoginResponse", LoginResponse.class);
        Map<String, Object> body =Map.of("name", group, "description", "RAS");
        FluxExchangeResult<ShopGroup> result = webTestClient.post().uri("/api/shopper/group")
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(body)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .returnResult(ShopGroup.class);
        var response = result.getResponseBody().blockFirst();
        storeSession.saveResult("createdShopGroup", response);
    }

    @And("A shopper add {string} roles for {string} group")
    public void aShopperAddRolesForGroup(String arg0, String arg1) {

    }

    @Then("A shopper add {string} as {string}")
    public void aShopperAddAs(String arg0, String arg1) {
    }
}
