package org.afrinnov.cucumber.steps.common.brand;

import com.google.gson.Gson;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.SneakyThrows;
import org.afrinnov.security.dto.LoginResponse;
import org.afrinnov.sheet.rest.support.input.BrandRequest;
import org.afrinnov.sheet.rest.support.output.BrandResponse;
import org.afrinnov.cucumber.tools.StoreSession;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.IMAGE_PNG_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BrandStepdefs {

    private final WebTestClient webTestClient;
    private final StoreSession storeSession;
    private final MockMvc mockMvc;

    private final Resource resourceImage;


    public BrandStepdefs( WebTestClient webTestClient, MockMvc mockMvc, StoreSession storeSession,
                          @Value("classpath:images/nikeLogo.png") Resource resourceImage) {
        this.webTestClient = webTestClient;
        this.storeSession = storeSession;
        this.mockMvc = mockMvc;
        this.resourceImage = resourceImage;
    }

    @Given("Record of label {string} of a new brand")
    public void record_of_label_of_a_new_brand(String label) {
        BrandRequest request = new BrandRequest(label);
        storeSession.saveResult("BrandRequest", request);
    }

    @When("I post request of brand")
    public void i_post_request_of_brand() {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        BrandRequest requestBrand = storeSession.get("BrandRequest", BrandRequest.class);
        BrandResponse brandResponse = webTestClient.post().uri("/api/brands")
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(requestBrand)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .returnResult(BrandResponse.class).getResponseBody().blockFirst();
        storeSession.saveResult("savedBrand", brandResponse);
    }

    @Then("I must get the label of brand")
    public void i_must_get_the_label_of_brand() {
        BrandResponse brandSaved = storeSession.get("savedBrand", BrandResponse.class);
        assertThat(brandSaved).isNotNull();
        assertThat(brandSaved.message()).isNotEmpty();
        assertThat(brandSaved.label()).isEqualTo("NIKE");
        assertThat(brandSaved.status()).isEqualTo("CREATED");
    }

    @SneakyThrows
    @And("I add image logo to brand")
    public void i_add_image_logo_to_brand() {

        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        BrandResponse brandSaved = storeSession.get("savedBrand", BrandResponse.class);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/brands/{brandCode}/add-image-logo", brandSaved.brandCode())
                        .file(new MockMultipartFile("file", "add-image-logo.png", IMAGE_PNG_VALUE,
                                resourceImage.getContentAsByteArray()))
                        .header("Authorization", "Bearer " + loginResponse.token())
                )
                .andExpect(status().isCreated());
        MvcResult mvcResult = resultActions.andReturn();
        String responseBody = mvcResult.getResponse().getContentAsString();
        BrandResponse brandResponseWithImageLogo = new Gson().fromJson(responseBody, BrandResponse.class);

        storeSession.saveResult("savedBrandWithLogo", brandResponseWithImageLogo);
    }

    @Then("I must get label and image logo of brand")
    public void i_must_get_label_and_image_logo_of_brand() {
        BrandResponse brandSaved = storeSession.get("savedBrandWithLogo", BrandResponse.class);
        assertThat(brandSaved).isNotNull();
        assertThat(brandSaved.message()).isNotEmpty();
        assertThat(brandSaved.label()).isEqualTo("ADIDAS");
        assertThat(brandSaved.status()).isEqualTo("CREATED");
        assertThat(brandSaved.message()).contains("add-image-logo.png");
    }
}
