package org.afrinnov.cucumber.steps.product;

import io.cucumber.java.en.When;
import lombok.RequiredArgsConstructor;
import org.afrinnov.available.rest.input.EntryRequest;
import org.afrinnov.available.rest.input.EntrySize;
import org.afrinnov.available.rest.input.WithdrawalRequest;
import org.afrinnov.security.dto.LoginResponse;
import org.afrinnov.cucumber.tools.StoreSession;
import org.afrinnov.sheet.dto.ColorDto;
import org.afrinnov.sheet.dto.ProductSheet;
import org.afrinnov.sheet.dto.SizeDto;
import org.afrinnov.sheet.rest.support.input.ColorRequest;
import org.afrinnov.sheet.rest.support.input.SizeRequest;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@RequiredArgsConstructor
public class ProductEntryStepdefs {
    private final WebTestClient webTestClient;
    private final StoreSession storeSession;


    @When("I entry {string} product into store")
    public void iPostProductIntoStore(String color) {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        var entryRequest = storeSession.get(color, EntryRequest.class);
        var productSheet = storeSession.get("savedProductSheet", ProductSheet.class);
        webTestClient.post().uri("/api/product/{sku}/store/entry", productSheet.getSku())
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(entryRequest)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated();
    }

    private String givenSize(String size, LoginResponse loginResponse) {
        var request = new SizeRequest(null, size, "RAS");
        var result = webTestClient.put().uri("/api/sizes")
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(request)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .returnResult(SizeDto.class);

        var response = result.getResponseBody().blockFirst();
        assert response != null;
        return response.code();
    }

    private String givenAColor(String color, LoginResponse loginResponse) {
        var request = new ColorRequest(null, color, color);
        var result = webTestClient.put().uri("/api/colors")
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(request)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .returnResult(ColorDto.class);

        var response = result.getResponseBody().blockFirst();
        assert response != null;
        return response.code();
    }

    @When("I withdrawal {string} shoes with size {string}")
    public void iWithdrawalShoesWithSize(String color, String size) {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        String colorRef = givenAColor(color, loginResponse);
        String sizeRef = givenSize(size, loginResponse);
        var productSheet = storeSession.get("savedProductSheet", ProductSheet.class);
        WithdrawalRequest request = WithdrawalRequest.builder()
                .colorRef(colorRef)
                .withdrawalDate(LocalDateTime.now())
                .sizes(List.of(new WithdrawalRequest.WithdrawalSize(sizeRef, 1)))
                .build();
        webTestClient.post().uri("/api/product/{sku}/store/withdrawal", productSheet.getSku())
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(request)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated();
    }

    @When("I have quantity\\({int}) for each {string} shoes in the box with {string} type of size")
    public void iHaveQuantityForEachShoesInTheBoxWithTypeOfSize(int qty, String color, String sizes) {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        String colorRef = givenAColor(color, loginResponse);
        List<EntrySize> entrySizes = Arrays.stream(sizes.split(",")).map(size -> givenSize(size, loginResponse))
                .map(code -> new EntrySize(code, qty, null)).toList();

        var entryRequest = EntryRequest.builder().colorRef(colorRef).entryDate(LocalDateTime.now()).sizes(entrySizes).build();
        storeSession.saveResult(color, entryRequest);
    }
}
