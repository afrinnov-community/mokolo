package org.afrinnov.cucumber.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import lombok.RequiredArgsConstructor;
import org.afrinnov.cucumber.tools.StoreSession;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@RequiredArgsConstructor
public class ActuatorStepdefs {
    private final StoreSession storeSession;
    private final MockMvc mockMvc;
    private final WebTestClient webTestClient;


    @Given("Ping application")
    public void pingApplication() {

    }

    @When("I call actuator health endpoint")
    public void iCallActuatorEndpoint() {
        RequestSpecification request = RestAssured.given();

        WebTestClient.ResponseSpec ok = webTestClient.get().uri("/actuator/health")
                .accept(APPLICATION_JSON).exchange().expectStatus().isOk();
        storeSession.storeResponseSpec(ok);

    }
    @Then("I get response status {string}")
    public void iGetResponseStatus(String status) {
        WebTestClient.ResponseSpec responseSpec = storeSession.getResponseSpec();
        //responseSpec.expectBody().jsonPath("$.status").isEqualTo(status);

    }
}
