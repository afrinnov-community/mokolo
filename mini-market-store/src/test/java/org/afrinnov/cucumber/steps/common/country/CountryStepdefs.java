package org.afrinnov.cucumber.steps.common.country;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.afrinnov.security.dto.LoginRequest;
import org.afrinnov.sheet.dto.Country;
import org.afrinnov.sheet.rest.support.input.CountryRequest;
import org.afrinnov.sheet.rest.support.output.CountryResponse;
import org.afrinnov.cucumber.tools.StoreSession;
import org.afrinnov.security.dto.LoginResponse;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;

public class CountryStepdefs {


    private final WebTestClient webTestClient;
    private final StoreSession storeSession;

    public CountryStepdefs(WebTestClient webTestClient, StoreSession storeSession) {
        this.webTestClient = webTestClient;
        this.storeSession = storeSession;
    }

    @Before("@AsAdminUser")
    public void asAdminUser() {
        LoginRequest loginRequest = new LoginRequest("admin", "admin");

        LoginResponse loginResponse = webTestClient.post().uri("/api/authenticate")
                .contentType(APPLICATION_JSON)
                .bodyValue(loginRequest)
                .accept(APPLICATION_JSON).exchange().expectStatus().isOk()
                .returnResult(LoginResponse.class).getResponseBody().blockFirst();
        storeSession.saveResult("loginResponse", loginResponse);
    }

    @Given("Record of iso code {string} english name {string} and name {string} of new country")
    public void recordOfIsoCodeEnglishNameAndNameOfNewCountry(String isoCode, String nameUs, String name) {
        CountryRequest request = new CountryRequest(isoCode, name, nameUs);
        storeSession.saveResult("CountryRequest", request);
    }


    @When("I post request of country")
    public void iPostRequestOfCountry() {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        CountryRequest requestCountry = storeSession.get("CountryRequest", CountryRequest.class);
        CountryResponse countryResponse = webTestClient.post().uri("/api/countries")
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(requestCountry)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .returnResult(CountryResponse.class).getResponseBody().blockFirst();
        storeSession.saveResult("savedCountry", countryResponse);

    }

    @Then("I must get the iso code of country")
    public void i_must_get_the_iso_code_of_country() {
        CountryResponse countrySaved = storeSession.get("savedCountry", CountryResponse.class);
        assertThat(countrySaved).isNotNull();
        assertThat(countrySaved.code()).isEqualTo("CMR");
        assertThat(countrySaved.status()).isEqualTo("CREATED");
    }

    @And("I select this country")
    public void iSelectThisCountry() {
        CountryRequest requestCountry = storeSession.get("CountryRequest", CountryRequest.class);
        assertThat(requestCountry).isNotNull();
        storeSession.saveResult("selectedCountry", Country.builder().isoCode(requestCountry.code()).build());
    }

    @And("this element content {string} telephone code of country")
    public void thisElementContentTelephoneCodeOfCountry(String telephoneCode) {
        CountryRequest request = storeSession.get("CountryRequest", CountryRequest.class);
        List<Country> countries = storeSession.getArray("countries", Country.class);
        assertThat(countries).hasSizeGreaterThanOrEqualTo(1);
        Country country = countries.stream().filter(elt -> request.code().equals(elt.getIsoCode())).findFirst()
                .orElseThrow();
        assertThat(country.getIsoCode()).isEqualTo(request.code());
        assertThat(country.getTelephoneCode()).isNotNull();
        assertThat(country.getTelephoneCode().getCode()).isEqualTo(telephoneCode);

    }
}
