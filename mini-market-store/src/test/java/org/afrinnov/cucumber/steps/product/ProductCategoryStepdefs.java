package org.afrinnov.cucumber.steps.product;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.afrinnov.security.dto.LoginResponse;
import org.afrinnov.cucumber.tools.StoreSession;
import org.afrinnov.sheet.dto.CategorySizes;
import org.afrinnov.sheet.rest.support.input.CategoryRequest;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;

public class ProductCategoryStepdefs {
    private final WebTestClient webTestClient;
    private final StoreSession storeSession;

    public ProductCategoryStepdefs(WebTestClient webTestClient, StoreSession storeSession) {
        this.webTestClient = webTestClient;
        this.storeSession = storeSession;
    }

    @Given("I have the {string} and the {string} of the category")
    public void iHaveCategoryInformation(String label, String description) {
        CategoryRequest categoryRequest = new CategoryRequest(label,description,"0005");
        storeSession.saveResult("ProductCategoryRequest",categoryRequest);
    }

    @When("I post request to initialize the category")
    public void iPostRequestToInitializeCategory() {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        CategoryRequest categoryRequest = storeSession.get("ProductCategoryRequest", CategoryRequest.class);

        FluxExchangeResult<CategorySizes> result = webTestClient.post().uri("/api/categories")
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(categoryRequest)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .returnResult(CategorySizes.class);

        List<String> location = result.getResponseHeaders().get("Location");
        assertThat(location).isNotNull().hasSize(1);

        CategorySizes response = result.getResponseBody().blockFirst();
        storeSession.saveResult("ProductCategoryResponseUri", location.getFirst());
        storeSession.saveResult("savedProductCategory", response);
    }

    @Then("I get category link which content the code of this category")
    public void iGetCategoryWithCode() {
        String uri = storeSession.get("ProductCategoryResponseUri", String.class);
        assertThat(uri).contains("/api/categories/");

        CategorySizes category = storeSession.get("savedProductCategory", CategorySizes.class);
        assertThat(category).isNotNull();
        assertThat(category.code()).isNotNull();
    }

    @And("I get request to retrieve the new category")
    public void iRequestTheSavedCategory() {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        var uri = storeSession.get("ProductCategoryResponseUri", String.class);
        var category = storeSession.get("savedProductCategory", CategorySizes.class);

        webTestClient.get().uri(uri)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .accept(APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody(CategorySizes.class)
                .consumeWith(result -> assertThat(Objects.requireNonNull(result.getResponseBody()).code()).isEqualTo(category.code()));
        //var response = responseSpec.returnResult(ProductCategory.class).getResponseBody().blockFirst();
        /*assertThat(response).isNotNull();*/
    }
}
