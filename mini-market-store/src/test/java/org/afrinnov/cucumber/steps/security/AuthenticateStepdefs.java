package org.afrinnov.cucumber.steps.security;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.RequiredArgsConstructor;
import org.afrinnov.security.dto.*;
import org.afrinnov.sheet.services.NotifyService;
import org.afrinnov.security.Messager;
import org.afrinnov.cucumber.tools.StoreSession;
import org.mockito.ArgumentCaptor;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Map;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RequiredArgsConstructor
public class AuthenticateStepdefs {

    public static final String NEW_USER_EMAIL_KEY = "newUserEmail";
    private final WebTestClient webTestClient;
    private final StoreSession storeSession;
    private final NotifyService notifyService;

    @Given("I have a user with user name {string} and password {string}")
    public void i_have_a_user_with_user_name_and_password(String userName, String password) {
        LoginRequest loginRequest = new LoginRequest(userName, password);
        storeSession.saveResult("loginRequest", loginRequest);
    }

    @When("I post a login request")
    public void i_post_a_login_request() {
        LoginRequest loginRequest = storeSession.get("loginRequest", LoginRequest.class);
        LoginResponse loginResponse = webTestClient.post().uri("/api/authenticate")
                .contentType(APPLICATION_JSON)
                .bodyValue(loginRequest)
                .accept(APPLICATION_JSON).exchange().expectStatus().isOk()
                .returnResult(LoginResponse.class).getResponseBody().blockFirst();
        storeSession.saveResult("loginResponse", loginResponse);
    }

    @Then("I get the jwt token")
    public void i_get_the_jwt_token() {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        assertThat(loginResponse).isNotNull();
        assertThat(loginResponse.token()).isNotEmpty();
    }

    @Then("I get the refresh token")
    public void i_get_the_refresh_token() {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        assertThat(loginResponse).isNotNull();
        assertThat(loginResponse.refreshToken()).isNotEmpty();
    }

    @Then("I get {string} as response")
    public void iGetAsResponse(String message) {
        UserConnected userConnected = storeSession.get("userConnected", UserConnected.class);
        assertThat(userConnected).isNotNull();
        assertThat(userConnected.getMessage()).isEqualTo(message);

    }

    @When("I post refresh token")
    public void iPostRefreshToken() {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        LoginResponse loginRefreshResponse = webTestClient.post().uri("/api/refresh-token")
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.refreshToken()))
                .accept(APPLICATION_JSON).exchange().expectStatus().isOk()
                .returnResult(LoginResponse.class).getResponseBody().blockFirst();
        assertThat(loginRefreshResponse).isNotNull();
        storeSession.saveResult("loginRefreshResponse", loginRefreshResponse);
    }

    @Then("I check the new jwt token")
    public void iCheckTheNewJwtToken() {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        LoginResponse loginRefreshResponse = storeSession.get("loginRefreshResponse", LoginResponse.class);
        assertThat(loginRefreshResponse.token()).isNotEqualTo(loginResponse.token());
        assertThat(loginRefreshResponse.refreshToken()).isEqualTo(loginResponse.refreshToken());
    }

    @Given("I have new admin user data information")
    public void iHaveNewAdminUserDataInformation() {
        String email = givenUserEmail();
        RegistrationRequest request = RegistrationRequest.builder()
                .email(email)
                .firstname("Guy")
                .phoneNumber("+237968541" + randomNumeric(4))
                .groupCode("000000")
                .build();
        storeSession.saveResult("RegistrationRequest", request);
        storeSession.saveResult(NEW_USER_EMAIL_KEY, email);
    }

    private static String givenUserEmail() {
        return String.format("goodadmin%s@afrinnov.com", randomAlphanumeric(5));
    }

    @When("I send a new user registration request")
    public void iSendANewUserRegistrationRequest() {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        var body = storeSession.get("RegistrationRequest", RegistrationRequest.class);
        when(notifyService.sendMail(any(Messager.class))).thenReturn(true);
        webTestClient.post().uri("/api/users/registration")
                .contentType(APPLICATION_JSON)
                .bodyValue(body)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .expectBody().jsonPath("$.status").isEqualTo("CREATED");
    }

    @Then("I check if new user request is pending and mail sent to new user address mail")
    public void iCheckIfNewUserRequestIsPendingAndMailSentToNewUserAddressMail() {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        String email = storeSession.get(NEW_USER_EMAIL_KEY, String.class);
        var regUser = webTestClient.get().uri("/api/users/{email}", email)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .accept(APPLICATION_JSON).exchange().expectStatus().isOk()
                .returnResult(RegUser.class).getResponseBody().blockFirst();
        assert regUser != null;
        assertThat(regUser.getConfirmationPending()).isEqualTo("PENDING");
        assertThat(regUser.getMailSent()).isEqualTo("SENT");
    }

    @Given("Admin create my account with my user data information")
    public void adminCreateMyAccountWithMyUserDataInformation() {
        LoginRequest loginRequest = new LoginRequest("admin", "admin");

        LoginResponse loginResponse = webTestClient.post().uri("/api/authenticate")
                .contentType(APPLICATION_JSON)
                .bodyValue(loginRequest)
                .accept(APPLICATION_JSON).exchange().expectStatus().isOk()
                .returnResult(LoginResponse.class).getResponseBody().blockFirst();
        assertThat(loginResponse).isNotNull();
        String email = givenUserEmail();
        storeSession.saveResult(NEW_USER_EMAIL_KEY, email);
        RegistrationRequest body = RegistrationRequest.builder()
                .email(email)
                .firstname("Guy")
                .phoneNumber("+237968541" + randomNumeric(4))
                .groupCode("000000")
                .build();
        webTestClient.post().uri("/api/users/registration")
                .contentType(APPLICATION_JSON)
                .bodyValue(body)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .expectBody().jsonPath("$.status").isEqualTo("CREATED");
        var messagerCaptor = ArgumentCaptor.forClass(Messager.class);
        verify(notifyService).sendMail(messagerCaptor.capture());
        Messager messager = messagerCaptor.getValue();
        assertThat(messager).isNotNull();
        assertThat(messager.data()).isNotNull();
        assertThat(messager.data().get("otp")).isNotNull();
        storeSession.saveResult("generatedOTP", messager.data().get("otp"));
    }

    @When("After receiving OTP code, i send request with new password and otp code to activate an account")
    public void afterReceivingOTPCodeISendRequestWithNewPasswordAndOtpCodeToActivateAnAccount() {
        String generatedOTP = storeSession.get("generatedOTP", String.class);
        String email = storeSession.get(NEW_USER_EMAIL_KEY, String.class);
        Map<String, Object> body = Map.of(
                "email", email,
                "otp", generatedOTP,
                "newPassword", "Stong@password123"
        );
        webTestClient.post().uri("/api/users/first-change-password")
                .contentType(APPLICATION_JSON)
                .bodyValue(body)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .expectBody().jsonPath("$.status").isEqualTo("CHANGED");
    }

    @Then("I receive new mail witch confirm account is activated")
    public void iReceiveNewMailWitchConfirmAccountIsActivated() {
        var messagerCaptor = ArgumentCaptor.forClass(Messager.class);
        verify(notifyService, times(2)).sendMail(messagerCaptor.capture());
        Messager messager = messagerCaptor.getValue();
        assertThat(messager).isNotNull();
        assertThat(messager.data()).isNotNull();
    }

    @When("I use my account to connect")
    public void iUseMyAccountToConnect() {
        String email = storeSession.get(NEW_USER_EMAIL_KEY, String.class);
        LoginRequest loginRequest = new LoginRequest(email, "Stong@password123");

        LoginResponse loginResponse = webTestClient.post().uri("/api/authenticate")
                .contentType(APPLICATION_JSON)
                .bodyValue(loginRequest)
                .accept(APPLICATION_JSON).exchange().expectStatus().isOk()
                .returnResult(LoginResponse.class).getResponseBody().blockFirst();
        assertThat(loginResponse).isNotNull();
        storeSession.saveResult("loginResponse", loginResponse);
    }

    @When("I resend a new OTP code")
    public void iResendANewOTPCode() {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        String email = storeSession.get(NEW_USER_EMAIL_KEY, String.class);
        var body = new UserOtpRequest(email);
        when(notifyService.sendMail(any(Messager.class))).thenReturn(true);
        webTestClient.post().uri("/api/resend-otp")
                .contentType(APPLICATION_JSON)
                .bodyValue(body)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .accept(APPLICATION_JSON).exchange().expectStatus().isOk()
                .expectBody().jsonPath("$.status").isEqualTo("UPDATED");
    }

    @Then("I check if new user receive a otp mail and user status is in pending mode")
    public void iCheckIfNewUserReceiveAOtpMailAndUserStatusIsInPendingMode() {
        var messagerCaptor = ArgumentCaptor.forClass(Messager.class);
        verify(notifyService, times(2)).sendMail(messagerCaptor.capture());
        Messager messager = messagerCaptor.getValue();
        assertThat(messager).isNotNull();
        assertThat(messager.data()).isNotNull();
    }
}
