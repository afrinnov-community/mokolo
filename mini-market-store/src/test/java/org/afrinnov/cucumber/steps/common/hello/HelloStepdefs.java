package org.afrinnov.cucumber.steps.common.hello;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.RequiredArgsConstructor;
import org.afrinnov.cucumber.tools.StoreSession;
import org.afrinnov.security.dto.UserConnected;
import org.afrinnov.security.dto.LoginResponse;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.StatusAssertions;
import org.springframework.test.web.reactive.server.WebTestClient;

import static java.util.Optional.ofNullable;

@RequiredArgsConstructor
public class HelloStepdefs {
    private final WebTestClient webTestClient;
    private final StoreSession storeSession;


    @When("I say hello")
    public void iSayHello() {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        WebTestClient.ResponseSpec exchange = webTestClient.get().uri("/api/hello")
                .headers(headers -> ofNullable(loginResponse).ifPresent(response -> headers.setBearerAuth(response.token())))
                .accept(MediaType.APPLICATION_JSON)
                .exchange();
        StatusAssertions status = exchange.expectStatus();
        try {
            UserConnected userConnected = exchange.returnResult(UserConnected.class)
                    .getResponseBody().blockFirst();
            storeSession.saveResult("userConnected", userConnected);
        } catch (Exception ignore) {
        }

        storeSession.storeHttpStatus(status);
    }

    @Then("I receive {int} http status error message")
    public void iReceiveHttpStatusErrorMessage(int httpStatusCode) {
        StatusAssertions statusAssertions = storeSession.getStatusAssertions();
        statusAssertions.isEqualTo(httpStatusCode);
    }

    @Then("I receive {int} message")
    public void iReceiveMessage(int httpStatusCode) {
        StatusAssertions statusAssertions = storeSession.getStatusAssertions();
        statusAssertions.isEqualTo(httpStatusCode);
    }
}
