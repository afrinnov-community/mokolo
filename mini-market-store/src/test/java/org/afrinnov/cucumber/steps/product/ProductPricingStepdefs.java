package org.afrinnov.cucumber.steps.product;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.RequiredArgsConstructor;
import org.afrinnov.security.dto.LoginResponse;
import org.afrinnov.cucumber.tools.StoreSession;
import org.afrinnov.pricing.dto.Price;
import org.afrinnov.pricing.dto.Prices;
import org.afrinnov.pricing.dto.PricingNature;
import org.afrinnov.pricing.id.PricingCode;
import org.afrinnov.pricing.rest.input.PriceRequest;
import org.afrinnov.sheet.dto.ProductSheet;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RequiredArgsConstructor
public class ProductPricingStepdefs {
    private final WebTestClient webTestClient;
    private final StoreSession storeSession;

    @And("I have {string} pricing with purchase price {int}, selling price {int} and currency {string}")
    public void iHavePricingWithPurchasePriceSellingPriceAndCurrency(String nature,
                                                                     Integer purchasePrice, Integer sellingPrice, String currency) {
        var request = PriceRequest.builder()
                .sellingPrice(sellingPrice.doubleValue())
                .purchasePrice(purchasePrice.doubleValue())
                .currency(currency)
                .build();
        storeSession.saveResult("pricingRequest", request);
    }

    @When("I post a {string} pricing for an article")
    public void iPostAPricingForAnArticle(String nature) {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        var pricingRequest = storeSession.get("pricingRequest", PriceRequest.class);
        var productSheet = storeSession.get("savedProductSheet", ProductSheet.class);

        webTestClient.post().uri("/api/product/{sku}/pricing/{nature}/nature",
                        productSheet.getSku(), PricingNature.CLASSIC)
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(pricingRequest)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .returnResult(PricingCode.class).getResponseBody().blockFirst();
    }

    @When("I request prices of product")
    public void iRequestPricesOfProduct() {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        var productSheet = storeSession.get("savedProductSheet", ProductSheet.class);
        var result = webTestClient.get().uri("/api/product/{sku}/pricing", productSheet.getSku())
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .accept(APPLICATION_JSON).exchange().expectStatus().isOk()
                .returnResult(Prices.class);
        storeSession.saveResult("prices", result.getResponseBody().blockFirst());
    }

    @Then("I check {string} pricing with purchase price {int}, selling price {int} and {string} currency")
    public void iCheckPricingWithPurchasePriceSellingPriceAndCurrency(String nature, Integer purchasePrice,
                                                                      Integer sellingPrice, String currency) {
        var prices = storeSession.get("prices", Prices.class);
        assertThat(prices).isNotNull();
        assertThat(prices.prices()).isNotEmpty();
        Optional<Price> result = prices.prices().stream().filter(p -> nature.equals(p.getNature())).findFirst();
        assertThat(result).isPresent();
        Price price = result.get();
        assertThat(price.getSellingPrice()).isEqualTo(sellingPrice.doubleValue());
        assertThat(price.getPurchasePrice()).isEqualTo(purchasePrice.doubleValue());
        assertThat(price.getCurrency()).isEqualTo(currency);
        assertThat(price.getCode()).isNotEmpty();

    }
}
