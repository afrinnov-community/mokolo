package org.afrinnov.cucumber.steps.common.delivery;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.dto.DeliveryMode;
import org.afrinnov.sheet.rest.support.input.DeliveryModeRequest;
import org.afrinnov.sheet.rest.support.output.DeliveryModeResponse;
import org.afrinnov.cucumber.tools.StoreSession;
import org.afrinnov.security.dto.LoginResponse;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  27/12/2023 -- 06:08<br></br>
 * By : @author alexk<br></br>
 * Project : mokolo<br></br>
 * Package : org.afrinnov.cucumber.steps.common.delivery<br></br>
 */

@RequiredArgsConstructor
public class DeliveryModeStepdefs {
    private final WebTestClient webTestClient;
    private final StoreSession storeSession;

    @Given("Record of {string} delivery mode and his description {string}")
    public void record_of_delivery_mode_and_his_description(String mode, String description) {
        var request = new DeliveryModeRequest(mode, description);
        storeSession.saveResult("DeliveryModeRequest", request);
    }
    @When("I post request of delivery mode")
    public void i_post_request_of_delivery_mode() {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        DeliveryModeRequest request = storeSession.get("DeliveryModeRequest", DeliveryModeRequest.class);
        DeliveryModeResponse response = webTestClient.post().uri("/api/delivery-mode")
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(request)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .returnResult(DeliveryModeResponse.class).getResponseBody().blockFirst();
        storeSession.saveResult("DeliveryModeResponse", response);
    }
    @Then("I find this delivery mode by {string} value")
    public void i_find_this_delivery_mode_by_value(String mode) {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        webTestClient.get().uri("/api/delivery-mode/{mode}", mode)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .exchange().expectStatus().isOk().expectBody()
                .jsonPath("$.mode").isEqualTo(mode);
    }

    @Then("I check {string} delivery mode when I retrieve all the delivery mode")
    public void i_check_delivery_mode_when_i_retrieve_all_the_delivery_mode(String mode) {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        List<DeliveryMode> deliveryModes = webTestClient.get().uri("/api/delivery-mode", mode)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .exchange().expectStatus().isOk()
                .returnResult(DeliveryMode.class).getResponseBody().collectList().block();

        assertThat(deliveryModes).isNotEmpty();
        assertThat(deliveryModes.stream().anyMatch(deliveryMode -> deliveryMode.mode().equals(mode))).isTrue();
    }

}
