package org.afrinnov.cucumber.steps.telephone;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.afrinnov.sheet.dto.Country;
import org.afrinnov.cucumber.tools.StoreSession;
import org.afrinnov.security.dto.LoginResponse;
import org.afrinnov.sheet.id.TelephoneCode;
import org.afrinnov.sheet.rest.support.input.TelephoneCodeRequest;
import org.afrinnov.sheet.rest.input.TelephoneCodeValidRequest;
import org.afrinnov.sheet.rest.output.TelephoneCodeValidatedResponse;
import org.afrinnov.sheet.rest.support.output.TelephoneResponse;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;


public class TelephoneStepdefs {
    private final WebTestClient webTestClient;
    private final StoreSession storeSession;

    public TelephoneStepdefs(WebTestClient webTestClient, StoreSession storeSession) {
        this.webTestClient = webTestClient;
        this.storeSession = storeSession;
    }

    @And("I select {string} country")
    public void iSelectCountry(String isoCode) {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        List<Country> countries = webTestClient.get().uri("/api/countries")
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .exchange().expectStatus().isOk()
                .returnResult(Country.class).getResponseBody().collectList().block();
        assertThat(countries).isNotEmpty();
        countries.stream().filter(country -> isoCode.equals(country.getIsoCode())).findFirst()
                .ifPresent(country -> storeSession.saveResult("selectedCountry", country));
    }

    @Given("I prepare a record with code {string} and country")
    public void iPrepareARecordWithCodeAndCountry(String code) {
        Country selectedCountry = storeSession.get("selectedCountry", Country.class);
        assertThat(selectedCountry).isNotNull();
        TelephoneCodeRequest request = new TelephoneCodeRequest(code, selectedCountry.getIsoCode(), null);
        storeSession.saveResult("TelephoneCodeRequest", request);
    }

    @When("I post request to save telephone code")
    public void iPostRequest() {
        var request = storeSession.get("TelephoneCodeRequest", TelephoneCodeRequest.class);
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        TelephoneResponse telephoneResponse = webTestClient.post().uri("/api/telephone-code")
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(request)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .returnResult(TelephoneResponse.class).getResponseBody().blockFirst();
        storeSession.saveResult("savedResponse", telephoneResponse);
    }

    @Then("I must check new telephone code added")
    public void iMustCheckNewTelephoneCodeAdded() {
        TelephoneResponse telephoneResponse = storeSession.get("savedResponse", TelephoneResponse.class);
        assertThat(telephoneResponse).isNotNull();
        assertThat(telephoneResponse.code()).isEqualTo("237");
        assertThat(telephoneResponse.status()).isEqualTo("CREATED");
    }

    @Given("Load countries")
    public void loadCountries() {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        List<Country> countries = webTestClient.get().uri("/api/countries")
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .exchange().expectStatus().isOk()
                .returnResult(Country.class).getResponseBody().collectList().block();

        assertThat(countries).isNotEmpty();

        storeSession.saveResult("countries", countries);
    }


    @When("I send to delete {string} telephone code")
    public void iSendToDeleteTelephoneCode(String code) {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        webTestClient.delete().uri("/api/telephone-code/{code}", code)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody().jsonPath("$.status", Predicate.isEqual("DELETED"));
    }

    @Then("I get {int} status code when i find {string} telephone code")
    public void iGetStatusCodeWhenIFindTelephoneCode(int httpStatus, String code) {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        TelephoneCode telephoneCode = webTestClient.get().uri("/api/telephone-code/{code}", code)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .exchange().expectStatus().isEqualTo(httpStatus).returnResult(TelephoneCode.class)
                .getResponseBody().blockFirst();
        storeSession.saveResult("TelephoneCode", telephoneCode);
    }

    @And("I check current telephone code with enable {string}")
    public void iCheckCurrentTelephoneCodeWithEnable(String enable) {
        TelephoneCode telephoneCode = storeSession.get("TelephoneCode", TelephoneCode.class);
        assertThat(telephoneCode).isNotNull();
        assertThat(telephoneCode.isEnable()).isEqualTo(Boolean.getBoolean(enable));
    }

    @And("I prepare a record with code {string}, country and validation format {string}")
    public void iPrepareARecordWithCodeCountryAndValidationFormat(String code, String format) {
        Country selectedCountry = storeSession.get("selectedCountry", Country.class);
        assertThat(selectedCountry).isNotNull();
        TelephoneCodeRequest request = new TelephoneCodeRequest(code, selectedCountry.getIsoCode(), format);
        storeSession.saveResult("TelephoneCodeRequest", request);
    }

    @When("I request check phone numbers are valide")
    public void iRequestCheckPhoneNumbersAreValide(DataTable dataTable) {
        TelephoneResponse telephoneResponse = storeSession.get("savedResponse", TelephoneResponse.class);
        Map<String, String> data = dataTable.asMap();
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        data.forEach((phoneNumber, expectedResult) -> {
            var validatedResponse = webTestClient.post()
                    .uri("/api/telephone-code/{code}/validate-format", telephoneResponse.code())
                    .bodyValue(new TelephoneCodeValidRequest(phoneNumber))
                    .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                    .exchange().expectStatus().isOk().returnResult(TelephoneCodeValidatedResponse.class)
                    .getResponseBody().blockFirst();
            assertThat(validatedResponse).isNotNull();
            assertThat(validatedResponse.isMatches()).isEqualTo(Boolean.valueOf(expectedResult));
        });
    }

    @Then("I check current telephone number with status")
    public void iCheckCurrentTelephoneNumberWithStatus() {

    }


}
