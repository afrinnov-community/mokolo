package org.afrinnov.cucumber.steps.common.shipping;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.RequiredArgsConstructor;
import org.afrinnov.sheet.rest.support.input.ShippingModeRequest;
import org.afrinnov.sheet.rest.support.output.ShippingModeResponse;
import org.afrinnov.cucumber.tools.StoreSession;
import org.afrinnov.security.dto.LoginResponse;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@RequiredArgsConstructor
public class ShippingModeStepdefs {
    private final WebTestClient webTestClient;
    private final StoreSession storeSession;
    @Given("Record of {string} mode and his description {string}")
    public void recordOfModeAndHisDescription(String mode, String description) {
        var request = new ShippingModeRequest(mode, description);
        storeSession.saveResult("ShippingModeRequest", request);
    }

    @When("I post request of shipping mode")
    public void iPostRequestOfShippingMode() {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        ShippingModeRequest request = storeSession.get("ShippingModeRequest", ShippingModeRequest.class);
        ShippingModeResponse response = webTestClient.post().uri("/api/shipping-mode")
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(request)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .returnResult(ShippingModeResponse.class).getResponseBody().blockFirst();
        storeSession.saveResult("ShippingModeResponse", response);
    }

    @Then("I find this shipping mode by {string} value")
    public void iFindThisShippingModeByValue(String mode) {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        webTestClient.get().uri("/api/shipping-mode/{mode}", mode)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .exchange().expectStatus().isOk().expectBody()
                .jsonPath("$.mode").isEqualTo(mode);
    }
}
