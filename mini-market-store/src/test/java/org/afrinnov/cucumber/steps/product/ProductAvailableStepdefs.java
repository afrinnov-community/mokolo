package org.afrinnov.cucumber.steps.product;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.RequiredArgsConstructor;
import org.afrinnov.available.dto.Availables;
import org.afrinnov.security.dto.LoginResponse;
import org.afrinnov.cucumber.tools.StoreSession;
import org.afrinnov.sheet.dto.ProductSheet;
import org.afrinnov.sheet.rest.input.ProductSheetRequest;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;
import java.util.Optional;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RequiredArgsConstructor
public class ProductAvailableStepdefs {
    private final WebTestClient webTestClient;
    private final StoreSession storeSession;

    @Given("I have articles in the box")
    public void iHaveArticlesInTheBox() {
        var request = ProductSheetRequest.builder().title("hello" + randomAlphabetic(5)).build();
        storeSession.saveResult("ProductSheetRequest", request);
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        FluxExchangeResult<ProductSheet> result = webTestClient.post().uri("/api/product/sheet")
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(request)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .returnResult(ProductSheet.class);
        List<String> location = result.getResponseHeaders().get("Location");
        assertThat(location).isNotNull().hasSize(1);
        var response = result.getResponseBody().blockFirst();
        storeSession.saveResult("ProductSheetResponseUri", location.getFirst());
        storeSession.saveResult("savedProductSheet", response);
    }


    @When("I request available product")
    public void iRequestAvailableProduct() {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        var productSheet = storeSession.get("savedProductSheet", ProductSheet.class);
        var result = webTestClient.get().uri("/api/product/{sku}/store", productSheet.getSku())
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .accept(APPLICATION_JSON).exchange().expectStatus().isOk()
                .returnResult(Availables.class);
        storeSession.saveResult("available", result.getResponseBody().blockFirst());
    }


    @Then("I check two colors list")
    public void iCheckTwoColorsList() {
        var available = storeSession.get("available", Availables.class);
        assertThat(available).isNotNull();
        assertThat(available.colors()).hasSize(2);
    }

    @And("I check list size\\({int}) for {string} color")
    public void iCheckListSizeForColor(int sizeCount, String color) {
        var available = storeSession.get("available", Availables.class);
        assertThat(available).isNotNull();
        Optional<Availables.Color> first = available.colors().stream().filter(col -> color.equals(col.label())).findFirst();
        assertThat(first).isPresent();
        assertThat(first.get().sizes()).hasSize(sizeCount);


    }

    @And("I check quantity\\({int}) of {string} and size {string} shoe")
    public void iCheckQuantityOfAndSizeShoe(int qty, String color, String size) {
        var available = storeSession.get("available", Availables.class);
        assertThat(available).isNotNull();
        Optional<Availables.Color> first = available.colors().stream().filter(col -> color.equals(col.label())).findFirst();
        assertThat(first).isPresent();
        assertThat(first.get().sizes()).isNotEmpty();
        Optional<Availables.Size> result = first.get().sizes().stream()
                .filter(siz -> size.equals(siz.label())).findFirst();
        assertThat(result).isPresent();
        assertThat(result.get().qty()).isEqualTo(qty);

    }


}
