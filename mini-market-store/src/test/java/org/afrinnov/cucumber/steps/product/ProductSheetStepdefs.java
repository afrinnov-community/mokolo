package org.afrinnov.cucumber.steps.product;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.afrinnov.security.dto.LoginResponse;
import org.afrinnov.sheet.dto.ShippingMode;
import org.afrinnov.cucumber.tools.StoreSession;
import org.afrinnov.sheet.dto.ProductSheet;
import org.afrinnov.sheet.rest.input.ProductSheetRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.IMAGE_PNG_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductSheetStepdefs {
    private final WebTestClient webTestClient;
    private final MockMvc mockMvc;
    private final StoreSession storeSession;
    private final Resource resourceImage1;
    private final Resource resourceImage2;

    public ProductSheetStepdefs(WebTestClient webTestClient, MockMvc mockMvc, StoreSession storeSession,
                                @Value("classpath:images/image1.png") Resource resourceImage1,
                                @Value("classpath:images/image2.jpg") Resource resourceImage2) {
        this.webTestClient = webTestClient;
        this.mockMvc = mockMvc;
        this.storeSession = storeSession;
        this.resourceImage1 = resourceImage1;
        this.resourceImage2 = resourceImage2;
    }

    @Given("I have {string} product sheet data information")
    public void iHaveProductSheetDataInformation(String productData) {
        if ("product with shipping mode".equals(productData)) {
            ProductSheetRequest request = ProductSheetRequest.builder()
                    .shippingModes(List.of("PLANE", "BOAT"))
                    .title("hello shipping mode").build();
            storeSession.saveResult("ProductSheetRequest", request);
        } else {
            storeSession.saveResult("ProductSheetRequest", ProductSheetRequest.builder().title("hello").build());
        }
    }

    @When("I post request to initialize the product")
    public void iPostRequestToInitializeTheProduct() {
        LoginResponse loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        ProductSheetRequest request = storeSession.get("ProductSheetRequest", ProductSheetRequest.class);
        FluxExchangeResult<ProductSheet> result = webTestClient.post().uri("/api/product/sheet")
                .contentType(APPLICATION_JSON)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .bodyValue(request)
                .accept(APPLICATION_JSON).exchange().expectStatus().isCreated()
                .returnResult(ProductSheet.class);
        List<String> location = result.getResponseHeaders().get("Location");
        assertThat(location).isNotNull().hasSize(1);
        ProductSheet response = result.getResponseBody().blockFirst();
        storeSession.saveResult("ProductSheetResponseUri", location.getFirst());
        storeSession.saveResult("savedProductSheet", response);
    }

    @Then("I get product sheet link witch content sku of this product")
    public void iGetProductSheetLinkWitchContentSkuOfThisProduct() {
        var uri = storeSession.get("ProductSheetResponseUri", String.class);
        assertThat(uri).contains("/api/product/sheet/");
        var productSheet = storeSession.get("savedProductSheet", ProductSheet.class);
        assertThat(productSheet).isNotNull();
    }

    @And("I get request to retrieve the new product")
    public void iGetRequestToRetrieveTheNewProduct() {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        var uri = storeSession.get("ProductSheetResponseUri", String.class);
        var productSheet = storeSession.get("savedProductSheet", ProductSheet.class);
        WebTestClient.ResponseSpec responseSpec = webTestClient.get().uri(uri)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .exchange().expectStatus().isOk();
        responseSpec.expectBody()
                .jsonPath("$.sku").isEqualTo(productSheet.getSku());
        var response = responseSpec.returnResult(ProductSheet.class).getResponseBody().blockFirst();
        storeSession.saveResult("ProductSheet", response);
    }

    @And("I check a supported shipping mode of product")
    public void iCheckASupportedShippingModeOfProduct() {
        var productSheet = storeSession.get("ProductSheet", ProductSheet.class);
        assertThat(productSheet).isNotNull();
        assertThat(productSheet.getSupportedShippingModes()).hasSize(2);
        assertThat(productSheet.getSupportedShippingModes().stream().map(ShippingMode::mode)).contains("PLANE", "BOAT");
    }

    @When("I add preview image for product sheet")
    public void iAddPreviewImageForProductSheet() throws Exception {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        var productSheet = storeSession.get("savedProductSheet", ProductSheet.class);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/product/sheet/{sku}/preview-image", productSheet.getSku())
                        .file(new MockMultipartFile("file", "preview-image.png", IMAGE_PNG_VALUE,
                                resourceImage1.getContentAsByteArray()))
                        .header("Authorization", "Bearer " + loginResponse.token())
                )
                .andExpect(status().isCreated());

    }

    @And("Id add wizard images for product sheet")
    public void idAddWizardImagesForProductSheet() throws Exception {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        var productSheet = storeSession.get("savedProductSheet", ProductSheet.class);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/product/sheet/{sku}/wizard-image", productSheet.getSku())
                        .file(new MockMultipartFile("file", "preview-image.jpg", IMAGE_PNG_VALUE,
                                resourceImage2.getContentAsByteArray()))
                        .header("Authorization", "Bearer " + loginResponse.token())
                )
                .andExpect(status().isCreated());
        //mvcResult.getResponse().get;
    }

    @Then("I check a preview image url of product sheet")
    public void iCheckAPreviewImageUrlOfProductSheet() {
        var loginResponse = storeSession.get("loginResponse", LoginResponse.class);
        var uri = storeSession.get("ProductSheetResponseUri", String.class);
        var productSheet = storeSession.get("savedProductSheet", ProductSheet.class);
        WebTestClient.ResponseSpec responseSpec = webTestClient.get().uri(uri)
                .headers(headers -> headers.setBearerAuth(loginResponse.token()))
                .exchange().expectStatus().isOk();
        responseSpec.expectBody()
                .jsonPath("$.sku").isEqualTo(productSheet.getSku());
        var response = responseSpec.returnResult(ProductSheet.class).getResponseBody().blockFirst();
        assertThat(response).isNotNull();
        assertThat(response.getPreviewImageUrl()).contains("/api/files/open/");
        storeSession.saveResult("ProductSheet", response);
    }

    @And("I check a list of others images url of product sheet")
    public void iCheckAListOfOthersImagesUrlOfProductSheet() {
        var response = storeSession.get("ProductSheet", ProductSheet.class);
        assertThat(response).isNotNull();
        assertThat(response.getWizardImageUrls()).hasSize(1);
    }
}
