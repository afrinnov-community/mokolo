package org.afrinnov.cucumber.tools;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import lombok.Getter;
import lombok.Setter;
import org.springframework.test.web.reactive.server.StatusAssertions;
import org.springframework.test.web.reactive.server.WebTestClient.ResponseSpec;

import java.util.*;

@Getter
public class StoreSession {
    private final Map<String, Object> storage = new HashMap<>();
    private final ObjectMapper objectMapper;
    @Setter
    private Response response;
    @Getter
    private StatusAssertions statusAssertions;
    private ResponseSpec responseSpec;

    public StoreSession(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }


    public void saveResult(String key, Object value) {
        this.storage.put(key, value);
    }

    public <T> T get(String key, Class<T> clazz) {
        Object object = storage.get(key);
        if (Objects.isNull(object)) {
            return null;
        }
        return objectMapper.convertValue(object, clazz);
    }

    public <T> List<T> getArray(String key, Class<T> clazz) {
        Object object = storage.get(key);
        Objects.requireNonNull(object);
        List<LinkedHashMap<Object, Object>> ts = objectMapper.convertValue(object, new TypeReference<>() {
        });
        return ts.stream().map(elt -> parse(elt, clazz)).toList();
    }

    private <T> T parse(LinkedHashMap<Object, Object> elt, Class<T> clazz) {
        return objectMapper.convertValue(elt, clazz);
    }

    public void storeHttpStatus(StatusAssertions statusAssertions) {
        this.statusAssertions = statusAssertions;
    }

    public void storeResponseSpec(ResponseSpec responseSpec) {
        this.responseSpec = responseSpec;
    }
}
