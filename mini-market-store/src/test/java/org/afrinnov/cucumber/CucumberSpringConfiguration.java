package org.afrinnov.cucumber;


import io.cucumber.spring.CucumberContextConfiguration;
import org.afrinnov.sheet.services.NotifyService;
import org.afrinnov.sheet.services.mails.MailSendService;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@CucumberContextConfiguration
@SpringBootTest(
        webEnvironment = RANDOM_PORT,
        properties = {"afrinnov.notification.provider: NOOP"})
@AutoConfigureMockMvc
//@ConfigurationParameter(key = GLUE_PROPERTY_NAME, value = "org.afrinnov.cucumber.steps")
public class CucumberSpringConfiguration {

    @MockBean
    private MailSendService mailSendService;


}
