package org.afrinnov;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.modulith.core.ApplicationModules;
import org.springframework.modulith.docs.Documenter;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class MiniMarketStoreApplicationTests {

    private final ApplicationModules applicationModules = ApplicationModules.of(MiniMarketStoreApplication.class);


    @Test
    void contextLoads() {
        applicationModules.verify();
    }

    @Test
    void generateDoc() {
        new Documenter(applicationModules).writeDocumentation();
    }

    @Test
    void passwordEncoder(@Autowired PasswordEncoder passwordEncoder) {
        //Act
        String encodedPwd = passwordEncoder.encode("bonjourdev");
        //Assert
        assertThat(encodedPwd).isNotEmpty();
    }

    @Test
    void testUUID() {
        //Act
        String uid = UUID.randomUUID().toString();
        //Assert
        assertThat(uid).isNotEmpty();
    }

}
