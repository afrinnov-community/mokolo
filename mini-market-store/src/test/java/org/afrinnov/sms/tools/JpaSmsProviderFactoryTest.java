package org.afrinnov.sms.tools;

import org.afrinnov.sms.dto.ProviderStatus;
import org.afrinnov.sms.dto.ProviderType;
import org.afrinnov.sms.entities.SmsDataConfigEntity;
import org.afrinnov.sms.repository.SmsDataConfigRepository;
import org.afrinnov.sms.service.client.SMSClient;
import org.afrinnov.sms.service.client.impl.InfoBipSMSClientProvider;
import org.afrinnov.sms.service.client.impl.NoOPTSMSClient;
import org.afrinnov.sms.service.client.impl.TwilioSMSClientProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JpaSmsProviderFactoryTest {
    private JpaSmsProviderFactory providerFactory;

    @Mock
    private SmsDataConfigRepository dataConfigRepository;
    @Mock
    private TwilioSMSClientProvider twilioSMSCLientProvider;
    @Mock
    private InfoBipSMSClientProvider infoBipSMSClientProvider;
    @Mock
    private NoOPTSMSClient noOPTSMSClient;

    @BeforeEach
    void setUp() {
        providerFactory = new JpaSmsProviderFactory(List.of(noOPTSMSClient, infoBipSMSClientProvider, twilioSMSCLientProvider), dataConfigRepository);
    }

    @Test
    void shouldReturnDefaultNoPSMSClientImplementation() {
        //Act
        SMSClient smsClient = providerFactory.get();

        //Assert
        assertThat(smsClient).isInstanceOf(NoOPTSMSClient.class);
    }

    @Test
    void shouldReturnInfoBipSMSClientProviderImplementation() {
        //Arrange
        SmsDataConfigEntity smsDataConfig = new SmsDataConfigEntity();
        smsDataConfig.setProviderName(ProviderType.INFOBIP);

        when(dataConfigRepository.findByDeactivatedIsFalseAndStatus(ProviderStatus.CURRENT)).thenReturn(Optional.of(smsDataConfig));
        //Act
        SMSClient smsClient = providerFactory.get();

        //Assert
        assertThat(smsClient).isInstanceOf(InfoBipSMSClientProvider.class);
    }

    @Test
    void shouldReturnTwilioSMSClientProviderImplementation() {
        //Arrange
        SmsDataConfigEntity smsDataConfig = new SmsDataConfigEntity();
        smsDataConfig.setProviderName(ProviderType.TWILIO);

        when(dataConfigRepository.findByDeactivatedIsFalseAndStatus(ProviderStatus.CURRENT)).thenReturn(Optional.of(smsDataConfig));
        //Act
        SMSClient smsClient = providerFactory.get();

        //Assert
        assertThat(smsClient).isInstanceOf(TwilioSMSClientProvider.class);
    }

}