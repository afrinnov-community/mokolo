package org.afrinnov.sheet.common.telephone.service;

import org.afrinnov.sheet.id.TelephoneCodeId;
import org.afrinnov.sheet.rest.input.TelephoneCodeValidRequest;
import org.afrinnov.sheet.entities.TelephoneCodeEntity;
import org.afrinnov.sheet.repositories.TelephoneCodeRepository;
import org.afrinnov.sheet.services.TelephoneCodeService;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TelephoneCodeServiceTest {
    @InjectMocks
    private TelephoneCodeService service;
    @Mock
    private TelephoneCodeRepository telephoneCodeRepository;

    @ParameterizedTest
    @ValueSource(strings = {"0023793635509", "00237 93 63 55 09", "+237 93635509"})
    void shouldValidatePhoneNumber(String phoneNumber) {
        //Arrange
        var entity = Mockito.mock(TelephoneCodeEntity.class);
        when(entity.getFormat()).thenReturn("^(\\+|00)237\\s*([\\s.-]*\\d{2}){4}");
        when(telephoneCodeRepository.findOneByCode("237")).thenReturn(Optional.of(entity));
        //Act
        var validate = service.validate(new TelephoneCodeId("237"), new TelephoneCodeValidRequest(phoneNumber));

        //Assert
        assertThat(validate.isMatches()).isTrue();
    }

}