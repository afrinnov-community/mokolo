package org.afrinnov.sheet.common.product.service;

import org.afrinnov.helper.DataFaker;
import org.afrinnov.sheet.dto.CategorySizes;
import org.afrinnov.sheet.rest.support.input.CategoryRequest;
import org.afrinnov.sheet.dto.SizeDto;
import org.afrinnov.sheet.rest.support.input.SizeRequest;
import org.afrinnov.sheet.entities.CategoryEntity;
import org.afrinnov.sheet.repositories.CategoryRepository;
import org.afrinnov.sheet.repositories.SizeRepository;
import org.afrinnov.sheet.services.CategoryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CategorySizesServiceTest {
    @Mock
    private CategoryRepository categoryRepository;
    @Mock
    private SizeRepository sizeRepository;
    @InjectMocks
    private CategoryService categoryService;

    @Captor
    private ArgumentCaptor<CategoryEntity> categoryEntityCaptor;
    @Captor
    private ArgumentCaptor<String> categoryCodeCaptor;

    private static final String PRODUCT_CATEGORY_ENTITY = "datasets/product/category/product_category_entity.json";
    private static final String PRODUCT_CATEGORY_REQUEST = "datasets/product/category/product_category_request.json";
    private static final String PRODUCT_CATEGORY = "datasets/product/category/product_category.json";
    private static final String PRODUCT_SIZE = "datasets/product/size/product_size.json";
    private static final String PRODUCT_SIZE_REQUEST = "datasets/product/size/product_size_request.json";

    @Test
    void should_create_a_new_category() throws IOException {
        CategoryEntity createdCategory = DataFaker.getObject(PRODUCT_CATEGORY_ENTITY, CategoryEntity.class);
        CategoryRequest categoryRequest = DataFaker.getObject(PRODUCT_CATEGORY_REQUEST, CategoryRequest.class);

        when(categoryRepository.save(any(CategoryEntity.class))).thenReturn(createdCategory);

        categoryService.createCategory(categoryRequest);

        verify(categoryRepository).save(categoryEntityCaptor.capture());
        CategoryEntity savedCategory = categoryEntityCaptor.getValue();

        assertThat(savedCategory).isNotNull();
        assertThat(savedCategory.getCode()).isNotNull();
    }

    @Test
    void should_update_an_existing_category() throws IOException {
        CategoryEntity createdCategory = DataFaker.getObject(PRODUCT_CATEGORY_ENTITY, CategoryEntity.class);
        CategorySizes categoryToUpdate = DataFaker.getObject(PRODUCT_CATEGORY, CategorySizes.class);

        when(categoryRepository.findByCode(any(String.class))).thenReturn(Optional.of(createdCategory));
        categoryService.updateCategory(categoryToUpdate.code(), categoryToUpdate);

        verify(categoryRepository).save(categoryEntityCaptor.capture());
        CategoryEntity updatedCategory = categoryEntityCaptor.getValue();

        assertThat(updatedCategory.getDescription()).isEqualTo(categoryToUpdate.description());
        assertThat(updatedCategory.getCode()).isEqualTo(categoryToUpdate.code());
    }

    @Test
    void should_get_all_saved_categories() throws IOException {
        CategoryEntity savedCategory = DataFaker.getObject(PRODUCT_CATEGORY_ENTITY, CategoryEntity.class);

        when(categoryRepository.findAll()).thenReturn(List.of(savedCategory));
        List<CategorySizes> allCategories = categoryService.getAllCategories();
        CategorySizes existingCategory = DataFaker.getObject(PRODUCT_CATEGORY, CategorySizes.class);

        assertThat(allCategories.size()).isEqualTo(1);
        assertThat(allCategories).contains(existingCategory);
    }

    @Test
    void should_get_an_existing_category() throws IOException {
        CategoryEntity savedCategory = DataFaker.getObject(PRODUCT_CATEGORY_ENTITY, CategoryEntity.class);
        String existingCode = "cat123";

        when(categoryRepository.findByCode(any(String.class))).thenReturn(Optional.of(savedCategory));
        CategorySizes result = categoryService.getCategory(existingCode);

        verify(categoryRepository).findByCode(categoryCodeCaptor.capture());
        assertThat(result).isNotNull();
    }

    @Test
    void should_delete_an_existing_category() throws IOException {
        CategoryEntity savedCategory = DataFaker.getObject(PRODUCT_CATEGORY_ENTITY, CategoryEntity.class);
        String existingCode = "cat123";

        when(categoryRepository.findByCode(any(String.class))).thenReturn(Optional.of(savedCategory));
        categoryService.deleteCategory(existingCode);

        verify(categoryRepository).findByCode(categoryCodeCaptor.capture());
        verify(categoryRepository).deleteById(savedCategory.getSid());
    }

    @Test
    void should_add_a_size_to_a_category() throws IOException {
        CategoryEntity savedCategory = DataFaker.getObject(PRODUCT_CATEGORY_ENTITY, CategoryEntity.class);
        SizeRequest sizeRequest = DataFaker.getObject(PRODUCT_SIZE_REQUEST, SizeRequest.class);
        String existingCode = "cat123";

        when(categoryRepository.findByCode(any(String.class))).thenReturn(Optional.of(savedCategory));
        CategorySizes category = categoryService.addSizeToCategory(existingCode,sizeRequest);
        SizeDto sizeAdded = DataFaker.getObject(PRODUCT_SIZE, SizeDto.class);

        verify(sizeRepository).save(any());
        assertThat(category.sizes()).contains(sizeAdded);
    }
}