package org.afrinnov.sheet.common.countries.rest;

import org.afrinnov.sheet.rest.CountriesFileController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
class CountriesFileControllerTest {

    @Test
    void shouldLoadCountriesFromFile(@Value("classpath:db/countries.json") Resource countries) throws IOException {
        //Arrange
        var controller = new CountriesFileController(countries);

        //Act
        var element = controller.countries();

        //Assert
        assertThat(element).isNotEmpty();
    }

}