package org.afrinnov.sheet.common.countries.service;

import org.afrinnov.sheet.dto.Country;
import org.afrinnov.sheet.entities.CountryEntity;
import org.afrinnov.sheet.id.TelephoneCode;
import org.afrinnov.sheet.entities.TelephoneCodeEntity;
import org.afrinnov.sheet.repositories.TelephoneCodeRepository;
import org.afrinnov.sheet.repositories.CountryRepository;
import org.afrinnov.sheet.services.CountryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CountryServiceTest {
    @InjectMocks
    private CountryService service;
    @Mock
    private CountryRepository countryRepository;
    @Mock
    private TelephoneCodeRepository telephoneCodeRepository;

    @Test
    void shouldGetAllCountries() {
        //Arrange
        var entity = givenACountryEntity();
        when(countryRepository.findAll()).thenReturn(List.of(entity));
        //Act
        List<Country> countries = service.getCountries();

        //Assert
        assertThat(countries).hasSize(1);
        Country actual = countries.getFirst();
        assertThat(actual).isNotNull();
        assertThat(actual.getIsoCode()).isEqualTo("CMR");
        assertThat(actual.getName()).isEqualTo("Cameroun");
        assertThat(actual.getNameUs()).isEqualTo("Cameroon");
        assertThat(actual.getTelephoneCode()).isNull();
    }

    @Test
    void shouldGetAllCountriesWithTelephoneCode() {
        //Arrange
        var entity = givenACountryEntity();
        when(countryRepository.findAll()).thenReturn(List.of(entity));
        when(telephoneCodeRepository.findOneByIsoCountryCode("CMR")).thenReturn(Optional.of(givenATelephoneCode()));
        //Act
        List<Country> countries = service.getCountries();

        //Assert
        assertThat(countries).hasSize(1);
        Country actual = countries.getFirst();
        assertThat(actual).isNotNull();
        TelephoneCode telephoneCode = actual.getTelephoneCode();
        assertThat(telephoneCode).isNotNull();
        assertThat(telephoneCode.getCode()).isEqualTo("237");
        assertThat(telephoneCode.getCountry()).isEqualTo("CMR");
        assertThat(telephoneCode.getFormat()).isEqualTo("format");
        assertThat(telephoneCode.isEnable()).isTrue();
    }

    private static TelephoneCodeEntity givenATelephoneCode() {
        TelephoneCodeEntity telephoneCode = new TelephoneCodeEntity();
        telephoneCode.setIsoCountryCode("CMR");
        telephoneCode.setCode("237");
        telephoneCode.setFormat("format");
        telephoneCode.setEnable(true);
        return telephoneCode;
    }

    private static CountryEntity givenACountryEntity() {
        var entity = new CountryEntity();
        entity.setName("Cameroun");
        entity.setNameUs("Cameroon");
        entity.setIsoCode("CMR");
        entity.setSid(UUID.randomUUID());
        return entity;
    }

}