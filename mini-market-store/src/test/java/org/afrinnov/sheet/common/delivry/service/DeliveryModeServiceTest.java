package org.afrinnov.sheet.common.delivry.service;

import org.afrinnov.helper.DataFaker;
import org.afrinnov.sheet.dto.DeliveryMode;
import org.afrinnov.sheet.rest.support.input.DeliveryModeRequest;
import org.afrinnov.sheet.entities.DeliveryModeEntity;
import org.afrinnov.sheet.repositories.DeliveryModeRepository;
import org.afrinnov.sheet.services.DeliveryModeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.*;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/12/2023 -- 18:39<br></br>
 * By : @author alexk<br></br>
 * Project : mokolo<br></br>
 * Package : org.afrinnov.common.delivry.service<br></br>
 */
@ExtendWith(MockitoExtension.class)
class DeliveryModeServiceTest {
    @Mock
    private DeliveryModeRepository repository;

    @InjectMocks
    private DeliveryModeService service;

    @Captor
    private ArgumentCaptor<DeliveryModeEntity> deliveryModeCaptor;
    @Captor
    private ArgumentCaptor<String> modeCaptor;

    private static final String DELIVERY_MODE_ENTITY = "datasets/delivery_mode/delivery_mode_entity_saved.json";

    @Test
    void should_have_DeliveryMode_When_CreateDeliveryMode_using_DeliveryRequest() {
        //Give
        when(repository.save(any(DeliveryModeEntity.class))).thenReturn(new DeliveryModeEntity());

        //When
        service.create(new DeliveryModeRequest("mode", "description"));

        //Then
        verify(repository).save(deliveryModeCaptor.capture());
        DeliveryModeEntity savedEntity = deliveryModeCaptor.getValue();

        assertThat(savedEntity).isNotNull();
        assertThat(savedEntity.getMode()).isEqualTo("mode");
        assertThat(savedEntity.getDescription()).isEqualTo("description");

    }


    @Test
    void getDeliveryMode() {
        //Given
        when(repository.findByModeIgnoreCase(any(String.class))).thenReturn(Optional.of(new DeliveryModeEntity()));

        // when
        service.getDeliveryMode("mode");

        //
        verify(repository).findByModeIgnoreCase(modeCaptor.capture());
        String mode = modeCaptor.getValue();

        assertThat("mode").isEqualTo(mode);
    }

    @Test
    void getAllDeliveryMode() throws IOException {
        //Given
        DeliveryModeEntity entity = DataFaker.getObject(DELIVERY_MODE_ENTITY, DeliveryModeEntity.class);

        when(repository.findAll()).thenReturn(Collections.singletonList(entity));

        //When
        List<DeliveryMode> allDeliveryMode = service.getAllDeliveryMode();

        //Then
        assertThat(allDeliveryMode).isNotEmpty();
        DeliveryMode deliveryMode = allDeliveryMode.getFirst();

        assertAll(
                () -> assertThat(deliveryMode).extracting(DeliveryMode::mode).isEqualTo("TO_HOME"),
                () -> assertThat(deliveryMode).extracting(DeliveryMode::description).isEqualTo("Livraison à domicile")
        );

    }
}