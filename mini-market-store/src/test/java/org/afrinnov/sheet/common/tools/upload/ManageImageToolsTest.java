package org.afrinnov.sheet.common.tools.upload;

import org.afrinnov.sheet.tools.upload.ManageImageTools;
import org.junit.jupiter.api.Test;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

class ManageImageToolsTest {

    @Test
    void shouldAddWaterMark() {
        //Act
        String logoText = "Mark by Fleurette";
        URL url = ManageImageToolsTest.class.getResource("/images/image1.png");
        //String targerPath3="./target/out2.png";

        byte[] bytes = ManageImageTools.addWaterMark(url, logoText);
        assertThat(bytes).isNotEmpty();
    }
}