package org.afrinnov.sheet.common.product.service;

import org.afrinnov.helper.DataFaker;
import org.afrinnov.sheet.dto.SizeDto;
import org.afrinnov.sheet.rest.support.input.SizeRequest;
import org.afrinnov.sheet.entities.CategoryEntity;
import org.afrinnov.sheet.entities.SizeEntity;
import org.afrinnov.sheet.repositories.CategoryRepository;
import org.afrinnov.sheet.repositories.SizeRepository;
import org.afrinnov.sheet.services.ProductSizeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SizeServiceTest {
    @Mock
    private SizeRepository sizeRepository;
    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private ProductSizeService productSizeService;

    @Captor
    private ArgumentCaptor<SizeEntity> sizeEntityArgumentCaptor;
    @Captor
    private ArgumentCaptor<String> sizeCodeCaptor;

    private static final String PRODUCT_SIZE_CREATED = "datasets/product/size/product_size_entity.json";
    private static final String PRODUCT_SIZE_REQUEST = "datasets/product/size/product_size_request.json";
    private static final String PRODUCT_SIZE = "datasets/product/size/product_size.json";
    private static final String PRODUCT_SIZE_CATEGORY = "datasets/product/category/product_category_entity.json";

    @Test
    void should_create_a_new_size() throws IOException {
        SizeEntity savedSize = DataFaker.getObject(PRODUCT_SIZE_CREATED, SizeEntity.class);
        SizeRequest sizeRequest = DataFaker.getObject(PRODUCT_SIZE_REQUEST, SizeRequest.class);

        when(sizeRepository.save(any(SizeEntity.class))).thenReturn(savedSize);

        productSizeService.createSize(sizeRequest);

        verify(sizeRepository).save(sizeEntityArgumentCaptor.capture());
        SizeEntity savedSizeEntity = sizeEntityArgumentCaptor.getValue();

        assertThat(savedSizeEntity).isNotNull();
        assertThat(savedSizeEntity.getCode()).isNotNull();
    }

    @Test
    void should_update_an_existing_size() throws IOException {
        SizeEntity savedSize = DataFaker.getObject(PRODUCT_SIZE_CREATED, SizeEntity.class);
        SizeDto sizeToUpdate = DataFaker.getObject(PRODUCT_SIZE, SizeDto.class);
        CategoryEntity category = DataFaker.getObject(PRODUCT_SIZE_CATEGORY, CategoryEntity.class);

        when(sizeRepository.findByCode(sizeToUpdate.code())).thenReturn(Optional.of(savedSize));
        when(categoryRepository.findByCode(sizeToUpdate.categoryCode())).thenReturn(Optional.of(category));

        productSizeService.updateSize(sizeToUpdate.code(), sizeToUpdate);

        verify(sizeRepository).save(sizeEntityArgumentCaptor.capture());
        SizeEntity updatedSizeEntity = sizeEntityArgumentCaptor.getValue();

        assertThat(updatedSizeEntity).isNotNull();
        assertThat(updatedSizeEntity.getCode()).isEqualTo(sizeToUpdate.code());
        assertThat(updatedSizeEntity.getDescription()).isEqualTo(sizeToUpdate.description());
    }

    @Test
    void should_get_all_existing_sizes() throws IOException {
        SizeEntity createdSize = DataFaker.getObject(PRODUCT_SIZE_CREATED, SizeEntity.class);
        when(sizeRepository.findAll()).thenReturn(List.of(createdSize));

        List<SizeDto> sizes = productSizeService.getAllSizes();

        assertThat(sizes).isNotEmpty();
        assertThat(sizes.size()).isEqualTo(1);
    }

    @Test
    void should_get_an_existing_size() throws IOException {
        SizeEntity createdSize = DataFaker.getObject(PRODUCT_SIZE_CREATED, SizeEntity.class);
        String existingCode = "pdct123";

        when(sizeRepository.findByCode(any(String.class))).thenReturn(Optional.of(createdSize));

        SizeDto result = productSizeService.getSize(existingCode);
        verify(sizeRepository).findByCode(sizeCodeCaptor.capture());
        String capturedCode = sizeCodeCaptor.getValue();

        assertThat(capturedCode).isEqualTo("pdct123");
        assertThat(result.description()).isEqualTo(createdSize.getDescription());
    }

    @Test
    void should_delete_an_existing_size() throws IOException {
        SizeEntity createdSize = DataFaker.getObject(PRODUCT_SIZE_CREATED, SizeEntity.class);
        String existingCode = "pdct123";

        when(sizeRepository.findByCode(existingCode)).thenReturn(Optional.of(createdSize));

        productSizeService.deleteSize(existingCode);

        verify(sizeRepository).findByCode(sizeCodeCaptor.capture());
        verify(sizeRepository).deleteById(createdSize.getSid());
    }
}