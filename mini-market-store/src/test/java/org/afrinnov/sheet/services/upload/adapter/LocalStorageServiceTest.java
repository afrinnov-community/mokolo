package org.afrinnov.sheet.services.upload.adapter;

import org.afrinnov.sheet.DownloadException;
import org.afrinnov.sheet.FileUploadException;
import org.afrinnov.sheet.id.FileId;
import org.afrinnov.sheet.services.upload.StorageService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class LocalStorageServiceTest {
    private static final Path LOCATION = Paths.get(System.getProperty("java.io.tmpdir"), "mokito", UUID.randomUUID().toString());
    private StorageService service;

    @BeforeEach
    void setUp() {
        service = new LocalStorageService(LOCATION.toString());
        service.init();
    }

    @Test
    void shouldFailedWhenMultipartFileIsFailed() throws IOException {
        //Arrange
        MultipartFile multipartFile = mock(MultipartFile.class);
        when(multipartFile.getInputStream()).thenThrow(new IOException());
        //Act
        var exception = assertThrows(FileUploadException.class, () -> service.save(multipartFile));
        //Assert
        assertThat(exception.getMessage()).isEqualTo("java.io.IOException");
    }

    @Test
    @Order(1)
    void shouldFailedWhenLoadUnExistingFile() {
        //Act
        DownloadException exception = assertThrows(DownloadException.class, () -> service.loadAsResource("testte"));
        //Assert
        assertThat(exception.getMessage()).isEqualTo("Could not read file: testte");
    }

    @Test
    @Order(2)
    void shouldSaveFile() throws IOException {
        //Arrange
        MultipartFile multipartFile = mock(MultipartFile.class);
        when(multipartFile.getInputStream()).thenReturn(LocalStorageServiceTest.class.getResourceAsStream("/images/image1.png"));
        when(multipartFile.getOriginalFilename()).thenReturn("totorata.png");
        //Act
        FileId save = service.save(multipartFile);
        //Assert
        Path destinationFile = LOCATION.resolve(
                        Paths.get(save.id().toString()))
                .normalize().toAbsolutePath();
        assertThat(Files.isRegularFile(destinationFile)).isTrue();
        assertThat(Files.isReadable(destinationFile)).isTrue();
    }

    @Test
    @Order(3)
    void shouldSaveWithMarkedFile() throws IOException {
        //Arrange
        MultipartFile multipartFile = mock(MultipartFile.class);
        when(multipartFile.getInputStream()).thenReturn(LocalStorageServiceTest.class.getResourceAsStream("/images/image1.png"));
        when(multipartFile.getOriginalFilename()).thenReturn("totorataMark.png");
        //Act
        FileId fileId = service.saveWithMark(multipartFile);
        //Assert
        Path destinationFile = LOCATION.resolve(
                        Paths.get(fileId.id().toString()))
                .normalize().toAbsolutePath();
        assertThat(Files.isRegularFile(destinationFile)).isTrue();
        assertThat(Files.isReadable(destinationFile)).isTrue();
    }


}