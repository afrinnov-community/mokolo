package org.afrinnov.sheet.services.upload.adapter;

import org.afrinnov.sheet.DownloadException;
import org.afrinnov.sheet.id.FileId;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AbstractLocalStorageServiceTest {
    private static final Path LOCATION = Paths.get(System.getProperty("java.io.tmpdir"), "mokito", UUID.randomUUID().toString());
    @InjectMocks
    private DummyAbstractLocalStorageService service;

    @Test
    @Order(1)
    void shouldFailedWhenFolderNotExist() {
        //Act
        DownloadException exception = assertThrows(DownloadException.class, () -> service.loadAll());
        //Assert
        assertThat(exception.getMessage()).isEqualTo("Failed to read stored files");
    }

    @Test
    @Order(2)
    void shouldCreateEmptyDirectory() {
        //Act
        service.init();
        //Assert
        assertThat(Files.isDirectory(LOCATION)).isTrue();
        assertThat(Files.isReadable(LOCATION)).isTrue();
        assertThat(Files.isWritable(LOCATION)).isTrue();
    }

    @Test
    @Order(3)
    void shouldFindFiles() {
        //Act
        List<Path> paths = service.loadAll();
        //Assert
        assertThat(paths).isEmpty();
    }

    private static class DummyAbstractLocalStorageService extends AbstractLocalStorageService {

        protected DummyAbstractLocalStorageService() {
            super(LOCATION);
        }

        @Override
        public FileId save(MultipartFile file) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Path load(String filename) {
            throw new UnsupportedOperationException();
        }

        @Override
        public FileId saveWithMark(MultipartFile file) {
            throw new UnsupportedOperationException();
        }
    }

}