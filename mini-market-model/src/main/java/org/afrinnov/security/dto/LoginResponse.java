package org.afrinnov.security.dto;

public record LoginResponse(String token, String refreshToken) {
}
