package org.afrinnov.security.dto;

import jakarta.validation.constraints.NotEmpty;

public record LoginRequest(@NotEmpty String userName, @NotEmpty String password) {
}
