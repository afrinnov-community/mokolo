package org.afrinnov.sheet.id;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class TelephoneCode {
    private String code;
    private String country;
    private boolean enable;
    private String format;
}
