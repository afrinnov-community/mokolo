package org.afrinnov.sheet.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;
import org.afrinnov.sheet.id.TelephoneCode;

@Getter
@Builder
@Jacksonized
public class Country {
    private String isoCode;
    private String name;
    private String nameUs;
    private TelephoneCode telephoneCode;
}
