package org.afrinnov;

import org.hibernate.annotations.Type;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Type(UUIDType. class)
@Target({METHOD, FIELD})
@Retention(RUNTIME)
public @interface IdUUID {
}
