package org.afrinnov;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.EnhancedUserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import java.util.UUID;

import static java.sql.Types.VARCHAR;

public class UUIDType implements EnhancedUserType<UUID> {

    @Override
    public int getSqlType() {
        return VARCHAR;
    }

    @Override
    public Class<UUID> returnedClass() {
        return UUID.class;
    }

    @Override
    public boolean equals(UUID x, UUID y) {
        return Objects.equals(x, y);
    }

    @Override
    public int hashCode(UUID x) {
        return x.hashCode();
    }

    @Override
    public UUID nullSafeGet(ResultSet rs, int position, SharedSessionContractImplementor session, Object owner) throws SQLException {
        String uuidStringValue = rs.getString(position);
        return rs.wasNull() ? null : UUID.fromString(uuidStringValue);
    }

    @Override
    public void nullSafeSet(PreparedStatement st, UUID value, int index, SharedSessionContractImplementor session) throws SQLException {
        if (value == null) {
            st.setNull(index, VARCHAR);
        } else {
            st.setString(index, value.toString());
        }
    }

    @Override
    public UUID deepCopy(UUID value) {
        return value;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(UUID value) {
        return value;
    }

    @Override
    public UUID assemble(Serializable cached, Object owner) {
        return (UUID) cached;
    }


    @Override
    public String toSqlLiteral(UUID value) {
        if (Objects.isNull(value)) {
            return null;
        }
        return value.toString();
    }

    @Override
    public String toString(UUID value) throws HibernateException {
        if (Objects.isNull(value)) {
            return null;
        }
        return value.toString();
    }

    @Override
    public UUID fromStringValue(CharSequence sequence) throws HibernateException {
        if (Objects.isNull(sequence)) {
            return null;
        }
        return UUID.fromString(sequence.toString());
    }
}
