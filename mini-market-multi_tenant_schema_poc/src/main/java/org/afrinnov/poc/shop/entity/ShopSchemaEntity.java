package org.afrinnov.poc.shop.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;

@Entity
@Table(name = "t_shop_schema", schema = "public")
@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class ShopSchemaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    protected UUID sid;
    @Column(name = "_schema")
    private String schema;
    @Column(name = "enable")
    private boolean enable;
    @Enumerated(EnumType.STRING)
    private ShopSchemaType status;
}
