package org.afrinnov.poc.shop.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.afrinnov.poc.shop.controller.request.ShopRequest;
import org.afrinnov.poc.shop.entity.ShopEntity;
import org.afrinnov.poc.shop.entity.ShopSchemaEntity;
import org.afrinnov.poc.shop.entity.ShopSchemaType;
import org.afrinnov.poc.shop.repository.ShopSchemas;
import org.afrinnov.poc.shop.repository.Shops;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Service
@RequiredArgsConstructor
@Slf4j
public class ShopEngineService {
    private final DataSource dataSource;
    private final Shops shops;
    private final ShopSchemas shopSchemas;

    private void createSchema(String tenant) {
        String query = "CREATE SCHEMA IF NOT EXISTS " + tenant;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            connection.setAutoCommit(false);
            statement.executeLargeUpdate();
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Transactional
    public void init(ShopRequest request) {
        ShopEntity savedShop = shops.save(ShopEntity.builder().code(generateCode()).name(request.name()).build());
        ShopSchemaEntity savedSchema = shopSchemas.save(ShopSchemaEntity.builder()
                .schema(savedShop.getCode())
                .enable(true)
                .status(ShopSchemaType.NEW)
                .build());
        createSchema(savedSchema.getSchema());
        savedSchema.setStatus(ShopSchemaType.CREATED);
    }

    private String generateCode() {
        return String.format("ESHOP" + RandomStringUtils.randomNumeric(6));
    }
}
