package org.afrinnov.poc.shop.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.poc.shop.controller.request.ShopRequest;
import org.afrinnov.poc.shop.service.ShopEngineService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/shops")
@RequiredArgsConstructor
public class ShopController {
    private final ShopEngineService service;
    @PostMapping
    public void create(@RequestBody @Valid ShopRequest request) {
        service.init(request);
    }
}
