package org.afrinnov.poc.shop.controller.request;

import jakarta.validation.constraints.NotEmpty;

public record ShopRequest(@NotEmpty String name) {
}
