package org.afrinnov.poc.shop.repository;

import org.afrinnov.poc.shop.entity.ShopSchemaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ShopSchemas extends JpaRepository<ShopSchemaEntity, UUID> {
}
