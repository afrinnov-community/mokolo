package org.afrinnov.poc.shop.repository;

import org.afrinnov.poc.shop.entity.ShopEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface Shops extends JpaRepository<ShopEntity, UUID> {
}
