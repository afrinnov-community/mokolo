package org.afrinnov.poc.shop.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.NaturalId;

import java.util.UUID;

@Entity
@Table(name = "c_shop", schema = "public")
@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class ShopEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    protected UUID sid;
    @NaturalId
    private String code;
    private String name;
}
