package org.afrinnov.poc.customer.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CustomerData {
    private String code;
    private String phoneNumber;
    private String email;
    private String firstName;
    private String lastName;
}
