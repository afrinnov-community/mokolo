package org.afrinnov.poc.customer.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.NaturalId;

import java.util.UUID;

@Entity
@Table(name = "c_customer")
@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class CustomerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Basic
    //@JdbcTypeCode(SqlTypes.CHAR)
    protected UUID sid;
    @NaturalId
    private String code;
    @Column(name = "phone_number")
    private String phoneNumber;
    private String email;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;

}
