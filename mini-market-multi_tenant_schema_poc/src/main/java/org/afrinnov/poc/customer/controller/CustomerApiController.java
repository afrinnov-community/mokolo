package org.afrinnov.poc.customer.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afrinnov.poc.customer.controller.request.CustomerRequest;
import org.afrinnov.poc.customer.model.CustomerData;
import org.afrinnov.poc.customer.service.CustomerService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customers")
@RequiredArgsConstructor
public class CustomerApiController {
    private final CustomerService customerService;

    @PostMapping
    public CustomerData create(@RequestBody @Valid CustomerRequest customer) {
        return customerService.createCustomer(customer);
    }

    @GetMapping
    public List<CustomerData> getCustomers() {
        return customerService.getCustomerLites();
    }

    @GetMapping(value = "/{customerId}")
    public CustomerData getCustomer(@PathVariable String customerId) {
        return customerService.getCustomer(customerId);
    }

}
