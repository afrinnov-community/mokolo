package org.afrinnov.poc.customer.controller.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class CustomerRequest {
    @NotEmpty
    private String phoneNumber;
    @Email
    private String email;
    private String lastName;
    @NotEmpty
    private String firstName;
}
