package org.afrinnov.poc.customer.service;

import lombok.RequiredArgsConstructor;
import org.afrinnov.poc.customer.controller.request.CustomerRequest;
import org.afrinnov.poc.customer.entity.CustomerEntity;
import org.afrinnov.poc.customer.model.CustomerData;
import org.afrinnov.poc.customer.repository.CustomerRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    @Transactional(readOnly = true)
    public List<CustomerData> getCustomerLites() {
        return customerRepository.findAll().stream()
                .map(CustomerService::mapEntityToDto).toList();
    }

    private static CustomerData mapEntityToDto(CustomerEntity entity) {
        return CustomerData.builder()
                .email(entity.getEmail())
                .firstName(entity.getFirstName())
                .phoneNumber(entity.getPhoneNumber())
                .lastName(entity.getLastName())
                .build();
    }

    @Transactional(readOnly = true)
    public CustomerData getCustomer(String code) {
        return customerRepository.findOneByCode(code)
                .map(CustomerService::mapEntityToDto)
                .orElseThrow();
    }

    @Transactional
    public CustomerData createCustomer(CustomerRequest request) {
        CustomerEntity entity = CustomerEntity.builder()
                .email(request.getEmail())
                .code(generateCode())
                .phoneNumber(request.getPhoneNumber())
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .build();
        CustomerEntity saved = customerRepository.save(entity);
        return customerRepository.findOneByCode(saved.getCode())
                .map(CustomerService::mapEntityToDto)
                .orElseThrow();
    }

    private String generateCode() {
        return RandomStringUtils.randomNumeric(6);
    }
}
