package org.afrinnov.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(exclude = { LiquibaseAutoConfiguration.class })
@EnableTransactionManagement
@EnableAsync
public class MiniMarketMultiTenantSchemaPocApplication {

    public static void main(String[] args) {
        SpringApplication.run(MiniMarketMultiTenantSchemaPocApplication.class, args);
    }

}
