package org.afrinnov.poc.currency.service;

import lombok.RequiredArgsConstructor;
import org.afrinnov.poc.currency.dto.Currency;
import org.afrinnov.poc.currency.repository.CurrencyRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class CurrencyService {

    private final CurrencyRepository currencyEntityRepository;

    public List<Currency> getCurrencies() {
        return currencyEntityRepository.findByNameNotNullOrderByNameAsc();
    }
}
