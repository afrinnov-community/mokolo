package org.afrinnov.poc.currency.dto;

/**
 * Projection for {@link org.afrinnov.poc.currency.entity.CurrencyEntity}
 */
public interface Currency {
    String getIsoCode();

    String getSymbol();

    String getName();
}