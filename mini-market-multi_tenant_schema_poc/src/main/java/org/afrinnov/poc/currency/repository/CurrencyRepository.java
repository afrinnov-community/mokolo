package org.afrinnov.poc.currency.repository;

import org.afrinnov.poc.currency.dto.Currency;
import org.afrinnov.poc.currency.entity.CurrencyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CurrencyRepository extends JpaRepository<CurrencyEntity, String> {

    List<Currency> findByNameNotNullOrderByNameAsc();
}
