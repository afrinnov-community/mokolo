package org.afrinnov.poc.currency.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "tools", name = "c_currency")
@Getter
@Setter
public class CurrencyEntity {
    @Id
    @Column(name = "iso_code")
    private String isoCode;
    private String symbol;
    private String name;
}
