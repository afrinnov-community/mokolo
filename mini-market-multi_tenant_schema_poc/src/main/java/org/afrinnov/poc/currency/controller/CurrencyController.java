package org.afrinnov.poc.currency.controller;

import lombok.RequiredArgsConstructor;
import org.afrinnov.poc.currency.dto.Currency;
import org.afrinnov.poc.currency.service.CurrencyService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/currencies")
@RequiredArgsConstructor
public class CurrencyController {
    private final CurrencyService currencyService;

    @GetMapping
    public List<Currency> getCurrencies() {
        return currencyService.getCurrencies();
    }

}
