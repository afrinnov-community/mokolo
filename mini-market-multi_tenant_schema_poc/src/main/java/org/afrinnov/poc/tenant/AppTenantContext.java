package org.afrinnov.poc.tenant;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Objects;

@Component
public class AppTenantContext implements Filter {

    private static final String PRIVATE_TENANT_HEADER = "X-PrivateTenant";
    public static final String LOGGER_TENANT_ID = "tenant_id";
    private static final ThreadLocal<String> currentTenant = new InheritableThreadLocal<>();
    public static final String DEFAULT_TENANT = "public";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String privateTenant = request.getHeader(PRIVATE_TENANT_HEADER);

        if (privateTenant != null) {
            AppTenantContext.setCurrentTenant(privateTenant.toLowerCase());
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    public static void setCurrentTenant(String tenant) {
        MDC.put(LOGGER_TENANT_ID, tenant);
        currentTenant.set(tenant);
    }

    public static String getCurrentTenant() {
        String tenant = currentTenant.get();
        MDC.put(LOGGER_TENANT_ID, tenant);
        return Objects.requireNonNullElse(tenant, DEFAULT_TENANT);
    }

    public static void clear() {
        MDC.clear();
        currentTenant.remove();
    }
}
