package org.afrinnov.poc.config;

import com.google.common.collect.Lists;
import liquibase.integration.spring.MultiTenantSpringLiquibase;
import liquibase.integration.spring.SpringLiquibase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableConfigurationProperties(LiquibaseProperties.class)
@Slf4j
public class LiquibaseConfig {


    @Bean
    public SpringLiquibase liquibase(DataSource dataSource, LiquibaseProperties liquibaseProperties) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog(liquibaseProperties.getChangeLog());
        liquibase.setContexts(liquibaseProperties.getContexts());
        liquibase.setDefaultSchema(liquibaseProperties.getDefaultSchema());
        liquibase.setDropFirst(liquibaseProperties.isDropFirst());
        liquibase.setShouldRun(liquibaseProperties.isEnabled());
        return liquibase;
    }

    @Bean
    @DependsOn("liquibase")
    public MultiTenantSpringLiquibase liquibaseMT(DataSource dataSource, LiquibaseProperties liquibaseProperties) throws SQLException {
        List<String> schemas = query(dataSource);

        List<List<String>> schemasPart = Lists.partition(schemas, 5);
        for (List<String> schemaList : schemasPart) {
            try (Connection connection = dataSource.getConnection()) {
                for (String schema:schemaList) {
                    try (Statement statement = connection.createStatement()) {
                        statement.executeUpdate("CREATE SCHEMA IF NOT EXISTS " + schema);
                    } catch (SQLException e) {
                        log.error(e.getMessage(), e);
                    }
                }
            } catch (SQLException e) {
                log.error(e.getMessage(), e);
            }
        }

        var liquibase = new MultiTenantSpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog("db/changelog/tenant/tenant.xml");
        liquibase.setContexts(liquibaseProperties.getContexts());
        liquibase.setDefaultSchema(liquibaseProperties.getDefaultSchema());
        liquibase.setDropFirst(liquibaseProperties.isDropFirst());
        liquibase.setShouldRun(liquibaseProperties.isEnabled() && !schemas.isEmpty());
        liquibase.setSchemas(schemas);

        return liquibase;
    }

    private List<String> query(DataSource dataSource) throws SQLException {
        List<String> schemas=new ArrayList<>();
        String sql = "SELECT _schema FROM t_shop_schema sh";
        try(Connection connection=dataSource.getConnection(); Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                String schema = resultSet.getString("_schema");
                schemas.add(schema);
            }
        }
        return schemas;
    }
}
