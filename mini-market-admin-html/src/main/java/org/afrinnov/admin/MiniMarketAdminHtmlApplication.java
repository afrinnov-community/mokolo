package org.afrinnov.admin;

import org.afrinnov.client.annotation.EnableMokoloClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableMokoloClient
public class MiniMarketAdminHtmlApplication {

    public static void main(String[] args) {
        SpringApplication.run(MiniMarketAdminHtmlApplication.class, args);
    }

}
