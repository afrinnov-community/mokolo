package org.afrinnov.admin.security.provider.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.function.Function;

import static java.nio.charset.StandardCharsets.UTF_8;

@Component
public class JwtUtil {
    private final KeyFactory keyFactory;
    private final Key publicKey;

    public JwtUtil(@Value("${application.security.publicKeyFile}") Resource publicKeyFile) throws Exception {
        keyFactory = KeyFactory.getInstance("RSA");
        this.publicKey = makePublicKeyFromFile(publicKeyFile);
    }

    public Claims extractAllClaims(String token) {
        return Jwts.parser()
                .verifyWith((PublicKey) publicKey)
                .build().parseSignedClaims(token)
                .getPayload();
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private static Key loadKey(Resource resource, Function<String, String> formater,
                               Function<byte[], Key> keyParser) throws IOException {
        String content = resource.getContentAsString(StandardCharsets.UTF_8);
        String formatedContent = formater.apply(content);
        byte[] encoded = Base64.getDecoder().decode(formatedContent.getBytes(UTF_8));
        return keyParser.apply(encoded);
    }

    private Key makePublicKeyFromFile(Resource publicKeyFile) throws Exception {
        Function<String, String> formater = p -> p
                .replaceAll("\n", "")
                .replaceAll("\r", "")
                .replace("-----BEGIN PUBLIC KEY-----", "")
                .replace("-----END PUBLIC KEY-----", "");

        return loadKey(publicKeyFile, formater, bytes -> {
            X509EncodedKeySpec spec = new X509EncodedKeySpec(bytes);
            try {
                return keyFactory.generatePublic(spec);
            } catch (InvalidKeySpecException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
