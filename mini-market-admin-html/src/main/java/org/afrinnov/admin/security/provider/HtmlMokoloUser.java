package org.afrinnov.admin.security.provider;

import org.afrinnov.client.config.MokoloUser;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

public class HtmlMokoloUser extends MokoloUser {
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of();
    }

    @Override
    public String getPassword() {
        return "";
    }

    @Override
    public String getUsername() {
        return "";
    }
}
