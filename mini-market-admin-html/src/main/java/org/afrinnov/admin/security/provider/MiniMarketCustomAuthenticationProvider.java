package org.afrinnov.admin.security.provider;

import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.afrinnov.admin.security.provider.utils.JwtUtil;
import org.afrinnov.client.config.MokoloUser;
import org.afrinnov.client.security.AuthenticateClient;
import org.afrinnov.security.dto.LoginRequest;
import org.afrinnov.security.dto.LoginResponse;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@ConditionalOnProperty(havingValue = "MINI_MARKET", prefix = "application.security", name = "provider")
public class MiniMarketCustomAuthenticationProvider implements AuthenticationProvider {
    private final AuthenticateClient authenticateClient;
    private final JwtUtil jwtUtil;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        final String name = authentication.getName();
        final String password = authentication.getCredentials().toString();

        LoginResponse loginResponse = authenticateClient.login(new LoginRequest(name, password));
        return authenticateAgainstThirdPartyAndGetAuthentication(name, password, loginResponse);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    private UsernamePasswordAuthenticationToken authenticateAgainstThirdPartyAndGetAuthentication(String name, String password,
                                                                                                          LoginResponse loginResponse) {
        Claims claims = jwtUtil.extractAllClaims(loginResponse.token());
        final List<GrantedAuthority> grantedAuths = new ArrayList<>();
        claims.get("roles", List.class).forEach(role->{
            grantedAuths.add(new SimpleGrantedAuthority(role.toString()));
        });
        final UserDetails principal = new User(name, password, grantedAuths);
        MokoloUser mokoloUser = new HtmlMokoloUser();
        mokoloUser.setToken(loginResponse.token());
        claims.getSubject();
        return new UsernamePasswordAuthenticationToken(mokoloUser, "", grantedAuths);
    }
}
