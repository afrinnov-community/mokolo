package org.afrinnov.gatling

object SystemPropertiesUtil {
    fun getAsStringOrElse(key: String, fallback: String): String {
        return System.getProperty(key) ?: fallback
    }

    fun getAsDoubleOrElse(key: String, fallback: Double): Double {
        return System.getProperty(key)?.toDouble() ?: fallback
    }

    fun getAsIntOrElse(key: String, fallback: Int): Int {
        return System.getProperty(key)?.toInt() ?: fallback
    }
}