package org.afrinnov.client.config;

import lombok.Data;

@Data(staticConstructor = "of")
public class ClientStore {
    private final String className;
    private final Object client;
}
