package org.afrinnov.client.config;

import feign.Feign;
import feign.Logger;
import feign.RequestInterceptor;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import org.afrinnov.client.common.AccessProtectedClient;
import org.afrinnov.client.security.AuthenticateClient;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.annotation.RequestScope;

import java.util.Objects;
import java.util.Set;

import static org.reflections.scanners.Scanners.SubTypes;

@AutoConfiguration
public class MokoloApiClientAutoConfig {
     private static final Decoder decoder = new GsonDecoder();
    private static final Encoder encoder = new GsonEncoder();
    @Value("${mokolo.client.url:http://localhost:8080/}")
    private String serverUrl;
    @Bean
    public AuthenticateClient authenticateClient() {
        return Feign.builder()
                .decoder(decoder)
                .encoder(encoder)
                .logLevel(Logger.Level.BASIC)
                .target(AuthenticateClient.class, serverUrl);
    }

    @Bean
    public ClientStores accessProtectedClients(RequestInterceptor jwtRequestInterceptor) {
        Reflections reflections = new Reflections("org.afrinnov.client");
        Set<Class<?>> subTypes = reflections.get(SubTypes.of(AccessProtectedClient.class).asClass());
        return ClientStores.of(subTypes.stream()
                .map(clazz-> ClientStore.of(clazz.getName(), Feign.builder()
                .decoder(decoder)
                .encoder(encoder)
                .logLevel(Logger.Level.BASIC)
                .requestInterceptor(jwtRequestInterceptor)
                .target(clazz, serverUrl)))
                .toList());
    }


    @Bean
    @RequestScope(proxyMode = ScopedProxyMode.TARGET_CLASS)
    public RequestInterceptor jwtRequestInterceptor() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof MokoloUser user) {
            return template -> template
                    .header("Authorization", "Bearer " + user.getToken());
        }
        return template -> template.header("Authorization", Objects.requireNonNull(principal).toString());
    }
}
