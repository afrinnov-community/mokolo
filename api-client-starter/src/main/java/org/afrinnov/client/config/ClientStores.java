package org.afrinnov.client.config;


import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ClientStores {
    private final List<ClientStore> clientStores;

    public static ClientStores of(List<ClientStore> clientStores) {
        return new ClientStores(clientStores);
    }

    public <T> T getClient(Class<T> clazz) {
        return clientStores.stream().filter(client -> client.getClassName().equals(clazz.getName()))
                .map(client -> (T) client.getClient())
                .findFirst().orElseThrow();
    }
}
