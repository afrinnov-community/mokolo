package org.afrinnov.client.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.userdetails.UserDetails;

@Getter
@Setter
public abstract class MokoloUser implements UserDetails {
    protected String token;

}
