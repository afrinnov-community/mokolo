package org.afrinnov.client.annotation;

import org.afrinnov.client.config.MokoloApiClientAutoConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({MokoloApiClientAutoConfig.class})
public @interface EnableMokoloClient {
}
