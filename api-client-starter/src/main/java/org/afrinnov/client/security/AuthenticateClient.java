package org.afrinnov.client.security;

import feign.Headers;
import feign.RequestLine;
import org.afrinnov.security.dto.LoginRequest;
import org.afrinnov.security.dto.LoginResponse;

@Headers({"Accept: application/json", "Content-Type: application/json"})
public interface AuthenticateClient {
    @RequestLine("POST /api/authenticate")
    LoginResponse login(LoginRequest loginRequest);
}
