package org.afrinnov.client.common;

import feign.Headers;
import feign.RequestLine;
import org.afrinnov.sheet.dto.Country;

import java.util.List;

@Headers({"Accept: application/json", "Content-Type: application/json"})
public interface CountriesClient extends AccessProtectedClient {

    @RequestLine("GET /api/countries")
    List<Country> countries();
}
