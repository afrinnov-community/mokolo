package org.afrinnov.client.common;

import feign.Headers;

@Headers({"Accept: application/json", "Content-Type: application/json"})
public interface AccessProtectedClient {
}
