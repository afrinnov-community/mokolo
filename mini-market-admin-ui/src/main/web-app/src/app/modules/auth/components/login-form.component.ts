import {Component, EventEmitter, Output} from "@angular/core";
import {Credentials} from "../models";
import {FormControl, FormGroup} from "@angular/forms";
import {LayoutService} from "../../../layout/service/app.layout.service";


@Component({
  selector: 'bc-login-form',
  templateUrl: './login-form.component.html'
})
export class LoginFormComponent {
  @Output()
  submitted = new EventEmitter<Credentials>();
  password!: string;
  username!: string;

  form: FormGroup = new FormGroup<any>({
    username: new FormControl(''),
    password: new FormControl(''),
  });

  submit() {
    console.log('submit');
    const credentials: Credentials = {username: this.username, password: this.password};
    this.submitted.emit(credentials);
  }

  constructor(public layoutService: LayoutService) {
  }
}
