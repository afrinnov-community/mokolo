import {Injectable} from "@angular/core";
import {Credentials, User} from "../models";
import {Observable, of, throwError} from "rxjs";


@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private fakeAuthenticated = false;


  login({username, password}: Credentials): Observable<User> {
    if (username !== 'test' && username !== 'ngrx') {
      return throwError(() => 'Invalid username or password');
    }

    return of({name: 'User'});
  }

  fakeLogin() {
    this.fakeAuthenticated = true;
  }

  fakeLogout() {
    this.fakeAuthenticated = false;
  }

  isFakeAuthenticated(): boolean {
    return this.fakeAuthenticated;
  }

  isAuthenticated(): boolean {
    const token = localStorage.getItem('token');

    return false;
  }

  logout() {
    return of(true);
  }
}
