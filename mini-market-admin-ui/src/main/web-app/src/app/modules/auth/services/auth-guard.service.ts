import {Observable, of} from "rxjs";
import {inject} from "@angular/core";
import {AuthService} from "./auth.service";
import {Router} from "@angular/router";


export const AuthGuard = () => {
  const authService = inject(AuthService);
  const router = inject(Router);

  if (!authService.isFakeAuthenticated()) {
    router.navigateByUrl('/login').then(r => true);
    return false;
  }
  return true;
}
