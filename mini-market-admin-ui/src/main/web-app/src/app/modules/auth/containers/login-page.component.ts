import {Component} from "@angular/core";
import {AuthService} from "../services/auth.service";
import {Credentials} from "../models";
import {Router} from "@angular/router";


@Component({
  selector: 'bc-login-page',
  template: `<bc-login-form (submitted)="onSubmit($event)"></bc-login-form>`
})
export class LoginPageComponent {

  constructor(private authService: AuthService, private router: Router) {
  }

  onSubmit(credentials: Credentials) {
    this.authService.fakeLogin();
    this.router.navigateByUrl('/').then(value => value);
  }

}
