import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
  {
    path: 'empty', loadChildren: () => import('./empty/empty.module').then(m => m.EmptyModule),
  },
  {
    path: 'products', loadChildren: () => import('./products/products.module').then(m => m.ProductsModule),
  },
  {
    path: '**',
    redirectTo: '/notfound'
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
})
export class PagesRoutingModule { }
