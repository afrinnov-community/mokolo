import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
import {EmptyComponent} from "./empty.component";


@NgModule({
  imports: [RouterModule.forChild([
    { path: '', component: EmptyComponent }
  ])],
  exports: [RouterModule]
})
export class EmptyRoutingModule { }
