import {ActionReducer, ActionReducerMap, MetaReducer} from "@ngrx/store";
import {isDevMode} from "@angular/core";
import {booksReducer} from "../sample/state/books.reducer";
import {collectionReducer} from "../sample/state/collection.reducer";


export interface State {

}

export const rootReducer: ActionReducerMap<State> = {
  ['books']: booksReducer,
  ['collection']: collectionReducer,
}


export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return (state, action) => {
    const result = reducer(state, action);
    console.groupCollapsed(action.type);
    console.log('prev state', state);
    console.log('action', action);
    console.log('next state', result);
    console.groupEnd();
    return result;
  };
}

export const metaReducers: MetaReducer<State>[] = isDevMode() ? [logger] : [];
