import {createFeatureSelector, createSelector} from "@ngrx/store";
import {Book} from "../book-list/books.model";


export const selectBooks = createFeatureSelector<ReadonlyArray<Book>>('books');

export const selectCollectionState = createFeatureSelector<ReadonlyArray<string>>('collection');

export const selectBookCollection = createSelector(
  selectBooks,
  selectCollectionState,
  (books, collection) => {
    return books.filter((book)=>collection.findIndex((id)=>id===book.id)!==-1);
  }
);
