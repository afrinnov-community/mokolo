import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {GoogleBooksService} from "../book-list/books.service";
import {BooksApiActions} from "./books.actions";
import {catchError, exhaustMap, map, of} from "rxjs";


@Injectable()
export class BookEffects {
  books$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BooksApiActions.loadBookList),
      exhaustMap((action) =>
        this.booksService.getBooks().pipe(
          map((books) => BooksApiActions.retrievedBookList({books})),
          catchError((error) => of(BooksApiActions.loadBookFailed({error})))
        )
      )
    )
  );

  constructor(private actions$: Actions, private booksService: GoogleBooksService) {
  }
}
