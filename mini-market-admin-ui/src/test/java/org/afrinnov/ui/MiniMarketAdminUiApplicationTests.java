package org.afrinnov.ui;

import org.afrinnov.client.annotation.EnableMokoloClient;
import org.afrinnov.client.common.CountriesClient;
import org.afrinnov.client.config.ClientStores;
import org.afrinnov.client.config.MokoloUser;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;

@SpringBootTest
@EnableMokoloClient
class MiniMarketAdminUiApplicationTests {

    @Test
    void contextLoads(@Autowired ClientStores accessProtectedClients) {
        MokoloUser mokoloUser= Mockito.mock(MokoloUser.class);
        Mockito.when(mokoloUser.getToken()).thenReturn("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTcxNDM4MzE2OSwiYXV0aCI6IkFETUlOIFVTRVIiLCJpYXQiOjE3MTM1MTkxNjksImp0aSI6IjA4M2I4YjE2LTI5OGUtNDhhMi05ODFiLWEzOTM3Zjk0YjllOSJ9.C8ME3aSwsm7DQhhK4yfVKM6eUWJRZMhbIlLQLs2DqHl6AyB6l7uCCJzZqKd8auUMOAxroC6HCITeiKpHhfNP7A");
        Authentication authentication = UsernamePasswordAuthenticationToken.authenticated(mokoloUser, "",
                List.of(new SimpleGrantedAuthority("ADMIN")));
        SecurityContext securityContext = SecurityContextHolder.getContextHolderStrategy().createEmptyContext();
        securityContext.setAuthentication(authentication);
        SecurityContextHolder.setContext(securityContext);
        CountriesClient client = accessProtectedClients.getClient(CountriesClient.class);
        //List<Country> countries = client.countries();
        System.out.println();
    }

}
